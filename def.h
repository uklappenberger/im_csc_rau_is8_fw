/******************************************************************************
* Allg. Definitionsdatei - enth�lt verschiedene Vordefinitionen z.B. Bit-Strukturen
******************************************************************************
*
* Programm:			IX 8 Pro			SW-Nr.:	3 807 278
* Prozessor:		PIC24HJ64GP206
* Autor:			Johannes Koch / Dieter Werner
* Datum:			25.1.08	
*
* IX8 PRO Versionen
******************************************************************************/
//1.02	24.07.09 	Befehl 'gt' wg. R�ckmeldung nach Ger�tetemp.-Abgleich hinzu 	
//1.03	29.07.09	7-Segment-Display: Werte > 1000 Grad ggf. aufrunden
//1.04	30.07.09	1. DP Kurve anzeigen, MAX = 3 Sek, AVG = 3 Sek 
//1.05	06.08.09	Befehl ve sendet "ve"+Ger�te-ID (wie die anderen Handger�te) 
//1.06				Testversion					
//1.07	15.09.09	Intervallspeichern: 1. Messwert im MB, keine weitere MB-Pr�fung   						
//					EmiMax von 1,023 auf 1,000
//1.08	02.11.09	Selbsthaltung PowerOn (PORTC2) in Endlosschleife zur�cksetzen 
//					um Ger�t sicher abzuschalten (Waitend)	
//					EEDatenZeigen: gesp. Messdaten nach Speicherseqenz immer in  
//					Grafikbuffer laden, um die Kurve bei num-graf Umschaltung anzuzeigen.
//1.09	19.11.09	Men� INT, IntCal beendet Dauersenden / Speichern 
//1.10	15.12.09	PORTD11 (Licht) nach Einschalten auf 1 setzen  
//1.11	19.01.10	Befehl bn eingebaut  
//1.12	01.02.10	Ger�t sofort abschalten bei zu niedriger Batteriespg. (BatMode=2) 
//					Beeper von Contin. Pulse auf Toggle Mode umgestellt. 
//1.13	17.02.10	RTC Einstellen wenn beim Einschalten PAR Taste gedr�ckt ist	
//1.14	22.02.10	Hold Time von 10 in 15 Sek. ge�ndert (Kundenwunsch)
//1.15	18.03.10	Grafische Anzeige auf 4 Sek./48�C skaliert 
//					Graf. Anzeige, Senkrechte Linie markiert akt. Messwert in Nachbetrachtung	
//					THold einstellbar �ber EEpr. Zelle 015 in 25ms Stufen (Standard = 600, = 15 Sek)
//					Variablenumstellung der meisten CHAR auf INT (spart Code und geht schneller) 
//					Nach Dr�cken von CONT Avg/Max neu initialisieren
//1.16 	19.05.10	Grafische Anzeige: Neue hinzuk. Pixel w�hrend der lfd. Ausgabe 
//					nur ber�cksichtigen, wenn innerhalb Skalierung (sonst Grafikfehler)    
//					SW-Version nach Power-On f�r ca. 5 Sek anzeigen					
//					THold nach Power-On pr�fen (min. = 1 Sek) und ggf. setzen 
//					EEpr.-Bereich E000...E01D im Block laden
//					Emi-Switch-Modus (umschalten wischen zwei voreingestellten Emiss.grad, Taste Enter) 					
//1.17	16.07.10	Schaltfrequenz SLR2016 auf 3KHz erh�ht, statt 1 x 100us bei DataVald 2
//					jetzt 3 x 15us bei DataValid1, DataValid2, Release State  
//1.18	02.08.10	Workaround flimmerndes Display SLR2016: 
//					Je 4 mal 15�s Ein, 235�s Aus, synchronisiert zum 1ms Intervall
//1.19	08.10.12	1. 7-Seg. Display: Messwert in ganzen Grad anzeigen (wenn SaveStatus,GanzeGrad=1)  
//					2. Einzel-Speichermodus, Anzeige Dot-Matrix: ca. 1 Sek. Messwert, anschl. kurz "====", 
//						anschl. Speicherzellen Nr. (mit "=" Zeichen an f�hrenden Nullen), anschl. wieder Messwert.
//  				3.Erstinitialisierung: Boot-Word als letzten Wert schreiben, bei EE.-Fehler Fehlermeldung 
//1.20 	10.04.14	Einzelspeichermodus: alle Temp-Werte >= MBUvl, <= MBOvl speichern (vorher > MBUvl < MBOvl) 
//
// ******************************************************************************/

#ifndef __DEF_H
#define __DEF_H

//#include <p24FJ64GA006.h>
//#include <p24HJ64GP206.h>
#include "C:/Program Files/Microchip/xc16/v2.00/support/PIC24H/h/p24HJ64GP206.h"

//#include "/Programme/Microchip/Mplab C30/support/h/peripheral_24F/Generic.h"
//#include "/Programme/Microchip/Mplab C30/support/h/peripheral_24F/timer.h"

#define	SoftVers 		0x0120	//HighByte = VK-Stelle, LowByte = NK-Stelle 
#define	SoftTag			0x04	//Tag
#define SoftMon			0x02	//Monat
#define SoftJahr 		0x16	//Jahr
/******************************************************************************
* Constant Definitions
******************************************************************************/
//Wegen Synchronit�t DDC114 <-> Prozessor Takt herabgesetzt (Einschaltoffset)
#define	ClkFrequ 		18432000 //Fosc(Hz)
//#define	GrfMinSkal		48		//Grafik: min. Temp.-Skalierung Y-Achse (1/10�C)
#define	GrfMinSkal		480		//Grafik: min. Temp.-Skalierung Y-Achse (1/10�C)
//#define THold			0x258	//Abschaltverz�gerung Hold in 25ms Schritten
#define CR 				0x0D	
#define EEBufflen		512		//EEPROM Messdaten-Zwischenspeicher

#define wrjahr			0x86	//RTC-Schreibcode -> Jahr
#define wrmonat			0x85	//RTC-Schreibcode -> Monat
#define wrday			0x84	//RTC-Schreibcode -> Date (Tag)
#define wrstunde		0x82	//RTC-Schreibcode -> Stunde
#define wrminute		0x81	//RTC-Schreibcode -> Minute
#define wrsekunde		0x80	//RTC-Schreibcode -> Sekunde
/******************************************************************************
* Reihenfolge der Menuepunkte, Tasten
******************************************************************************/
#define MenuEmi			0x01	//Emissionsgrad
#define MenuMax			0x02	//Maximalwert off/on
#define MenuAvg			0x03	//Mittelwert off/on
#define MenuInt			0x04	//8 Stufen Intervall
#define MenuEmiD		0x05	//Emi Direktverstellung off/on
#define MenuCF			0x06	//Umschaltung �C/�F
#define MenuMem			0x07	//Memory Anzeige 1...4000

//RTC Uhrdaten
#define MenuRtcYear		0x11	//RTC Jahr
#define MenuRtcMon		0x12	//RTC Monat
#define MenuRtcDay		0x13	//RTC Tag
#define MenuRtcHour		0x14	//RTC Stunde
#define MenuRtcMin		0x15	//RTC Minute
/******************************************************************************
* EEpromadressen, Flashadressen
******************************************************************************/
#define EE_Offset 		0x7D00	//Startadresse EEPROM-Parameter
#define FL_Offset 		0xA000	//Startadresse Tabellenbereich

#define EE_Emi 			0x000*2+EE_Offset	
#define EE_EmiMax 		0x001*2+EE_Offset	
#define EE_EmiMin 		0x002*2+EE_Offset	
#define EE_NkFkt		0x003*2+EE_Offset	
#define EE_ErrStatus 	0x004*2+EE_Offset 	
#define EE_Menuzeit		0x005*2+EE_Offset
#define EE_FunktionA 	0x006*2+EE_Offset	
#define EE_FunktionB 	0x007*2+EE_Offset
#define EE_FunktionC 	0x008*2+EE_Offset
#define EE_TrapErr 		0x009*2+EE_Offset

#define EE_SaveStatus	0x00B*2+EE_Offset
#define EE_MBA			0x00C*2+EE_Offset	//Messbereichsanfang in 1/10 Grad C
#define EE_MBE			0x00D*2+EE_Offset	//Messbereichsende in 1/10 Grad C
#define EE_IntTime		0x00E*2+EE_Offset

//#### EEPROM Neuorganisation als Ringspeicher
#define EE_CurMemCell	0x010*2+EE_Offset	//aktuell gew�hlte Speicherzelle EEPROM (0...4000, 0 = Speicher leer)
#define EE_MemBelegt	0x011*2+EE_Offset	//Anzahl belegte Speicherzellen im EEPROM (0...4000)
#define EE_MemStart		0x012*2+EE_Offset	//Startadresse des EE-Ringspeicher (0...3999)
#define EE_RMAMax		0x013*2+EE_Offset	//Max. Rohwert f�r RW-Mittelung
#define EE_RSTMax		0x014*2+EE_Offset	//Max. Stufe f�r RW-Mittelung bei RW 0
#define EE_THold		0x015*2+EE_Offset	//Abschaltzeit HOLD in 25ms Schritten
#define EE_RWBatWrn		0x016*2+EE_Offset	//Batterieniederspannungswarnungsanzeige
#define EE_RWBatAus		0x017*2+EE_Offset	//Batteriespannung Ger�t ausschalten
#define EE_KalInTemp	0x018*2+EE_Offset	//Kalibrierwert Ger�tetemperatur
#define EE_GerTempMax	0x019*2+EE_Offset
#define EE_GeraeteID	0x01A*2+EE_Offset	//Ger�te-ID=38
#define EE_ErstIni		0x01B*2+EE_Offset	//Wert <> 0x0069 -> EEPROM Erstinitialisierung durchf�hren
#define EE_EmiSw1		0x01C*2+EE_Offset	//Emi 1 (Enter-Switch-Funktion)
#define EE_EmiSw2		0x01D*2+EE_Offset	//Emi 2 (Enter-Switch-Funktion)


#define EE_ArtnrL		0x030*2+EE_Offset	//Artikel-Nr. High Befehl bn
#define EE_ArtnrH		0x031*2+EE_Offset	//Artikel-Nr. Low Befehl bn

#define EE_BootWord		0x034*2+EE_Offset	//0x0101 -> Update wird gstartet
#define EE_Kal1Dat		0x050*2+EE_Offset	//Erste Werksjustage
#define EE_Kal2Dat		0x053*2+EE_Offset	//Weitere Justagen

#define EE_RwOffKurz	0x060*2+EE_Offset	//ADW Offset kurze Ti, Range 1...7 
#define EE_RwOffLang	0x067*2+EE_Offset	//ADW Offset lange Ti, Range 1...7
#define EE_RwKalKurz	0x06E*2+EE_Offset
#define EE_RwKalLang	0x07C*2+EE_Offset
#define EE_RMASchwelle	0x08A*2+EE_Offset

#define EE_TmKurz		0x092*2+EE_Offset	//kurze Integrationszeit (Timerschritte)
#define EE_TmLang		0x093*2+EE_Offset	//lange Integrationszeit (Timerschritte)

#define EE_SWVersion	0x097*2+EE_Offset

/******************************************************************************
* Flashadressen
******************************************************************************/

#define Tab_EinK 0x7000
/******************************************************************************
* PORTA nicht vorhanden			Definitionen von Portpins
* PORTE nicht vorhanden
******************************************************************************/
// 					VREF+=AVDD 		[19]
//					VREF-=AVSS 		[20]
/******************************************************************************
* PORTB (RB0...RB15)				Pin-Nr.	I/O
******************************************************************************/
#define CE1305		LATBbits.LATB15	//[30]	[O] CE RTC MAX-DS1305 -> highaktiv
#define WR2016		LATBbits.LATB14	//[29]	[O] WR SLR2016 
//es sollte immer in Latch geschrieben werden, lesen aber vom Port.
#define CS_LCD		LATBbits.LATB13	//[28]	[O] CS ST7565 (DOGM128) -> lowaktiv
#define CS_EE		LATBbits.LATB12	//[27]	[O] CS EE-FM25256B -> lowaktiv
#define RstDogM		LATBbits.LATB11	//[24]	[O] Reset LCD
//es sollte immer in Latch geschrieben werden, lesen aber vom Port.
#define ANull		LATBbits.LATB10	//[23]	[O] ADR A0 LCD
//		TstDP1		PORTBbits.RB9	//[22]	[I] Hauptschalter Ein/Aus
//		TstClrMem	PORTBbits.RB8	//[21]	[I] Taste Clear Memory
//		TstDown		PORTBbits.RB7	//[18]	[I]	Taste DOWN
//		TstEnter	PORTBbits.RB6	//[17]	[I]	Taste ENTER
//		TstUp		PORTBbits.RB5	//[11]	[I]	Taste UP
//		TstPar		PORTBbits.RB4	//[12]	[I]	Taste PAR (low aktiv)
//		TstCont		PORTBbits.RB3	//[13]	[I] Taste CONT
// 		NumGrf		PORTBbits.RB2	//[14]	[I] Taste Num/Graf
//					PORTBbits.RB1	//[15]	[I] AN1, int. ADC (NTC, KTY82 -> Ger.-temp.)
//					PORTBbits.RB0	//[16]	[I] AN0, int. ADC (Batteriespannung)	

#define TRISB_INIT	0b0000001111111111
#define PORTB_INIT	0x73F8 				//alle CS disable, Tasten nicht gedr�ckt, Hauptschalter ein			
/******************************************************************************
* PORTC (RC1,RC2,RC12...RC15)
******************************************************************************/
//			OSC2	PORTCbits.RC15	//[40]	[O] Clock Out
//					PORTCbits.RC14	//[48]	[O]
//					PORTCbits.RC13	//[47]	[O]
//			OSC1	PORTCbits.RC12	//[39]	[I] Clock In
#define	PowerOn		LATCbits.LATC2	//[3]	[O] Selbsthaltung und Abschaltung
//					PORTCbits.RC1	//[2]	[I] Clock-In DDC114 18,432 MHz)
#define TRISC_INIT	0b0001000000000010
#define PORTC_INIT	0x0004 				//Selbsthaltung ein 
/******************************************************************************
* PORTD (RD0...RD11)
******************************************************************************/
#define Light		PORTDbits.RD11  //[45]	[O] LCD-Hintergrundbeleuchtung on/off 
#define VUSB		PORTDbits.RD10 	//[44]	[I] Stromquelle: 1=USB 0=Batterie
#define TstDP2		PORTDbits.RD9	//[43]	[I] Zweiter Druckpunkt Haupteinschalter
#define nDValid		PORTDbits.RD8	//[42]	[I] Data Valid DDC114 
#define ClkEnab		PORTDbits.RD7   //[55]	[O]	Und-Gatter Clock DDC114
#define	TestMode	PORTDbits.RD6 	//[54]	[O] Test DDC114
#define	Range2		PORTDbits.RD5 	//[53]	[O] Range 2 DDC114
#define	Range1		PORTDbits.RD4 	//[52]	[O] Range 1 DDC114
#define	Range0		PORTDbits.RD3	//[51]	[O] Range 0 DDC114 
#define nReset		PORTDbits.RD2 	//[50]  [O] Reset DDC114
//					PORTDbits.RD1	//[49]	[O] Summer OC2/PWM
#define	OC1			PORTDbits.RD0	//[46]	[O] CONV-Toggle DDC114
#define	TRISD_INIT	0x0700
#define PORTD_INIT	0x0800	//Licht ein
/******************************************************************************
* PORTF (RF0...RF6)
******************************************************************************/
// Uart muss auf UART2 laufen ! (wegen SPI) 
#define	DClock		PORTFbits.RF6	//[35]	[O]	SCK1, SPI1 Clock 
//			U2TX	PORTFbits.RF5	//[32]	[O]	Uart2 Transmit 
//			U2RX	PORTFbits.RF4	//[31]	[I]	Uart2 Recieve 
// 					PORTFbits.RF3	//[33]	[O]	SDO1
//					PORTFbits.RF2	//[34]	[I]	SDI1 
#define A1			LATFbits.LATF1	//[59]	[O] A1 SLR2016
#define A0			LATFbits.LATF0	//[58]	[O] A0 SLR2016
#define	TRISF_INIT	0b0000000000010100
#define PORTF_INIT	0x0000
/******************************************************************************
* PORTG (RG0...RG3,RG6...RG9,RG12...RG15)
******************************************************************************/
#define	DezPunkt	LATGbits.LATG15	//[1]	[O] 7-Segment Dezpunkt
#define D1			LATGbits.LATG14	//[62]	[O] B (D1 SLR2016 - D2 74HC4511)
#define D3			LATGbits.LATG13	//[64]	[O] D (D3 SLR2016 - D4 74HC4511)
#define D2			LATGbits.LATG12	//[63]	[O] C (D2 SLR2016 - D3 74HC4511)
#define Sg1000		LATGbits.LATG9	//[8]	[O] 7-Segment 1000er
#define Sg0100		LATGbits.LATG8	//[6]	[O] 7-Segment 100er	
//		  	SDI2	PORTGbits.RG7	//[5]	[I]	SPI2 Data In <- Data Out DDC114
//		  	SCK2	PORTGbits.RG6	//[4]	[O]	SPI2 Clock -> Data Clock DDC114
#define Sg0010		LATGbits.LATG3	//[36]	[O] 7-Segment 10er
#define Sg0001		LATGbits.LATG2	//[37]	[O] 7-Segment 1er
#define BL2016		LATGbits.LATG1	//[60]	[O] BL SLR2016
#define D0			LATGbits.LATG0	//[61]	[O] A (D0 SLR2016 - D1 74HC4511) 
#define	TRISG_INIT	0x0080
#define PORTG_INIT	0x0000

/******************************************************************************
* Bitdefinitionen Unionen/Strukturen
******************************************************************************/
union{
	unsigned int Word:16;
	struct{
		unsigned int Bit0:1;
		unsigned int Bit1:1;
		unsigned int Bit2:1;
		unsigned int Bit3:1;
		unsigned int Bit4:1;
		unsigned int Bit5:1;
		unsigned int Bit6:1;
		unsigned int Bit7:1;
		unsigned int Bit8:1;
		unsigned int Bit9:1;
		unsigned int Bit10:1;
		unsigned int Bit11:1;
		unsigned int Bit12:1;
		unsigned int Bit13:1;
		unsigned int Bit14:1;
		unsigned int Bit15:1;
	}Bit;
}ComStatus_,AdwStatus_,AuxStatus_,OutStatus_,LcdStatus_,LcdRequest_,SendStatus_,Tasten_,TastenStatus_;

#define AdwStatus	AdwStatus_.Word
	#define AdwNeu		AdwStatus_.Bit.Bit1		//Neue Rohwerte
	#define TiKurz		AdwStatus_.Bit.Bit3		//kurze Ti l�uft (DDC114)
	#define RMAIni		AdwStatus_.Bit.Bit4		//Rohwertmittelung neu initialisieren
	#define OVLang		AdwStatus_.Bit.Bit5		//Overflow lange Ti
	#define MemDsp		AdwStatus_.Bit.Bit7		//Display nach Einzelwertspeicherung einfrieren

#define AuxStatus	AuxStatus_.Word
	#define ContMode	AuxStatus_.Bit.Bit0		//Merkflag ContMode
	#define Max2		AuxStatus_.Bit.Bit1		//Doppelspeicherauswahl
	#define AVGInit		AuxStatus_.Bit.Bit2		//AVG/MAX neu initialisieren
	#define DDCInit		AuxStatus_.Bit.Bit3		//DDC114 initialisieren
	#define An0An1		AuxStatus_.Bit.Bit4		//ToggleBit Kanalauswahl
	#define StCF		AuxStatus_.Bit.Bit5		//Grad C/F gesp. Messdaten
	#define OKDP2		AuxStatus_.Bit.Bit6		//Tastenfunktion 2. Druckpunkt HS ausgef�hrt
	#define Zw2016		AuxStatus_.Bit.Bit7		//Zwischenspeicher BL2016 (Dot-Matrix Ein/Aus)
	#define NewPix		AuxStatus_.Bit.Bit8		//kompl. Grafik zur Laufzeit ausgeben
	#define GrfOn		AuxStatus_.Bit.Bit9		//Grafik aus EEprom geladen
	#define HoldMode	AuxStatus_.Bit.Bit10	//Status = Hold 
	#define HoldNeu		AuxStatus_.Bit.Bit11	//�nderungsstatus Hold
	#define BlinkOn		AuxStatus_.Bit.Bit12	//Blinkintervall <Ein>
	#define KeyLock		AuxStatus_.Bit.Bit13	//Tastatursperre Ein/Aus
	#define KrvLck		AuxStatus_.Bit.Bit14	//Grafikdarstellung DP1 gesperrt
	#define EnabGrf		AuxStatus_.Bit.Bit15	//LCD-Grafik

#define ComStatus	ComStatus_.Word
	#define KaliEin		ComStatus_.Bit.Bit0
	#define RCVBefehl	ComStatus_.Bit.Bit1
	#define IntCal		ComStatus_.Bit.Bit2		//Intervall off + Senden aus

#define ErrStatus_ (*(struct ibits*)&ErrStatus)
	#define ErrEEprom	ErrStatus_.Bit0
	#define ErrFlash	ErrStatus_.Bit1
	#define ErrWDTO		ErrStatus_.Bit2			//WDTO Reset 
	#define ErrBOR		ErrStatus_.Bit3			//Brow-Out Reset 
	#define ErrTRAPR	ErrStatus_.Bit4			//Trap Conflict Reset 

#define FunktionC_ (*(struct ibits*)&FunktionC)
	#define GSMode		FunktionC_.Bit2				//GS Ger�t 

//ACHTUNG: Anforderungs- und Bearbeitungsflags m�ssen in gleicher Reihenfolge sein !
#define LcdStatus	LcdStatus_.Word
	#define LcdGrf		LcdStatus_.Bit.Bit0		//LCD-Ausgabe Grafik Page1...7
	#define LcdGrZ		LcdStatus_.Bit.Bit1		//LCD-Ausgabe Grafik Page 0
	#define LcdCtHdBt	LcdStatus_.Bit.Bit2		//LCD-Ausgabe CONT/HOLD/BAT
	#define LcdGrX		LcdStatus_.Bit.Bit3		//LCD-Ausgabe Untere Statuszeile Grafik
	#define LcdCls		LcdStatus_.Bit.Bit4		//LCD-Ausgabe CLS
	#define LcdAxM		LcdStatus_.Bit.Bit5		//LCD-Ausgabe untere Statuszeilen
	#define LcdTmp		LcdStatus_.Bit.Bit6		//LCD-Ausgabe Messwert Gross
	#define LcdLck		LcdStatus_.Bit.Bit7		//LCD-Ausgabe 'Locked'
	#define LcdMAd		LcdStatus_.Bit.Bit8		//LCD-Ausgabe Speicheradresse
	#define LcdClk		LcdStatus_.Bit.Bit8		//LCD-Ausgabe Uhrdaten
	#define LcdMEm		LcdStatus_.Bit.Bit10	//LCD-Ausgabe Int und Mem
	#define LcdDat		LcdStatus_.Bit.Bit11	//LCD-Ausgabe Info Speicherdaten
	#define LcdVer		LcdStatus_.Bit.Bit12	//LCD-Ausgabe SW-Version
//1.19
	#define LcdErr		LcdStatus_.Bit.Bit13	//LCD-Ausgabe Error-Code

//	#define LcdCwe		LcdStatus_.Bit.Bit14	//res. ReqCwe
//	#define LcdRtc		LcdStatus_.Bit.Bit15	//res. ReqRtc

//ACHTUNG: Anforderungs- und Bearbeitungsflags m�ssen in gleicher Reihenfolge sein !
#define LcdRequest	LcdRequest_.Word
	#define ReqGrf		LcdRequest_.Bit.Bit0	//LCD-Anforderung Grafik Zeile 1...7 
	#define ReqGrZ		LcdRequest_.Bit.Bit1	//LCD-Anforderung Grafik Zeile 0
	#define ReqCtHdBt	LcdRequest_.Bit.Bit2	//LCD-Anforderung CONT/HOLD/BAT
	#define ReqGrX		LcdRequest_.Bit.Bit3	//LCD-Anforderung Untere Statuszeile Grafik
	#define ReqCls		LcdRequest_.Bit.Bit4	//LCD-Anforderung CLS
	#define ReqAxM		LcdRequest_.Bit.Bit5	//LCD-Anforderung untere 2 Statuszeilen
	#define ReqTmp		LcdRequest_.Bit.Bit6	//LCD-Anforderung Messwert Gross
	#define ReqLck		LcdRequest_.Bit.Bit7	//LCD-Anforderung 'Locked'
	#define ReqMAd		LcdRequest_.Bit.Bit8	//LCD-Anforderung Speicheradresse
	#define ReqClk		LcdRequest_.Bit.Bit9	//LCD-Anforderung Uhrdaten
	#define ReqMEm		LcdRequest_.Bit.Bit10	//LCD-Anforderung Int und Mem
	#define ReqDat		LcdRequest_.Bit.Bit11	//LCD-Anforderung Info Speicherdaten
	#define ReqVer		LcdRequest_.Bit.Bit12	//LCD-Anforderung SW-Version
//1.19
	#define ReqErr		LcdRequest_.Bit.Bit13	//LCD-Anforderung Error-Code

	#define ReqCwe		LcdRequest_.Bit.Bit14	//LCD-Anforderung 'Confirm with Enter'
	#define ReqRtc		LcdRequest_.Bit.Bit15	//Rtc-Anforderung Uhrdaten lesen

#define OutStatus	OutStatus_.Word
	#define SwReset		OutStatus_.Bit.Bit0		//Soft Reset ausf�hren 
	#define SendOK		OutStatus_.Bit.Bit1		//OK Senden
	#define ParRtc		OutStatus_.Bit.Bit2		//Modus -> Uhrstellen

#define SaveStatus_ (*(struct ibits*)&SaveStatus)
	#define SaveCF		SaveStatus_.Bit1		//0=Celsius			1=Fahrenheit
	#define SaveMax		SaveStatus_.Bit2		//0=kein Maxi-Mode	1=Maxi-Mode
	#define SaveAVG		SaveStatus_.Bit3		//0=kein AVG-Mode	1=AVG-Mode
	#define SaveEmiD	SaveStatus_.Bit4		//0=Emidirektverstellung aus 1=ein
//#### Achtung SaveGrf nicht �ndern ! (Bit 6)
	#define SaveGrf		SaveStatus_.Bit6		//Displ. 0=numerisch 1=grafisch 
	#define GanzeGrad	SaveStatus_.Bit7		//0= Anzeige 10el Grad (7-Seg, bei T< 1000) 1=Anzeige immer ganze Grad 

#define SendStatus	SendStatus_.Word
	#define MSNeu		SendStatus_.Bit.Bit0	//Messwert zum Senden liegt vor
	#define MMNeu		SendStatus_.Bit.Bit1	//Messwert zum Speichern liegt vor
	#define RwNeu		SendStatus_.Bit.Bit2	//Rohwert liegt vor
	#define RMNeu		SendStatus_.Bit.Bit3	//gemittelte Rohwerte liegen vor
	#define Xmtrw		SendStatus_.Bit.Bit4	//Anforderungsflag Rohwert
	#define Xmtms		SendStatus_.Bit.Bit5	//Anforderungsflag Messwert
	#define Xmtmd		SendStatus_.Bit.Bit6	//Anforderungsflag gepackte Speicherdaten
	#define XmtRM		SendStatus_.Bit.Bit7	//Anforderungsflag gemittelte Rohwerte
	#define SegNeu		SendStatus_.Bit.Bit8	//Neuer Messwert 7-Segment/Dot-Matrix
	#define XmtOut		SendStatus_.Bit.Bit9	//Kont. Messwertausgabe

#define Tasten	Tasten_.Word
	#define	TstNumGrf	Tasten_.Bit.Bit2		//NumGrf gedr�ckt
	#define	TstCont		Tasten_.Bit.Bit3		//Cont gedr�ckt
	#define	TstPar		Tasten_.Bit.Bit4		//Par gedr�ckt
	#define	TstUp		Tasten_.Bit.Bit5		//Up gedr�ckt
	#define TstEnter	Tasten_.Bit.Bit6		//Enter gedr�ckt
	#define	TstDown		Tasten_.Bit.Bit7		//Down gedr�ckt
	#define TstClrMem	Tasten_.Bit.Bit8		//ClrMem gedr�ckt
	#define TstDP1		Tasten_.Bit.Bit9		//Hauptschalter gedr�ckt

#define TastenStatus	TastenStatus_.Word
	#define OKNumGrf	TastenStatus_.Bit.Bit2	//Tastenfunktion NumGraf ausgef�hrt
	#define OKCont		TastenStatus_.Bit.Bit3	//Tastenfunktion Cont ausgef�hrt
	#define OKPar		TastenStatus_.Bit.Bit4	//Tastenfunktion Par ausgef�hrt
	#define OKUp		TastenStatus_.Bit.Bit5	//Tastenfunktion Up ausgef�hrt (bzw. kein Autorepeat)
	#define OKEnter		TastenStatus_.Bit.Bit6	//Tastenfunktion Enter ausgef�hrt
	#define OKDown		TastenStatus_.Bit.Bit7	//Tastenfunktion Down ausgef�hrt (bzw. kein Autorepeat)
	#define OKClrMem	TastenStatus_.Bit.Bit8	//Tastenfunktion ClrMem ausgef�hrt
	#define OKDP1		TastenStatus_.Bit.Bit9	//Tastenfunktion TstDP1 ausgef�hrt
/******************************************************************************
* Bytedefinitionen Unionen/Strukturen
******************************************************************************/
#define IntZw_ (*(struct hilo*)&IntZw)				//Vordefinierte allg.			

#define AdwTMR_ (*(struct hilo*)&AdwTMR)
#define IntRwKurz_ (*(struct WLoHi*)&IntRwKurz)
#define IntRwLang_ (*(struct WLoHi*)&IntRwLang)
#define IntRwZw_ (*(struct WLoHi*)&IntRwZw)
#define RwMKurz_ (*(struct tpuphilo*)&RwMKurz)
#define RwMLang_ (*(struct tpuphilo*)&RwMLang)
#define RwKurz_ (*(struct tpuphilo*)&RwKurz)
#define RwLang_ (*(struct tpuphilo*)&RwLang)
#define RMASumme_ (*(struct tpuphilo*)&RMASumme)
#define Rohwert_ (*(struct tpuphilo*)&Rohwert)
#define RwKalKurz_ (*(struct WLoHi*)&RwKalKurz[1])
#define RwKalLang_ (*(struct WLoHi*)&RwKalLang[1])
#define RwZw_ (*(struct WLoHi*)&RwZw)
#define RMAMax_ (*(struct hilo*)&RMAMax)

struct WLoHi
{
	unsigned int WLo;
	unsigned int WHi;
};

struct tpuphilo
{
	unsigned char Lo;
	unsigned char Hi;
	unsigned char Up;
	unsigned char Tp;
};

struct hilo
{
	unsigned char Lo;
	unsigned char Hi;
};

struct cbits {
	unsigned Bit0:1;
	unsigned Bit1:1;
	unsigned Bit2:1;
	unsigned Bit3:1;
	unsigned Bit4:1;
	unsigned Bit5:1;
	unsigned Bit6:1;
	unsigned Bit7:1;
};

struct ibits {
	unsigned Bit0:1;
	unsigned Bit1:1;
	unsigned Bit2:1;
	unsigned Bit3:1;
	unsigned Bit4:1;
	unsigned Bit5:1;
	unsigned Bit6:1;
	unsigned Bit7:1;
	unsigned Bit8:1;
	unsigned Bit9:1;
	unsigned Bit10:1;
	unsigned Bit11:1;
	unsigned Bit12:1;
	unsigned Bit13:1;
	unsigned Bit14:1;
	unsigned Bit15:1;
};


//Linker will allocate these buffers from the bottom of DMA RAM. 
	struct{ 
		unsigned int Adc1Ch0[1];			//AN0 Scanning Mode 
		unsigned int Adc1Ch1[1]; 			//AN1 Scanning Mode
	}BufferA __attribute__((space(dma)));
//
//	struct{ 								//Buffer B nur im DMA Ping-Pong Mode n�tig	
//		unsigned int Adc1Ch0[128]; 
//		unsigned int Adc1Ch1[128]; 
//		unsigned int Adc1Ch10[128]; 
//	}BufferB __attribute__((space(dma)));

unsigned char TxRSpi1Buf[140] __attribute__((space(dma)));
signed int RMABuf80[80] __attribute__((space(dma)));

//unsigned char RcSpi1Buf[8] __attribute__((space(dma)));

//#define RcvUSART2Err(XMTbits.XMTDaten||U2STAbits.UTXEN||U2STAbits.PERR||U2STAbits.OERR||U2STAbits.FERR||RCVbits.RCVBefehl||RcvCnt>sizeof(RcvBuf))
//Unlock/Lock Registers Sequenz 
//#define UNLOCK_REG() {__asm__ volatile ("MOV #OSCCON,w1 \n MOV #0x46,w2 \n MOV #0x57, w3 \n MOV.b w2, [w1] \n MOV.b w3, [w1] \n BCLR OSCCON,#6");}
//#define LOCK_REG() {__asm__ volatile ("MOV #OSCCON,w1 \n MOV #0x46,w2 \n MOV #0x57, w3 \n MOV.b w2, [w1] \n MOV.b w3, [w1] \n BSET OSCCON,#6");}

#define NOP() {__asm__ volatile ("nop");}
#define CLRWDT() {__asm__ volatile ("clrwdt");}
#define RESET() {__asm__ volatile ("reset");}
#endif
