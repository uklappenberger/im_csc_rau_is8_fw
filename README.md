![AE IMPAC](images/impac.jpg)
![OCTOCAT](https://myoctocat.com/assets/images/base-octocat.svg)
---
!<img src="/images/impac.jpg" style="width:200px;">

---
 
# IS8 Pro Firmware

---

## Compiler
XC16 (v2.00)

## Platform
Microchip PIC24HJ64TQ-64

## Assembly
LG-03807311

## PCB
LG-03807952

## Schematic
3807301b
