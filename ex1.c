/******************************************************************************

******************************************************************************/
#include "def.h"				//allgemeine Definitionsdatei

/******************************************************************************
* Configuration Settings
******************************************************************************/
//FBS: Boot Code Segment Configuration Register
//		RBS<1:0>: Boot Segment RAM Code Protection
//			11 = No Boot RAM defined
//			10 = Boot RAM is 128 bytes
//			01 = Boot RAM is 256 bytes
//			00 = Boot RAM is 1024 bytes
//		BSS<2:0>: Boot Segment Program Flash Code Protection Size
//		
//		BWRP: Boot Segment Program Flash Write Protection
//			1 = Boot segment may be written
//			0 = Boot segment is write-protected
//	_FBS(BWRP_WRPROTECT_ON & BSS_SMALL_FLASH_STD & RBS_SMALL_RAM)
	_FBS(BWRP_WRPROTECT_OFF & BSS_SMALL_FLASH_STD & RBS_SMALL_RAM)

//
//FSS: Secure Code Segment Configuration Register
//		RSS<1:0>: Secure Segment RAM Code Protection
//			11 = No Secure RAM defined
//			10 = Secure RAM is 256 bytes less BS RAM
//			01 = Secure RAM is 2048 bytes less BS RAM
//			00 = Secure RAM is 4096 bytes less BS RAM
//		SSS<2:0>: Secure Segment Program Flash Code Protection Size (see specific device data sheet for
//			more information
//		SWRP: Secure Segment Program Flash Write Protection
//			1 = Secure segment may be written
//			0 = Secure segment is write-protected
	_FSS(SWRP_WRPROTECT_OFF & SSS_NO_FLASH & RSS_NO_RAM)

//
//FGS: General Code Segment Configuration Register
//		GSS<1:0>: General Segment Code-Protect bit
//			11 = User program memory is not code-protected
//			10 = Standard security
//			0x = High security
_FGS(GSS_OFF & GWRP_OFF &  GCP_OFF)


//FOSCSEL: Oscillator Source Selection Register
//     FNOSC_FRC            Fast RC oscillator
//     FNOSC_FRCPLL         Fast RC oscillator w/ divide and PLL
//     FNOSC_PRI            Primary oscillator (XT, HS, EC)
//     FNOSC_PRIPLL         Primary oscillator (XT, HS, EC) w/ PLL
//     FNOSC_SOSC           Secondary oscillator
//     FNOSC_LPRC           Low power RC oscillator
//     FNOSC_FRCDIV16       Fast RC oscillator w/ divide by 16
//     FNOSC_LPRCDIVN        Low power Fast RC oscillator w/divide by N
//   Two-speed Oscillator Startup :
//     IESO_OFF             Disabled
//     IESO_ON              Enabled
// Kein PLL wegen Syncr.-Problem DDC114
//_FOSCSEL(IESO_ON& FNOSC_PRIPLL) 
_FOSCSEL(IESO_OFF& FNOSC_PRI) 

// _FOSC: Oscillator Configuration Register
//   Clock switching and clock monitor:
//     FCKSM_CSECME         Both enabled
//     FCKSM_CSECMD         Only clock switching enabled
//     FCKSM_CSDCMD         Both disabled
//   OSC2 Pin function:
//     OSCIOFNC_ON          Digital I/O
//     OSCIOFNC_OFF         OSC2 is clock O/P
//   Oscillator Selection:
//     POSCMD_EC            External clock
//     POSCMD_XT            XT oscillator
//     POSCMD_HS            HS oscillator
//     POSCMD_NONE          Primary disabled
//_FOSC(FCKSM_CSECME & OSCIOFNC_OFF & POSCMD_HS);
_FOSC(FCKSM_CSDCMD & OSCIOFNC_OFF & POSCMD_XT)
//_FOSC(FCKSM_CSECME & OSCIOFNC_OFF & POSCMD_XT);


//Power-Up Timer nach Brown Out 128 ms
_FPOR(FPWRT_PWR128)			

//Watchdog-Timer: 128*512*31.25�s=2.048 Sek.
#ifdef debug
	_FICD(BKBUG_ON & JTAGEN_OFF & ICS_PGD2)
	_FWDT(FWDTEN_OFF & WINDIS_OFF & WDTPRE_PR128 & WDTPOST_PS512)
#else
	//_FICD(BKBUG_OFF & JTAGEN_OFF & ICS_PGD2)
	_FWDT(FWDTEN_ON & WINDIS_OFF & WDTPRE_PR128 & WDTPOST_PS512)
#endif
/******************************************************************************
* Function Prototypes
******************************************************************************/
void COM_Auswertung(void);
void Befehlem(void);
void Befehlgt(void); 
void Befehlmd(void);
void Befehlms(void);
void Befehlre(void);
void Befehlrw(void);
void Befehlve(void);
void Befehlul(void);
void Befehlus(void);
void Befehlbr(void); 			//Testbefehle br,aa,ab 
void Befehlbn(void); 
void Befehlaa(void);
void Befehlab(void);

void BefehlKE(void);
void BefehlKS(void);
void BefehlKV(void);
void BefehlWR(void);
void BefehlRD(void);
void BefehlRM(void);
void BefehlRW(void);

void KeineAntwort(void); 

void MesswertSenden(void);

void SendeAntwort(void);
void SendeNo(void);
void SendeOk(void);

void SetupADC(void);
void SetupEmiNk(unsigned int);
void SetupEE(void);
void SetupPort(void);
void SetupTimer1(void);
void SetupPWM2(void);
void SetupUart(void);
void SetupSPI_1(void);
void SetupSPI_2(void);
void SetupLCD(void);

void LcdDisplay(void);
void LcdDispAxM(void);
void LcdDispDat(void);
void LcdDispVer(void);
//1.19
void LcdDispErr(void);
void LcdDispCwe(void);
void LcdDispTmp(void);

void LcdDispCtHdBt(void);
void LcdDispLck(void);
void LcdDispMEm(void);
void LcdDispMAd(void);

void SpiSendeLcd(void);
void SpiSendeCls(void);
void SpiSendeGrf(void);
void SpiSendeGrZ(void);
void SpiSendeGrX(void);
void SpiSendeTmp(void);
void SpiSendeClk(void);
void SpiSendePar(void);
void SpiSendeDat(void);

void SpiWriteRtc(unsigned char,unsigned char);
void SpiReadRtc(void);
void SpiStartRtc(void);
void SpiStoppRtc(void);

void TastePar(void);
void TasteUp(void);
void TasteDown(void);
void TasteEnter(void);
void TasteClearMem(void);
void TasteNumGrf(void);

void KBDRepeat(void);
void KBDNoMenu(void);

void MenuDisEmi(void);
void MenuDisMax(void);
void MenuDisAvg(void);
void MenuDisMem(void);
void DispCellNum(unsigned int);
void MenuDisInt(void);
void MenuDisEmiD(void);
void MenuDisCF(void);
void MenuDisRtcYear(unsigned int);
void MenuDisRtcMon(unsigned char);
void MenuDisRtcDay(unsigned char);
void MenuDisRtcHour(unsigned char);
void MenuDisRtcMin(unsigned char);

void RwMittelung(void);

void RechMessTemp(void); 
void RechMemMaxAvg(void); 

void RechGerTemp(void); 
void RechUBat(void);

void Tastenabfrage(void); 

void PowerOff(void);
void SegDisplay(void);
int RechAnzahlWerte(void);

void Beep(void);

void MessdatenVerpacken(void); 
void MessdatenEntpacken(void); 

void MessdatenSpeichern(void);
void MessdatenSkala128(void);
void MessdatenSkala128Init(void);
void MessdatenSpalteAusgeben(void);
void MessdatenLadeSpalte(void);
void MessdatenSkala48Grenzen(void);
void MessdatenKurve(void);
void EEDatenZeigen(void);

void MessdatenLoeschen(unsigned int,unsigned int); 
void MessdatenInitAdr(void);
void MessdatenZellePlus(void);
void MessdatenZelleMinus(void);
void MessdatenBlockPlus(void);
void MessdatenBlockMinus(void);
void MessdatenBlockAnzahl(void);

void GrafikGesamtZeit(void);
void GrafikMemZeit(void);
void GrafikAktZeit(void);
void GrafikMemProzent(void);
void GrafikBlockNr(void);

void EEpromRead(unsigned int,unsigned int,unsigned int*);
void EEpromWrite(unsigned int,unsigned int,unsigned int*);
void EEpromWriteVerify(unsigned int,unsigned int,unsigned int*);
unsigned char EEpromVerify(unsigned int,unsigned int,unsigned int*);

void FlashWriteVerify(unsigned int,unsigned char,unsigned int*);

extern void FlashRead(unsigned int,unsigned char,unsigned int*);
extern void FlashWrite(unsigned int,unsigned char,unsigned int*);
extern unsigned char FlashVerify(unsigned int,unsigned char,unsigned int*);

extern void FlashErasePage(unsigned int*);

extern unsigned int WandleC10F10(unsigned int); //1/10�C in 1/10�F wandeln
extern void cHexAscii(unsigned char,unsigned char*); //Wandlung Hex->Ascii 00...FF
extern void iHexAscii(unsigned int,unsigned char*);	//Wandlung Hex->Ascii 0000...FFFF
extern void wHexAscii(unsigned long,unsigned char*); //Wandlung Hex->Ascii 00000000...FFFFFFFF
extern void HexAsc5(unsigned int,unsigned char*); //Wandlung Hex->Ascii-Dez 0...65535
extern void HexAsc4(unsigned int,unsigned char*); //Wandlung Hex->Ascii-Dez 0...9999
extern void HexAsc2(unsigned char,unsigned char*); //Wandlung Hex->Ascii-Dez 0...99
extern void HexBcd5(unsigned int,unsigned char*); //Wandlung Hex->Bcd 0...65535
extern unsigned char AsciiHex(unsigned int,unsigned int); //Wandlung 2 Byte Ascii -> 1 Byte Hex

extern signed int LinTemp(signed long,unsigned long); //Rohwert linearisieren

extern unsigned int TK(unsigned int,unsigned int); //TK aus Matrix ermitteln

extern signed long Mul3216Div15(signed long,unsigned int); //Spezal-Multiplikation 32*16/2^15
extern signed long Mul3216Div10(signed long,unsigned int); //Spezal-Multiplikation 32*16/2^10
extern unsigned long Mul3232Div16(unsigned long,unsigned long); //Spezial-Multiplikation 32*32/2^16

extern void Lade16x32(unsigned char,unsigned char*,unsigned char*); //Pixeldaten laden
extern void Lade8x16(unsigned char,unsigned char*,unsigned char*);
extern void Lade8x8(unsigned char*,unsigned char*);

extern void Lade4x6Hi(unsigned char*,unsigned char*); //4x6 b�ndig mit Oberkante
extern void Lade4x6Lo(unsigned char*,unsigned char*); //4x6 b�ndig mit Unterante

extern void Lade5x6(unsigned char*,unsigned char*); //5x6 b�ndig mit Oberkante

extern void LadeVar4x6(unsigned char*,unsigned char*); //True Type 4/5x6

extern void MMAPositionen(void);
extern void MMASpalte(unsigned int,unsigned int,unsigned int,unsigned int,unsigned int*,unsigned char*);


extern void WarteUsec(unsigned long); //Warteschleife in �s
extern unsigned long DivUW1616(unsigned int,unsigned int);
extern unsigned int DivUD3216(unsigned long,unsigned int);
extern unsigned long DivLongUD3216(unsigned long,unsigned int);

extern unsigned long DivUD6432(unsigned long,unsigned long);

extern void	Pixelspalte(unsigned int,unsigned int,unsigned int*,unsigned int*,unsigned int*);
extern void MinMaxAvg(unsigned int*);

extern void LadeSpiBufLow(unsigned char*,unsigned int*);
extern void LadeSpiBufHigh(unsigned char*,unsigned int*);

extern unsigned long InitGrfBuf(unsigned int*);
extern unsigned long Uhrdaten(unsigned long);

extern void WriteDotMatrix(unsigned char*);

extern unsigned int RMARauschen(signed int*);
extern void RMAInitialisieren(signed int,signed int*);
extern signed int RMAMittelwert(signed long,unsigned int);
extern unsigned int RMARechStufe(signed int,signed int,unsigned int);

extern void SetupBoot(void);

void __builtin_btg (unsigned int*,unsigned int); 

/******************************************************************************
* Variable declarations
******************************************************************************/
unsigned char XmtBuf[33]__attribute__ ((aligned(1)));
unsigned char RcvBuf[32]__attribute__ ((aligned(1)));
unsigned char BcdBuf[17]__attribute__ ((aligned(1)));
unsigned char LcdBuf[38]__attribute__ ((aligned(1)));
unsigned char LineBuf[16];

unsigned char StSek,StMin,StStd,StTag,StMon,StJahr;	//tempor�re Uhrdaten / Stoppuhr


signed int LoPixAlt,HiPixAlt; 	//obere und untere Position Verbindungslinie Grafikspalte 
signed int LoPix,HiPix; 
unsigned int XBufCnt;	//Anzahl eingetragener Pixel im Grafikbuffer (0...127)
unsigned int XBufPos;	//Bufferposition Temperaturbuffer (0...127)
unsigned int XSpalte;	//Spaltenposition von MdCur in Grafikbuffer (0...127)
unsigned int TAvgCntPix; 	//Anzahl Messwerte pro Pixel (Grafik)
unsigned int YCnt;			//aktuelle Spalte im Grafikbuffer (0...127)
unsigned int DigitCount;
unsigned int CntDispTemp,AvgFaktor,ZwCount;
unsigned int CntTastenAbfrage,CntTastAlt;	//Z�hler 25ms Tastenabfrage
unsigned int CntEineSek;
unsigned int DatBuff[20];	// allg. Datenpuffer
unsigned int IntCnt;
unsigned int BatMode;
unsigned int RcvCnt;
unsigned int XmtBufCnt,XmtCnt;
unsigned int Range;
unsigned int MenuAktuell;
unsigned int SelClrMem;
unsigned int KBDStep;
unsigned int CntDispMem;
unsigned int CntAnalog;
unsigned int LcdPageCount,LcdData,SpiCnt;
unsigned int StModus,StIntTime;
unsigned int RtcSek,RtcMin,RtcStd,RtcTag,RtcMon,RtcJahr;	//rtc Uhrdaten
unsigned int CntDispVer; //25 ms Counter SW-Version zeigen
unsigned long ZwLong,DispTempSum,EmiNk;
signed long Rohwert,RwSumLang,RwSumKurz,RwMLang,RwMKurz,RwKurz,RwLang,AvgTempSum;
unsigned long IntRwKurz,IntRwLang;
unsigned int LichtCount,ZwInt;
unsigned int MessTemp,MemTempMerk,StTemp,DispTemp,MANTemp,GerTemp,RwMCnt;
unsigned int mscnt,RWAn0,RWAn1,MenuValue,GrenzeMin,GrenzeMax;
unsigned int TMinGes,TMaxGes,TAvgGes,MaxTemp1,MaxTemp2;
unsigned int EmiAlt,SaveStAlt,MemStartAlt,MemBelegtAlt,CurMemCellAlt;
unsigned int TmWait,TmWait1,TmWait2; 	//WaitState in Timerschritten 
signed int CntSendeMesswert,CntDispNeu,RMAAvg64,CntSegDisp;
// 1.19
unsigned int ErrEEpromCnt;

//Rohwertmittelung
signed long RMASumme,RMASum64;
unsigned int RMASumCnt;
unsigned int RMAPtr,RMAStufe;

unsigned int TSLRSt,TSLROn,TSLROff;
unsigned int Test3; //Zum Test

unsigned int CntRef,RschAmpSum,RschAmp;
signed int RMASgnPtr;
unsigned int TastenAlt,TastenNeu;

unsigned int CountHold,CntMenuzeit,MaxCount,Cnt1000MSec,RwDisc;
unsigned long CntIntMs;

unsigned int RwFkt[8];	//Ausgleichsfaktor Rohwerte kurze Ti -> lange Ti (mal 2e11)  
unsigned int MdStart,MdEnd,MdCur,MdGes,MdCnt,TAvgPix,QGanz,QRest,QGanzGes,QRestGes;

unsigned int TLimOG,TLimUG,TLimDif;

unsigned long TAvgSumPix,SkaFkt48;
unsigned int TMaxPix,TMinPix,YMax,YMin,YAvg;

unsigned int EE_Dat[EEBufflen];
unsigned int mdmax=EEBufflen/4;
unsigned long IntMs[8]; 		//Intervallzeiten 0...7 in msec

unsigned int EEStart,MBlock,KBDRep;

unsigned int TAvgBuf[128];			// Temperaturbuffer f�r grafische Darstellung
unsigned int TMaxBuf[128];			// Achtung, Reihenfolge und Gr�sse
unsigned int TMinBuf[128];			// darf nicht ge�ndert werden !
unsigned int PixMinBuf[128];		// Pixelbuffer f�r grafische Darstellung	
unsigned int PixMedBuf[128];		// Achtung, Reihenfolge und Gr�sse 			
unsigned int PixMaxBuf[128];		// darf nicht ge�ndert werden !		

unsigned long CntMsec;
unsigned int StEmi;
unsigned int MBUvl,MBOvl,CurMemBlck,AnzMemBlck;

//Beginn EEpromdaten (ext. EEprom RamTron FM25256B Adr. 7D00...7FFF = 384 Word)
//A) z.Z. E000...E01D	30 Word
unsigned int Emi;			
unsigned int EmiMax;
unsigned int EmiMin;
unsigned int NkFkt;
unsigned int ErrStatus;
unsigned int Menuzeit;
unsigned int FunktionA;
unsigned int FunktionB;
unsigned int FunktionC;
unsigned int TrapErr;
unsigned int Test1;			//nicht belegt
unsigned int SaveStatus;
unsigned int MBA;
unsigned int MBE;
unsigned int IntTime;
unsigned int Test2;			//nicht belegt
unsigned int CurMemCell;
unsigned int MemBelegt;
unsigned int MemStart;
signed int RMAMax;
unsigned int RSTMax;
unsigned int THold;
unsigned int RWBatWrn;
unsigned int RWBatAus;
unsigned int KalInTemp;
unsigned int GerTempMax;
unsigned int GeraeteID;
unsigned int ErstIni;
unsigned int EmiSw1;
unsigned int EmiSw2;

//B) z.Z. E060...E093 
//Blockladen nicht m�glich (Long Format)
unsigned int RwOffKurz[8];		//Wandleroffsets/Faktor je Range 0...7
unsigned int RwOffLang[8];		//Wandleroffsets/Faktor je Range 0...7
unsigned long RwKalKurz[8];		//Rohwerte bei K2/Range1 max.	
unsigned long RwKalLang[8];		//Rohwerte bei K2/Range1 max.	
unsigned int RMASchwelle[8];	//Schwelle f�r Sprungampl.
unsigned int TmKurz;
unsigned int TmLang;
//Ende EEpromdaten
/******************************************************************************
* Funktion: SetupEE
* Beschreibung: EEPROM einlesen / EEPROM-Initialisierung 
******************************************************************************/
void SetupEE(void)
{
	unsigned char c;
	
	ErrEEpromCnt=0;

	EEpromRead(EE_SWVersion,1,&ZwInt);	//SW-Version pr�fen
	if(ZwInt!=SoftVers){				//bei Ungleichheit in EEPROM schreiben
		ZwInt=SoftVers;
		EEpromWriteVerify(EE_SWVersion,1,&ZwInt);
	}

	EEpromRead(EE_Emi,30,&Emi);			//EEpr.-Bereich E000...E01D lesen

	if(ErstIni==0x0069){				//EEPROM-Erstinitialisierung
//EEpr.-Bereich E000...E01D wird im Block gelesen
//		EEpromRead(EE_Emi,1,&Emi);
//		EEpromRead(EE_EmiMax,1,&EmiMax);
//		EEpromRead(EE_EmiMin,1,&EmiMin);
//		EEpromRead(EE_NkFkt,1,&NkFkt);
//		EEpromRead(EE_ErrStatus,1,&ErrStatus);
//		EEpromRead(EE_Menuzeit,1,&Menuzeit);
//		EEpromRead(EE_FunktionA,1,&FunktionA);
//		EEpromRead(EE_FunktionB,1,&FunktionB);
//		EEpromRead(EE_FunktionC,1,&FunktionC);
//		EEpromRead(EE_SaveStatus,1,&SaveStatus);
//		EEpromRead(EE_MBA,1,&MBA);
//		EEpromRead(EE_MBE,1,&MBE);
//		EEpromRead(EE_IntTime,1,&IntTime);
//		EEpromRead(EE_CurMemCell,1,&CurMemCell);
//		EEpromRead(EE_MemBelegt,1,&MemBelegt);
//		EEpromRead(EE_MemStart,1,&MemStart);
//		EEpromRead(EE_RMAMax,1,&RMAMax);	//RMAMax ist Signed
//		EEpromRead(EE_RSTMax,1,&RSTMax);
//		EEpromRead(EE_THold,1,&THold);
//		EEpromRead(EE_RWBatWrn,1,&RWBatWrn);
//		EEpromRead(EE_RWBatAus,1,&RWBatAus);
//		EEpromRead(EE_KalInTemp,1,&KalInTemp);
//		EEpromRead(EE_GerTempMax,1,&GerTempMax);
//		EEpromRead(EE_GeraeteID,1,&GeraeteID);
//		EEpromRead(EE_ErstIni,1,&ErstIni);
//		EEpromRead(EE_EmiSw1,1,&EmiSw1);
//		EEpromRead(EE_EmiSw2,1,&EmiSw2);	
//EEpr.-Bereich E060...E093 lesen 
		EEpromRead(EE_RwOffKurz,7,&RwOffKurz[1]);
		EEpromRead(EE_RwOffLang,7,&RwOffLang[1]);
		EEpromRead(EE_RwKalKurz,14,&RwKalKurz_.WLo);
		EEpromRead(EE_RwKalLang,14,&RwKalLang_.WLo);
		EEpromRead(EE_RMASchwelle,7,&RMASchwelle[1]);
		EEpromRead(EE_TmKurz,1,&TmKurz);
		EEpromRead(EE_TmLang,1,&TmLang);
	}
	else{											//EEPROM-Erstinitialisierung

//1.19
		ErrStatus=0;
		EEpromWriteVerify(EE_ErrStatus,1,&ErrStatus);
		Emi=1000;
		EEpromWriteVerify(EE_Emi,1,&Emi);
		EmiMax=1000;
		EEpromWriteVerify(EE_EmiMax,1,&EmiMax);
		EmiMin=100;
		EEpromWriteVerify(EE_EmiMin,1,&EmiMin);
		NkFkt=1000;
		EEpromWriteVerify(EE_NkFkt,1,&NkFkt);
		Menuzeit=30;
		EEpromWriteVerify(EE_Menuzeit,1,&Menuzeit);
		FunktionA=0x0021;
		EEpromWriteVerify(EE_FunktionA,1,&FunktionA);
		FunktionB=0x0004;
		EEpromWriteVerify(EE_FunktionB,1,&FunktionB);
		FunktionC=0x0010;
		EEpromWriteVerify(EE_FunktionC,1,&FunktionC);
		ZwInt=0;
		EEpromWriteVerify(EE_TrapErr,1,&ZwInt);
		SaveStatus=0;
		SaveEmiD=1;	
		EEpromWriteVerify(EE_SaveStatus,1,&SaveStatus);
		MBA=950;
		EEpromWriteVerify(EE_MBA,1,&MBA);
		MBE=11875;
		EEpromWriteVerify(EE_MBE,1,&MBE);
		IntTime=0;
		EEpromWriteVerify(EE_IntTime,1,&IntTime);
		CurMemCell=0;
		EEpromWriteVerify(EE_CurMemCell,1,&CurMemCell);
		MemBelegt=0;
		EEpromWriteVerify(EE_MemBelegt,1,&MemBelegt);
		MemStart=0;
		EEpromWriteVerify(EE_MemStart,1,&MemStart);
		RMAMax=0x7FFF;		
		EEpromWriteVerify(EE_RMAMax,1,(unsigned int*)&RMAMax);
		RSTMax=80; //darf nicht gr�sser sein wegen Bufferl�nge	
		EEpromWriteVerify(EE_RSTMax,1,&RSTMax);
		THold=0x0258;	//Hold Zeit in 25ms Schritten, 600*25=15000=15Sek.
		EEpromWriteVerify(EE_THold,1,&THold);
		RWBatWrn=0;
		EEpromWriteVerify(EE_RWBatWrn,1,&RWBatWrn);
		RWBatAus=0;
		EEpromWriteVerify(EE_RWBatAus,1,&RWBatAus);
		KalInTemp=0;
		EEpromWriteVerify(EE_KalInTemp,1,&KalInTemp);
		GerTempMax=0;
		EEpromWriteVerify(EE_GerTempMax,1,&GerTempMax);
		GeraeteID=0x38;
		EEpromWriteVerify(EE_GeraeteID,1,&GeraeteID);
		ZwInt=0;									//Bootword deaktivieren
		EEpromWriteVerify(EE_BootWord,1,&ZwInt);
		EmiSw1=0;									//Emi-Switch	
		EEpromWriteVerify(EE_EmiSw1,1,&EmiSw1);
		EmiSw2=0;
		EEpromWriteVerify(EE_EmiSw2,1,&EmiSw2);
		EE_Dat[0]=0;
		EE_Dat[1]=0;
		EE_Dat[2]=0;
		EEpromWriteVerify(EE_Kal1Dat,3,&EE_Dat[0]);
		EEpromWriteVerify(EE_Kal2Dat,3,&EE_Dat[0]);
		for(c=1;c<8;c++){		
			RwOffKurz[c]=4096;
			RwOffLang[c]=4096;
		}
		EEpromWriteVerify(EE_RwOffKurz,7,&RwOffKurz[1]);
		EEpromWriteVerify(EE_RwOffLang,7,&RwOffLang[1]);
		RwKalKurz[1]=100000;
		RwKalLang[1]=1000000;
		RwKalKurz[2]=50000;
		RwKalLang[2]=500000;
		RwKalKurz[3]=33333;
		RwKalLang[3]=333333;
		RwKalKurz[4]=25000;
		RwKalLang[4]=250000;
		RwKalKurz[5]=20000;
		RwKalLang[5]=200000;
		RwKalKurz[6]=16666;
		RwKalLang[6]=166666;
		RwKalKurz[7]=14285;
		RwKalLang[7]=142857;
		EEpromWriteVerify(EE_RwKalKurz,14,&RwKalKurz_.WLo);
		EEpromWriteVerify(EE_RwKalLang,14,&RwKalLang_.WLo);
		RMASchwelle[1]=90;
		RMASchwelle[2]=80;
		RMASchwelle[3]=70;
		RMASchwelle[4]=65;
		RMASchwelle[5]=60;
		RMASchwelle[6]=55;
		RMASchwelle[7]=50;
		EEpromWriteVerify(EE_RMASchwelle,7,&RMASchwelle[1]);
		TmKurz=184;
		EEpromWriteVerify(EE_TmKurz,1,&TmKurz);
		TmLang=1840;
		EEpromWriteVerify(EE_TmLang,1,&TmLang);

//1.19 immer nach Erstinitialisierung anzeigen
		ReqErr=1;	//Fehlercode anzeigen
		EnabGrf=0;	// Grafikmode abschalten
		if(ErrEEpromCnt==0){
			ErstIni=0x0069;
			EEpromWriteVerify(EE_ErstIni,1,&ErstIni);
		}
	}
 
//Einige Sicherheitsabfragen
//min. und max. Holdzeit (nach Update kann in entspr. EEpr.-Zelle Unsinn stehen) 
//Ist der Wert f�r THold zu klein, schaltet das Ger�t erst gar nicht ein.
//nach einem Update kann in der entspr. EEpr.-Zelle (E015) Unsinn stehen (0xAAAA = EEpr. unbeschr.  

	if(THold<0x28||THold>0x3A98){				// HoldZeit Min = 1 Sek Max = 10 Min
		THold=0x0258;
		EEpromWriteVerify(EE_THold,1,&THold);	// Korrigierte THold speichern
	}
//Plausibilit�tspr�fung Emi-Switch
//Emi-Switch ist aktiv, wenn EmiSw1 > 0 und EmiSw2 > 0
//nach einem Update kann in den entspr. EEpr.-Zellen (E01C,E01D) Unsinn stehen (0xAAAA = unbeschrieben)  
	if(EmiSw1>0||EmiSw2>0){
		if(EmiSw1<50||EmiSw1>1023||EmiSw2<50||EmiSw2>1023){
			EmiSw1=0;
			EmiSw2=0;
			EEpromWriteVerify(EE_EmiSw1,1,&EmiSw1);
			EEpromWriteVerify(EE_EmiSw2,1,&EmiSw2);
		}
	}

	if(RMAMax<0)						//RMAMax (Signed) max. = 7FFF 				 
		RMAMax=0x7FFF;
	if(RSTMax>80)						//max. 80 Werte im Ringspeicher
		RSTMax=80;
//Messwertmittelung Display
	if(GSMode)
		CntRef=500;						//T_Erfass 0.5 Sek			
	else
		CntRef=333;
	RMAIni=1;
	MBUvl=MBA-50;						//Temp. Underflow
	MBOvl=MBE+200;						//Temp. Overflow
	EmiAlt=Emi;
	SetupEmiNk(Emi);					//Faktor aus Emi und Nachkalibrierfaktor vorberechnen
	SaveStAlt=SaveStatus;
	CurMemCellAlt=CurMemCell;
	MemBelegtAlt=MemBelegt;
	MemStartAlt=MemStart;
//cont Mode liegt immer ab 1470 Timerschritten vor (T kritisch = 0,21701 * 1470 = 319,01 �s)
//TmLang max. = 300�s/0.1085=2764 ! sonst st�ndig Wechsel ncont/cont Mode
//1.17
	TmWait2=700;						//700 ca. 76�s Relaese State DDC114		
	TmWait1=9216-TmWait2-TmKurz-TmLang;	//9216 Intervallzeit 1000�s

//Berechnung der Ausgleichfaktoren f�r Kanal 1 (kurze Ti) 
//zur Angleichung an Kanal 2 (lange Ti) Range 1...7	
	for(c=7;c>0;c--){
		if(RwKalKurz[c]!=0){
			ZwLong=(long long)RwKalLang[c]*1024;
			RwFkt[c]=ZwLong/RwKalKurz[c];
		}
	}
//Speicherintervallzeiten f�r Men�punkt <INT> in msec -1 initialisiert  
	IntMs[0]=0xFFFFFFFF;				// -> Intervall aus
	IntMs[1]=0;							//1 msec
	IntMs[2]=19;						//20 msec
	IntMs[3]=99;						//100 msec
	IntMs[4]=999;						//1 Sek
	IntMs[5]=9999;						//10 Sek
	IntMs[6]=99999;						//100 Sek
	IntMs[7]=499999;					//500 Sek	
}
/******************************************************************************
* Funktion: SetupPort
* Beschreibung: Konfiguration der I/O Ports 
******************************************************************************/
void SetupPort(void)
{
	TRISB=TRISB_INIT;	
	LATB=PORTB_INIT;
	TRISC=TRISC_INIT;	
	LATC=PORTC_INIT;
	TRISD=TRISD_INIT;	
	LATD=PORTD_INIT;
	TRISF=TRISF_INIT;	
	LATF=PORTF_INIT;
	TRISG=TRISG_INIT;	
	LATG=PORTG_INIT;
}
/******************************************************************************
* Funktion: SetupTimer1
* Einstellen des Taktverh�ltnis f�r Dot Matrix Display SLR2016
* Das Display wird jede mSec im Data-Valid Intervall eingeschaltet 
* ClkFrequ=18432000
* TEin = ClkFrequ/N * 0.108 us
* Bsp: 	ClkFrequ/20000 * 0.1085 us = 100 us
*		ClkFrequ/13270 * 0.1085 us = 150 us
*		ClkFrequ/10000 * 0.1085 us = 200 us
*
*		===>>> PR1 = t[us] / 0.1085
******************************************************************************/
void SetupTimer1(void)	
{
	T1CON=0x00; 			// Timer1 aus, Prescale = 1:1
	TMR1=0x00; 				// Timer1 zur�cksetzen
//1.17
//	PR1=ClkFrequ/20000;		// 100 �s Zeitbasis 
//	PR1=138;				// 15us 

	IPC0bits.T1IP=5; 		// Timer1 Interrupt priority
	IFS0bits.T1IF=0; 		// Timer1 interrupt status flag = 0
	IEC0bits.T1IE=1; 		// Timer1 interrupts enable 

//1.18
	TSLROn=138;				//15us
	TSLROff=2304-TSLROn;	//235us
	TSLRSt=460;				//50us

	PR1=TSLROff;
	T1CONbits.TON=1;
}
/******************************************************************************
* Funktion: SetupPWM2 
* Beschreibung: Konfiguration PWM2 f�r Summeransteuerung
* Timer3 = Base Timer (OC2CON.OCTSEL=1)
* Timer4 = Tondauer -> IRQ gesteuert (T4Interrupt)
*
* Einstellen der Frequenz (1kHz):
*
* 	1kHz=1ms=1000�s=1000000ns
*	TCY=54.26ns @ 18.432MHz
*	PWM Period = (PR3 + 1) x TCY x (Timer3 Prescale Value)
*	1000000ns = (PR3 + 1) x 54.26ns x 1
*	PR3 = (1000000 / 54.26) - 1 = 18430
*
*			|<----|<----|<----|<-- OC2RS=PR3: OC2->Low, Timer3 Reset
* Timer--->/|    /|    /|    /|
*		  /	|   / |   / |   / |
*		 /<----/<----/<----/<-- OC2R=Timer3: OC2->High 
*		/   | /   | /   | /   | 
*	   /    |/    |/    |/    |/
*		  __    __	  __    __
*	OC2__|  |__|  |__|  |__|  |_
*
******************************************************************************/
void SetupPWM2(void)
{

// alt: Continuos Pulse Mode
//	OC2CON=0;				//OC2 aus
//	OC2RS=0x47FE; 			//Lade Compare Register f�r fallende Flanke
////	OC2RS=0x1200; 			//Lade Compare Register f�r fallende Flanke
//	OC2R=OC2RS/2; 			//Lade Compare Register f�r steigende Flanke 
//	PR3=OC2RS;				//Lade Periodenregister f�r Timer Reset
//	T3CON=0;				//Timer3 aus,1:1 prescale value,Internal clock (FOSC/2)
//	IEC0bits.OC2IE=0;		//keine IRQs
//	IEC0bits.T3IE=0;
//	T3CONbits.TON=1;		//Timer3 einschalten
//	T4CON=0x30;				//Timer4 aus,1:256 prescale value,Internal clock (FOSC/2)
//	IFS1bits.T4IF=0;		
//	IEC1bits.T4IE=1;
//	IPC6bits.T4IP=3;		


//neu: Toggle Mode
	OC2CON=0;				//OC2 aus
	T3CON=0;				//Timer3 aus,1:1 prescale value,Internal clock (FOSC/2)
//	PR3=0x47FE;				//Timer3 Reset value
	PR3=0x191E;				//Timer3 Reset value
	OC2R=PR3/2; 			//OC2R muss kleiner PR3 sein 

	IEC0bits.OC2IE=0;		//keine IRQs
	IEC0bits.T3IE=0;
	T3CONbits.TON=1;		//Timer3 einschalten

	T4CON=0x30;				//Timer4 aus,1:256 prescale value,Internal clock (FOSC/2)
	IFS1bits.T4IF=0;		
	IEC1bits.T4IE=1;
	IPC6bits.T4IP=3;		


}
/******************************************************************************
* Funktion: SetupSPI1
* Beschreibung: Konfiguration des SPI1 Moduls PIC24HJXXXGP06
* SPI1: EEPROM FM25256B 	schreiben und lesen (max. 20 MHz)  
*							-> CS_EE enable = 0
*
*		RTC MAX-DS1305 		Uhrdaten schreiben und lesen 
*							-> CE1305 enable = 1
*
*		SLR2016 			4 mal 7 x 5 Pixel Innen Display Matrix 
*							-> WR2016 enable = 0
*
*		ST7565 				Displaytreiber f�r DOGM128, nur schreiben (max. 20 MHz)
*							-> CS_LCD enable = 0
*
* Der Primary- und und der Secundary Prescaler werden direkt vor Ort eingestellt.
* Achtung, laut Datenblatt PIC24HJxxxGA006 betr�gt die maximale SPI-Frequenz 10 MHz !
******************************************************************************/
void SetupSPI_1(void)
{
	SPI1STAT=0;							//SPIEN muss 0 sein um Config Register zu �ndern !  
	
	SPI1CON1=0x0120;
	SPI1CON1bits.CKE=0;					
	SPI1CON1bits.CKP=0;
	SPI1CON1bits.MODE16=0;				//8 Bit Modus einstellen
	SPI1CON1bits.MSTEN=1;				//Master Mode Enable	
	SPI1CON1bits.SMP=0;					//Sample Data at Middle	
	SPI1CON1bits.PPRE=3;				//Primary Prescale = 1:1 (Clk = 18,432 MHz)
	SPI1CON1bits.SPRE=7;				//Secondary Prescale SPI1= 1:1
	SPI1CON2=0;
	IPC2bits.SPI1IP=4;					//SPI1 Interrupt Priority level
	IFS0bits.SPI1IF=0;					//Clear SPI1 IF
	IEC0bits.SPI1IE=0;					//keine SPI1 Interrupts
	SPI1STATbits.SPIEN=1;
}
/******************************************************************************
* Funktion: SetupSPI_2
* Beschreibung: Konfiguration SPI1 (EE,RTC,LCD) und SPI2 (DDC114) und 
* 				DMA0 und DMA1 (intern ADW, 2 Kan�le)
* 				f�r SPI1-Transmit, um das Senden zu automatisieren
* (18,432 MHz) (9,216 MHz)
*
* DMA0: LCD senden, RTC schreiben
*
* DMA1: ADC1 (AN0,AN1)
*
* SPI2:	Daten lesen vom AD-Wandler DDC114
*		Konfiguration: 	3-pin mode
*						SDIx:	SPI Dateneingang PIC24HJ64 	
*						SDOx:	SPI Datenausgang PIC24HJ64
*						SCKx:	SPI Clock (max. 16 MHz)
*						SSx\:	n.b., da 3-pin mode
*
******************************************************************************/
void SetupSPI_2(void)
{
//DMA0 / SPI1 funktioniert nicht !
//Setup DMA Channel 0 for SPI Transmit (CHannel 0 = h�chste Priority)
//	DMA0CON=0x6001;						//One-Shot, Size=Byte, Read from DPSRAM write to Periph. 
//	DMA0PAD=(volatile unsigned int)&SPI1BUF; 
//	DMA0CON=0x6000;						//Interrupt after all moved/Normal Operation
//										//Register Indirect mit Postincrement, Ping-Pong disabled
//	DMA0CONbits.AMODE=2; 				//DMA Peripheral indirect mode 
//	DMA0CONbits.MODE=0; 				//DMA Continuous mode, Ping-Pong disabled 
//	DMA0REQ=10; 						//Select SPI1 as DMA0 Request source
//	DMA0STA=__builtin_dmaoffset(RcSpi1Buf); 
//	DMA0CNT=63; 						//DMA request jedes 64 mal auswerten!  
//	DMA0CONbits.CHEN=1;
//	IFS0bits.DMA0IF=0; 					//Clear the DMA0 interrupt Flag
//	IEC0bits.DMA0IE=1; 					//Enable DMA0 interrupt

//	DMA0REQbits.FORCE=1;
//	while(DMA0REQbits.FORCE==1);

//DMA2 / SPI1 funktioniert nicht !
//Setup DMA Channel 2 for SPI Recieve RTC
	DMA2CON=0x0001;						//DMA2 off/Size=Byte/Read from Periph. write to DPSRAM
//										//Interrupt after all moved/Normal Operation
//										//Register Indirect mit Postincrement/One Shot, Ping-Pong disabled
//	DMA2STA=__builtin_dmaoffset(TxRSpi1Buf); 
//	DMA2PAD=(volatile unsigned int)&SPI1BUF; 
//	DMA2CNT=6; 							//Interrupt nach 7  empf. Byte
//	DMA2REQ=10; 						//Select SPI1 as DMA0 Request source
////#### ChEN vor Ort setzen
//
//	DMA2CONbits.SIZE=1;
//	DMA2CONbits.CHEN=1;					//DMA2 on
//	IFS1bits.DMA2IF=0; 
//	IEC1bits.DMA2IE=1; 

//Konfig SPI2
	SPI2STAT=0;							//SPIEN muss 0 sein um Config Register zu �ndern !  
	SPI2CON1=0;							//CKE=0, CKP=0
	SPI2CON1bits.DISSDO=1;				//SDOx pin is not used by module, pin functions as I/O
	SPI2CON1bits.MODE16=1;				//Communication is word-wide (16 bits)
	SPI2CON1bits.MSTEN=1;				//Master Mode Enable	
	SPI2CON1bits.SPRE=7;				//Secondary Prescale = 1:1 (Clk = 9,216 MHz)
	SPI2CON1bits.PPRE=3;				//Primary Prescale = 1:1
	SPI2CON2=0;
	IPC8bits.SPI2IP=6;					//SPI2 Interrupt Priority level=6
	IFS2bits.SPI2IF=0;					//Clear SPI2 IF
	IEC2bits.SPI2IE=0;					//SPI2 Interrupt disable
	SPI2STATbits.SPIEN=1;				//SPI2 enable
}
/******************************************************************************
* Funktion: SetupDDC114
* Beschreibung: Konfiguration Output Compare 1 f�r Conv-Toggle 
*				Startup-Sequenz DDC114
******************************************************************************/
void SetupDDC114(void)
{
	ClkEnab=0;							// Clock aus	(wg. mgl. Offsetfehler DDC114
	ClkEnab=0;							// Clock aus	(PORT 2 mal schalten ! 

	nReset=0;							// Reset ein 

	OC1CON=0x0000; 						// OC1 aus
	OC1CONbits.OCM=0b010; 				// OC1 Pin (High if OCM=0b010)
	OC1CON=0x0000; 						// OC1 aus

	OC1CONbits.OCTSEL=0; 				// Select Timer2 as output compare time base
	OC1R=0;								// Special Case Toggle Mode

	IPC0bits.OC1IP=7; 					// Setup OC 1 interrupt Priority
	IFS0bits.OC1IF=0; 					// Clear OC 1 interrupt flag
	IEC0bits.OC1IE=1; 					// Interrupts enable

	INTCON2bits.INT1EP=1;				// External Interrupt 1 (Data Valid) = Negative Flanke
	IPC5bits.INT1IP=6;					// External Interrupt 1 (Data Valid) Interrupt Priority = 6
	IFS1bits.INT1IF=0;					// Ext. Interrupt 1 status flag = 0 (Data Valid)
	IEC1bits.INT1IE=1;					// Ext. Interrupt 1 Enable (Data Valid)

	T2CON=0x00; 						// Timer2 Stopp
	TMR2=0x00; 							// Timer2 Clear
	T2CONbits.TCS=0;					// Timer2 Clock Source intern Fosc/2

	WarteUsec(10000);					// 10 ms warten f�r Startup DDC114	
	nReset=1;							// DDC114 aktivieren
	WarteUsec(50);						// 50 �s warten f�r Startup DDC114

// Conv = Low
	OC1CON=0x0000; 						// OC1 aus
	OC1CONbits.OCM = 0b001; 			// OC1 Pin (Low if OCM=0b001)
	OC1CON=0x0000; 						// OC1 aus

	ClkEnab=1;							// Takt dazuschalten
	TMR2=0x00; 							// Timer2 Clear
	PR2=461;							// 50�s warten
	IntCnt=0;							// Kennung f�r OC1-IRQ

	OC1CONbits.OCM=3; 					// OC Toggle Mode	 					
	T2CONbits.TON=1; 					// Start Timer2 f�r OC1

	DDCInit=0;							// 0=initialisiert							
}
/******************************************************************************
* Funktion: SetupLCD
* Beschreibung: LCD konfigurieren und Einschalten. 
*
* 	Command						Remark									Code(Hex)
* 	-----------------------------------------------------------------------------
* 	Display start line set		Display start line 0					0x40
*	ADC set						ADC reverse								0xA1
*	Common output mode select	Normal COM0~COM63						0xC0
*   Display normal/reverse		Display normal							0xA6
*	LCD bias set				Set bias 1/9 (Duty 1/65)				0xA2
*	Power control set			Booster, Regulator and Follower on		0x2F
*	Booster ratio set			Set internal Booster to 4x				0xF8,0x00
*	V0 voltage regulator set	Contrast set							0x27
*	Electronic volume mode set	Contrast set							0x81,0x16
*	Static indicator set		No indicator							0xAC,0x00
* 	Display ON/OFF				Display on								0xAF  
******************************************************************************/
void SetupLCD(void)
{
	CS_LCD=0; 
	ANull=0;							//0=Sende Kommando, 1=Sende Daten
	RstDogM=1;							//Reset DOGM aus

	SPI1STATbits.SPIEN=0; 				
	SPI1CON1bits.MODE16=0;				//8 Bit Modus einstellen
	SPI1CON1bits.CKE=1;					
	SPI1CON1bits.CKP=0;
	SPI1CON1bits.PPRE=3;				//Primary Prescale SPI1= 1:1 (Clk = 14.7456 MHz)
	SPI1CON1bits.SPRE=7;				//Secondary Prescale SPI1= 1:1 (Clk = 7.3728 MHz)
	SPI1STATbits.SPIEN=1;				//SPI1 enable

	WarteUsec(1);						//bisserl warten

//Reset			
	SPI1BUF=0xE2;	
	while(SPI1STATbits.SPITBF);				

//Display start line set		Display start line 0=40
	SPI1BUF=0x40;	
	while(SPI1STATbits.SPITBF);			

//	ADC set	normal/reverse		normal=A0, reverse=A1 (Spiegelverkehrt)
	SPI1BUF=0xA1;	
	while(SPI1STATbits.SPITBF);			

//	Common output mode select	Normal COM0~COM63=C0, Reverse COM63...COM0=C8
	SPI1BUF=0xC0;	
	while(SPI1STATbits.SPITBF);

//   Display normal/reverse		Display normal=A6, reverse=A7 (hell/dunkel invertiert)
	SPI1BUF=0xA6;	
	while(SPI1STATbits.SPITBF);

//	LCD bias set				Set bias 1/9 (Duty 1/65)=A2
	SPI1BUF=0xA2;	
	while(SPI1STATbits.SPITBF);

//	Power control set			Booster, Regulator and Follower on=2F
	SPI1BUF=0x2F;	
	while(SPI1STATbits.SPITBF);

//	Booster ratio set			Set internal Booster to 4x=F8,00
	SPI1BUF=0xF8;	
	while(SPI1STATbits.SPITBF);
	SPI1BUF=0x00;	
	while(SPI1STATbits.SPITBF);

//	V0 voltage regulator set	Contrast set=0x2X (X=0...7)
	SPI1BUF=0x26;	
	while(SPI1STATbits.SPITBF);

//	Electronic volume mode set	Contrast set=81,XX (XX=0...63)
	SPI1BUF=0x81;	
	while(SPI1STATbits.SPITBF);
	SPI1BUF=0x1C;	
	while(SPI1STATbits.SPITBF);

//	Static indicator set		ON=AD, OFF=AC
	SPI1BUF=0xAC;				//SI ausschalten = 1 Command
	while(SPI1STATbits.SPITBF);
	SPI1BUF=0x00;				//SI einschalten = 2 Commands	
	while(SPI1STATbits.SPITBF);

// 	Display all Points ON/OFF	Display all Points on=A5, Display all Points off=A4
//	SPI1BUF=0xA5;	
//	while(SPI1STATbits.SPITBF);
 
//	Display all Points ON/OFF	Display all Points off=A4 
//	SPI1BUF=0xA4;	
//	while(SPI1STATbits.SPITBF);

// 	Display ON/OFF				Display on=AF, Display off=AE  
	SPI1BUF=0xAF;	
	while(SPI1STATbits.SPITBF);

	CS_LCD=1;					//Chip deselect LCD 
}
/******************************************************************************
* Funktion: PowerOff 
* Beschreibung: Ger�t ausschalten 
* 1.LCD Abschaltsequenz ausf�hren um zuf�llige Pixelmuster beim
*   Abschalten zu vermeiden.
* 2.Dot Matrix vorladen
* 3.Selbsthaltung abschalten, warten in Endlosschleife 
******************************************************************************/
void PowerOff(void)
{
	CS_LCD=0; 
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SPI1STATbits.SPIEN=0; 				
	SPI1CON1bits.PPRE=3;				// Primary Prescale SPI1= 1:1
	SPI1CON1bits.SPRE=7;				// Secondary Prescale SPI1= 1:1
	SPI1STATbits.SPIEN=1;				// SPI1 enable

// 	Display OFF				Display on=AF, Display off=AE  
	SPI1BUF=0xAE;	
	while(SPI1STATbits.SPITBF);

// 	Display all Points ON	Display all Points on=A5, Display all Points off=A4
	SPI1BUF=0xA5;	
	while(SPI1STATbits.SPITBF);

	CS_LCD=1;							// Chip deselect LCD 
	CS_LCD=1;							// Chip deselect LCD 

	RstDogM=0;							// Reset DOGM ein

	Zw2016=0;		
	BL2016=0;		
	HexBcd5(MBUvl,&BcdBuf[0]);
	WriteDotMatrix(&BcdBuf[0]);			//inneres Display nach dem Einschalten

	WarteUsec(80000);					// ca. 80 ms. warten
										// bis V0 to V4 Capacity < 1.8 Volt ist
Waitend:
	PowerOn=0;			// Selbsthaltung aus -> Ger�t schaltet ab
	CLRWDT();			
	goto Waitend;		// Warten bis zum Ende 
}	
/******************************************************************************
* Funktion: SetupUART
* Beschreibung: Konfiguration von UART2 auf feste Baudrate -> 115200 Bd 
* 
* Baudratenregister schreiben (UxBRG)	
*
*	Fcy = Fosc / 2 = 18.432 MHz / 2 =  9.216 MHz
* =============================================================================
*	A) BRGH=0										B) BRGH=1 
* =============================================================================
*   		   	Fcy						   						Fcy
*	UxBRG =	-------------- - 1						UxBRG =	-------------- - 1
*		 	 	16 * BR					 						4 * BR
*
*				Fcy												Fcy
*	BR = 	--------------							BR = 	--------------	
*			16 � (BRGx + 1)									4 � (BRGx + 1)
*
*	
*	Beispiel: Fosc= 36.864 MHz -> Fcy=18.432 MHz
* 
*				18.432 MHz										18.432 MHz		
*	UxB2BRG = -------------- - 1 =  9				UxB2BRG = -------------- - 1 =  39
*				16 * 115200										4 * 115200
*
*
*				18.432 MHz										18.432 MHz		
*	UxB2BRG = -------------- - 1 =  119				UxB2BRG = -------------- - 1 =  479
*				16 * 9600										4 * 9600
*
*
*	Beispiel: Fosc = 18.432 MHz -> Fcy = 9.216 MHz
*
*				9.216 MHz					
*	UxB2BRG = -------------- - 1 =  4
*				16 * 115200
*
*
*				9.216 MHz					
*	UxB2BRG = -------------- - 1 = 59 
*				16 * 9600
*
*  Die UART2 wird fest auf 115200 Baud eingestellt, ein Bit dauert dann 8,68 �s. 
*  Es werden 8 Datenbit + 1 Stoppbit = 9 Bit pro Charakter �bertragen, d.h.
*  ein Zeichen dauert 78,12 �s. (Parit�t = even)
*
*  Das Senden der Daten geschieht durch Setzen des Bit <U2STAbits.UTXEN>.
*  Dadurch wird gleichzeitig das Interruptflag <IFS1bits.U2RXIF gesetzt>, und
*  das Programm wird in der Interrupt Service Routine (ISR) fortgesetzt.
*  Dort wird ein Datenbyte in den Transmit Buffer geschrieben.
*
*  || ACHTUNG, zwischen UTXENable und schreiben in Transmit Buffer muss  
*  '' mind. eine Bitzeit gewartet werden ! sonst gibts �bertragungsfehler ! 
*
*  Ist das Datenbyte vom Transmit Buffer in das Transmit Shift Register
*  �bertragen, wird wieder ein Interrupt ausgel�st. (d.h. mindestens ein freier
*  Platz im 4 Byte FIFO Buffer) und das n�chste zu �bertragende Zeichen wird
*  verschickt.
*
*  Der Empfang von Daten geschieht in der entsprechenden ISR, immer wenn ein
*  Datenbyte im Empfangsbuffer (U2RXREG) vollst�ndig �bertragen ist wird das   
*  Interruptflag <IFS1bits.U2RXIF> gesetzt.
*  Erst wenn das Endekennungszeichen CR (=0xD) empfangen wurde, werden die
*  empfangenen Daten ausgewertet.
******************************************************************************/
void SetupUart(void)
{
    U2BRG=((ClkFrequ/115200)/32)-1 ;	//115200 Baud einstellen
	U2MODE=0x0002;						//8 Datenbits, Even Parity, 1 Stoppbit, Brgh=0
	IPC7bits.U2TXIP=4;					//Set Transmit Interrupt Priority
	U2STAbits.UTXISEL1=0;				//Interrupt if any character is transmitted to Transmit Shift Register
	U2STAbits.UTXISEL0=0;
	IFS1bits.U2TXIF=0;					//Clear Transmit Interrupt Flag
	IEC1bits.U2TXIE=0;					//Enable Transmit Interrupt
	IPC7bits.U2RXIP=4;					//Set Receive Interrupt Priority
	U2STAbits.URXISEL=0;				//Interrupt after one RX character is received
	IFS1bits.U2RXIF=0;					//Clear Receive Interrupt Flag
	IEC1bits.U2RXIE=1;					//Enable Receive Interrupt
	RcvCnt=0;
	XmtCnt=0;
	U2MODEbits.UARTEN=1;				//Enable UART
	U2STAbits.UTXEN=1;	

//zum testen Loopback Mode
//	U2MODEbits.LPBACK=1;

}
/******************************************************************************
* Funktion: SetupADC
* Beschreibung: Konfiguration interner ADW f�r DMA1  - > 10-Bit Mode
* -----------------------------------------------------------------------------
* TCY			= 2 / 18.432 MHz = 108.51 ns
* TSample 		= TAD 				* SAMC
* 			 	= ADCS 	* TCY 		* 31
*				= 64 	* 108.51 ns	* 31	
*				= 6.94 �s			* 31 =  215.28 �s
* TConversion	= TAD 				* 14
*				= 6.94 �s 			* 14 = 97.16 �s
* Intervallzeit = TSample + TConversion = 215.28 �s + 97.16 �s =  312.44 �s
******************************************************************************/
void SetupADC(void)
{
	AD1PCFGH=0xFFFF;					//alle ADW Pins digital I/O...
	AD1PCFGL=0xFFFC;					//bis auf: AN0,AN1=Analog Input  

//ADxCON1
	AD1CON1=0;							//ADON=0 ADC aus
										//FORM=00 Data Output Format Integer (DOUT = 0000 00dd dddd dddd )
	AD1CON1bits.SIMSAM=0;				//Samples multiple channels individually in sequence
	AD1CON1bits.AD12B=0;				//Select 10-bit mode
	AD1CON1bits.ASAM=1;					//Automatischer Sample-Mode
	AD1CON1bits.SSRC=0b111;				//Auto convert
	AD1CON1bits.ADDMABM=0;				//DMA Storage in Scatter/Gather mode

//ADxCON2
	AD1CON2=0;							//BUFM=0 Always starts filling the buffer from the start adress
	AD1CON2bits.CSCNA=1;				//Scan Inputs Selection for CH0
	AD1CON2bits.VCFG=0b000;				//VREFH = Avdd = 3.3V, VREFL = Avss = 0V
	AD1CON2bits.SMPI=0b0001;			//2 ADC buffers

//ADxCON3
	AD1CON3=0;							//ADRC=0 Clock derived from system clock
	AD1CON3bits.ADCS=0b111111;			//ADC Conversion Clock Select bits = 64 
	AD1CON3bits.SAMC=0b11111;			//Auto Sample Time bits = 31 * TAD

//ADxCON4
	AD1CON4bits.DMABL=0b000;			//Each buffer contains 1 words

	AD1CHS0bits.CH0NA=0;				//Channel 0 negative input is VREF-
	AD1CHS0bits.CH0SA=0b0000;			//Channel 0 positive input is AN0

	AD1CHS123bits.CH123SA=0; 			//MUXA +ve input selection (AIN0) for CH1 
	AD1CHS123bits.CH123NA=0;			//MUXA -ve input selection (Vref-) for CH1
	AD1CSSH=0;
	AD1CSSL=3;							//Scanning AN0,AN1

//DMA Channel 1
	DMA1PAD=(volatile unsigned int)&ADC1BUF0;	//(0x0300=ADC1BUF0)
	DMA1CON=0;							
	DMA1CONbits.AMODE=2; 				//DMA Peripheral indirect mode 
	DMA1CONbits.MODE=0; 				//DMA Continuous mode, Ping-Pong disabled
	DMA1CNT=63; 						//DMA request jedes 64 mal auswerten!  

	DMA1REQ=13;							//Select ADC1 as DMA1 Request source
	DMA1STA = __builtin_dmaoffset(&BufferA);
//	DMA1STB = __builtin_dmaoffset(&BufferB);	//Nur im Ping-Pong Mode n�tig

	IFS0bits.DMA1IF=0;					//Clear DMA1 Interrupt Flag
	IEC0bits.DMA1IE=1;					//DMA1 Interrupt Enable

	IPC3bits.AD1IP=4;					//Set ADC1 Interrupt Priority
	IFS0bits.AD1IF=0;					//Clear ADC1 Interrupt Flag
	IEC0bits.AD1IE=0;					//No ADC1 Interrupt Enable

	AD1CON1bits.ADON=1;					//ADC1 On
	DMA1CONbits.CHEN=1;					//DMA1 On 
}
/******************************************************************************
* Funktion: SetupEmiNk
* Beschreibung: Faktor aus Emi und Nachkal.-Faktor vorberechnen und 
* 				passend dazu beste Einstellung Range 1...7 w�hlen 
* Rechenweg: EmiNk(long) = NkFkt(uint)/Emi(uint (*2^16)
* Eingang:
* 	NkFkt: 	0,8...1,25 x 1000 -> 0x0320...0x04E2
* 	Emi:	0,05...1,2 x 1000) -> 0x0032...0x04B0
* Ausgang: EmiNk (ulong)
* 	max:	1,25 / 0,05 = 25 x 2^16 	= 0x00190000		
*	mid:	1,0 / 1,0  = 1,0 x 2^16 = 0x00010000
* 	min:	0,8 / 1,2  = 0,67 x 2^16 = 0x0000AAAA
******************************************************************************/
void SetupEmiNk(unsigned int ZwInt)
{
	unsigned char c;

	EmiNk=DivLongUD3216(NkFkt*0x10000,ZwInt);	//Faktor ohne Rangeanpassung
// In Assembler wg. Fehler im Debuggmode
//	ZwLong=((long long)EmiNk*RwKalLang[7])/0x10000; 
	ZwLong=Mul3232Div16(EmiNk,RwKalLang[7]);
	for(c=1;c<7;c++){					//Range anpassen
		if(ZwLong>=RwKalLang[c]){
			EmiNk=DivUD6432(ZwLong,RwKalLang[c]);	//Faktor mit Rangeanpassung
			break;
		}
	}	
	Range=c;							//Range an den Portpins 
	Range0=0x01&Range;					//einstellen
	Range1=0x01&(Range/2);
	Range2=0x01&(Range/4);	

	RschAmp=RMASchwelle[Range];
}
/******************************************************************************
* Funktion: main
* Beschreibung: Standard C Hauptroutine 
******************************************************************************/
int main(void) 
{
	CLRWDT();

	SetupPort();						// Ports konfigurieren, Selbsthaltung ein

	Zw2016=0;							// SLR2016 erstmal nicht einschalten
	CntSegDisp=50;						// Wartezeit in ms Ersteinschaltung SLR2016

	Light=1;							// Licht ein PORTD11
//	LichtCount=400;						// Lichttimer = 25ms * 400 = 10 Sek.

	SetupSPI_1();						// SPI1/DMA -> muss vor EEPROM Zugriff
 	SetupEE();							// Ger�teeinstellungen vornehmen
	if(!RCONbits.POR){					// Kein Power-On-Reset ?
		if(RCONbits.BOR)				// dann Fehler-Flag speichern
			ErrBOR=1;
		if(RCONbits.WDTO)
			ErrWDTO=1;
		if(RCONbits.TRAPR)
			ErrTRAPR=1;
		EEpromWriteVerify(EE_ErrStatus,1,&ErrStatus);
	}
	RCON=0;								// Clear Reset-Flags

	SetupSPI_2();						// SPI2
	SetupTimer1();						// f�r Dot Matrix Takt
	SetupDDC114();						// Startup-Sequenz DDC114

	RwDisc=4;							// 4 ersten RW nicht verarbeiten ! 

	SetupLCD();							// Init LCD
	SetupUart();						// RS232 
	SetupADC();							// Interner ADC
	SetupPWM2();						// Summer an OC2

	AVGInit=1;							// AVG Register Initialisieren 
	ContMode=0;
	IntCal=0;							// interner Send- und Speicherstopp aus
	NewPix=0;

// Faktor=4*t90/10*tmess t90=3000ms	tmess=1ms 4*3000/10=1200
	AvgFaktor=1200;						// AVG = 50ms (4*50ms/10*1ms)=200ms/10=20)
// Faktor=4*t90/10*tmess t90=3000ms	tmess=500ms 4*3000/5000=2,4

	SendStatus=0;						// kein Mess/Rohwert vorh.
	RCVBefehl=0;						// kein Befehl empfangen
	mscnt=0;							// keine Messwerte senden
	CntAnalog=0;						// Init int. ADW
	TastenStatus=0;						// Tastenflags l�schen
	CntDispNeu=0;						// Counter und Summenspeicher
	CntDispTemp=0;						// Display Temp alle null
	DispTempSum=0;
	MessdatenInitAdr();					// Speicherzeiger initialisieren

	SpiStartRtc();						// Uhr starten
	SpiReadRtc();						// Uhrdaten lesen
	Cnt1000MSec=0;						
	CntSendeMesswert=0;


// RTC Verstellung per Tastatur
	if(~PORTB&0x0010){					// PAR Taste gedr�ckt ?
		ParRtc=1;						// ja, merken f�r RTC einstellen
		MenuAktuell=MenuRtcYear-1;
		ReqCls=1;
		CntTastenAbfrage=25;			// sofort loslegen
	}
	else{
		KBDNoMenu();					// sonst Men� Init off
		CntDispVer=250;					// SW-Version einblenden. (Ausblnd. bei 100 -> 150*25 ms)
		ReqVer=1;						// Request f�r SW-Version
	}

	while(1) 
	{
		CLRWDT();

//		if(AdwNeu)						// neue Rohwerte ?	 
		asm ("btsc _AdwStatus_,#1");	// neue Rohwerte ? (geht schneller)
			RechMessTemp();				// Messwert berechnen (->MessTemp)						

		if(mscnt>0){					// Mess/Rohwerte senden ?
			if(IEC1bits.U2TXIE==0)		// Schnittstelle frei ?			
				MesswertSenden();		// ja	
		}					
		else if(CntSendeMesswert>4){	// Dauersenden: alle 5ms (Maximum PortaWin, ca. 60 Werte/Sek)
			CntSendeMesswert=0;
			if(IntCal==0&&HoldMode==0&&KaliEin==0){
				if(CntDispNeu>0){		// gibts schon einen Messwert ? 
					XmtOut=1;			// verpackten Messwert senden
					mscnt=1;			// 1 Wert
				}
			}
		}

//		if(RCVBefehl)					// Befehl empfangen ?
		asm ("btsc _ComStatus_,#1");	// (geht schneller)
			COM_Auswertung();			// ja

		if(CntSegDisp<=0){				// im 5ms Intervall
			CntSegDisp=5;				// 7-Segment Display, Dot-Matrix
			SegDisplay();				// aktualisieren
		}

		if(CntAnalog>5){				// ca. alle 100 ms
			CntAnalog=1;				
			if(An0An1){					// abwechselnd
				RechUBat();				// Batteriespannung 
				An0An1=0;
			}
			else{
				RechGerTemp();			// Ger�tetemperatur berechnen
				An0An1=1;
			}
		}

//Messwerte speichern
//1.15 grafische Anzeige fest auf 4 Sek. skaliert
		if(MMNeu){						// neuer Messwert zum speichern ?
			MMNeu=0;
			if(IntCal==0){				// Dauersenden / Speichern aus ?
//a. Intervall
				if(IntTime>0){						// INT > 0, Intervall speichern
					if(ContMode)				// CONT = Dauerspeichern			
						MessdatenSpeichern();		// Messwert speichern
					else if(MenuAktuell==0){ 
						if(!TstDP2){			//2.DP gedr�ckt = Blockspeichern
							KrvLck=1;				//Grafiksperre 1.DP ein
							if(MemBelegt<4000){ //
								MessdatenSpeichern(); //Messwert speichern
							}
							else	
								Beep();				//akkust. Meldung -> Speicher voll
						}
						else if(TstDP1){		// 1. Druckpunkt gedr�ckt
							if(KrvLck==0){			// Grafiksperre aus		
								MessdatenKurve();
							}
						}
					}
				}
//b. Kein Intervall
				else if(MenuAktuell==0){				//normaler Messbetrieb 
					if(!TstDP2&&!OKDP2){		//2.DP gedr�ckt = Einzelwert speichern 
						OKDP2=1;
						KrvLck=1;					//Grafiksperre 1.DP ein
						if(MemBelegt<4000){			
							MessdatenSpeichern();	//Messwert speichern
							if(MBlock==0x8000){		//Sofort auswerten: wurde wirklich gespeichert ?
								MBlock=0x8001;		//Erster Messwert n�chstes Intervall sofort speichern
								MessdatenInitAdr();	//Positionszeiger/MdGes ermitteln
								EEDatenZeigen();	//Speicherdaten zeigen
								MemDsp=1;			//Flag, Einzelwert wurde gespeichert (Display einfrieren)
								SegNeu=1;			//Speicherwert auf Dot Matrix / 7-Segment sofort anzeigen
							}
						}
						else
							Beep();					//akkust. Meldung -> Speicher voll
					}
					else if(TstDP1){			//1.Druckpunkt gedr�ckt
						if(KrvLck==0)				//Grafiksperre		
							MessdatenKurve();		//graf. Ausgabe
					}
				}
			}
		}

		if(CntTastenAbfrage>24)					// alle 25ms
			Tastenabfrage();					// Tasten abfragen,
		else if(CntTastenAbfrage!=CntTastAlt){	// dazwischen jede msec...
			CntTastAlt=CntTastenAbfrage;		// Merkregister f�r erledigt
			if(SaveGrf&&NewPix)					// graf. Anzeige: neue Werte vorh. ? 
				MessdatenSpalteAusgeben();		// ja, Laufzeit-Grafikausgabe		 
		}		
		
		if(LcdStatus||LcdRequest)		// ist was auf LCD auszugeben ?
			LcdDisplay();				// ja

//		if(DDCInit)						// Neu-Ini DDC114 ?
		asm ("btsc _AuxStatus_,#3");	// (das selbe, aber schneller)
			SetupDDC114();				// Startup-Sequenz DDC114

		if(SwReset){
			if(U2STAbits.TRMT&&!RCVBefehl&&!XmtCnt&&!mscnt){
				SwReset=0;
				SetupEE();
				DDCInit=1;
				if(SendOK){
					SendeOk();
					SendOK=0;
				}
			}
		}
	}
}

/******************************************************************************
* Funktion: MessdatenVerpacken
* Beschreibung: Daten verpacken zum Speichern im EEPROM bzw. senden  
* Eingang: 	Messdaten: MANTemp,Emi,IntTime,Save_C
* 			Uhrdaten: RtcSek,RtcMin,RtcStd,RtcTag,RtcMon,RtcJahr
*			
*			Modus: 0=Normal, 1=MAX, 2=AVG
* Ausgang: 	EE_Dat[0]...EE_Dat[3] = 4x16 Bit
* Zeitbadarf: 43 IC
* 
* |--------|--------|-----------|--------|---------|---------|---------|---------|
* |Word 0  			|Word 1     		 |Word 2   			 |Word 3    		 |
* |--------+--------+-----------+--------+---------+---------+---------+---------|
* |76543210|76543210|76|543|2|10|76543210|765432|10|7654|3210|76543|210|76|543210|
* |--------+--------+--|---|-|--+--------+------|--+----|----+-----|---+--|------|
* |	  MessTemp 	    |M |Int| |	Emi      |      | 	    |	 |	   |   	  |	     |
* |	 		 	    |O | T |C| 			 |sek   |min	|mon |std  |tag   |jahr  |
* |	 Hi	   	 Lo	    |D | I |/|Hi  	Lo   |  	| 	    |	 |	   |      |	     |
* |		   		    |U | M |F|           | 	    |	    |	 |	   |	  |	     |
* |		   		    |S | E | |  		 |	    |	    |	 |	   |	  |	     |
* |--------+--------|--|---|-|--|--------+------|--+----|----|-----|------|------|
******************************************************************************/
void MessdatenVerpacken(void)
{
	EE_Dat[0]=MANTemp;
	EE_Dat[1]=(SaveMax+2*SaveAVG)*0x4000+IntTime*0x800+SaveCF*0x400+Emi;	
	EE_Dat[2]=RtcSek*0x400+RtcMin*0x10+RtcMon;
	EE_Dat[3]=RtcStd*0x800+RtcTag*0x40+RtcJahr;
}	
/******************************************************************************
* Funktion: MessdatenEntpacken
* Beschreibung: Entpacken eines EEPROM-Datensatzes 
* Eingang: EE_Dat[0]...EE_Dat[3] (4 Wort Daten)
* Ausgang: Gespeicherte Daten in Stxxx  
* Zeitbedarf: 49 IC
******************************************************************************/
void MessdatenEntpacken(void)
{
	StTemp=EE_Dat[0];					//zw.-sp. MessTemp
	StModus=0x03&(EE_Dat[1]/0x4000);
	StIntTime=0x07&(EE_Dat[1]/0x800);
	StCF=0x01&(EE_Dat[1]/0x400); 
	StEmi=0x03FF&EE_Dat[1];
	StSek=0x3F&(EE_Dat[2]/0x400);	
	StMin=0x3F&(EE_Dat[2]/0x10);	
	StMon=0x0F&EE_Dat[2];	
	StStd=0x1F&(EE_Dat[3]/0x800);	
	StTag=0x1F&(EE_Dat[3]/0x40);	
	StJahr=0x3F&EE_Dat[3];	
}
/******************************************************************************
* Funktion: MessdatenSkala128Init
* Beschreibung: Initialisierung der Register f�r Grafikausgabe  
* Skalierung 128 Pixel auf X-Achse (Zeit) und 48 Pixel auf Y-Achse (Temperatur)
******************************************************************************/
void MessdatenSkala128Init(void)
{
	YCnt=0;							// Startposition Skala48
	MdGes=0;
	TAvgSumPix=0;
	TAvgCntPix=0;
	TMaxPix=0;						// Int Max / Pixel
	TMinPix=0xFFFF;					// Int Min / Pixel
	XBufCnt=0;
	TMinGes=MANTemp;
	TMaxGes=MANTemp;

//1.15 MdCnt fest auf 4000
	MdCnt=4000;						// Skalierung 4000 Werte = 4 Sek			

//	switch(IntTime){				// Skalierung auf X-Achse je nach Intervall
//		case 0: case 1:			 	// INT OFF, 1ms: 4000 Werte = 4 Sek
//			MdCnt=4000;				
//			break;
//		case 2:						// INT 20 ms = 4 Sek
//			MdCnt=200;				
//			break;
//		case 3:						// 100 ms = 12,8 Sek
//			MdCnt=128;				
//			break;
//		case 4:						// 1 Sek = 128 Sek
//			MdCnt=128;				
//			break;
//		case 5:						// 10 Sek = 21 Min 20 Sek
//			MdCnt=128;				
//			break;
//		case 6:						// 100 Sek = 3 Std 33 Min 20 Sek
//			MdCnt=128;				
//			break;
//		case 7:						// 500 Sek = 17 Std 46 Min 40 Sek
//			MdCnt=128;				
//			break;
//	}

	ZwLong=DivUW1616(0x8000,MdCnt);	// X-Pixelanteil (128 x 2e8) pro Messwert
	QGanz=ZwLong;					// ganzzahliger Anteil
	QRest=ZwLong/0x10000;			// Rest
	QGanzGes=QRestGes=0;
}
/******************************************************************************
* Funktion: MessdatenSkala128
* Beschreibung: Messwert skalieren und puffern
* Eingang:	MdGes		Anzahl einger. Messwerte (1...4000)
*			MANTemp		Messwert
* Ausgang:	Messwerte in TAvgBuf[]
******************************************************************************/
void MessdatenSkala128(void)
{
	if(MdGes<4000)
		MdGes++;

	TAvgSumPix+=MANTemp;
	TAvgCntPix++;
	if(MANTemp>TMaxPix)
		TMaxPix=MANTemp;
	if(MANTemp<TMinPix)
		TMinPix=MANTemp;
	QGanzGes+=QGanz;					// Pixelanteil des Messwertes in QGanzGes aufsummieren
	QRestGes+=QRest;
	if(QRestGes>=MdCnt){				// falls Nachkommaanteil >= 1 (=> Anzahl Messwerte)
		QGanzGes++;						// 		zum ganzzahliger Anteil  1 addieren
		QRestGes-=MdCnt;				//  	vom Nachkommaanteil Anzahl Messwerte subtrahiern
	}
	if(QGanzGes>=256){					// Pixel ist fertig:
		QGanzGes-=256;
		TAvgPix=DivUD3216(TAvgSumPix,TAvgCntPix);
		TAvgBuf[XBufCnt]=TAvgPix;		// Messwert puffern
		TMaxBuf[XBufCnt]=TMaxPix;
		TMinBuf[XBufCnt]=TMinPix;
		if(++XBufCnt==128){				// Buffer voll ? 
			XBufCnt=0;

//1.15 MdCnt fest auf 4000			
//			if(MdCnt<4000){				// Skalierung erh�hen
//				MdCnt*=2;
//				if(MdCnt>4000)			// maximal 4000 Werte
//					MdCnt=4000;			
//				ZwLong=DivUW1616(0x8000,MdCnt);		
//				QGanz=ZwLong;			// Skalierungsz�hler berechnen			
//				QRest=ZwLong/0x10000;				
//			}
		
		}

		TAvgCntPix=0;					// Init n�chstes Pixel
		TAvgSumPix=0;
		TMaxPix=0;					// Int Max / Pixel
		TMinPix=0xFFFF;				// Int Min / Pixel
		NewPix=1;
	}
}
/******************************************************************************
* Funktion: MessdatenLadeSpalte
* Beschreibung: Eine Spalte (0...127) in Grafikbuffer laden 
* Der Grafikbuffer umfasst 48 Zeilen mal 128 Spalten und besteht aus
* den drei Buffern PixMinBuf, PixMedBuf, PixMaxBuf f�r den oberen, mittleren
* und unteren Grafikbereich.
* Eingang:	YCnt			aktuelle Spalte im Grafikbuffer (0...127)
* 			XBufPos			aktuelle Spalte im Temperaturbuffer TAvgBuf (0...127)
*			TAvgBuf[]		Temperaturbuffer	
*			LoPix,HiPix		Zwischen diesen Pixel wird Verbindungslinie gezeichnet 
*			SkaFkt48		Skalierungsfaktor Grad/Pixel
*			TLimUG,TLimOG	untere/obere Skalentemperatur
*			YMin,YAvg,YMax	Pixelpositionen (0...47) f�r MIN,AVG,MAX Anzeige
* Ausgang:  PixMinBuf[YCnt]	Grafikbuffer Page 5,6
*			PixMedBuf[YCnt]	Grafikbuffer Page 3,4
*			PixMaxBuf[YCnt] Grafikbuffer Page 1,2 
******************************************************************************/
void MessdatenLadeSpalte(void)
{
	ZwInt=TAvgBuf[XBufPos];

	if((ZwInt>0)&&(ZwInt<=TLimOG)&&(ZwInt>=TLimUG)){	// Messwerte vorhanden und innerhalb Skalierung
														// 		
		LoPixAlt=LoPix;
		HiPixAlt=HiPix;

		LoPix=(SkaFkt48*(ZwInt-TLimUG))/0x1000000;

		HiPix=LoPix;

		if(YCnt==0)
			LoPixAlt=LoPix;
		if(LoPix<LoPixAlt-1)
			HiPix=LoPixAlt-1;	
		else if(LoPix>LoPixAlt+1)
			HiPix=LoPixAlt+1;	

//Pixeldaten f�r eine Spalte in Pixelbuffer eintragen
		Pixelspalte(LoPix,HiPix,&PixMinBuf[YCnt],&PixMedBuf[YCnt],&PixMaxBuf[YCnt]);

	}
	else{
		PixMinBuf[YCnt]=0;			// keine Messwerte vorhanden
		PixMedBuf[YCnt]=0;
		PixMaxBuf[YCnt]=0;
	}
//Hilfslinien + numerische Temperaturwerte zeichnen
	MMASpalte(YMin,YAvg,YMax,YCnt,&PixMinBuf[YCnt],&LineBuf[0]);
}
/******************************************************************************
* Funktion: MessdatenSpalteAusgeben
* Beschreibung: Echtzeitgrafikausgabe, eine Spalte ausgeben
* Es wird spaltenweise ausgegeben (1 Spalte = 48 Pixel auf Y-Achse) 
* Pro Grafik 128 Spalten => 128 Durchl�ufe 
*
* Eingang:	XBufCnt		aktuelle Position auf X-Achse (0...127), = �ltester Messwert
* 			TMaxGes		bisherige Max-Temp
*			TMinGes		bisherige Min-Temp
* Ausgang:  PixMinBuf[128], PixMedBuf[128], PixMaxBuf[128]
******************************************************************************/
void MessdatenSpalteAusgeben(void)
{
	if(YCnt==0){						// Pixelgrafik initialisieren	
		if(TAvgBuf[127]==0)				// Buffer voll ?
			XBufPos=YCnt;				// nein, Startwert = 0 
		else
			XBufPos=XBufCnt;			// ja, Startwert = �lteste Position 

		MessdatenSkala48Grenzen();		// Skalierung Y-Achse
	}		
	
	XSpalte=0;							// keine zus�tzl. Markierungslinie 
	MessdatenLadeSpalte();

	if(++XBufPos==128)
		XBufPos=0;
	if(++YCnt==128){					// 128 Spalten fertig ?
		YCnt=0;
		NewPix=0;						// Pixel ist fertig
		ReqGrf=1;						// Flag Grafikausgabe
	}
}
/******************************************************************************
* Funktion: MessdatenSkala48Grenzen
* Beschreibung: Skalierung der Gesamtgrafik auf Y-Achse (48 Pixel) 
* 				obere und untere Skalentemperatur festlegen
*				MaxTemp - MinTemp = Grad pro Pixel  
* Diese Funktion wird 	a) von der Echtzeitanzeige
* 						b) von der Speicheranzeige
* einmal bei Spalte 0 aufgerufen.	
*
* Eingang: 	TMaxGes		max. Temperaturwert der Kurve in 1/10 Grad
*			TMinGes		min. Temperaturwert der Kurve in 1/10 Grad
*			GrfMinSkal	minimale Skalierung Y-Achse in 1/10 Grad
*
* Ausgang:	TLimOG		obere Skalentemperatur
*			TLimUG		untere Skalentemperatur
*			SkaFkt48	Skalierungsfaktor Grad/Pixel
******************************************************************************/
void MessdatenSkala48Grenzen(void)
{
	MinMaxAvg(&TAvgBuf[0]);			// TAvgGes,TMaxGes,TMinGes ermitteln

	if(TAvgGes>0){					// enth�lt TAvgBuf Eintr�ge ?
		TLimOG=TMaxGes;
		TLimUG=TMinGes;
		if(TLimOG-TLimUG<GrfMinSkal){				// Mindest-Temperaturbereich Y-Achse	
			TLimOG=(TMaxGes+TMinGes+GrfMinSkal)/2;
			if(TLimOG<GrfMinSkal)
				TLimOG=GrfMinSkal;
			TLimUG=TLimOG-GrfMinSkal;
		}

		TLimDif=TLimOG-TLimUG+1;
		SkaFkt48=DivLongUD3216(0x30000000,TLimDif);	// 0x3000000 = 48 x 2 hoch 24
		HiPix=47;
		LoPix=0;

//Hilfslinien und num. Temperatur f�r Min/AVG/Max 	
		YMin=(SkaFkt48*(TMinGes-TLimUG))/0x1000000;	// Positionen (0...47)
		YMax=(SkaFkt48*(TMaxGes-TLimUG))/0x1000000;	// f�r die 3 Werte
		YAvg=(SkaFkt48*(TAvgGes-TLimUG))/0x1000000;	// berechnen
		MMAPositionen();							// evtl. �berl�ufe korrigieren
	}
	else{							// keine Daten	
		YMin=4;						// Oberkante f�r Leerzeichen auf 4 (0...4)
		YMax=4;
		YAvg=4;		
	}

	HexAsc5(TMinGes,&LineBuf[0]);
	HexAsc5(TAvgGes,&LineBuf[5]);
	HexAsc5(TMaxGes,&LineBuf[10]);
	if(LineBuf[0]=='0'){
		LineBuf[0]=' ';
		if(LineBuf[1]=='0'){
			LineBuf[1]=' ';
			if(LineBuf[2]=='0'){
				LineBuf[2]=' ';
				if(LineBuf[3]=='0'){
					LineBuf[3]=' ';
					if(LineBuf[4]=='0'){
						LineBuf[4]=' ';
					}
				}
			}
		}
	}
	if(LineBuf[5]=='0'){
		LineBuf[5]=' ';
		if(LineBuf[6]=='0'){
			LineBuf[6]=' ';
			if(LineBuf[7]=='0'){
				LineBuf[7]=' ';
				if(LineBuf[8]=='0'){
					LineBuf[8]=' ';
					if(LineBuf[9]=='0'){
						LineBuf[9]=' ';
					}
				}
			}
		}
	}
	LineBuf[15]=16;							//Dezimalpunkt erstmal setzen

	if(LineBuf[10]=='0'){
		LineBuf[10]=' ';
		if(LineBuf[11]=='0'){
			LineBuf[11]=' ';
			if(LineBuf[12]=='0'){
				LineBuf[12]=' ';
				if(LineBuf[13]=='0'){
					LineBuf[13]=' ';
					if(LineBuf[14]=='0'){
						LineBuf[14]=' ';
						LineBuf[15]=0;		//Dezimalpunkt l�schen
					}
				}
			}
		}
	}
}
/******************************************************************************
* Funktion: MessdatenBlockPlus
* Beschreibung: aktuellen Block inkrementieren (Men�punkt MEM, Taste Up)
* Eingang: CurMemBlck,CurMemCell
* Ausgang: CurMemBlck,CurMemCell,MdStart,MdEnd,MdCur
******************************************************************************/
void MessdatenBlockPlus(void)
{
	unsigned int Cnt,Adr;
	
	MdStart=MdEnd+1;
	if(MdStart>3999)
		MdStart-=4000;
	MdEnd=MdStart;
	MdCur=MdStart;
	if(MemStart>MdStart)
		Cnt=4000+MdStart-MemStart;
	else
		Cnt=MdStart-MemStart;

	CurMemCell=Cnt+1;					// akt. Zelle auf 1. Wert im Block setzen
	CurMemBlck++;

	while(++Cnt<MemBelegt){
		Adr=Cnt+MemStart;
		if(Adr>3999)
			Adr-=4000;
		EEpromRead(Adr*8,1,&EE_Dat[0]);
		if(EE_Dat[0]<0x8000)
			break;
		MdEnd=Adr;
	}
	MessdatenBlockAnzahl();
}
/******************************************************************************
* Funktion: MessdatenBlockMinus
* Beschreibung: aktuellen Block dekrementieren (Men�punkt MEM, Taste Down)
* Eingang: CurMemBlck,CurMemCell
* Ausgang: CurMemBlck,CurMemCell,MdStart,MdEnd,MdCur
******************************************************************************/
void MessdatenBlockMinus(void)
{
	unsigned int Cnt;

	if(MemStart>MdStart)
		Cnt=4000+MdStart-MemStart;
	else
		Cnt=MdStart-MemStart;
	MdEnd=MdStart;
	if(MdEnd--==0)
		MdEnd=3999;
	MdCur=MdEnd;						// akt. Zelle auf 
	CurMemCell=Cnt;						// letzten Wert im Block setzen
	CurMemBlck--;
	while(Cnt-->0){
		MdStart=Cnt+MemStart;
		if(MdStart>3999)
			MdStart-=4000;
		EEpromRead(MdStart*8,1,&EE_Dat[0]);
		if(EE_Dat[0]<0x8000)
			break;
	}
	MessdatenBlockAnzahl();
}
/******************************************************************************
* Funktion: MessdatenZellePlus
* Beschreibung: aktuelle Speicherzelle inkrementieren
* Eingang: CurMemBlck,CurMemCell
* Ausgang: CurMemBlck,CurMemCell,MdStart,MdEnd,MdCur
******************************************************************************/
void MessdatenZellePlus(void)
{
	unsigned int Cnt,Adr;

	Adr=MdCur;
	if(++MdCur>3999)
		MdCur-=4000;
	if(Adr==MdEnd){
		MdStart=MdCur;
		MdEnd=MdCur;
		CurMemBlck++;
		Cnt=CurMemCell;
		while(++Cnt<MemBelegt){
			Adr=Cnt+MemStart;
			if(Adr>3999)
				Adr-=4000;
			EEpromRead(Adr*8,1,&EE_Dat[0]);
			if(EE_Dat[0]<0x8000)
				break;			
			MdEnd=Adr;
		}
	}
	CurMemCell++;
	MessdatenBlockAnzahl();
}
/******************************************************************************
* Funktion: MessdatenZelleMinus
* Beschreibung: aktuelle Speicherzelle dekrementieren
* Eingang: CurMemBlck,CurMemCell
* Ausgang: CurMemBlck,CurMemCell,MdStart,MdEnd,MdCur
******************************************************************************/
void MessdatenZelleMinus(void)
{
	unsigned int Cnt,Adr;

	CurMemCell--;

	Adr=MdCur;
	if(MdCur--==0)
		MdCur=3999;
	if(Adr==MdStart){
		MdStart=MdCur;
		MdEnd=MdCur;
		CurMemBlck--;
		Cnt=CurMemCell;
		while(Cnt-->0){
			Adr=Cnt+MemStart;
			if(Adr>3999)
				Adr-=4000;
			MdStart=Adr;
			EEpromRead(Adr*8,1,&EE_Dat[0]);
			if(EE_Dat[0]<0x8000)
				break;			
		}
	}
	MessdatenBlockAnzahl();
}
/******************************************************************************
* Funktion: MessdatenBlockAnzahl
* Beschreibung: Anzahl DS des akt. Blocks feststellen
* Eingang: 		MdStart,MdEnd (Blockstart, Blockende)
* Ausgang: 		MdGes (Anzahl DS Block)
******************************************************************************/
void MessdatenBlockAnzahl(void)
{
	if(MdEnd<MdStart)
		MdGes=4001-MdStart+MdEnd;				
	else
		MdGes=MdEnd-MdStart+1;
}
/******************************************************************************
* Funktion: MessdatenInitAdr
* Beschreibung: Aus den drei im EEPROM gespeicherten Werten
*				MemStart,MemBelegt,CurMemCell, die Positionszeiger zum aktuell
*				anzuzeigendem Block ermitteln.  
*
* Eingang: 	MemStart	\Werte 		EE-Adresse �ltester Wert 
*			MemBelegt	 |aus		Anzahl belegter EE-Zellen
*			CurMemCell	/EEPROM		aktuell ausgew�hlte EE-Zelle (1...4000)
*
* Ausgang:	CurMemBlck	\ diese Werte werden
*			AnzMemBlck	 \ zur 
*			MdStart		  \ Laufzeit
*			MdEnd		  / nur im RAM
*			MdCur		 / gehalten
*			MdGes		/
******************************************************************************/
void MessdatenInitAdr(void)
{
	unsigned int Cnt,Adr;
	unsigned char Flag;	

	Cnt=0;
	Flag=0;
	
	MdStart=MemStart;
	MdEnd=MemStart;
	MdCur=MemStart;
	if(MemBelegt==0){					// Init keine DS vorh.
		AnzMemBlck=0;
		CurMemBlck=0;	
		CurMemCell=0;	
	}
	else{
		AnzMemBlck=1;					// Init f�r ersten DS
		CurMemBlck=1;	
		if(CurMemCell==0)
			CurMemCell=1;
		if(CurMemCell>MemBelegt)
			CurMemCell=MemBelegt;
		if(CurMemCell==1)
			Flag=1;
		while(++Cnt<MemBelegt){			// Init ab zwei DS 
			Adr=Cnt+MemStart;
			if(Adr>3999)
				Adr-=4000;
			EEpromRead(Adr*8,1,&EE_Dat[0]);
			if(EE_Dat[0]<0x8000){
				AnzMemBlck++;	
				if(Flag==0)
					MdStart=Adr;
				else
					Flag=2;
			}
			if(Cnt+1==CurMemCell){
				CurMemBlck=AnzMemBlck;
				MdCur=Adr;
				Flag=1;
			}
			if(Flag==1)
				MdEnd=Adr;
		}
	}
	MessdatenBlockAnzahl();
}
/******************************************************************************
* Funktion: MessdatenLoeschen
* Beschreibung: blockweises Verschieben (�berschreiben) von DS 
* Eingang: 	SchreibAdr	erster zu loeschender DS (1....4000)
* 			LeseAdr 	erster zu verschiebender DS (1....4000)
*			MemStart	0...3999
*			MemBelegt	0...4000
* Ausgang:	falls letzter belegter Block gel�scht wird: CurMemBlck - 1
******************************************************************************/
void MessdatenLoeschen(unsigned int SchreibAdr,unsigned int LeseAdr)
{
	unsigned int EndAdr,Cnt,CntGes;

	LeseAdr++;
	if(LeseAdr>3999)
		LeseAdr-=4000;
	EndAdr=MemStart+MemBelegt-1;		
	if(EndAdr>3999)
		EndAdr-=4000;
	if(EndAdr<LeseAdr)
		CntGes=4001-LeseAdr+EndAdr;				
	else
		CntGes=EndAdr-LeseAdr+1;				
	if(CntGes==4000)
		CntGes=0;

	if(LeseAdr<SchreibAdr)
		MemBelegt-=4000-SchreibAdr+LeseAdr;				
	else
		MemBelegt-=LeseAdr-SchreibAdr;

	while(CntGes>0){
		Cnt=CntGes;
		if(LeseAdr+Cnt>4000)
			Cnt=4000-LeseAdr;
		if(SchreibAdr+Cnt>4000)
			Cnt=4000-SchreibAdr;
		if(Cnt>mdmax)
			Cnt=mdmax;

		EEpromRead(LeseAdr*8,Cnt*4,&EE_Dat[0]);
		if(SchreibAdr==MdStart)		
			EE_Dat[0]&=0x7FFF;				// Ersten DS des Blocks markieren
	
		EEpromWrite(SchreibAdr*8,Cnt*4,&EE_Dat[0]);			

		LeseAdr+=Cnt;
		if(LeseAdr>3999)
			LeseAdr-=4000;
		SchreibAdr+=Cnt;
		if(SchreibAdr>3999)
			SchreibAdr-=4000;
		CntGes-=Cnt;
	}	
}
/******************************************************************************
* Funktionen: Beep
* Beschreibung: kurze akkustische Meldung 
* Duration=
******************************************************************************/
void Beep(void)
{
	if(T4CONbits.TON==0){		// l�uft Summer noch ? 
		PR4=5000;				// nein dann einschalten (PR4=Tondauer) 
		OC2CON=0x000B;			// OC2 ein -> Continuous Pulse mode
		T4CONbits.TON=1; 		// Timer4 (Tondauer) ein
	}				
}


/******************************************************************************
* Funktionen: MessdatenKurve
* Beschreibung: Grafische Darstellung 1. DP Hauptschalter (ohne Speichern)
******************************************************************************/
void MessdatenKurve(void)
{
//	CntIntMs=0;								// Clear Intervall Z�hler

	if(MBlock!=0x1000){						// Neue Sequenz
		if(MANTemp>MBUvl&&MANTemp<MBOvl){	// 1. Wert muss innerhalb MB liegen
			MBlock=0x1000; 
			MessdatenSkala128Init();		
			InitGrfBuf(&TAvgBuf[0]);		
			CntMsec=0;						// Zeit-Counter
		}
		else
			return;
	}
	else{
//		if(IntTime!=0)
//			CntMsec+=IntMs[IntTime]+1;	// Intervall > 0	
//		else
			CntMsec++;					// Intervall 0 = INT 1 ms
	}
	MessdatenSkala128();				// Pixelgrafik aktualisieren
	ReqGrX=1;							//untere Statuszeile

}
/******************************************************************************
* Funktionen: MessdatenSpeichern
* Beschreibung: Datens�tze in EEPROM schreiben
* Eingang: 	MemStart	Startadresse Speicher (0...3999)	
*			MemBelegt	Anzahl Gespeicherte DS im EEPROM (0...4000)
*			4 Word gepackt ( -> MessdatenVerpacken)
* Ausgang:	MemBelegt			
*			CurMemCell  akt. Speicherzelle 1...4000
*
******************************************************************************/
void MessdatenSpeichern(void)
{
	if(CntIntMs>IntMs[IntTime]||MBlock!=0x8000){	

		MessdatenVerpacken();
	
		if(MBlock!=0x8000){						//Neue Speichersequenz:
//1.20 	�nderung: im Einzelspeichermodus (IntTime=0) alle Temp-Werte speichern (auch MBUvl, MBOvl) 		
			if((IntTime==0)||(MANTemp>MBUvl&&MANTemp<MBOvl)){	//1. Wert muss gr�sser MBA und kleiner MBA sein 			
				MBlock=0x8000;					//Blockkennung Bit 15
				MessdatenSkala128Init();
				InitGrfBuf(&TAvgBuf[0]);
				CntIntMs=0;							//Clear Intervall Z�hler
				CntMsec=0;						//Zeitmessung
			}
			else
				return;
		}
		else{
			EE_Dat[0]|=MBlock;					//Blockkennung Bit 15
			CntIntMs=0;							//Clear Intervall Z�hler

//		CntMsec+=IntMs[IntTime]+1;	

		}
	
		MdCur=MemStart+MemBelegt;				//EEPROM Adr. (0...3999)
		if(MdCur>3999)
			MdCur-=4000;

		EEpromWrite(MdCur*8,4,&EE_Dat[0]);

		if(++MemBelegt>4000){
			MemBelegt=4000;
			if(++MemStart>3999)
				MemStart=0;
		}
		CurMemCell=MemBelegt;				
		CntDispMem=80;					//Anzeige der Zellenadr. f�r 2 Sek
		MemTempMerk=MANTemp;			//Messwert f�r 7-Seg./DotMatrix merken
	}

	CntMsec++;	
	MessdatenSkala128();				//Pixelgrafik aktualisieren
	ReqGrX=1;							//untere Statuszeile aktualisieren

}
/******************************************************************************
* Funktion: EEDatenZeigen
* Beschreibung: Vorbereiten zur Anzeige gespeicherter Messdaten. Die Daten werden 
* aus dem EEPROM in die entspr. Buffer geladen (grafisch/numerisch). 
* Diese Prozedur wird normalerweise im Men� 'MEM' aufgerufen, aber auch zur
* (grafischen) Anzeige direkt nach einer Speichersequenz im Messbetrieb.  
*
* Grafische Darstellung:
* Bei 128 Messwerten wird pro Pixel 1 Messwert angezeigt.
* Ist die Anzahl der Messwerte > 128, werden mehrere Werte je Pixel angezeigt.
* Ist die Anzahl der Messwerte < 128, werden mehrere Pixel je Messwert angezeigt.
* Die Berechnung der Werte je Pixel erfolgt durch Restwertsummenbildung. 
* 
* Die Min/Max/Avg Buffer werden zun�chst mit den 128 Messwerten vorgeladen. (X-Achse)
* Anschliessend erfolgt eine Skalierung auf 48 Pixel (Y-Achse)
*
* Eingang:		CurMemBlck:		aktuell gew�hlter Block Messdaten EEPROM 	 
*
* Ausgang:		PixMinBuf[0...127]	Pixeldaten Page 1,2  -> Pixel 32...47
*				PixMedBuf[0...127]  Pixeldaten Page 3,4  -> Pixel 16...31
*				PixMaxBuf[0...127]  Pixeldaten Page 5,6  -> Pixel  0...15
******************************************************************************/
void EEDatenZeigen(void)
{
	unsigned int Cnt,Adr;

	if(SaveGrf==0&&MenuAktuell==MenuMem){	// Num. Anzeige, Men� 'MEM' 
		ReqTmp=1;	
		if(MemBelegt>0){					// Daten sind vorhanden
			EEpromRead(MdCur*8,4,&EE_Dat[0]);	// akt. DS lesen
			EE_Dat[0]&=0x7FFF;					// Messtemp Bit 15 ausblenden
			MessdatenEntpacken(); 
		}
	}
	else{								// Grafische Anzeige						
		NewPix=0;						// lfd. Grafikausgabe beenden
		MBlock=0;						// Grafik im RAM wird �berschrieben
		ReqGrZ=1;						// Anforderung obere Statuszeile anzeigen
		ReqGrX=1;						// Anforderung untere Statuszeile anzeigen
		if(MemBelegt>0){				// Daten vorh. ?
			GrfOn=1;						// Grafikpuffer wird �berschrieben ! 
			ReqGrf=1;						// Anforderung Grafik anzeigen
//aktuellen DS im Block lesen wg. Anzeige Temp/Modus/CF/Emi in Statuszeile 
//diese Einstellungen k�nnen w�hrend Speichersequenz (theoretisch) ge�ndert werden !
			EEpromRead(MdCur*8,4,&EE_Dat[0]);	// Aktuellen DS komplett lesen
			EE_Dat[0]&=0x7FFF;				// Bit 15 (Blockkennung) l�schen
			MessdatenEntpacken(); 				

			ZwLong=DivUW1616(0x8000,MdGes);	// X-Pixelanteil (128 x 2e8) pro Messwert
			QGanz=ZwLong;					// ganzzahliger Anteil
			QRest=ZwLong/0x10000;			// Rest
			QGanzGes=QRestGes=0;
			TAvgSumPix=0;					// Temperatur-Summe / Pixel 
			TAvgCntPix=0;					// Messwertecounter / Pixel
			TMaxPix=0;						// Maximalwert / Pixel
			TMinPix=0xFFFF;					// Minimalwert / Pixel
			XBufCnt=0;
		
	// Skalierung X-Achse
	// TAvgBuf laden. Anzahl Messwerte / 128 Pixel = Messwerte pro Pixel  
	
			CntMsec=0;						// Gesamtmesszeit in msec	
			XSpalte=0;						// Spaltenposition akt. Temp. in Grafik 

			for(Cnt=0;Cnt<MdGes;Cnt++){			// Anzahl Messwerte im Block 
				Adr=MdStart+Cnt;
				if(Adr>3999)
					Adr-=4000;
				EEpromRead(Adr*8,2,&EE_Dat[0]);	// n�chste DS Messwert + IntTime lesen			
				EE_Dat[0]&=0x7FFF;				// Bit 15 (Blockkennung) l�schen
				StIntTime=0x07&(EE_Dat[1]/0x800);
				CntMsec+=IntMs[StIntTime]+1;	// Gesamtdauer 	

				if(Adr==MdCur){					// hor. Linie markiert Messwert im Block 
					if(MdGes>1)					// f�r Einzelwerte keine Linie anzeigen
						XSpalte=0x8000+XBufCnt;	// XSpalte Bit 15 = MerkFlag
				}
				
				TAvgSumPix+=EE_Dat[0];			
				TAvgCntPix++;					// Messwertz�hler Pixel + 1
				if(EE_Dat[0]>TMaxPix)
					TMaxPix=EE_Dat[0];
				if(EE_Dat[0]<TMinPix)
					TMinPix=EE_Dat[0];
				QGanzGes+=QGanz;				// Pixelanteil des Messwertes in QGanzGes aufsummieren
				QRestGes+=QRest;
				if(QRestGes>=MdGes){			// falls Nachkommaanteil >= 1 (=> Anzahl Messwerte)
					QGanzGes++;					// ganzzahliger Anteil  1 addieren
					QRestGes-=MdGes;			// Nachkommaanteil - Anzahl Messwerte
				}
//Verteilung der Messwerte auf 128 Pixel
//Messwerte werden 'gestreckt' bei < 128 Pixel
				while(QGanzGes>=256){			// Pixel ist fertig
					QGanzGes-=256;	
					TAvgPix=DivUD3216(TAvgSumPix,TAvgCntPix);
					TMaxBuf[XBufCnt]=TMaxPix;
					TMinBuf[XBufCnt]=TMinPix;
					TAvgBuf[XBufCnt++]=TAvgPix;	// Messwert puffern
					if(QGanzGes<256){			// Messwert ist fertig
						if(XSpalte>=0x8000)		//Merkflag gesetzt ?
							XSpalte=(XSpalte+XBufCnt)/2;	
						TAvgCntPix=0;			// Wertespeicher f�r n�chsten
						TAvgSumPix=0;			// Messwert initialisieren
						TMaxPix=0;				// Int Max / Pixel
						TMinPix=0xFFFF;			// Int Min / Pixel
					}
				}
			}
			
			CntMsec-=IntMs[StIntTime]+1;		// letzten Wert Intervall nicht mitz�hlen 

			MessdatenSkala48Grenzen();			// Skalierung Y-Achse

			for(YCnt=0;YCnt<128;YCnt++){
				XBufPos=YCnt;
				MessdatenLadeSpalte();
			}
		}
		else{								// keine Daten vorh.
			ReqCls=1;						// leeres Display zeigen
		}
	}
}
/******************************************************************************
* Funktion: RwMittelung
* Beschreibung: Rohwerte der langen Ti addaptiv mitteln 
* Abh�ngig von der Gr�sse des Eingangsrohwertes wird die Mittelungsstufe	
* ermittelt (max. 80). Bei Spr�ngen erfolgt keine Mittelung.
*
* Eingang: 	RwLang		Rohwert	
*			RMAMax		Startschwelle Mittelung
*			RSTMax		Maximalstufe (<=80)		
* Ausgang: 	RwLang
******************************************************************************/
void RwMittelung(void)
{
	signed int RNeu;					// RNeu = SIGNED INT (min: -32768, max +32767)

	if(RwLang>RMAMax)			// Rohwert > Max, keine Mittelung 
		RMAIni=1;						// Neu Ini nach n�chstem unterschreiten
	else{
		RNeu=RwLang;					// <- RwLang=LONG
		if(RMAIni){
			RMAInitialisieren(RNeu,&RMABuf80[0]);
			RMAIni=0;
			RMASumCnt=1;
			RMASumme=RNeu;
			RMAPtr=0;
		}
		else{
			RMAStufe=RMARechStufe(RNeu,RMAMax,RSTMax); //Stufe abh�ngig vom Rohwert
			if(++RMAPtr>79)
				RMAPtr=0;
			RMASumme+=RNeu;					//neuen Wert zur Summe addieren
			if(++RMASumCnt>RMAStufe){		//muss �ltester Wert abgezogen werden ?
				RMASumCnt--;				//ja: Summenz�hler - 1
				RMASgnPtr=RMAPtr-RMASumCnt; //Adresse des �ltesten Werts berechnen
				if(RMASgnPtr<0)
					RMASgnPtr+=80;
				RMASumme-=RMABuf80[RMASgnPtr]; //Wert von Summe abziehen
				if(RMASumCnt>RMAStufe){		//zweit�ltesten Wert auch abziehen ?
					RMASumCnt--;			//ja: Summenz�hler - 1
					if(++RMASgnPtr>79)		//Adresse des zweit�ltesten Werts berechnen
						RMASgnPtr-=80;
					RMASumme-=RMABuf80[RMASgnPtr];	//Wert von Summe abziehen
				}			
			}
	
	//Summe Sprungmittelung, um 16 Werte verz�gert aktualisieren
			RMASum64-=RMABuf80[RMAPtr];
			RMASgnPtr=RMAPtr-16;
			if(RMASgnPtr<0)
				RMASgnPtr+=80;
			RMASum64+=RMABuf80[RMASgnPtr];
			
	//Ringbuffer aktualisieren
			RMABuf80[RMAPtr]=RNeu;
	
			RMAAvg64=RMASum64/64;	

			if(RNeu<RMAAvg64)			//Rauschampl. feststellen 
				ZwInt=RMAAvg64-RNeu;	//nach ZwInt laden	 
			else
				ZwInt=RNeu-RMAAvg64;		
	//Sprungampl. �berschritten
			if(ZwInt>RschAmp){
				RMAInitialisieren(RNeu,&RMABuf80[0]);
				RMASumCnt=1;
				RMASumme=RNeu;
			}
			else{		
				RwLang=RMAMittelwert(RMASumme,RMASumCnt);
			}
		}
	}
}
/******************************************************************************
* Funktion: RechMessTemp
* Beschreibung: Wird immer ausgef�hrt wenn es neue Rohwerte gibt 
* Rohwert aus IRQ �bernehmen und 	a) Messwert berechnen (Messbetrieb)
*									b) Rohwerte mitteln (Justieren)
*------------------------------------------------------------------------------
*	MessTemp				aktueller Messwert in 1/10 �C/�F		  
*------------------------------------------------------------------------------
* 	DisplayTemp				256 Messwerte in 1/10 �C/�F
*------------------------------------------------------------------------------
* 	MaxTemp					Maximum von MessTemp per Doppelspeicher
*							L�schzeit 3 Sekunden (CONT, 2.DP)
*------------------------------------------------------------------------------
*	AVGTemp					50ms gleitemd gemittelt
*							1. Summe = Summe - Ergebnis + Messwert
*							2. Ergebnis = Summe / Faktor  (gemittelte Temperatur)
*							Faktor = (4*t90)/(10*tRead)
*							t90 = 3000ms 
*							tRead = Zeit zw. zwei Berechnungen = 1 ms
*							Faktor=(4*50ms)/(10*1ms)=20
*							Vorladewerte: Ergebnis = aktueller Messwert
*							Summe = akt. Messwert * Faktor
******************************************************************************/
void RechMessTemp(void) 
{
	AdwNeu=0;							//Clear RW-Flag
	RwKurz=IntRwKurz;					//Rohwerte �bernehmen
	RwLang=IntRwLang;					 
	if(AdwNeu){							//falls neuer IRQ zwischendurch
		RwKurz=IntRwKurz;				//Rohwerte �bernehmen
		RwLang=IntRwLang;					 
		AdwNeu=0;
	}

	RwNeu=1;							//Neue Werte Befehl 'rw'

//Justiermodus

	if(KaliEin){						//Justieren:
		RwMCnt++;
		RwSumKurz+=RwKurz;				//Rohwerte mitteln
		RwSumLang+=RwLang;
		RMAPtr=RwMCnt&63;
		if(RwLang<0x7FFF)
			RMABuf80[RMAPtr]=RwLang;
		else
			RMABuf80[RMAPtr]=0x7FFF;
		if(RMAPtr==0)
			RschAmpSum+=RMARauschen(&RMABuf80[0]); //Betrag des Rauschens summieren
		if(RwMCnt>255){							//entspricht 4fach-Mittelung		
			RwMKurz=RwSumKurz/256;
			RwMLang=RwSumLang/256;
			RwSumKurz=RwSumLang=RwMCnt=0;
			RschAmp=RschAmpSum/4;		//Rauschampl. wird im EEprom gespeichert
			RschAmpSum=0;
			RMNeu=1;					//Flag: neue gemittelte Rohwerte
		}
		return;
	}

//Messbetrieb
	if(RwDisc>0){		//die ersten Rohwerte nicht verarbeiten
		RwDisc--;
		return;
	}
	RwMittelung();						//Rohwertmittelung lange Ti

	if(HoldMode==0||CntDispNeu==0){		//Messbetrieb Startsequenz
		if(CntAnalog==0)					//Warten bis brauchbare Ger.-temp. vorh.
			return;							//(ca. 20ms, keine Temp.-Berechnung)

		if(OVLang)
			Rohwert=Mul3216Div10(RwKurz,RwFkt[Range]); 	//Kurze Ti
		else
			Rohwert=RwLang;								//lange Ti

		Rohwert=Mul3216Div15(Rohwert,TK(MessTemp,GerTemp)); //Rohwert x TK ( -> TK vom letzter Messwert)
		ZwInt=LinTemp(Rohwert,EmiNk);	// A) Rohwert x NKFkt B) Linarisierung
		if(ZwInt<MBUvl)					// Messwert begrenzen:
			ZwInt=MBUvl;				// kleinster Temp.-Anzeigebereich
		else if(ZwInt>MBOvl)
			ZwInt=MBOvl;				// gr�sster Temp.-Anzeigebereich
		if(SaveCF)						// falls Grad F Einstellung
			ZwInt=WandleC10F10(ZwInt);	// Messwert nach Grad F umrechnen

		if(GSMode){	// GS-Ger�t, Erfassungszeit 500ms
			if(CntDispNeu==0){
				MessTemp=ZwInt;
				DispTemp=ZwInt;
				DispTempSum=0;
				CntDispTemp=0;
				CntDispNeu=1;
			}
			else{
				DispTempSum+=ZwInt;		//Messwertmittelung f�r Displayausgabe
				if(++CntDispTemp==CntRef){
					DispTemp=DivUD3216(DispTempSum,CntDispTemp);
					MessTemp=DispTemp;
					DispTempSum=0;
					CntDispTemp=0;
				}
			}
			RechMemMaxAvg();
		}
		else{	//normales Ger�t, kein GS
			MessTemp=ZwInt;
			RechMemMaxAvg();
			if(CntDispNeu==0){			// Erster Messwert nach dem Einschalten
				DispTemp=MessTemp;  
				DispTempSum=0;
				CntDispTemp=0;
				CntDispNeu=1;
			}
			else{
				DispTempSum+=MessTemp;	// Messwertmittelung f�r Displayausgabe
				if(++CntDispTemp==CntRef){
					DispTemp=DivUD3216(DispTempSum,CntDispTemp);
					DispTempSum=0;
					CntDispTemp=0;
				}
			}
		}			
		MSNeu=1;						// neuer Messwert zum Senden
		MMNeu=1;						// neuer Messwert zum Speichern
	}
	else if(++CntDispTemp==CntRef)		// Hold-Mode, nur Counter f�r  
		CntDispTemp=0;					// Displayaktualisierung bedienen 

	if(ParRtc)							// im RTC Stellmodus keine Messwerte anzeigen
		return;

	if(CntDispTemp==0){					// Displayaktualisierung 	
		asm ("btg _AuxStatus_,#12");	// BlinkOn=!BlinkOn
		ReqTmp=1;						// Messwert (gross) LCD + C/F
		ReqGrZ=1;						// Messwert Grafik obere Statuszeile 			
		if(IntTime>0||MemDsp==0){		// kein Einzelwert gespeichert und 2.DP gedr�ckt
			SegNeu=1;					// 7-Segment, DotMatrix
			ReqGrX=1;					// untere Statuszeile Grafik (Uhrzeit) 			
			ReqCtHdBt=1;				// obere Statuszeile		
		}
		if(SaveMax||SaveAVG){
			if(MenuAktuell==0)
				ReqAxM=1;				// untere Statuszeile (Max/AVG) schreiben				
		}
	}
}

/******************************************************************************
* Funktion: 	RechMemMaxAvg
* Beschreibung: Ermittlung des Messwertes f�r Anzeige MAX,AVG sowie des Wertes 
*				der zum Speichern genommen wird. Im Max- oder AVG-Mode ist
*				das der aktuelle MAX-/AVG-Wert, sonst der normale Messwert.				
* Eingang: 		MessTemp
* Ausgang:		MANTemp
******************************************************************************/
void RechMemMaxAvg(void) 
{
	if(SaveMax){					//Modus MAX:
		if(AVGInit){				// Doppelspeicher initialisieren
			AVGInit=0;
			MaxTemp1=MessTemp;
			MaxTemp2=MessTemp;
			MANTemp=MessTemp;
			MaxCount=0;
		}
		else{						// Doppelspeicher aktualisieren
			if(MessTemp>MaxTemp1)
				MaxTemp1=MessTemp;
			if(MessTemp>MaxTemp2)
				MaxTemp2=MessTemp;
		}
		if(ContMode){				// Cont Mode: feste L�schzeit 3 Sek
			if(++MaxCount>2999){	// L�schzeit abgelaufen ?	
				MaxCount=0;			// 
				if(Max2){			// Doppelspeicher l�schen
					MaxTemp2=MessTemp;
					Max2=0;
				}
				else{
					MaxTemp1=MessTemp;
					Max2=1;
				}
			}
		}
		if(MaxTemp1>MaxTemp2)		// Maximum suchen
			MANTemp=MaxTemp1;		// und �bernehmen
		else
			MANTemp=MaxTemp2;		// und �bernehmen
	}

	else if(SaveAVG){				//Modus AVG ?
		if(AVGInit){				//Initialisieren
			AvgTempSum=(long)MessTemp*(long)AvgFaktor;
			MANTemp=MessTemp;
			AVGInit=0;
		}		
		else{						//AVG berechnen
			AvgTempSum=AvgTempSum-MANTemp+MessTemp;
			MANTemp=AvgTempSum/AvgFaktor;	
		}
	}
	else
		MANTemp=MessTemp;			// Speicherwert laden
}
/******************************************************************************
* Funktion: RechGerTemp
* Beschreibung: Berechnung der Ger�tetemperatur 
*				Intervall 200 ms
* Eingang: 		RWAn1:		4fach gemittelter RW (0x0000...0x3FF0)
* 				KalInTemp:	Kalibrierwert, 10 Bit, positiv
*
* Ausgang:		GerTemp	x 256 (High Byte = VK, Low Byte = NK)	
* Formel:		GerTemp = RW + KalInTemp - RW[0�] x Grad/Bit 
*						= RW + KalInTemp - 220.72 x 0.12055
*				RW[0�]=220.72
*				RW[25�]=425.94
*				RW[50�]=633.33
*	Grad/Bit=25/(RW[50�]-RW[25�])=25/(633.33-425.94)=25/207.39=0.12055							
******************************************************************************/
void RechGerTemp(void) 
{
	signed long sl;
	sl=(signed)(RWAn1+KalInTemp*4-883);	// RW[0�]220,72*4=883
	if(sl>0)								
		GerTemp=(sl*0x1EDC)/0x400;			// Grad/Bit*2e16=0,12055*2e16=0x1EDC geteilt durch 4*2e8
	else									// 
		GerTemp=0;	
	if(GerTemp>GerTempMax){					// GerTemp > max. Ger�tetemperatur
		GerTempMax=GerTemp;					// ja, merken
		EEpromWriteVerify(EE_GerTempMax,1,&GerTempMax);
	}
}
/******************************************************************************
* Funktion: RechUBat
* Beschreibung: Vergleich der Batteriespannung mit den 2 Schwellwerten f�r 
*				- Batteriewarnanzeige 
*				- Batterieniederspannungsger�teabschaltung
* Intervall: 	200 ms				
* Eingang: 		RWAn0		gemittelter RW der Batteriespannung, 14 Bit positiv
* 				RWBatWrn	RW Anzeige Batteriesymbol, 10 Bit positiv 
*				RWBatAus	RW Ger�t schaltet ab, 10 Bit positiv
* Ausgang:		BatMode		0=ok	1=Batt. niedrig		2=Batt. leer => Power Off
******************************************************************************/
void RechUBat(void) 
{
	//Test
	//RWAn0=4;
	
	if(RWAn0/4>RWBatWrn||VUSB||KaliEin)// USB / Kal.-Mode / BattOK:
		BatMode=0;
	else if(RWAn0/4>RWBatAus)		// Batterie niedrig:
		BatMode=1;
	else							// Batterie leer
		PowerOff();				// Ger�t abschalten.
}
/******************************************************************************
* Funktion: Tastenabfrage
* Beschreibung: Auswertung der Tasten
*				Intervall = 25ms	
* Tastenbelegung:	
*	RB2 NumGrf
*	RB3	TstCont		
*	RB4	TstPar	
*	RB5	TstUp
*	RB6	TstEnter
*	RB7	TstDown
*	RB8	TstClrMem 	
*	RB9	TstDP1		1. Druckpunkt Hauptschalter
* 	RD9	TstDP2		2. Druckpunkt Hauptschalter -> Abfrage in main
* 
* Men�tasten
* 	Pfeiltasten koennen z.T. gehalten werden.
* 	Die Verstellung dieser Parameter wird mit der Zeit schneller
* 2. Druckpunkt Hauptschalter 
*   Es wird entspr. Intervallzeit gespeichert + gesendet
*	Intervall 0: Es wird 1 Messwert gespeichert + gesendet 
******************************************************************************/
void Tastenabfrage(void) 
{
	PowerOn=1;							// Selbsthaltung ein

	CntTastenAbfrage=0;					// 25ms Z�hler zur�cksetzen

	if(SelClrMem!=0)					// Blinkende Anzeige f�r Clear Mem
		ReqAxM=1;						// anzeigen

	if(CntDispMem>0){					// 2 Sek Anzeige nach Speichervorgang
		CntDispMem--;					// (hier im 25 ms Intervall)					
		ReqMEm=1;						// Mem/Emi anzeigen 
		ReqMAd=1;						// Zellen-Nr. anzg.
		ReqGrX=1;						// Speicheradresse Grafik untere Statuszeile
//1.19:
//Nach Speichervorgang auf DotMatrix anzeigen (2. Druckpunkt gedr�ckt, Einzel-Speichermodus) 
// - ca. 1 Sek. Messwert 
// - anschl. kurz "===="
// - anschl. Speicherzellen Nr. (mit "=" Zeichen an f�hrenden Nullen)
// - anschl. wieder Messwert
//
		if(TstDP2==0&&IntTime==0){						//2. Druckpunkt gedr�ckt und Einzel-Speichermodus
			if(CntDispMem==0){
				WriteDotMatrix(&BcdBuf[6]);					
			}
			else if(CntDispMem==50){
				BcdBuf[12]=13;
				BcdBuf[13]=13;
				BcdBuf[14]=13;
				BcdBuf[15]=13;
				BcdBuf[16]=0;
				WriteDotMatrix(&BcdBuf[12]);	
			}
			else if(CntDispMem==30){
				HexBcd5(MdCur+1,&BcdBuf[12]);
				BcdBuf[12]=BcdBuf[13];
				BcdBuf[13]=BcdBuf[14];
				BcdBuf[14]=BcdBuf[15];
				BcdBuf[15]=BcdBuf[16];
				BcdBuf[16]=0;
				if(BcdBuf[12]==0){
					BcdBuf[12]=13;	
					if(BcdBuf[13]==0){
						BcdBuf[13]=13;	
						if(BcdBuf[14]==0){
							BcdBuf[14]=13;	
						}
					}
				}
				WriteDotMatrix(&BcdBuf[12]);	
			}
		}
	}

	if(CntDispVer>0){					// SW-Version eingeblendet ?
		if(--CntDispVer<100);			// Zeit abgelaufen ?
			ReqVer=1;					// ja, SW-Version ausblenden
	}

//	if(LichtCount>0){
//		if(--LichtCount==0)
//			Light=0;					// Licht aus
//		else
//			Light=1;					// Licht ein
//	}

	TastenAlt=TastenNeu;				// vorherigen Tastenzustand merken	
	TastenNeu=(~PORTB&0x03FC);			// Tasten lesen, Nicht-TastenBits l�schen
	__builtin_btg(&TastenNeu,9); 		// TstDP1 invertieren !
	Tasten=TastenAlt&TastenNeu;			// 2 x in Folge gedr�ckte Tasten auswerten
	TastenStatus&=Tasten;				// Taste nicht gedr�ckt, Status <Ausgef�hrt> zur�cksetzen
	HoldNeu=0;							// F�r Hold-Modus 

// Taste CONT hier abfragen wg. schneller Reaktion
	if(TstCont){						//Taste CONT gedr�ckt
		if(ParRtc)						// falls RTC eingestellt wird...
			KBDNoMenu();				// ...RTC Men� verlassen
		else{
			if(!OKCont){					//Tastenfunktion noch nicht ausgef�hrt 
				OKCont=1;					//Flag, Tastenfunktion ausgef�hrt
				if(IntCal==1)				//Stopp Modus (Kein Senden/Speichern)
					IntCal=0;				//beenden
				else
	//				ContMode=!ContMode;	//CONT-Modus umschalten
					__builtin_btg((unsigned int*)&AuxStatus_,0); 	//ContMode=!ContMode aber schneller
	
				ReqCtHdBt=1;				// Statuszeile aktualisieren
				ReqGrZ=1;					// dito
				CntDispNeu=0;				// N�chsten Messwert sofort anzeigen (wg.vor. Hold)
				CountHold=THold;			// Hold-Timer neu starten
				AVGInit=1;					// MAX/AVG Neu-Ini
				if(MenuAktuell==MenuInt){	
					if(ContMode&&IntTime&&MenuValue){
						KeyLock=1;			// Anzeige LOCKED
						MenuValue=IntTime;	// nur aktuelle Einstellung zeigen
						ReqAxM=1;			// untere Statuszeile aktualisieren		
					}
					else
						KeyLock=0;			// Keine Anzeige LOCKED
					ReqLck=1;
				}
				else if(MenuAktuell==MenuMem){			// CONT einschalten und...
					if(ContMode&&IntTime){				// Intervall eigestellt:
						SelClrMem=0;					// ClearMem ausschalten 
						KBDNoMenu();					// Men� verlassen
					}
				}
			}
		}
	}

	if(MenuAktuell>0){					//Men� aktiv
		if(TstPar||TstUp||TstDown||TstClrMem||TstNumGrf){	// Men�zeit neu laden ?
			CntMenuzeit=Menuzeit*40;	//ja, 40*25=1000ms, Men�zeit=30
//			LichtCount=400;				//Lichttimer neu laden
		}
		if(CntMenuzeit>0){				//Men�zeit runterz�hlen	
			if(--CntMenuzeit==0)		//Men�zeit abgelaufen ?
				KBDNoMenu();			//Ja, Men� ausschalten				
		}	
	}
	else{								//normaler Messbetrieb
		if(TstDP2){						//2. Druckpunkt NICHT gedr�ckt:
			OKDP2=0;						
			MemDsp=0;					//Display darf wieder aktualisiert werden
			if(TstDP1){					//1. Druckpunkt gedr�ckt:
				CountHold=THold;		//	Abschalttimer neu laden 
				if(OKDP1==0){			//	Tastenfunktion
					OKDP1=1;			//		ausgef�hrt
					if(!ContMode){		//	Falls nicht CONT
						AVGInit=1;		//		MAX/AVG Neu-Ini
						CntDispNeu=0;	//		Messwert sofort anzeigen
					}
				}
			}	
			else{ 						//1./2. DP nicht gedr�ckt
				KrvLck=0;				//Grafiksperre 1.DP aus
				if(ContMode==0||IntTime==0){
					if(MBlock==0x8000){			// Speicherintervall beendet:
						MessdatenInitAdr();		// Positionszeiger ermitteln
						EEDatenZeigen();		// Speicherdaten zeigen
					}
					MBlock|=0x0001;				// Ersten Messwert im n�chsten Intervall sofort speichern
					if(ContMode==0){
						//if(mscnt==0){//auskom., Ger�t schaltet sonst nicht ab bei Bat Low
							if(KaliEin==0&&IntCal==0){
								if(--CountHold<THold){
									HoldNeu=1;			// Hold Mode aktivieren 
									RMAIni=1;			// Neu-Ini Rohwertmittelung n�chste Messung
//1.12								if(CountHold==0||BatMode>1)	// Ger�t abschalten ?				
									if(CountHold==0)			// Ger�t abschalten ?				
										PowerOff();				// Ger�t abschalten.
								}	
							}
						//}
					}
				}
			}
		}

		if(HoldNeu!=HoldMode){			// Hold Mode auswerten
			HoldMode=HoldNeu;
			ReqCtHdBt=1;				// Statusanzeige links oben
			ReqGrZ=1;					// obere Statuszeile Grafik neu
		}
	}
	if(TstPar){
		if(!OKPar&&!KaliEin)			// Taste Par ausf�hren
			TastePar();
	}
	else if(TstEnter){					// Taste Enter ausf�hren
		CountHold=THold;				// Abschaltverz�gerung neu laden 
		if(!OKEnter&&!KaliEin)
			TasteEnter();
	}
	else if(TstClrMem){					// Taste ClearMem ausf�hren
		CountHold=THold;				// Hold-Time neu starten
		if(!OKClrMem&&!KaliEin)
			TasteClearMem();
	}
	else if(TstNumGrf){					// Taste NumGrf ausf�hren
		CountHold=THold;				// Abschaltverz�gerung neu laden 
		if(!OKNumGrf&&!KaliEin)
			TasteNumGrf();
	}
	else if(TstDown){					// Taste Down ausf�hren
		CountHold=THold;				// Abschaltverz�gerung neu laden 
		if(!OKDown&&!KaliEin)
			TasteDown();
	}	
	else if(TstUp){						// Taste Up ausf�hren
		CountHold=THold;				// Abschaltverz�gerung neu laden 
		if(!OKUp&&!KaliEin)
			TasteUp();
	}
	else{
		KBDRep=0;						// Clear Tastenwiederholz�hler
		if(++CntEineSek>40){			// ca. 1 mal / Sekunde
			CntEineSek=0;				
			if(KaliEin){				// a) Bootword f�r Update lesen
				EEpromRead(EE_BootWord,1,&ZwInt);
				if(ZwInt==0x0101){
					if(U2STAbits.TRMT&&!RCVBefehl&&!XmtCnt&&!mscnt){
						RESET();		// Reset -> Bootlader starten
					}
				}
			}
			else{
				if(EmiAlt!=Emi){		// Emi sichern ?
					EEpromWriteVerify(EE_Emi,1,&Emi); //ja
					EmiAlt=Emi;
				}
				else if(SaveStAlt!=SaveStatus){
					EEpromWriteVerify(EE_SaveStatus,1,&SaveStatus);
					SaveStAlt=SaveStatus;
				}
				else if(CntDispMem==0){	// nur wenn keine Speicheraktion
					if(CurMemCellAlt!=CurMemCell){
						EEpromWriteVerify(EE_CurMemCell,1,&CurMemCell);	
						CurMemCellAlt=CurMemCell;
					}
					else if(MemBelegtAlt!=MemBelegt){
						EEpromWriteVerify(EE_MemBelegt,1,&MemBelegt); 
						MemBelegtAlt=MemBelegt;
					}
					else if(MemStartAlt!=MemStart){
						EEpromWriteVerify(EE_MemStart,1,&MemStart);
						MemStartAlt=MemStart;
					}
				}
			}
		}
	}
}
/******************************************************************************
* Funktion: TastePar
* Beschreibung: Parametertaste bedienen 
******************************************************************************/
void TastePar(void)
{
	OKPar=1;							// Funktion ausgef�hrt

	ReqAxM=1;							//Anforderung LCD schreiben
	
	switch(++MenuAktuell)				// Men�punkt abfragen
	{
		case MenuEmi:
			SelClrMem=0;				// Clear Mem aus
			EnabGrf=0;					// Grafik-Display sperren 
			ReqCls=1;					// Clear Display
			ReqCtHdBt=1;				// Statusanzeige oben (PAR)
			HoldMode=0;					// Hold aus
//Messwert auf diese Art aktualisieren, ReqTmp=1 gibt Zufallwert wg. Ende Holdmode
			CntDispNeu=0;				// -> Messwert (gross) LCD + C/F
			ReqMEm=1;					// Mem/Emi anzeigen 
			MenuValue=Emi;
			GrenzeMin=EmiMin;
			GrenzeMax=EmiMax;
			break;
		
		case MenuMax:
			MenuValue=SaveMax;
			break;
	
		case MenuAvg:
			MenuValue=SaveAVG;
			break;
	
		case MenuInt:
			MenuValue=IntTime;
			GrenzeMax=7;
			GrenzeMin=0;
			if(ContMode!=0&&IntTime!=0){	//Keine Intervall-Verst. w�hrend lfd. Messung
				KeyLock=1;					// Anzeige LOCKED
				ReqLck=1;
			}
			break;

		case MenuEmiD:
			KeyLock=0;						// Keine Anzeige LOCKED
			ReqLck=1;
			MenuValue=SaveEmiD;	
			break;

		case MenuCF:
			MenuValue=SaveCF;	
			break;
//Taste Par
		case MenuMem:					// Speicherwerte anzeigen
			if(ContMode==0||IntTime==0){
				MessdatenInitAdr();
				EEDatenZeigen();			
				EnabGrf=SaveGrf;		// Grafik-Display entsperren
			}
			else
				KBDNoMenu();				
			break;

//ab hier RTC Men� <PAR>

		case MenuRtcYear:				// = 0x11 <PAR> RTC Men�  
			MenuValue=RtcJahr;	
			GrenzeMax=63;
			GrenzeMin=0;
			ReqClk=1;					// alle RTC Daten anzeigen
			break;

		case MenuRtcMon:
			MenuValue=RtcMon;	
			GrenzeMax=12;
			GrenzeMin=1;
			ReqClk=1;					// alle RTC Daten anzeigen
			break;

		case MenuRtcDay:
			MenuValue=RtcTag;	
			GrenzeMax=31;
			GrenzeMin=1;
			ReqClk=1;					// alle RTC Daten anzeigen
			break;

		case MenuRtcHour:
			MenuValue=RtcStd;	
			GrenzeMax=23;
			GrenzeMin=0;
			ReqClk=1;					// alle RTC Daten anzeigen
			break;

		case MenuRtcMin:
			MenuValue=RtcMin;	
			GrenzeMax=59;
			GrenzeMin=0;
			ReqClk=1;					// alle RTC Daten anzeigen
			break;

		default:
			KBDNoMenu();				//kein g�ltiger Men�punkt				
			break;
	}
}
/******************************************************************************
* Funktion: TasteUp
* Beschreibung: Hoch-Taste bedienen 
******************************************************************************/
void TasteUp(void)
{
	KBDRepeat();

	if(KBDStep){
		ReqAxM=1;						// Anforderung LCD schreiben

		switch(MenuAktuell)				// Men�punkt bedienen
		{
			case 0:						// Men� ist aus
				if(SaveEmiD){			// Emidirektverstellung ?		
					Emi+=KBDStep;		
					if(Emi>EmiMax)		// Emi wird vor dem Abschalten gespeichert
						Emi=EmiMax;
					SetupEmiNk(Emi);
					ReqMEm=1;			// Mem/Emi anzeigen 
					ReqGrZ=1;			// bzw. obere Zeile Grafik
				}
				break;
			
			case MenuEmi:
				MenuValue+=KBDStep;
				if(MenuValue>GrenzeMax)
					MenuValue=GrenzeMax;
				break;

			case MenuMax:
				OKUp=1;					//kein Autorepeat
				MenuValue=(MenuValue==0)? 0x01:0x00;
				break;

			case MenuAvg:
				OKUp=1;					//kein Autorepeat
				MenuValue=(MenuValue==0)? 0x01:0x00;
				break;

			case MenuInt:
				OKUp=1;					//kein Autorepeat
				if(MenuValue<GrenzeMax){
					MenuValue++;
					if(ContMode!=0&&IntTime!=0){
						MenuValue=IntTime;	// nur aktuelle Einstellung zeigen
						KeyLock=1;			// Anzeige LOCKED
						ReqLck=1;
					}
				}
				break;

			case MenuEmiD:
				OKUp=1;					//kein Autorepeat
				MenuValue=(MenuValue==0)? 0x01:0x00;
				break;

			case MenuCF:
				OKUp=1;					//kein Autorepeat
				SaveCF=(SaveCF==0)? 0x01:0x00;
				break;
//TasteUp
			case MenuMem:	
				if(SelClrMem){			//keine Anzeige Confirm with Enter'
					SelClrMem=0;
					ReqCtHdBt=1;				
				}
				if(SaveGrf){
					if(CurMemBlck<AnzMemBlck){
						MessdatenBlockPlus();
						EEDatenZeigen();			
					}
//					else
//						MdCur=MdEnd;	//Auf letzten Wert im Block setzen 	
				}
				else{
					while(KBDStep-->0){
						if(CurMemCell<MemBelegt){
							MessdatenZellePlus();
							EEDatenZeigen();			
						}
					}
				}	
				break;

//ab hier RTC Men� <UP>
			case MenuRtcYear: case MenuRtcMon: case MenuRtcDay: case MenuRtcHour: case MenuRtcMin:
				if(MenuValue<GrenzeMax)
					MenuValue++;
				break;


		}
	}
}
/******************************************************************************
* Funktion: TasteDown
* Beschreibung: Runter-Taste bedienen 
******************************************************************************/
void TasteDown(void)
{
	KBDRepeat();

	if(KBDStep){
	 	ReqAxM=1;						// Anforderung LCD schreiben

		switch(MenuAktuell)				// Men� bedienen
		{
			case 0:						// Men� ist aus
				if(SaveEmiD){			// Emidirektverstellung
					Emi-=KBDStep;
					if(Emi<EmiMin)
						Emi=EmiMin;
					SetupEmiNk(Emi);
					ReqMEm=1;			// Mem/Emi anzeigen 
					ReqGrZ=1;			// bzw. obere Zeile Grafik	
				}
				break;

			case MenuEmi:
				MenuValue-=KBDStep;
				if(MenuValue<GrenzeMin)
					MenuValue=GrenzeMin;
				break;

			case MenuMax:
				OKDown=1;					//kein Autorepeat
				MenuValue=(MenuValue==0)? 0x01:0x00;
				break;

			case MenuAvg:
				OKDown=1;					//kein Autorepeat
				MenuValue=(MenuValue==0)? 0x01:0x00;
				break;

			case MenuInt:
				OKDown=1;					//kein Autorepeat
				if(MenuValue>GrenzeMin){	
					MenuValue--;
					if(KeyLock){
						MenuValue=0;		// nur INT OFF zulassen
						KeyLock=0;			// keine Anzeige LOCKED
						ReqLck=1;
					}
				}
				break;

			case MenuEmiD:
				OKDown=1;					//kein Autorepeat
				MenuValue=(MenuValue==0)? 0x01:0x00;
				break;

			case MenuCF:
				OKDown=1;					//kein Autorepeat
				SaveCF=(SaveCF==0)? 0x01:0x00;
				break;
//Taste Down
			case MenuMem:
				if(SelClrMem){			//keine Anzeige Confirm with Enter'
					SelClrMem=0;
					ReqCtHdBt=1;				
				}
				if(SaveGrf){
					if(CurMemBlck>1){
						MessdatenBlockMinus();
						EEDatenZeigen();			
					}
//					else
//						MdCur=MdStart;	//Auf ersten Wert im Block setzen 	
				}
				else{
					while(KBDStep-->0){
						if(CurMemCell>1)
							MessdatenZelleMinus();
							EEDatenZeigen();			
					}
				}
				break;


//ab hier RTC Men� <DOWN>
			case MenuRtcYear: case MenuRtcMon: case MenuRtcDay: case MenuRtcHour: case MenuRtcMin:
				if(MenuValue>GrenzeMin)
					MenuValue--;
				break;
		}
	}
}
/******************************************************************************
* Funktion: KBDRepeat
* Beschreibung: Tastenwiederholung pr�fen
******************************************************************************/
void KBDRepeat(void)
{
	KBDStep=0;

	if(KBDRep++==0)				// nach 0 Sek Wert anzeigen
		KBDStep=1;
	else if(KBDRep>200)			// nach 200 x 25ms = 5 Sek schnelle Vestellung
		KBDStep=11;				// 11 Schritte
	else if(KBDRep>100)			// nach 100 x 25ms = 2,5 Sek mittlere Vestellung			
		KBDStep=1;				// 1 Schritt
	else if(KBDRep>20){			// nach 20 x 25 ms = 0,5 Sek langsame Vestellung
		if((KBDRep&0x7)==7)
			KBDStep=1;			// 1 Schritt
	}

}
/******************************************************************************
* Funktion: TasteEnter
* Beschreibung: Enter-Taste bedienen
******************************************************************************/
void TasteEnter(void)
{

	OKEnter=1;							//Funktion ausgef�hrt

	ReqAxM=1;							//Anforderung LCD schreiben

	switch(MenuAktuell)					//Men� bedienen, neue Einstellungen sichern
	{
//Emi-Switch-Funktion (Men� ist off)
		case 0:
			if(EmiSw1>0){
				if(EmiSw2>0){
					if(Emi==EmiSw1)
						Emi=EmiSw2;
					else
						Emi=EmiSw1;
					SetupEmiNk(Emi);
					ReqMEm=1;			// Mem/Emi anzeigen 
					ReqGrZ=1;			// bzw. obere Zeile Grafik	
				}
			}	
			break;


		case MenuEmi:
			Emi=MenuValue;				//wird sp�ter ins EEPROM geschrieben
			break;

		case MenuMax:
			if(SaveMax!=MenuValue){	
				SaveMax=MenuValue;
				if(SaveMax){			//Erstes Maximum sofort �bernehmen
					SaveAVG=0;
					MANTemp=MessTemp;
				}
			}
			break;

		case MenuAvg:
			if(SaveAVG!=MenuValue){	
				SaveAVG=MenuValue;
				if(SaveAVG)
					SaveMax=0;
				AVGInit=1;		//MAX/AVG Neu-Ini
			}
			break;
	
		case MenuInt:
			if(IntTime!=MenuValue&&KeyLock==0){
				EEpromWriteVerify(EE_IntTime,1,&MenuValue);
				IntTime=MenuValue;
			}
			break;
	
		case MenuEmiD:
			if(SaveEmiD!=MenuValue){	
				SaveEmiD=MenuValue;
			}
			break;

		case MenuCF:					// C<->F Verstellung erfolgt direkt
			break;

		case MenuMem:
			if(SelClrMem>0&&MemBelegt>0){						
				if(SelClrMem==1&&SaveGrf==0)					// Einzelwert l�schen	
					MessdatenLoeschen(MdCur,MdCur);		
				else if(SelClrMem==1&&SaveGrf==1&&AnzMemBlck>1)	// Einzelblock l�schen	
					MessdatenLoeschen(MdStart,MdEnd);		
				else{											// alles l�schen
					CurMemBlck=0;
					CurMemCell=0;
					AnzMemBlck=0;
					MemBelegt=0;
					MemStart=0;
				}
				SelClrMem=0;
				MessdatenInitAdr();		
				EEDatenZeigen();			
				ReqAxM=1;			// Unter Statuszeile (Clear...)		
				return;
			}
			break;						// Men� beenden

		case MenuRtcYear:				//ab hier RTC Men� <ENTER>
			if(RtcJahr!=MenuValue){
				SpiWriteRtc(wrjahr,MenuValue);
				RtcJahr=MenuValue;
			}
			TastePar();
			ReqClk=1;					// alle RTC Daten anzeigen
			return;

		case MenuRtcMon:
			if(RtcMon!=MenuValue){
				SpiWriteRtc(wrmonat,MenuValue);
				RtcMon=MenuValue;
			}
			TastePar();
			ReqClk=1;					// alle RTC Daten anzeigen
			return;

		case MenuRtcDay:
			if(RtcTag!=MenuValue){
				SpiWriteRtc(wrday,MenuValue);
				RtcTag=MenuValue;
			}
			TastePar();
			ReqClk=1;					// alle RTC Daten anzeigen
			return;

		case MenuRtcHour:
			if(RtcStd!=MenuValue){
				SpiWriteRtc(wrstunde,MenuValue);
				RtcStd=MenuValue;
			}
			TastePar();
			ReqClk=1;					// alle RTC Daten anzeigen
			return;

		case MenuRtcMin:
			if(RtcMin!=MenuValue){
				SpiWriteRtc(wrminute,MenuValue);
				RtcMin=MenuValue;
			}
			TastePar();
			break;	
		
		default:
			return;
	}

	KBDNoMenu();						//Men� beenden				
}
/******************************************************************************
* Funktion: TasteClearMem 
* Beschreibung: Ist das Parametermen� 'MEM' nicht aktiv, wird erst in dieses geschaltet.
* Bei erstem Tastendruck 'Clear' und den zu l�schenden Einzelwert / Block anzeigen
* Bei zweitem Tastendruck 'Clear all' anzeigen
* In der oberen Statuszeile 'Confirm with Enter' anzeigen
* keine Daten: Anzeige: 'no Data'
* Das L�schen der Daten geschieht nach Tastendruck 'Enter'
******************************************************************************/
void TasteClearMem(void)
{
	OKClrMem=1;						//Flag, Tastenfunktion ausgef�hrt

	if(ParRtc){						// nicht im RTC Stellmodus !
		KBDNoMenu();				// ...RTC Men� verlassen
		return;
	}

	if(ContMode==0||IntTime==0){	//nicht w�hrend lfd. Speicherintervall ausf�hren
		if(MenuAktuell!=MenuMem){	//falls nicht im Men�punkt 'MEM'
			MenuAktuell=MenuMem;	//Men�punkt 'MEM' einstellen
			MessdatenInitAdr();		//EEpr.-Positionszeiger ermitteln
			ReqCtHdBt=1;			//'PAR' anzeigen
		}
		if(++SelClrMem<=2){			//1.Tastendruck=Clear Single  2.Tastendruck=Clear All
			if(MemBelegt==0&&SelClrMem==2)	//Speicher leer, dann bei 2. Tastendruck
				KBDNoMenu();				//Men� verlassen
			else{
				EEDatenZeigen();	//Anzeige der erforderl. EEpr.-Daten		
				ReqAxM=1;			//Anforderung untere Statuszeile anzeigen	
			}
		}
		else
			KBDNoMenu();			//3.Tastendruck Men� wieder verlassen
	}
	else
		Beep();			//akkust. Meldung Tastenfunktion verboten

}	
/******************************************************************************
* Funktion: TasteNumGrf
* Beschreibung: Umschalten zwischen numerischer und grafischer Anzeige 
* Wird ausgef�hrt im normalen Messbetrieb und im Men� 'MEM'
******************************************************************************/
void TasteNumGrf(void)
{
	OKNumGrf=1;								//Flag, Tastenfunktion ausgef�hrt

	if(ParRtc){								// nicht im RTC Stellmodus !
		KBDNoMenu();						// ...RTC Men� verlassen
		return;
	}

	if(!KaliEin){							//keine Tastenfunktion w�hrend Justieren 
		if(MenuAktuell==0||MenuAktuell==MenuMem){ //Normaler Messbetrieb oder Men� 'MEM'
			__builtin_btg(&SaveStatus,6); 	//SaveGrf=!SaveGrf
			EnabGrf=SaveGrf;
			if(SaveGrf==0){					//num. Anzeige
				ReqTmp=1;	
				ReqCls=1;
				ReqAxM=1;					//untere Statuszeile
				ReqCtHdBt=1;
				if(MenuAktuell!=MenuMem)
					ReqMEm=1;				//Mem/Emi anzeigen 
			}
			else{							//graf. Anzeige
				if(MenuAktuell==MenuMem){	//Men� 'MEM'	
					EEDatenZeigen();		//EEpr.-Daten anzeigen	
				}
				else{						//normaler Messbetrieb RAM-Daten anzeigen
					ReqGrf=1;				//Zeile 1..6
					ReqGrZ=1;				//obere Statuszeile
					ReqGrX=1;				//untere Statuszeile
				}
			}
		}
	}
}
/******************************************************************************
* Funktion: MenuDisMem
* Beschreibung: aktuellen Wert des Messwertespeichers anzeigen 
* Page 6,7  	MEM + Speicheradresse anzeigen
*				bei aktiver Clear MEM Funktion: 'CLEAR [ALL]' blinkend anzeigen
* Page 2,3,4,5  Speicherwert bzw '----' falls Speicher leer
* Page 0		Grad C/F vom Speicherwert bzw. '  ' falls Speicher leer  
******************************************************************************/
void MenuDisMem(void)
{
	LcdBuf[16]=0;						//Endekennung

	if(MemBelegt==0){
		LcdBuf[0]='M';					//Displaydaten laden		
		LcdBuf[1]='E';	
		LcdBuf[2]='M';	
		LcdBuf[3]=' ';	
		LcdBuf[4]=' ';	
		LcdBuf[5]=' ';	
		LcdBuf[6]=' ';	
		LcdBuf[7]=' ';	
		LcdBuf[8]=' ';	
		LcdBuf[9]='N';	
		LcdBuf[10]='O';	
		LcdBuf[11]=' ';	
		LcdBuf[12]='D';	
		LcdBuf[13]='A';	
		LcdBuf[14]='T';	
		LcdBuf[15]='A';	
	}
	else{
	
		if(SelClrMem==0){					//Clear MEM nicht aktiv:
	// 1.Zeile, 16 Zeichen (0..16)
			DispCellNum(CurMemCell);
		}	
		else{
			LcdBuf[0]='C';				//Displaydaten laden		
			LcdBuf[1]='L';	
			LcdBuf[2]='E';	
			LcdBuf[3]='A';	
			LcdBuf[4]='R';	
			LcdBuf[5]=' ';	
			if(SelClrMem==1){
				LcdBuf[6]='C';	
				LcdBuf[7]='E';	
				LcdBuf[8]='L';	
				LcdBuf[9]='L';	
				LcdBuf[10]='#';	
				LcdBuf[11]=' ';	
				HexAsc4(CurMemCell,&LcdBuf[12]);	//Zellen-Nr. (1...4000) nach Ascii wandeln
				if(LcdBuf[12]=='0'){
					LcdBuf[12]=' ';
					if(LcdBuf[13]=='0'){
						LcdBuf[13]=' ';
						if(LcdBuf[14]=='0')
							LcdBuf[14]=' ';
					}
				}
			}
			else{
				LcdBuf[6]=' ';	
				LcdBuf[7]=' ';	
				LcdBuf[8]=' ';	
				LcdBuf[9]=' ';	
				LcdBuf[10]=' ';	
				LcdBuf[11]=' ';	
				LcdBuf[12]=' ';	
				LcdBuf[13]='A'; 
				LcdBuf[14]='L';
				LcdBuf[15]='L';
			}
			ReqCwe=1;						//Confirm with Enter
		}
	}

	ReqDat=1;							// Gesp. Daten anzeigen
}
/*****************************************************************************
* Funktion: DispCellNum
* Beschreibung: Speicheradresse anzeigen
* Eingang: Speicheradresse 
* Wird im Men�punkt MEM und im Messbetrieb beim Speichern aufgerufen
******************************************************************************/
void DispCellNum(unsigned int i)
{
	LcdBuf[0]='M';						//Displaydaten laden		
	LcdBuf[1]='E';	
	LcdBuf[2]='M';	
	LcdBuf[3]=' ';	
	LcdBuf[4]=' ';	
	LcdBuf[5]=' ';	
	LcdBuf[6]='C';	
	LcdBuf[7]='E';	
	LcdBuf[8]='L';	
	LcdBuf[9]='L';	
	LcdBuf[10]='#';	
	LcdBuf[11]=' ';	
	HexAsc4(i,&LcdBuf[12]);				//akt. Speicherzelle
	if(LcdBuf[12]=='0'){
		LcdBuf[12]=' ';
		if(LcdBuf[13]=='0'){
			LcdBuf[13]=' ';
			if(LcdBuf[14]=='0'){
				LcdBuf[14]=' ';
			}
		}
	}		
	LcdBuf[16]=0;						//Endekennung
}
/*****************************************************************************
* Funktion: MenuDisEmi
* Beschreibung: ge�nderten Emisionsgrad in Berechnungsfaktor �bernehmen
* Men�punkt EMI in LCD-Ausgabepuffer schreiben
******************************************************************************/
void MenuDisEmi(void)
{
	SetupEmiNk(MenuValue);			//Faktor neu berechnen 

	LcdBuf[0]='E';	
	LcdBuf[1]='M';					//Displaydaten laden		
	LcdBuf[2]='I';	
	LcdBuf[3]=' ';	
	LcdBuf[4]=' ';	
	LcdBuf[5]=' ';	
	LcdBuf[6]=' ';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';	
	LcdBuf[9]=' ';	
	HexAsc4(MenuValue,&LcdBuf[10]);	//Emi nach Ascii wandeln
	if(LcdBuf[10]=='0'){				//f�hrende Nullen ausblenden
		LcdBuf[10]=' ';				
		if(LcdBuf[11]=='0'){
			LcdBuf[11]=' ';				
		}
	}
	LcdBuf[14]=LcdBuf[13];			
	LcdBuf[13]='.';
	LcdBuf[15]='%';
	LcdBuf[16]=0;					//Endekennung
}
/******************************************************************************
* Funktion: MenuDisMax
* Beschreibung: Men�punkt MAX in LCD-Ausgabepuffer schreiben 
******************************************************************************/
void MenuDisMax(void)
{
	LcdBuf[0]='M';	
	LcdBuf[1]='A';					//Displaydaten laden		
	LcdBuf[2]='X';	
	LcdBuf[3]=' ';	
	LcdBuf[4]=' ';	
	LcdBuf[5]=' ';	
	LcdBuf[6]=' ';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';	
	LcdBuf[9]=' ';	
	LcdBuf[10]=' ';	
	LcdBuf[11]=' ';	
	LcdBuf[12]=' ';	
	if(MenuValue){
		LcdBuf[13]=' ';	
		LcdBuf[14]='O';	
		LcdBuf[15]='N';	
	}
	else{
		LcdBuf[13]='O';	
		LcdBuf[14]='F';	
		LcdBuf[15]='F';	
	}
	LcdBuf[16]=0;					//Endekennung
}
/******************************************************************************
* Funktion: MenuDisAvg
* Beschreibung: Men�punkt AVG in LCD-Ausgabepuffer schreiben 
* z.Z nicht unterst�tzt: Einstellzeit in Sek. (off/0,01/0,05/0,25/1/3/10) 
******************************************************************************/
void MenuDisAvg(void)
{
	LcdBuf[0]='A';	
	LcdBuf[1]='V';					//Displaydaten laden		
	LcdBuf[2]='G';	
	LcdBuf[3]=' ';	
	LcdBuf[4]=' ';	
	LcdBuf[5]=' ';	
	LcdBuf[6]=' ';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';	
	LcdBuf[9]=' ';	
	LcdBuf[10]=' ';	
	LcdBuf[11]=' ';	
	LcdBuf[12]=' ';	
	if(MenuValue){
		LcdBuf[13]=' ';	
		LcdBuf[14]='O';	
		LcdBuf[15]='N';	
	}
	else{
		LcdBuf[13]='O';	
		LcdBuf[14]='F';	
		LcdBuf[15]='F';	
	}
	LcdBuf[16]=0;					//Endekennung
}
/******************************************************************************
* Funktion: MenuDisInt
* Beschreibung: Men�punkt Intervallzeit in Displayausgaberegister laden  
* Eingangswert (MenValue) ist 0...6 und wird wie folgt umgesetzt:
* Wegen der Bschr�nkung auf 7 Intervalle werden nur folgende Werte unterst�tzt.
* Nr.	Intervalzeiten/Sekunden
* -1	Cal (OFF, Dauersenden aus) 
* 0		OFF		
* 1		0,001
* 2		0,020
* 3		0,1
* 4		1,0
* 5		10
* 6		100
* 7		500
******************************************************************************/
void MenuDisInt(void)
{
	LcdBuf[0]='I';	
	LcdBuf[1]='N';						//Displaydaten laden		
	LcdBuf[2]='T';	
	LcdBuf[3]='/';	
	LcdBuf[4]='S';	
	LcdBuf[5]='E';	
	LcdBuf[6]='C';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';							
	LcdBuf[9]=' ';	
	LcdBuf[10]=' ';	
	switch(MenuValue)
	{
		case 0:							//Anzeigewert (in ms) laden
			LcdBuf[11]=' ';	
			LcdBuf[12]=' ';	
			LcdBuf[13]='O';	
			LcdBuf[14]='F';	
			LcdBuf[15]='F';	
			break;
		case 1:
			LcdBuf[11]='0';				//1 ms	
			LcdBuf[12]='.';
			LcdBuf[13]='0';
			LcdBuf[14]='0';
			LcdBuf[15]='1';
			break;
		case 2:
			LcdBuf[11]=' ';				//20 ms	
			LcdBuf[12]='0';
			LcdBuf[13]='.';
			LcdBuf[14]='0';
			LcdBuf[15]='2';
			break;
		case 3:
			LcdBuf[11]=' ';	
			LcdBuf[12]=' ';				//100 ms	
			LcdBuf[13]='0';
			LcdBuf[14]='.';
			LcdBuf[15]='1';
			break;
		case 4:
			LcdBuf[11]=' ';	
			LcdBuf[12]=' ';	
			LcdBuf[13]='1';				//1 Sek	
			LcdBuf[14]='.';
			LcdBuf[15]='0';
			break;
		case 5:
			LcdBuf[11]=' ';	
			LcdBuf[12]='1';	
			LcdBuf[13]='0';				//10 Sek	
			LcdBuf[14]='.';
			LcdBuf[15]='0';
			break;
		case 6:
			LcdBuf[11]='1';	
			LcdBuf[12]='0';	
			LcdBuf[13]='0';				//100 Sek	
			LcdBuf[14]='.';
			LcdBuf[15]='0';
			break;
		case 7:
			LcdBuf[11]='5';	
			LcdBuf[12]='0';				//500 Sek	
			LcdBuf[13]='0';
			LcdBuf[14]='.';
			LcdBuf[15]='0';
			break;
	}				
	LcdBuf[16]=0;					//Endekennung
}
/******************************************************************************
* Funktion: MenuDisEmiD
* Beschreibung: Emidirektverstellung on/off 
******************************************************************************/
void MenuDisEmiD(void)
{
	LcdBuf[0]='E';	
	LcdBuf[1]='M';						//Displaydaten laden		
	LcdBuf[2]='I';	
	LcdBuf[3]=' ';	
	LcdBuf[4]='D';	
	LcdBuf[5]='I';						//Pfeil hoch = Ascii 41 	
	LcdBuf[6]='R';						//Pfeil runter = Ascii 40
	LcdBuf[7]='E';	
	LcdBuf[8]='C';	
	LcdBuf[9]='T';	
	LcdBuf[10]=' ';	
	LcdBuf[11]=' ';	
	LcdBuf[12]=' ';	
	if(MenuValue){
		LcdBuf[13]=' ';	
		LcdBuf[14]='O';	
		LcdBuf[15]='N';	
	}
	else{
		LcdBuf[13]='O';	
		LcdBuf[14]='F';	
		LcdBuf[15]='F';	
	}
	LcdBuf[16]=0;					//Endekennung
}
/******************************************************************************
* Funktion: MenuDisCF
* Beschreibung: C/F in Displayausgaberegister laden
* Die Umstellung erfolgt sofort
******************************************************************************/
void MenuDisCF(void)
{
	unsigned char c;
	LcdBuf[0]='C';
	LcdBuf[1]='/'; 						//Grad-Zeichen						
	LcdBuf[2]='F';						//Displaydaten laden		
	LcdBuf[3]=' '; 	
	for(c=4;c<16;c++)
		LcdBuf[c]=' ';	
	LcdBuf[16]=0;						//Endekennung
}

/******************************************************************************
* Funktion: MenuDisRtcYear
* Beschreibung: RTC (Jahr) auf Display anzeigen 
******************************************************************************/
void MenuDisRtcYear(unsigned int dat)
{
	LcdBuf[0]=' ';	
//	if(MenuAktuell==MenuRtcYear) 
//		LcdBuf[0]='>';	
	LcdBuf[1]='Y';						//Displaydaten laden		
	LcdBuf[2]='E';	
	LcdBuf[3]='A';	
	LcdBuf[4]='R';	
	LcdBuf[5]=' ';	
	LcdBuf[6]=' ';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';	
	LcdBuf[9]=' ';	
	LcdBuf[10]=' ';	
	HexAsc4(dat+2000,&LcdBuf[11]);	//nach Ascii wandeln
	LcdBuf[15]=' ';	
	LcdBuf[16]=0;						//Endekennung
}
/******************************************************************************
* Funktion: MenuDisRtcMon
* Beschreibung: RTC (Monat) auf Display anzeigen 
******************************************************************************/
void MenuDisRtcMon(unsigned char dat)
{
	LcdBuf[0]=' ';	
//	if(MenuAktuell==MenuRtcMon) 
//		LcdBuf[0]='>';	
	LcdBuf[1]='M';						//Displaydaten laden		
	LcdBuf[2]='O';	
	LcdBuf[3]='N';	
	LcdBuf[4]='T';	
	LcdBuf[5]='H';	
	LcdBuf[6]=' ';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';	
	LcdBuf[9]=' ';	
	LcdBuf[10]=' ';	
	LcdBuf[11]=' ';	
	LcdBuf[12]=' ';	
	HexAsc2(dat,&LcdBuf[13]);		//nach Ascii wandeln
	LcdBuf[15]=' ';	
	LcdBuf[16]=0;						//Endekennung
}
/******************************************************************************
* Funktion: MenuDisRtcDay
* Beschreibung: RTC (Tag) auf Display anzeigen 
******************************************************************************/
void MenuDisRtcDay(unsigned char dat)
{
	LcdBuf[0]=' ';	
//	if(MenuAktuell==MenuRtcDay) 
//		LcdBuf[0]='>';	
	LcdBuf[1]='D';						//Displaydaten laden		
	LcdBuf[2]='A';	
	LcdBuf[3]='Y';	
	LcdBuf[4]=' ';	
	LcdBuf[5]=' ';	
	LcdBuf[6]=' ';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';	
	LcdBuf[9]=' ';	
	LcdBuf[10]=' ';	
	LcdBuf[11]=' ';	
	LcdBuf[12]=' ';	
	HexAsc2(dat,&LcdBuf[13]);		//nach Ascii wandeln
	LcdBuf[15]=' ';	
	LcdBuf[16]=0;						//Endekennung
}	
/******************************************************************************
* Funktion: MenuDisRtcHour
* Beschreibung: RTC (Stunde) auf Display anzeigen 
******************************************************************************/
void MenuDisRtcHour(unsigned char dat)
{
	LcdBuf[0]=' ';	
//	if(MenuAktuell==MenuRtcHour) 
//		LcdBuf[0]='>';	
	LcdBuf[1]='H';						//Displaydaten laden		
	LcdBuf[2]='O';	
	LcdBuf[3]='U';	
	LcdBuf[4]='R';	
	LcdBuf[5]=' ';	
	LcdBuf[6]=' ';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';	
	LcdBuf[9]=' ';	
	LcdBuf[10]=' ';	
	LcdBuf[11]=' ';	
	LcdBuf[12]=' ';	
	HexAsc2(dat,&LcdBuf[13]);		//nach Ascii wandeln
	LcdBuf[15]=' ';
	LcdBuf[16]=0;						//Endekennung
}	
/******************************************************************************
* Funktion: MenuDisRtcMin
* Beschreibung: RTC (Minute) auf Display anzeigen 
******************************************************************************/
void MenuDisRtcMin(unsigned char dat)
{
	LcdBuf[0]=' ';	
	LcdBuf[1]='M';						//Displaydaten laden		
	LcdBuf[2]='I';	
	LcdBuf[3]='N';	
	LcdBuf[4]=' ';	
	LcdBuf[5]=' ';	
	LcdBuf[6]=' ';	
	LcdBuf[7]=' ';	
	LcdBuf[8]=' ';	
	LcdBuf[9]=' ';	
	LcdBuf[10]=' ';	
	LcdBuf[11]=' ';	
	LcdBuf[12]=' ';	
	HexAsc2(dat,&LcdBuf[13]);		//nach Ascii wandeln
	LcdBuf[15]=' ';
	LcdBuf[16]=0;						//Endekennung
}	
/******************************************************************************
* Funktion: KBD_NoMenu
* Beschreibung: Tastenmenu beenden 
******************************************************************************/
void KBDNoMenu(void)
{
	unsigned char c;

	ParRtc=0;	
	MenuAktuell=0;						// Men� aus
	CntMenuzeit=0;
	CountHold=THold;
	SelClrMem=0;
	EnabGrf=SaveGrf;
	if(GrfOn==1||CntDispNeu==0){		// Grafikpuffer freigegeben / Startsequenz ? 
		GrfOn=0;						//  
		CntMsec=0;						// Zeitdauer der Messung
		InitGrfBuf(&TAvgBuf[0]);		
		MessdatenSkala128Init();
		for(c=0;c<128;c++)				// 128 Durchl�ufe
			MessdatenSpalteAusgeben();	// pro Grafik		 
	}
	if(SaveGrf==0){
		ReqCls=1;
	}
	ReqGrf=1;							// Einzel-Grafik auf LCD ausgeben
	ReqGrZ=1;							// obere Statuszeile
	ReqGrX=1;							// untere Statuszeile
	ReqTmp=1;	
	ReqMEm=1;							// Mem/Emi anzeigen 
	ReqCtHdBt=1;						// CONT / HOLD / BAT anzeigen
	ReqAxM=1;							// untere Statuszeile
}
/******************************************************************************
* Funktion: LcdDisplay (DOGM128,ST7565)
* Beschreibung: LCD-Display neu schreiben 
*
*		Column ->
* 		0   16  32  48  64  80  96  112 128		
*	 	|___|___|___|___|___|___|___|___|
* Page	|0 		    	    			|		
*	|	|1   _______________________	|
*	V	|2  |Display Temp           |C/F|
*		|3 	|6 Zeichen, 16 x 24	Pix	|   | 
*		|4	|Page 2,3,4   ab Col 16 |   |
*		|5            					|
*		|6							    |	
*		|7__16_Zeichen, 8x16/8x8 Pix____|
*			
******************************************************************************/
void LcdDisplay(void)
{
	if(LcdData>0){					// l�uft noch eine Sendung (Page) ? 
		CS_LCD=0;						// Chip select (falls Sendung unterbrochen wurde)
		SpiCnt+=2;						// n�chste Daten addressieren 	
		if(SpiCnt<LcdData)
			SpiSendeLcd();				// senden
		else{ 
			LcdData=0;					// Sendung fertig
			CS_LCD=1;					// Chip de-select LCD 
			if(--LcdPageCount==0)
				LcdStatus=0;			// Sendeauftrag erledigt
		}
	}
//hier Ausgabe �ber mehrere Pages
	else if(LcdPageCount>0){		// muss noch eine Page (0...7) gesendet werden ? 
		if(LcdCls)						// CLS
			SpiSendeCls();
		else if(LcdGrf)					// Grafische Anzeige Page 1...7
			SpiSendeGrf();
		else if(LcdTmp)					// Messwert (gross)
			SpiSendeTmp();
		else if(LcdDat)					// gesp. Messdaten (Pages 4...6)
			LcdDispDat();
		else if(LcdClk)					// Uhrdaten
			SpiSendeClk();
		else							
			LcdPageCount=0;				// verbleibt als letzte M�glichkeit
	}	
	else{							// Stehen neue Aufgaben an ?
		if(ReqRtc){
			SpiReadRtc();				// Uhrdaten lesen
			ReqRtc=0;
		}
		else if(ReqCls){				// Clear Display
			ReqCls=0;
			LcdCls=1;
			LcdPageCount=8;				// 8 Pages
			SpiSendeCls();
		}
		else if(EnabGrf){				// Grafik-Anzeige 
			if(ReqGrZ){					// Page 0
				ReqGrZ=0;
				LcdGrZ=1;
				LcdPageCount=1;
				SpiSendeGrZ();
			}
			else if(ReqGrf){			// Page 1...6
				ReqGrf=0;
				LcdGrf=1;
				LcdPageCount=6;			
				SpiSendeGrf();
			}
			else if(ReqGrX){			// Page 7
				ReqGrX=0;
				LcdGrX=1;
				LcdPageCount=1;
				SpiSendeGrX();
			}
			else
				LcdRequest=0;
		}
		else{							// numerische Anzeige
			if(ReqDat){					// EE-Daten gesp. Messwerte 
				ReqDat=0;
				LcdDat=1;
				LcdPageCount=3;			// 3 Pages
				LcdDispDat();
			}
			else if(ReqCwe)				// Confirm with Enter
				LcdDispCwe();
			else if(ReqTmp)				// Messwert Gross
				LcdDispTmp();
			else if(ReqAxM)				// Untere Statuszeilen
				LcdDispAxM();
			else if(ReqMEm)				// Emi / Mem 
				LcdDispMEm();
			else if(ReqCtHdBt)			// CONT/HOLD/PAR/Bat
				LcdDispCtHdBt();
			else if(ReqLck)				// Locked
				LcdDispLck();
			else if(ReqMAd)				// Speicheradresse
				LcdDispMAd();
			else if(ReqClk){			// Uhrdaten
				ReqClk=0;
				LcdClk=1;
				LcdPageCount=5;			
				SpiSendeClk();
			}
			else if(ReqVer)				// SW-Version
				LcdDispVer();
//1.19
			else if(ReqErr)				//Error-Code
				LcdDispErr();
			
			else
				LcdRequest=0;


//#### Abfrage der Reqest Bits in Assembler (schneller, aber un�bersichtlich) 
//			asm ("btss _LcdRequest_,#11\n bra TstCwe");
//				ReqDat=0;
//				LcdDat=1;
//				LcdPageCount=3;			// 3 Pages
//				LcdDispDat();
//			asm	("bra EndLcd");
//			asm ("TstCwe:\n btss _LcdRequest_,#14\n bra TstTmp");
//				LcdDispCwe();
//			asm	("bra EndLcd");
//			asm ("TstTmp:\n btss _LcdRequest_,#6\n bra TstAxM");
//				LcdDispTmp();
//			asm	("bra EndLcd");
//			asm ("TstAxM:\n btss _LcdRequest_,#5\n bra TstMEm");
//				LcdDispAxM();
//			asm	("bra EndLcd");
//			asm ("TstMEm:\n btss _LcdRequest_,#10\n bra TstCtHdBt");
//				LcdDispMEm();
//			asm	("bra EndLcd");
//			asm ("TstCtHdBt:\n btss _LcdRequest_,#2\n bra TstLck");
//				LcdDispCtHdBt();
//			asm	("bra EndLcd");
//			asm ("TstLck:\n btss _LcdRequest_,#7\n bra TstMAd");
//				LcdDispLck();
//			asm	("bra EndLcd");
//			asm ("TstMAd:\n btss _LcdRequest_,#8\n bra TstClk");
//				LcdDispMAd();
//			asm	("bra EndLcd");
//			asm ("TstClk:\n btss _LcdRequest_,#9\n bra TstVer");
//				ReqClk=0;
//				LcdClk=1;
//				LcdPageCount=5;			
//				SpiSendeClk();
//			asm	("bra EndLcd");
//			asm ("TstVer:\n btss _LcdRequest_,#12\n bra TstNxt");
//				LcdDispVer();
//			asm	("bra EndLcd");
//			asm ("TstNxt:");
//				LcdRequest=0;
//			asm	("EndLcd:");

		}
	}
}
/******************************************************************************
* Funktione: LcdDispTmp
* Beschreibung: LCD Buffer f�r Messwertausgabe + C/F (gross) laden  
******************************************************************************/
void LcdDispTmp(void)
{
	ReqTmp=0;
	LcdTmp=1;

	LcdPageCount=3;						// 3  Pages

	LcdBuf[0]=' ';						// 1 Leerzeichen links

	if(MenuAktuell==MenuMem){			// Ist Men�punkt MEM aktiv ?	
		if(MemBelegt==0){
			LcdBuf[1]=' ';				// ja, aber leerer Speicher
			LcdBuf[2]=' ';
			LcdBuf[3]=' ';
			LcdBuf[4]=' ';
			LcdBuf[5]=' ';
			LcdBuf[6]=' ';		
			LcdBuf[7]=' ';		
		}
		else{
			HexAsc5(StTemp,&LcdBuf[1]);		// Speicherwert anzeigen
			LcdBuf[6]=LcdBuf[5];			// Komma einf�gen
			LcdBuf[5]='.';					
			LcdBuf[7]=(StCF? '$':'#');		// $=F, #=C
		}
	}
	else{
		HexAsc5(DispTemp,&LcdBuf[1]);	//Temp nach Ascii wandeln
		LcdBuf[6]=LcdBuf[5];			// Dezimalpunkt einf�gen
		LcdBuf[5]='.';					
		LcdBuf[7]=(SaveCF? '$':'#');		// $=F, #=C
	}
	LcdBuf[8]=0;						// Endekennung laden					

	if(LcdBuf[1]=='0'){					// f�hrende Nullen ausblenden
		LcdBuf[1]=' ';				
		if(LcdBuf[2]=='0'){
			LcdBuf[2]=' ';
			if(LcdBuf[3]=='0'){
				LcdBuf[3]=' ';
			}
		}
	}
	SpiSendeTmp();
}

/******************************************************************************
* Funktion: LcdDispAxM
* Beschreibung: LCD Buffer f�r Ausgabe untere Statuszeile laden
******************************************************************************/
void LcdDispAxM(void)
{
	unsigned char c;

	ReqAxM=0;							// Anforderung l�schen
	LcdAxM=1;							// Bearbeitung setzen
	
	switch(MenuAktuell)			
	{
		case 0:							// Men� ist nicht aktiv:
			if(KaliEin){
				LcdBuf[0]='C';			//als Info 'CAL' anzeigen
				LcdBuf[1]='A';		
				LcdBuf[2]='L';	
				for(c=3;c<16;c++)
					LcdBuf[c]=' ';	
			}
			else if(SaveMax||SaveAVG){	//Max/AVG Modus ?
				if(SaveMax){
					LcdBuf[0]='M';		
					LcdBuf[1]='A';		
					LcdBuf[2]='X';	
					LcdBuf[3]=' ';	
				}
				else{					
					LcdBuf[0]='A';		
					LcdBuf[1]='V';		
					LcdBuf[2]='G';	
					LcdBuf[3]=' ';	
				}
				LcdBuf[4]=' ';	
				LcdBuf[5]=' ';	
				LcdBuf[6]=' ';	
				LcdBuf[7]=' ';	
				LcdBuf[8]=' ';	
				LcdBuf[9]=' ';	
				HexAsc5(MANTemp,&LcdBuf[10]);	//Temp nach Ascii wandeln
				LcdBuf[15]=LcdBuf[14];	//Komma einf�gen
				LcdBuf[14]='.';					
				if(LcdBuf[10]=='0')		//f�hrende Null ausblenden
					LcdBuf[10]=' ';	
			}
			else{
				for(c=0;c<16;c++)		//sonst 1 Page Leerzeichen
					LcdBuf[c]=' ';	
			}
			LcdBuf[16]=0;				//Endekennung
			break;		

		case MenuEmi:					//Men� ist aktiv:
			MenuDisEmi();
			break;	

		case MenuMax:	
			MenuDisMax();
			break;

		case MenuAvg:
			MenuDisAvg();	
			break;

		case MenuEmiD:
			MenuDisEmiD();	
			break;
	
		case MenuInt:
			MenuDisInt();
			break;

		case MenuCF:			
			MenuDisCF();
			break;

		case MenuMem:			
			MenuDisMem();	
			break;

		case MenuRtcYear:				//ab hier RTC Men�
			MenuDisRtcYear(MenuValue);
			break;

		case MenuRtcMon:
			MenuDisRtcMon(MenuValue);
			break;

		case MenuRtcDay:
			MenuDisRtcDay(MenuValue);
			break;

		case MenuRtcHour:
			MenuDisRtcHour(MenuValue);
			break;

		case MenuRtcMin:
			MenuDisRtcMin(MenuValue);
			break;
	}
	LcdPageCount=1;							// Eine Page
	Lade8x8(&LcdBuf[0],&TxRSpi1Buf[4]);		//Pixeldaten laden
	TxRSpi1Buf[0]=0xB7;						//Page 7	
	TxRSpi1Buf[1]=0x10;						//Column High Nibble (0)	
	TxRSpi1Buf[2]=0x00;						//Column Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;						//Display start line 0=0x40
	LcdData=132;							//128 + 4
	SpiCnt=0;
	CS_LCD=0; 
	ANull=0;								//0=Sende Kommando, 1=Sende Daten

	SpiSendeLcd();
}	

/******************************************************************************
* Funktion: LcdDispDat 
* Beschreibung: Pages 6,5,4 Daten der gespeicherten Messwerte anzeigen
******************************************************************************/
void LcdDispDat(void)
{
	unsigned char c;
	unsigned int i;

/******************************************************************************
* 	Page 6: Datum + Uhrzeit anzeigen
*
*	Leerzeichen:	'$' = 6 Bit
*					' ' = 5 Bit (Standard)
*					'"' = 4 Bit
*					'(' = 3 Bit
*					'&' = 2 Bit
*			 _ _ _ _ _ _ _ _ _ _ _ _ _ _   _ _ _ _ _ _ _ _ _ _ _ _ _ 
*			| Spalte links				| | Spalte rechts			|
*			|_ _ _ _ _ _ _ _ _ _ _ _ _ _| |_ _ _ _ _ _ _ _ _ _ _ _ _| 
*	LcdBuf	|0|1|2|3|4|5|6|7|8|9|0|1|2|3| |4|5|6|7|8|9|0|1|2|3|4|5|6| 27=Endekennung
* 	Zeichen	|T|T|.|M|M|.|J|J|J|J|$|$| |"| |H|H|:|M|M|:|S|S| |"|"|"|"| 	
*	Bits	|5|5|4|5|5|4|5|5|5|5|6|6|5|4| |5|5|4|5|5|4|5|5|5|4|4|4|4|
*			|0...47 	(48 Bit)|21 Bit	| |	     				    |	
*			|0...68     	(69 Bit)	| |69...127 (24+35=59 Bit)  |Ges=69+59=128 Bit	
******************************************************************************/
	if(LcdPageCount==3){
		if(MemBelegt==0){			// Speicher leer 
			for(c=0;c<20;c++)		// 1 Leerzeile ausgeben
				LcdBuf[c]='$';		// 6 * 20 + 8 = 128 Bit 
			LcdBuf[20]='"';
			LcdBuf[21]='"';
			LcdBuf[22]=0;
		}
		else{
	// Datum + Uhrzeit
			HexAsc2(StTag,&LcdBuf[0]);
			LcdBuf[2]='.';
			HexAsc2(StMon,&LcdBuf[3]);
			LcdBuf[5]='.';
			HexAsc4(2000+StJahr,&LcdBuf[6]);
			LcdBuf[10]='$';
			LcdBuf[11]='$';
			LcdBuf[12]=' ';
			LcdBuf[13]='"';
			HexAsc2(StStd,&LcdBuf[14]);
			LcdBuf[16]=':';
			HexAsc2(StMin,&LcdBuf[17]);
			LcdBuf[19]=':';
			HexAsc2(StSek,&LcdBuf[20]);
			LcdBuf[22]=' ';
			LcdBuf[23]='"';
			LcdBuf[24]='"';
			LcdBuf[25]='"';
			LcdBuf[26]='"';
			LcdBuf[27]=0;
		}
	}

/******************************************************************************
* 	Page 5: Block- + Messwert- Nr. / SW Version anzeigen
*
*	CntDispVer = 0:	Block Nr +	Messwert Nr. 	ausgeben
*	CntDispVer > 0:	SW-Version 					ausgeben
*
*	Block Nr +	Messwert Nr. ausgeben
*   bei >= 1000 Bl�cken und >= 1000 Werten im Block wird LCDBuf[4] �berschrieben 
*
*	Leerzeichen:	'$' = 6 Bit
*					' ' = 5 Bit (Standard)
*					'"' = 4 Bit
*					'(' = 3 Bit
*					'&' = 2 Bit
*			 _ _ _ _ _ _ _ _ _ _ _ _ _ _   _ _ _ _ _ _ _ _ _ _ _ _ 
*			| Spalte links			    | | Spalte rechts		  |
*			|_ _ _ _ _ _ _ _ _ _ _ _ _ _| |_ _ _ _ _ _ _ _ _ _ _ _| 
*	LcdBuf	|0|1|2|3|4|5|6|7|8|9|0|1|2|3| |4|5|6|7|8|9|0|1|2|3|4|5| 26=Ende
* 	Zeichen	|B|L|O|C|K|N|N|N|/|N|N|N|N| | |N|o|.|N|N|N|N|/|N|N|N|N| 	
*	Bits	|5|5|5|5|5|5|5|5|4|5|5|5|5|5| |5|5|5|5|5|5|5|4|5|5|5|5|
*			|0...68 (4+65=69)		  	| |69...127 (4+55=59 Bit) | Ges=69+59=128 Bit  
******************************************************************************/
	else if(LcdPageCount==2)
	{
		if(CntDispVer==0||MenuAktuell>0){	// CntDispVer > 0 : SW-Version einblenden	
			CntDispVer=0;				// Version nicht anzeigen
			if(MemBelegt==0){			// Speicher leer 
				for(c=0;c<20;c++)		// 1 Leerzeile ausgeben
					LcdBuf[c]='$';		// Space 6 Bit * 20 = 120 
				LcdBuf[20]='"';			// +4=124
				LcdBuf[21]='"';			// +4=128
				LcdBuf[22]=0;
			}
			else{
				LcdBuf[0]='B';	
				LcdBuf[1]='L';
				LcdBuf[2]='O';
				LcdBuf[3]='C';
				LcdBuf[4]='K';
				LcdBuf[5]=' ';
				LcdBuf[6]=' ';	
		//Anzahl Bl�cke (1...4000) nach Ascii wandeln
				HexAsc4(AnzMemBlck,&LcdBuf[9]);
				c=4;
				if(LcdBuf[9]=='0'){						
					c=5;
					if(LcdBuf[10]=='0'){
						c=6;
						if(LcdBuf[11]=='0')
							c=7;
					}
				}
				LcdBuf[c+4]='/';
		//Block-Nr. (1...4000) nach Ascii wandeln
				HexAsc4(CurMemBlck,&LcdBuf[c]);
				if(LcdBuf[c]=='0'){			
					LcdBuf[c]=' ';
					if(LcdBuf[c+1]=='0'){			
						LcdBuf[c+1]=' ';
							if(LcdBuf[c+2]=='0')			
								LcdBuf[c+2]=' ';
					}
				}
				LcdBuf[13]=' ';
				LcdBuf[14]='N';	
				LcdBuf[15]='o';	
				LcdBuf[16]=39; 		//Punkt 5 Bit 	
				LcdBuf[17]=' ';	
			//#### passt nicht in Zeile 
			//	LcdBuf[14]='C';	
			//	LcdBuf[15]='E';	
			//	LcdBuf[16]='L';	
			//	LcdBuf[17]='L';	
				LcdBuf[18]=' ';	
				LcdBuf[19]=' ';	
				HexAsc4(MdGes,&LcdBuf[22]);		//Anzahl DS (1...4000) nach Ascii wandeln
				c=17;
				if(LcdBuf[22]=='0'){			
					c=18;
					if(LcdBuf[23]=='0'){
						c=19;
						if(LcdBuf[24]=='0')
							c=20;
					}
				}
				LcdBuf[c+4]='/';
				
				if(MdCur<MdStart)			//DS innerhalb Block	
					i=MdCur+4001-MdStart;
				else
					i=MdCur-MdStart+1;
				HexAsc4(i,&LcdBuf[c]);		//Akt. DS (1...4000) nach Ascii wandeln
				if(LcdBuf[c]=='0'){			
					LcdBuf[c]=' ';
					if(LcdBuf[c+1]=='0'){			
						LcdBuf[c+1]=' ';
							if(LcdBuf[c+2]=='0')			
								LcdBuf[c+2]=' ';
					}
				}
				LcdBuf[26]=0;	
			}
		}
	}	
/******************************************************************************
* 	Page 4: Emi + Mode anzeigen (Gespeicherte Messdaten)
*
*	Leerzeichen:	'$' = 6 Bit
*					' ' = 5 Bit (Standard)
*					'"' = 4 Bit
*					'(' = 3 Bit
*					'&' = 2 Bit
*			 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
*			| Spalte links				| Spalte rechts			  |
*			|_ _ _ _ _ _ _ _ _ _ _ _ _ _|_ _ _ _ _ _ _ _ _ _ _ _ _| 
*	LcdBuf	|0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|5|6|7|8|9| 	|0|1|2|3|4| 25=Endekennung
*			|_ _ _ _ _ _ _ _ _ _ _ _ _ _|_ _ _ _ _ _|   |_ _ _ _ _|  
* 	Zeichen	|E|M|I|$|$| |N|N|N|.|N|%|&| |M|O|D|E|$|$| a)|$| |-|-|-| 	
*	Bits	|5|6|4|6|6|5|5|5|5|4|5|6|2|5|6|5|5|5|6|6| 	|6|5|5|5|5|
*			|_ _ _ _ _ _ _ _ _ _ _ _ _ _| _ _ _ _  _|	|_ _ _ _ _|
*			|0...68 (2+8+35+24=69 Bit)  |69..101(33)| b)| | |M|A|X|	
*			|_ _ _ _ _ _ _ _ _ _ _ _ _ _|_ _ _ _ _ _|  	|5|5|6|5|5| 	
*			|0...101 		(2+8+50+42=102 Bit)		| 	|_ _ _ _ _|
*			|_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _| c)| |$|A|V|G|	
* 										|69...127 (59) 	|5|6|5|5|5| 														  
* 														|_ _ _ _ _|	
*														|26 Bit   | 102+26=128 Bit
******************************************************************************/
	else{							// Page 4 verbleibt als letzte M�glichkeit
		if(MemBelegt==0){			// Speicher leer 
			for(c=0;c<20;c++)		// 1 Leerzeile ausgeben
				LcdBuf[c]='$';		// Space 6 * 20 + 8 = 128 Bit 
			LcdBuf[20]='"';
			LcdBuf[21]='"';
			LcdBuf[22]=0;
		}
		else{
			LcdBuf[0]='E';
			LcdBuf[1]='M';
			LcdBuf[2]='I';
			LcdBuf[3]='$';
			LcdBuf[4]='$';
			LcdBuf[5]=' ';	
			HexAsc4(StEmi,&LcdBuf[6]);
			if(LcdBuf[6]=='0'){
				LcdBuf[6]=' ';
				if(LcdBuf[7]=='0')
					LcdBuf[7]=' ';
			}
			LcdBuf[10]=LcdBuf[9];
			LcdBuf[9]='.';
			LcdBuf[11]='%';
			LcdBuf[12]='&';	
		
			if(StModus==0){
				LcdBuf[13]=' ';
				LcdBuf[14]='M';
				LcdBuf[15]='O';
				LcdBuf[16]='D';
				LcdBuf[17]='E';
				LcdBuf[18]='$';
				LcdBuf[19]='$';
				LcdBuf[20]='$';
				LcdBuf[21]=' ';
				LcdBuf[22]='-';
				LcdBuf[23]='-';
				LcdBuf[24]='-';
			}
			else{
				LcdBuf[13]=' ';
				LcdBuf[14]='M';
				LcdBuf[15]='O';
				LcdBuf[16]='D';
				LcdBuf[17]='E';
				LcdBuf[18]='$';
				LcdBuf[19]='$';
				LcdBuf[20]=' ';
				if(StModus==1){
					LcdBuf[21]=' ';
					LcdBuf[22]='M';
					LcdBuf[23]='A';
					LcdBuf[24]='X';
				}
				else{
					LcdBuf[21]='$';
					LcdBuf[22]='A';
					LcdBuf[23]='V';
					LcdBuf[24]='G';
				}
			}
			LcdBuf[25]=0;
		}
	}

	SpiSendeDat();
}

/******************************************************************************
* Funktion: LcdDispVer 
* Beschreibung: Softwareversion (nach Power On) kurz anzeigen Page 5, Colum 76
*
*	a) Lcd Buffer f�r komplette Zeile
*	LcdBuf	|0|1|2|3|4|5|6|7|8|9|0|1|2|	|3|4|5|6|7|8|9|0|1|2|3| 24=Ende
* 	Zeichen	|$|$|$|$|$|$|$|$|$|$|$|$|"|	|V|E|R|"|(|$|N|N|.|N|N|  	
*	Bits	|6|6|6|6|6|6|6|6|6|6|6|6|4|	|5|5|5|4|3|6|5|5|4|5|5| 128 Bit
*			|0...75 (12*6+4=76)		  |	|76...127 (3+8+35+6=52)
*
*	b) Lcd Buffer ab Colum 76
*	Einblenden:
*	LcdBuf	|0|1|2|3|4|5|6| 7=Ende
* 	Zeichen	|V|*|N|N|.|N|N|  	
*	Bits	|5|3|5|5|4|5|5| 32 Bit
*	 		|96...
*	Ausblenden:
*	LcdBuf	|0|1|2|3|4|5| 6=Ende
* 	Zeichen	|$|$|$|$|$|&|  	
*	Bits	|6|6|6|6|6|2| 32 Bit
*
******************************************************************************/
void LcdDispVer(void)
{
//	unsigned char c;

	ReqVer=0;
	LcdVer=1;

//	a) Lcd Buffer f�r komplette Zeile
//	if(CntDispVer>100){		
//		for(c=0;c<12;c++)			
//			LcdBuf[c]='$'; 		
//		LcdBuf[12]='"'; 		
//		LcdBuf[13]='V'; 		
//		LcdBuf[14]='E'; 		
//		LcdBuf[15]='R'; 		
//		LcdBuf[16]='"'; 		
//		LcdBuf[17]='('; 		
//		LcdBuf[18]='$'; 		
//		iHexAscii(SoftVers,&LcdBuf[19]);
//		//if(LcdBuf[19]=='0')//f�hrende 0 ausblenden			
//		//	LcdBuf[19]=' ';
//		LcdBuf[23]=LcdBuf[22];
//		LcdBuf[22]=LcdBuf[21];
//		LcdBuf[21]='.'; 		
//		LcdBuf[24]=0; 		
//	}
//	else{
//		CntDispVer=0;
//		for(c=0;c<20;c++)		// 1 Leerzeile ausgeben
//			LcdBuf[c]='$';		// Space 6 Bit * 20 = 120 
//		LcdBuf[20]='"';			// +4=124
//		LcdBuf[21]='"';			// +4=128
//		LcdBuf[22]=0;
//	}


//	b) Lcd Buffer ab Colum xx
	if(CntDispVer>100){		
		iHexAscii(SoftVers,&LcdBuf[2]);
		if(LcdBuf[2]=='0'){		//f�hrende 0 ausblenden			
			LcdBuf[0]=' '; 		
			LcdBuf[1]='V'; 		
			LcdBuf[2]='*'; 		//Punkt 2 Bit
		}
		else{
			LcdBuf[0]='V'; 		
			LcdBuf[1]='*'; 		
		}
		LcdBuf[6]=LcdBuf[5];
		LcdBuf[5]=LcdBuf[4];
		LcdBuf[4]='.'; 		
		LcdBuf[7]=0; 		
	}
	else{
		CntDispVer=0;
		LcdBuf[0]='$'; 			//Leerzeichen
		LcdBuf[1]='$'; 			// 5 x 6 Bit
		LcdBuf[2]='$'; 		
		LcdBuf[3]='$'; 		
		LcdBuf[4]='$'; 		
		LcdBuf[5]='&';			// 2 Bit
		LcdBuf[6]=0;
	}

	LcdPageCount=1;
	LadeVar4x6(&LcdBuf[0],&TxRSpi1Buf[4]); // Pixeldaten in SPI-Buffer laden
	TxRSpi1Buf[0]=0xB5;					// Page 5	
	TxRSpi1Buf[1]=0x16;					// Column Adress High Nibble (60h=96d)
	TxRSpi1Buf[2]=0x00;					// Column Adress Low Nibble
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40
	LcdData=36; 						// 32 + 4
	SpiCnt=0;
	CS_LCD=0;							// Chip Select LCD 
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}
/******************************************************************************
* Funktion: LcdDispCwe 
* Beschreibung: Textausgabe 'CONFIRM WITH ENTER'  (Page 0)
******************************************************************************/
void LcdDispCwe(void)
{
//Confirm with Enter
	unsigned char c;

	ReqCwe=0;
	LcdMEm=1;

	if(BlinkOn==0||SelClrMem==0){	// Leerzeile f�r Blink-Intervall
		for(c=0;c<16;c++)				// oder Text l�schen
			LcdBuf[c]='$';	//16*6=96
		LcdBuf[16]='"';		//4		
		LcdBuf[17]='"';		//4		
		LcdBuf[18]=0;
	}

//					__Confirm_Clear_with_Enter_
//					555554446554554564455554545    9*4+16*5+2*6=36+85+12=128

//					"$$$Confirm_with_Enter$$$"
//					46665554446564455554546664 = 129
	
//					$$$CONFIRM_WITH_ENTER_$""
//					6665555456564655556555644	= 5*4+12*5+8*6 = 20+60+48 = 128 

//					PAR$$CONFIRM_WITH_ENTER_"
//					5556655554565646555565554	= 3*4+16*5+6*6 = 20+60+48 = 128 

//					CONFIRM_WITH_ENTER"""
//					555545656465555655444	= 5*4+12*5+4*6=24+60+24=104 
		
	else{
		LcdBuf[0]='"';
		LcdBuf[1]='C';
		LcdBuf[2]='O';
		LcdBuf[3]='N';
		LcdBuf[4]='F';
		LcdBuf[5]='I';
		LcdBuf[6]='R';
		LcdBuf[7]='M';
		LcdBuf[8]=' ';
		LcdBuf[9]='W';
		LcdBuf[10]='I';
		LcdBuf[11]='T';
		LcdBuf[12]='H';
		LcdBuf[13]=' ';
		LcdBuf[14]='E';
		LcdBuf[15]='N';
		LcdBuf[16]='T';
		LcdBuf[17]='E';
		LcdBuf[18]='R';
		LcdBuf[19]=0;
	}
	LcdPageCount=1;
	LadeVar4x6(&LcdBuf[0],&TxRSpi1Buf[4]); // Pixeldaten in SPI-Buffer laden
	TxRSpi1Buf[0]=0xB0;					// Page 0	
	TxRSpi1Buf[1]=0x12;					// Command: Column Adress High Nibble
	TxRSpi1Buf[2]=0x00;					// Command: Column Adress Low Nibble
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40
	LcdData=108; 						// 104 + 4
	SpiCnt=0;
	CS_LCD=0;							// Chip Select LCD 
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}
/******************************************************************************
* Funktion: LcdDispMAd
* Beschreibung: Speicherzellen-Adr. 1...4000 anzeigen (Page 5, Col 64...127)
******************************************************************************/
void LcdDispMAd(void)
{
	ReqMAd=0;
	LcdMAd=1;

	if(CntDispMem>0){
//		DispCellNum(MdCur+1);				//MdCur+1 = tats�chliche Adr.
//Memory F�llstandsanzeige
		LcdBuf[0]='$';	// 6
		LcdBuf[1]='$';	// 6
		LcdBuf[2]='C';	// 5
		LcdBuf[3]='E';	// 5
		LcdBuf[4]='L';	// 5
		LcdBuf[5]='L';	// 5
		LcdBuf[7]='&';	// 2 
		LcdBuf[6]='#';	// 5 
		LcdBuf[8]=' ';	// 5
		HexAsc4(MdCur+1,&LcdBuf[9]);			//akt. Speicherzelle
		if(LcdBuf[9]=='0'){
			LcdBuf[9]=' ';
			if(LcdBuf[10]=='0'){
				LcdBuf[10]=' ';
				if(LcdBuf[11]=='0'){
					LcdBuf[11]=' ';
				}
			}
		}		
		LcdBuf[13]=0;						// Endekennung
	}
	else{
		LcdBuf[0]='$';	// 6
		LcdBuf[1]='$';	// 6
		LcdBuf[2]='$';	// 6
		LcdBuf[3]='$';	// 6
		LcdBuf[4]='$';	// 6
		LcdBuf[5]='$';	// 6
		LcdBuf[6]='$';	// 6
		LcdBuf[7]='$';	// 6
		LcdBuf[8]='$';	// 6
		LcdBuf[9]='$';	// 6
		LcdBuf[10]='"';	// 4
		LcdBuf[11]=0;						// Endekennung
	}
	LcdPageCount=1;							// Eine Page
	LadeVar4x6(&LcdBuf[0],&TxRSpi1Buf[4]); // Pixeldaten in SPI-Buffer laden
//	Lade8x8(&LcdBuf[0],&TxRSpi1Buf[4]);		//Pixeldaten laden
	TxRSpi1Buf[0]=0xB5;						//Page 5	
	TxRSpi1Buf[1]=0x14;						//Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;						//Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;						//Display start line 0=0x40
	LcdData=68;								//64 + 4
	SpiCnt=0;
	CS_LCD=0; 
	ANull=0;								//0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}

/******************************************************************************
* Funktion: LcdDispLck
* Beschreibung: Ausgabe LOCKED f�r gesperrte PAR-Funktionen
******************************************************************************/
void LcdDispLck(void)
{
	ReqLck=0;
	LcdLck=1;

	LcdBuf[0]=' ';
	LcdBuf[1]=' ';
	LcdBuf[2]=' ';
	LcdBuf[3]=' ';
	LcdBuf[4]=' ';
	if(KeyLock){			// LOCKED anzeigen
		LcdBuf[5]='L';
		LcdBuf[6]='O';
		LcdBuf[7]='C';
		LcdBuf[8]='K';
		LcdBuf[9]='E';
		LcdBuf[10]='D';
	}
	else{
		LcdBuf[5]=' ';
		LcdBuf[6]=' ';
		LcdBuf[7]=' ';
		LcdBuf[8]=' ';
		LcdBuf[9]=' ';
		LcdBuf[10]=' ';
	}
	LcdBuf[11]=' ';
	LcdBuf[12]=' ';
	LcdBuf[13]=' ';
	LcdBuf[14]=' ';
	LcdBuf[15]=' ';
	LcdBuf[16]=0;

	LcdPageCount=1;				// 1 Page
	Lade8x8(&LcdBuf[0],&TxRSpi1Buf[4]);	//Pixeldaten laden
	TxRSpi1Buf[0]=0xB6;					//Command: Page Adress Set	
	TxRSpi1Buf[1]=0x10;					//Command: Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;					//Command: Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;					//Display start line 0=0x40
	LcdData=132;						//128 + 4
	SpiCnt=0;
	CS_LCD=0; 
	ANull=0;							//0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();

}
/******************************************************************************
* Funktion: LcdDispCtHdBt 
* Beschreibung: Obere Statuszeile PAR/CONT/HOLD/BAT anzeigen
* Achtung: Anzeige <CONFIRM WITH ENTER> in -> LcdDispCwe (wg. unterschdl. Font)
******************************************************************************/
void LcdDispCtHdBt(void)
{
	ReqCtHdBt=0;
	LcdCtHdBt=1;


	if(IntCal){
		LcdBuf[0]='S';					// Send-Stopp anzeigen
		LcdBuf[1]='T';
		LcdBuf[2]='O';
		LcdBuf[3]='P';
	}
	else if(ContMode){
		LcdBuf[0]='C';					// CONT anzeigen
		LcdBuf[1]='O';
		LcdBuf[2]='N';
		LcdBuf[3]='T';
	}
	else if(HoldMode&&MenuAktuell==0){
		LcdBuf[0]='H';					// HOLD anzeigen
		LcdBuf[1]='O';
		LcdBuf[2]='L';
		LcdBuf[3]='D';
	}
	else{
		LcdBuf[0]=' ';
		LcdBuf[1]=' ';
		LcdBuf[2]=' ';
		LcdBuf[3]=' ';
	}
	LcdBuf[4]=' ';

	if(MenuAktuell>0){					// PAR anzeigen
		LcdBuf[5]='P';					
		LcdBuf[6]='A';
		LcdBuf[7]='R';
		LcdBuf[8]=' ';
	}
	else{
		LcdBuf[5]=' ';
		LcdBuf[6]=' ';
		LcdBuf[7]=' ';
		LcdBuf[8]=' ';
	}

	if(BatMode==0){
		LcdBuf[9]=' ';
		LcdBuf[10]=' ';
		LcdBuf[11]=' ';
	}
	else{
//1.12
//	else if(BatMode==1){				// Bat niedrig
		LcdBuf[9]=38;			// Batteriesymbol
		LcdBuf[10]=39;
		LcdBuf[11]=40;
	}
//	else if(BatMode>1){			// Bat leer
//		if(BlinkOn){			// Batteriesymbol
//			LcdBuf[9]=38;		// blinkt
//			LcdBuf[10]=39;
//			LcdBuf[11]=40;
//		}
//		else{
//			LcdBuf[9]=' ';
//			LcdBuf[10]=' ';
//			LcdBuf[11]=' ';
//		}
//	}
	LcdBuf[12]=' ';
	LcdBuf[13]=' ';
	LcdBuf[14]=' ';
	LcdBuf[15]=' ';

	LcdBuf[16]=0;				//Endekennung

	LcdPageCount=1;				// 1 Page
	Lade8x8(&LcdBuf[0],&TxRSpi1Buf[4]);	//Pixeldaten laden
	TxRSpi1Buf[0]=0xB0;					// Command: Page Adress Set	
	TxRSpi1Buf[1]=0x10;					// Command: Column Adress High Nibble (3) 30h=48dez	
	TxRSpi1Buf[2]=0x00;					// Command: Column Adress Low Nibble  (0)   	
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40
	LcdData=132;						// 128 + 4
	SpiCnt=0;
	CS_LCD=0; 
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();

}
/******************************************************************************
* Funktion: LcdDispMEm
* Beschreibung: Emissionsgrad [%] und Memory-Belegung [%] anzeigen (Page 4)
*-----------------------------------------------------------------------------
*LcdBuf	|0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|	|5|6|7|8|9|0|1|2|3|4| 25=Ende
*Char	|E|M|I| |N|N|N|.|N|%| | |"|$|$|	|M|E|M| |N|N|N|.|N|%| Ges=(4*4)+(14*5)+(7*6)	
*Bits	|5|6|4|5|5|5|5|4|5|6|5|5|4|6|6|	|6|5|6|5|5|5|5|4|5|6|	=16+70+42=128 Bit
*		|0...75 ((3*4)+(8*5)+(4*6)=76)|	|76...127 (4+30+18=52)	
******************************************************************************/
void LcdDispMEm(void)
{
	ReqMEm=0;							//Anforderungsflag l�schen
	LcdMEm=1;							//Bearbeitungsflag setzen
	
	LcdBuf[0]='E';					//5
	LcdBuf[1]='M';					//6
	LcdBuf[2]='I';					//4
	LcdBuf[3]=' ';					//4
	HexAsc4(Emi,&LcdBuf[4]);
	if(LcdBuf[4]=='0'){
		LcdBuf[4]=' ';
		if(LcdBuf[5]=='0')
			LcdBuf[5]=' ';
	}
	LcdBuf[8]=LcdBuf[7];
	LcdBuf[7]='.';					//4
	LcdBuf[9]='%';					//6
	LcdBuf[10]=' ';
	LcdBuf[11]=' ';
	LcdBuf[12]='"';

//Memory F�llstandsanzeige
	LcdBuf[13]='$';
	LcdBuf[14]='$';
	LcdBuf[15]='M';
	LcdBuf[16]='E';
	LcdBuf[17]='M';
	LcdBuf[18]=' ';
	if(MemBelegt<2000)							// weniger al 3 Werte im Speicher
		HexAsc4((MemBelegt+3)/4,&LcdBuf[19]);	//  -> 0.1% anzeigen
	else
		HexAsc4(MemBelegt/4,&LcdBuf[19]);		// max. = 99.9%
	if(LcdBuf[19]=='0'){
		LcdBuf[19]=' ';					// 100er Stelle
		if(LcdBuf[20]=='0'){
			LcdBuf[20]=' ';				// 10er Stelle
		}		
	}	
	LcdBuf[23]=LcdBuf[22];
	LcdBuf[22]='.';
	LcdBuf[24]='%';						// Prozent
	LcdBuf[25]=0;						// Endekennung
	TxRSpi1Buf[0]=0xB4;					// Page 4	

	SpiSendePar();
}


/******************************************************************************
* Funktion: LcdDispErr
******************************************************************************/
//1.19
void LcdDispErr(void)
{
	ReqErr=0;
	LcdErr=1;

	LcdBuf[0]='E';						//5
	LcdBuf[1]='E';						//5
	LcdBuf[2]='(';						//3 Leerzch. 3 Bit

	if(ErrEEpromCnt){
		LcdBuf[3]='E';						//5
		LcdBuf[4]='R';						//5
		LcdBuf[5]='R';						//5 
		LcdBuf[6]='O';						//5
		LcdBuf[7]='R';						//5
		LcdBuf[8]=' ';						//5
		HexAsc5(ErrEEpromCnt,&LcdBuf[9]);	//5 x 5	
	}
	else{
		LcdBuf[3]='O';						//5
		LcdBuf[4]='K';						//5
		LcdBuf[5]=' ';						//5
		LcdBuf[6]=' ';						//5
		LcdBuf[7]=' ';						//5
		LcdBuf[8]=' ';						//5
		LcdBuf[9]=' ';						//5
		LcdBuf[10]=' ';						//5
		LcdBuf[11]=' ';						//5
		LcdBuf[12]=' ';						//5
		LcdBuf[13]=' ';						//5
	}	


	LcdBuf[14]=0;						// Endekennung
	TxRSpi1Buf[0]=0xB6;					// Page 6	
	LcdPageCount=1;
	LadeVar4x6(&LcdBuf[0],&TxRSpi1Buf[4]); // Pixeldaten in SPI-Buffer laden
	TxRSpi1Buf[1]=0x10;					// Command: Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;					// Command: Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40
	LcdData=72; 						// 68 + 4
	SpiCnt=0;
	CS_LCD=0;							// Chip Select LCD 
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();

}
/******************************************************************************
* Funktionen: SpiSendeCls,SpiSendeTmp, SpiSendeBat
*			  SpiSendeHold, SpiSendeGrf
* Beschreibung: LCD-Pixeldaten in SPI-Buffer laden und senden
* Name des Pixelbuffers: TxRSpi1Buf
* Gr�sse des Pixelbuffers: 140 Byte (128 Byte Daten, max. 12 Byte f�r Commands)
* Vor dem Senden wird LcdData mit der Anzahl zu sendender Byte geladen, und 
* SpiCnt auf 0 gesetzt. Die A0-Leitung f�r Commands auf 0, f�r Daten auf 1 gesetzt
* Zuerst werden 4 Command Byte gesendet, danch die Daten.
* Commands:	1) Page Adress (0...7) 
* 			2) Column Adress High Nibble (0...15)
* 			3) Column Adress Low Nibble (0...15)
* 			4) No Operation = 0xE3 (wg. Alignment)	
* Das Umsetzen von ASCII-Daten vom LCD-Buffer f�r den Pixelbuffer geschieht im 
* Assamblerteil.
******************************************************************************/
void SpiSendeCls(void)
{
	unsigned char c;

	if(LcdPageCount==8){				//Sendebuffer mit Leerzeichen f�llen
		for(c=4;c<132;c++)
			TxRSpi1Buf[c]=0x00;
	}
	
	TxRSpi1Buf[0]=(0xB0|(LcdPageCount-1));//Command: Page Adress Set	
	TxRSpi1Buf[1]=0x10;					//Command: Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;					//Command: Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40
	LcdData=132;						//128 + 4
	SpiCnt=0;
	CS_LCD=0;							//Chip Select LCD 
	ANull=0;							//0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}
/******************************************************************************
* Funktionen: SpiSendeGrZ
* Beschreibung : Oberste Statuszeile ausgeben
* 	Page 0 		: obere Statuszeile
*
* Eingang:
* 	StCF		Grad C/F
*	StEmi		Emissionsgrad aus Speicher
*	Emi			aktueller Emissionsgrad 
*	MANTemp/DispTemp		Aktueller Wert der Messkurve
******************************************************************************/
void SpiSendeGrZ(void)
{
	unsigned char c;

	TxRSpi1Buf[0]=0xB0;				// Page Adress 0 Set	
	TxRSpi1Buf[1]=0x10;					// Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;					// Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40


	if(IntCal==1){
		LcdBuf[0]='S';
		LcdBuf[1]='T';
		LcdBuf[2]='O';
		LcdBuf[3]='P';	
	}
	else if(ContMode){
		LcdBuf[0]='C';
		LcdBuf[1]='O';
		LcdBuf[2]='N';
		LcdBuf[3]='T';	
	}
	else if(HoldMode&&MenuAktuell==0){
		LcdBuf[0]='H';
		LcdBuf[1]='O';
		LcdBuf[2]='L';
		LcdBuf[3]='D';	
	}
	else{
		LcdBuf[0]=' ';
		LcdBuf[1]=' ';
		LcdBuf[2]=' ';
		LcdBuf[3]=' ';	
	}
	LcdBuf[4]=' ';	
	
	if(MenuAktuell==MenuMem){	// PAR: Grafikspeicher zeigen
		LcdBuf[5]='P';					// PAR anzeigen
		LcdBuf[6]='A';
		LcdBuf[7]='R';
		LcdBuf[8]=' ';

		if(MemBelegt==0){				// Speicher leer
			for(c=9;c<32;c++) 
				LcdBuf[c]=' ';				
		}
		else{							// Speicher voll
			if(SelClrMem!=0){			// Speicher l�schen:
				if(BlinkOn){			// Confirm with Enter
					for(c=9;c<32;c++) 	// blinkt
						LcdBuf[c]=' ';
				}
				else{
					LcdBuf[9]=' ';
					LcdBuf[10]=' ';
					LcdBuf[11]=' ';
					LcdBuf[12]=' ';
					LcdBuf[13]=' ';
					LcdBuf[14]='C';
					LcdBuf[15]='O';
					LcdBuf[16]='N';
					LcdBuf[17]='F';
					LcdBuf[18]='I';
					LcdBuf[19]='R';
					LcdBuf[20]='M';
					LcdBuf[21]=' ';
					LcdBuf[22]='W';
					LcdBuf[23]='I';
					LcdBuf[24]='T';
					LcdBuf[25]='H';
					LcdBuf[26]=' ';
					LcdBuf[27]='E';
					LcdBuf[28]='N';
					LcdBuf[29]='T';
					LcdBuf[30]='E';
					LcdBuf[31]='R';
				}
			}
			else{
				if(StModus==2){				// 2=AVG
					LcdBuf[9]='A';
					LcdBuf[10]='V';
					LcdBuf[11]='G';
				}
				else if(StModus==1){		// 1=MAX
					LcdBuf[9]='M';
					LcdBuf[10]='A';
					LcdBuf[11]='X';
				}
				else{						// 0=Norm
					LcdBuf[9]=' ';
					LcdBuf[10]=' ';
					LcdBuf[11]=' ';
				}
				LcdBuf[12]=' ';				// Emissionsgrad
				LcdBuf[13]='E';
				LcdBuf[14]='M';
				LcdBuf[15]='I';
				LcdBuf[16]=' ';
				HexAsc4(StEmi,&LcdBuf[17]);
				if(LcdBuf[17]=='0'){
					LcdBuf[17]=' ';
					if(LcdBuf[18]=='0')
						LcdBuf[18]=' ';
				}
				LcdBuf[21]=LcdBuf[20];
				LcdBuf[20]='.';
				LcdBuf[22]='%';
				LcdBuf[23]=' ';

				HexAsc5(StTemp,&LcdBuf[24]);	// aktuelle Temperatur

				if(LcdBuf[24]=='0')
					LcdBuf[24]=' ';
				LcdBuf[29]=LcdBuf[28];
				LcdBuf[28]='.';
				LcdBuf[30]='!';				// '!' = Gradzeichen auf Ascii 33
				if(StCF)					// C/F
					LcdBuf[31]='F';				
				else
					LcdBuf[31]='C';

			}		
		}
	}
	else{ 									// 
		if(BatMode==0){						// Bat OK
			LcdBuf[5]=' ';					// Leerzeichen
			LcdBuf[6]=' ';
			LcdBuf[7]=' ';				
		}
		else{
//1.12		else if(BatMode==1){				// Bat niedrig
			LcdBuf[5]='(';					// Batteriesymbol
			LcdBuf[6]=')';
			LcdBuf[7]='*';
		}
//		else if(BatMode>1){					// Bat leer
//			if(BlinkOn){					// Batteriesymbol
//				LcdBuf[5]='(';				// blinkt
//				LcdBuf[6]=')';
//				LcdBuf[7]='*';
//			}
//			else{
//				LcdBuf[5]=' ';				// Leerzeichen
//				LcdBuf[6]=' ';
//				LcdBuf[7]=' ';				
//			}
//		}
		LcdBuf[8]=' ';	
		if(SaveAVG){				// 2=AVG
			LcdBuf[9]='A';
			LcdBuf[10]='V';
			LcdBuf[11]='G';
		}
		else if(SaveMax==1){		// 1=MAX
			LcdBuf[9]='M';
			LcdBuf[10]='A';
			LcdBuf[11]='X';
		}
		else{						// 0=Norm
			LcdBuf[9]=' ';
			LcdBuf[10]=' ';
			LcdBuf[11]=' ';
		}
		LcdBuf[12]=' ';				
		LcdBuf[13]='E';
		LcdBuf[14]='M';
		LcdBuf[15]='I';
		LcdBuf[16]=' ';
		HexAsc4(Emi,&LcdBuf[17]);

		if(LcdBuf[17]=='0'){
			LcdBuf[17]=' ';
			if(LcdBuf[18]=='0')
				LcdBuf[18]=' ';
		}
		LcdBuf[21]=LcdBuf[20];
		LcdBuf[20]='.';
		LcdBuf[22]='%';
		LcdBuf[23]=' ';
//1.12	if(BatMode<2){					// Batteriespannung  
			HexAsc5(DispTemp,&LcdBuf[24]);	// Temp nach Ascii wandeln
			LcdBuf[29]=LcdBuf[28];
			LcdBuf[28]='.';
//		}
//		else{								// Batterie leer
//			LcdBuf[24]=' ';					// kein Messwert anzeigen
//			LcdBuf[25]=' ';
//			LcdBuf[26]=' ';
//			LcdBuf[27]=' ';
//			LcdBuf[28]=' ';
//			LcdBuf[29]=' ';
//		}
		if(LcdBuf[24]=='0')					// F�hrende Null ausblenden
			LcdBuf[24]=' ';
		LcdBuf[30]='!';				// '!' = Gradzeichen auf Ascii 33
		if(SaveCF)					// C/F
			LcdBuf[31]='F';				
		else
			LcdBuf[31]='C';	
	}

	LcdBuf[32]=0;							//Endekennung
	Lade4x6Hi(&LcdBuf[0],&TxRSpi1Buf[4]); 	//Pixeldaten laden

	SpiCnt=0;
	LcdData=132; 						//128 + 4
	CS_LCD=0;							//Chip Select LCD 
	ANull=0;							//0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}
/******************************************************************************
* Funktionen: SpiSendeGrf
* Beschreibung : Messwerte als Grafik ausgeben
* 	Page 1..6	: Messwertkurve
* 	Page 7		: untere Stauszeile
*
* Eingang:
* 	TMaxGes		Maximum der Messkurve
* 	TMinGes		Minimum der Messkurve
*	MemBelegt	Speicherbelegung 0...4000 zur Umrechnung in Prozent
******************************************************************************/
void SpiSendeGrf(void)
{
	TxRSpi1Buf[0]=(0xB0|LcdPageCount);		// Page Adress Set	
	TxRSpi1Buf[1]=0x10;						// Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;						// Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;						// Display start line 0=0x40

	switch(LcdPageCount)
	{
		case 1:
			LadeSpiBufHigh(&TxRSpi1Buf[4],&PixMaxBuf[0]);
			break;
		case 2:
			LadeSpiBufLow(&TxRSpi1Buf[4],&PixMaxBuf[0]);
			break;
		case 3:
			LadeSpiBufHigh(&TxRSpi1Buf[4],&PixMedBuf[0]);
			break;
		case 4:
			LadeSpiBufLow(&TxRSpi1Buf[4],&PixMedBuf[0]);
			break;
		case 5:
			LadeSpiBufHigh(&TxRSpi1Buf[4],&PixMinBuf[0]);
			break;
		case 6:
			LadeSpiBufLow(&TxRSpi1Buf[4],&PixMinBuf[0]);
			break;
	}

	SpiCnt=0;
	LcdData=132; 						//128 + 4
	CS_LCD=0;							//Chip Select LCD 
	ANull=0;							//0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}

/******************************************************************************
* Funktionen: SpiSendeGrX
* Beschreibung 	: Untere Statuszeile ausgeben (Page 7)
******************************************************************************/

void SpiSendeGrX(void)
{
	TxRSpi1Buf[0]=0xB7;				// Page Adress 7 Set	
	TxRSpi1Buf[1]=0x10;					// Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;					// Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40

// a) gespeicherte EEPROM-Messdaten anzeigen
	if(MenuAktuell==MenuMem){
		if(MemBelegt==0){
			LcdBuf[0]='M';	
			LcdBuf[1]='E';	
			LcdBuf[2]='M';	
			LcdBuf[3]=' ';	
			LcdBuf[4]=' ';	
			LcdBuf[5]=' ';	
			LcdBuf[6]=' ';	
			LcdBuf[7]=' ';	
			LcdBuf[8]=' ';	
			LcdBuf[9]=' ';	
			LcdBuf[10]=' ';	
			LcdBuf[11]=' ';	
			LcdBuf[12]='N';	
			LcdBuf[13]='O';	
			LcdBuf[14]=' ';	
			LcdBuf[15]='D';	
			LcdBuf[16]='A';	
			LcdBuf[17]='T';	
			LcdBuf[18]='A';	
			LcdBuf[19]=' ';	
			LcdBuf[20]=' ';	
			LcdBuf[21]=' ';	
			LcdBuf[22]=' ';	
			LcdBuf[23]=' ';	
			LcdBuf[24]=' ';	
			LcdBuf[25]=' ';	
			LcdBuf[26]=' ';	
			LcdBuf[27]=' ';	
			LcdBuf[28]=' ';	
			LcdBuf[29]=' ';	
			LcdBuf[30]=' ';	
			LcdBuf[31]=' ';	
		}
		else{
			if(SelClrMem>0){
				LcdBuf[0]=' ';	
				LcdBuf[1]=' ';	
				LcdBuf[2]=' ';	
				LcdBuf[3]=' ';	
				LcdBuf[4]=' ';	
				LcdBuf[5]='C';	
				LcdBuf[6]='L';	
				LcdBuf[7]='E';	
				LcdBuf[8]='A';	
				LcdBuf[9]='R';	
				LcdBuf[10]=' ';	
				LcdBuf[11]=' ';	
				LcdBuf[12]=' ';	
				LcdBuf[13]=' ';	
				LcdBuf[14]=' ';	
				LcdBuf[15]=' ';	
				LcdBuf[16]=' ';	
				LcdBuf[17]=' ';	
				if(SelClrMem==1)
					GrafikBlockNr();		
				else{
					LcdBuf[18]=' ';	
					LcdBuf[19]=' ';	
					LcdBuf[20]=' ';	
					LcdBuf[21]=' ';	
					LcdBuf[22]=' ';	
					LcdBuf[23]=' ';	
					LcdBuf[24]='A';	
					LcdBuf[25]='L';	
					LcdBuf[26]='L';	
					LcdBuf[27]=' ';	
					LcdBuf[28]=' ';	
					LcdBuf[29]=' ';	
					LcdBuf[30]=' ';	
					LcdBuf[31]=' ';	
				}
			}
			else{
				//if(MdStart==MdEnd){						
				if(MdGes>1){			// Mehrere Werte im Block				
					GrafikGesamtZeit();	// Messdauer zeigen
					LcdBuf[16]=' ';	
				}
				else{					// Einzelwert
					GrafikMemZeit();	// gesp. Uhrzeit zeigen
				}
				LcdBuf[17]='|';	

				GrafikBlockNr();
			}
		}
	}
	else{


//b) Echtzeit-Grafik 
// Intervallmessung ODER Kurve 1. DP
// 1. DP MBlock 0x1000 = lfd. Messung
// 1. DP MBlock 0x1001 = fertige Messung

		if(MdGes==0)					// keine Werte:
			GrafikAktZeit();			// aktuelle Uhrzeit anzeigen
		else if(MdGes==1)				// Einzelwert:
			GrafikMemZeit();			// gespeicherte Uhrzeit anzeigen
		else{							// Mehrere Werte:
			GrafikGesamtZeit();			// Gesamte Messzeit anzeigen
			LcdBuf[16]=' ';

//			if(BlinkOn){
//				LcdBuf[6]=' ';			//blinkende DP w�hrend Messung
//				LcdBuf[9]=' ';
//			}

		}

//1.15 bei Intervallmessung Speicherzelle zeigen	
		LcdBuf[17]='|';

		if(CntDispMem>0){	
			LcdBuf[18]='M';
			LcdBuf[19]='E';
			LcdBuf[20]='M';
			LcdBuf[21]=' ';
			LcdBuf[22]='C';
			LcdBuf[23]='E';
			LcdBuf[24]='L';
			LcdBuf[25]='L';
			LcdBuf[26]='#';
			LcdBuf[27]=' ';
			HexAsc4(MdCur+1,&LcdBuf[28]);	//Reale Zellen-Nr. (1...4000) nach Ascii wandeln
			if(LcdBuf[28]=='0'){
				LcdBuf[28]=' ';
				if(LcdBuf[29]=='0'){
					LcdBuf[29]=' ';
					if(LcdBuf[30]=='0')
						LcdBuf[30]=' ';
				}
			}
		}				
		else{
			GrafikMemProzent();			
		}
	}

	LcdBuf[32]=0;						//Endekennung
	Lade4x6Lo(&LcdBuf[0],&TxRSpi1Buf[4]); 	//Pixeldaten in SPI-Buffer laden
	
	SpiCnt=0;
	LcdData=132; 						//128 + 4
	CS_LCD=0;							//Chip Select LCD 
	ANull=0;							//0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}

/******************************************************************************
* Funktionen: GrafikGesamtZeit
* Beschreibung : Zeitdauer der Messung (Tot HH:MM:SS.MSC) anzeigen
* Eingang:	CntMsec 
******************************************************************************/
void GrafikGesamtZeit(void)
{
	unsigned char c;

	LcdBuf[0]='T';
	LcdBuf[1]='O';
	LcdBuf[2]='T';
	LcdBuf[3]=' '; 
	ZwLong=Uhrdaten(CntMsec);
	ZwInt=ZwLong&0x03FF;				//Millisekunden
	HexAsc4(ZwInt,&LcdBuf[12]);
	LcdBuf[12]='.';
	c=(ZwLong/0x400)&0x3F;				//Sekunden
	HexAsc2(c,&LcdBuf[10]);
	LcdBuf[9]=':';
	c=ZwLong/0x10000;					//Minuten
	HexAsc2(c,&LcdBuf[7]);
	LcdBuf[6]=':';
	c=ZwLong/0x1000000;					//Stunden
	HexAsc2(c,&LcdBuf[4]);
}
/******************************************************************************
* Funktionen: GrafikMemZeit
* Beschreibung : Zeitpunkt der gesp. Messung (TT.MM.JJ|HH:MM:SS) anzeigen
******************************************************************************/
void GrafikMemZeit(void)
{
	HexAsc2(StTag,&LcdBuf[0]);		
	LcdBuf[2]='.';
	HexAsc2(StMon,&LcdBuf[3]);		
	LcdBuf[5]='.';
	HexAsc2(StJahr,&LcdBuf[6]);		
	LcdBuf[8]='|';
	HexAsc2(StStd,&LcdBuf[9]);		
	LcdBuf[11]=':';
	HexAsc2(StMin,&LcdBuf[12]);		
	LcdBuf[14]=':';
	HexAsc2(StSek,&LcdBuf[15]);		
}
/******************************************************************************
* Funktionen: GrafikAktZeit
* Beschreibung: akt. Uhrzeit (TT.MM.JJ|HH:MM:SS) anzeigen
******************************************************************************/
void GrafikAktZeit(void)
{
	HexAsc2(RtcTag,&LcdBuf[0]);		
	LcdBuf[2]='.';
	HexAsc2(RtcMon,&LcdBuf[3]);		
	LcdBuf[5]='.';
	HexAsc2(RtcJahr,&LcdBuf[6]);		
	LcdBuf[8]='|';
	HexAsc2(RtcStd,&LcdBuf[9]);		
	LcdBuf[11]=':';
	HexAsc2(RtcMin,&LcdBuf[12]);		
	LcdBuf[14]=':';
	HexAsc2(RtcSek,&LcdBuf[15]);		
}
/******************************************************************************
* Funktionen: GrafikMemProzent
* Beschreibung : Speicherausnutzung in Prozent (MEM XX.X%) anzeigen
******************************************************************************/
void GrafikMemProzent(void)
{
	LcdBuf[18]='M';
	LcdBuf[19]='E';
	LcdBuf[20]='M';
	LcdBuf[21]=' ';
	LcdBuf[22]=' ';
	LcdBuf[23]=' ';
	LcdBuf[24]=' ';
	LcdBuf[25]=' ';
	LcdBuf[26]=' ';	
	if(MemBelegt==4000){
		LcdBuf[27]=' ';
		LcdBuf[28]='1';
		LcdBuf[29]='0';
		LcdBuf[30]='0';
	}
	else{
		if(MemBelegt<2000)							// weniger al 3 Werte im Speicher
			HexAsc4((MemBelegt+3)/4,&LcdBuf[26]);	//  -> 0.1% anzeigen
		else
			HexAsc4(MemBelegt/4,&LcdBuf[26]);		// max. = 99.9%
		LcdBuf[26]=' ';								// 100er Stelle entf�llt

		if(LcdBuf[27]=='0')
			LcdBuf[27]=' ';
		LcdBuf[30]=LcdBuf[29];
		LcdBuf[29]='.';
	}
	LcdBuf[31]='%';
}
/******************************************************************************
* Funktionen: GrafikBlockNr
* Beschreibung : aktuelle Block-Nr/Anzahl Blocke anzeigen
******************************************************************************/
void GrafikBlockNr(void)
{
	LcdBuf[18]='B';	
	LcdBuf[19]='L';	
	LcdBuf[20]='O';	
	LcdBuf[21]='C';	
	LcdBuf[22]='K';	
	HexAsc4(CurMemBlck,&LcdBuf[23]);	//Block-Nr. (1...4000) nach Ascii wandeln
	if(LcdBuf[23]=='0'){
		LcdBuf[23]=' ';
		if(LcdBuf[24]=='0'){
			LcdBuf[24]=' ';
			if(LcdBuf[25]=='0'){
				LcdBuf[25]=' ';
			}
		}
	}				
	LcdBuf[27]='/';	
	HexAsc4(AnzMemBlck,&LcdBuf[28]);	//Anzahl Bl�cke (1...4000) nach Ascii wandeln
	if(LcdBuf[28]=='0'){			
		LcdBuf[28]=LcdBuf[29];
		LcdBuf[29]=LcdBuf[30];
		LcdBuf[30]=LcdBuf[31];
		LcdBuf[31]=' ';
		if(LcdBuf[28]=='0'){
			LcdBuf[28]=LcdBuf[29];
			LcdBuf[29]=LcdBuf[30];
			LcdBuf[30]=' ';		
			if(LcdBuf[28]=='0'){
				LcdBuf[28]=LcdBuf[29];
				LcdBuf[29]=' ';		
			}
		}
	}
}
/******************************************************************************
* Funktionen: SpiSendeTmp
* Beschreibung : Messwert anzeigen
******************************************************************************/
void SpiSendeTmp(void)
{
	Lade16x32(16*(LcdPageCount-1),&LcdBuf[0],&TxRSpi1Buf[4]);//Pixeldaten laden
	TxRSpi1Buf[0]=(0xB0|(LcdPageCount));	//Command: Page Adress Set	
	TxRSpi1Buf[1]=0x10;					//Command: Column Adress High Nibble (0)
	TxRSpi1Buf[2]=0x00;					//Command: Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;					//Display start line 0=0x40
	LcdData=132;						//128 + 4
	SpiCnt=0;
	CS_LCD=0; 
	ANull=0;							//0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();

// Test 8x16
//	Lade8x16(8*(LcdPageCount-1),&LcdBuf[0],&TxRSpi1Buf[4]);	// Pixeldaten laden
//	TxRSpi1Buf[0]=(0xB0|LcdPageCount);	// Command: Page Adress Set	
//	TxRSpi1Buf[1]=0x11;					// Command: Column Adress High Nibble (7) 6Ch=108dez	
//	TxRSpi1Buf[2]=0x08;					// Command: Column Adress Low Nibble  (0)   	
//	TxRSpi1Buf[3]=0xE3;					// nop
//	LcdData=52;							// 48 + 4
//	SpiCnt=0;
//	CS_LCD=0; 
//	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
//	SpiSendeLcd();

}

void SpiSendeBat(void)
{
//	Lade8x16(8*(LcdPageCount-1),&LcdBuf[0],&TxRSpi1Buf[4]);	//Pixeldaten laden
//	TxRSpi1Buf[0]=(0xB0|(LcdPageCount-1));	//Command: Page Adress Set	
	Lade8x8(&LcdBuf[0],&TxRSpi1Buf[4]);		//Pixeldaten laden
	TxRSpi1Buf[0]=0xB0;					//Command: Page Adress Set	
	TxRSpi1Buf[1]=0x13;					// Command: Column Adress High Nibble (3) 30h=48dez	
	TxRSpi1Buf[2]=0x00;					// Command: Column Adress Low Nibble  (0)   	
	TxRSpi1Buf[3]=0x40;					//Display start line 0=0x40
	LcdData=28;							// 24 + 4
	SpiCnt=0;
	CS_LCD=0;
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}

void SpiSendePar(void)
{
	LcdPageCount=1;
//	Lade5x6(&LcdBuf[0],&TxRSpi1Buf[4]); // Pixeldaten in SPI-Buffer laden
	LadeVar4x6(&LcdBuf[0],&TxRSpi1Buf[4]); // Pixeldaten in SPI-Buffer laden
	TxRSpi1Buf[1]=0x10;					// Command: Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;					// Command: Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40
	LcdData=132; 						// 128 + 4
	SpiCnt=0;
	CS_LCD=0;							// Chip Select LCD 
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}

void SpiSendeCont(void)
{
//	Lade8x16(8*(LcdPageCount-1),&LcdBuf[0],&TxRSpi1Buf[4]);	//Pixeldaten laden
	Lade8x8(&LcdBuf[0],&TxRSpi1Buf[4]);	//Pixeldaten laden
	TxRSpi1Buf[0]=(0xB0|(LcdPageCount-1));	//Command: Page Adress Set	
	TxRSpi1Buf[1]=0x10;					// Command: Column Adress High Nibble	
	TxRSpi1Buf[2]=0x00;					// Command: Column Adress Low Nibble      	
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40
	LcdData=36;							// 32 + 4
	SpiCnt=0;
	CS_LCD=0;
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}

void SpiSendeHold(void)
{
//	Lade8x16(8*(LcdPageCount-1),&LcdBuf[0],&TxRSpi1Buf[4]);	//Pixeldaten laden
//	TxRSpi1Buf[0]=(0xB0|(LcdPageCount-1));	//Command: Page Adress Set	
	Lade8x8(&LcdBuf[0],&TxRSpi1Buf[4]);	//Pixeldaten laden
	TxRSpi1Buf[0]=(0xB0|(LcdPageCount-1));	//Command: Page Adress Set	
	TxRSpi1Buf[1]=0x16;					// Command: Column Adress High Nibble (5) 60h=96dez	
	TxRSpi1Buf[2]=0x00;					// Command: Column Adress Low Nibble	 (8)   	
	TxRSpi1Buf[3]=0x40;					// Display start line 0=0x40
	LcdData=36;							// 32 + 4
	SpiCnt=0;
	CS_LCD=0;
	ANull=0;							// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}

// Messdaten aus EEprom anzeigen
void SpiSendeDat(void)
{
	TxRSpi1Buf[0]=(0xB0|(LcdPageCount+3));	// Pages 6,5,4
	LadeVar4x6(&LcdBuf[0],&TxRSpi1Buf[4]); 	// Pixeldaten in SPI-Buffer laden
	TxRSpi1Buf[1]=0x10;						// Command: Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;						// Command: Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;						// Display start line 0=0x40
	LcdData=132; 							// 128 + 4
	SpiCnt=0;
	CS_LCD=0;								// Chip Select LCD 
	ANull=0;								// 0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}
/******************************************************************************
* Funktion: LcdSendClk
* Beschreibung: Uhrdaten Jahr/Monat/Tag/Stunde/Minute im RTC Nen� anzeigen
*
* Function			MenuAktuell		Var
* -----------------------------------------------------------------------------
* MenuRtcYear		0x11			RtcJahr
* MenuRtcMon		0x12			RtcMon
* MenuRtcDay		0x13			RtcTag
* MenuRtcHour		0x14			RtcStd
* MenuRtcMin		0x15			RtcMin	
******************************************************************************/
void SpiSendeClk(void)
{
	if(LcdPageCount==1)
		MenuDisRtcYear(RtcJahr);
	else if(LcdPageCount==2)
		MenuDisRtcMon(RtcMon);
	else if(LcdPageCount==3)
		MenuDisRtcDay(RtcTag);
	else if(LcdPageCount==4)
		MenuDisRtcHour(RtcStd);
	else
		MenuDisRtcMin(RtcMin);
	
	LcdBuf[16]=0;							// Endekennung (z. Sicherheit nochmal)
	TxRSpi1Buf[0]=((0xB0)|(LcdPageCount-1));	// Page Adress Set	
	TxRSpi1Buf[1]=0x10;						// Column Adress High Nibble (0)	
	TxRSpi1Buf[2]=0x00;						// Column Adress Low Nibble	 (0)   	
	TxRSpi1Buf[3]=0x40;						// Display start line 0=0x40
	Lade8x8(&LcdBuf[0],&TxRSpi1Buf[4]);		//Pixeldaten laden
	LcdData=132; 						// 128 + 4
	SpiCnt=0;
	CS_LCD=0; 
	ANull=0;								//0=Sende Kommando, 1=Sende Daten
	SpiSendeLcd();
}
/******************************************************************************
* Funktion: SegDisplay
*
* Aufruf erfolgt im 5-ms Intervall
*
* Beschreibung: Messwert auf 7-Segment-Display (74HC4511)
*				und Dot-Matrix-Display (SLR2016) anzeigen.   	
*
* Eingangswert:	BCDBuf[ = 5 Byte ] enth�lt Messwert in 1/10el Grad im Bcd-Format 
*			
* Zur Ansteuerung sind 4 Datenleitungen (D0,D1,D2,D3) parallel an beide Displays geschaltet.
*
* SLR2016 Dot-Matrix:
* -------------------
* Das SLR2016 hat insgesamt 4 Zeichen a� 5 x 7 Pixel. Diese werden mit 2 Adressleitungen
* (A0,A1) eingestellt. 
* Die Dateneing�nge hei�en D0,D1,D2,D3, es gibt aber noch 3 Dateineing�nge die nicht 
* benutzt werden. Diese hei�en D4,D5,D6. D4 und D5 sind auf LOW gelegt D6 auf HIGH.
* Auf diese Weise lasen sich die ASCII-Zeichen 30h bis 3Fh anzeigen.
* Zum Schreiben muss vorher W(rite)E(nable) (an RB14) auf LOW gelegt werden.
* Um das Display dunkel zu schalten, muss BL (an RG1) auf LOW gelegt werden,
* wodurch alle 4 Digits aus sind. Das Leerzeichen (Ascii 20H) l�sst sich nicht einstellen
* (D4 nicht umschaltbar !) somit ist es nicht m�glich ein einzelnes Zeichen dunkel zu schalten. 
* Ansteuerung: 	1. Ausw�hlen des Digits 3...0 durch Schalten von A0 und A1 
*				2. Write enable durch Low-Pegel an WR2016
*				3. Schreiben der Daten an D0...D3
*				4. Write disable durch High-Pegel an WR2016
*
* 74HC4511 7-Segment:
* -------------------
* Digitauswahl 0...3: Pin Sg1000,Sg0100,Sg0010,Sg0001 es soll immer nur eine 
* Stelle aktiviert sein.
* 4 Datenleitungen D0...D3 zur BCD-Beschaltung 0...9 (andere Werte = Blanking)
* Ansteuerung:	1. Ausw�hlen des Digits 0...3 durch High-Pegel an Pin SgXXXX
*				2. Eistellen der BCD-Zahl an D0...D3
* 
******************************************************************************/
void SegDisplay(void)
{
	unsigned char c;	
	
	if(SegNeu){									// Neuer Messwert ?
		SegNeu=0;
		if(CntDispMem>0){						// Speicherwert anzeigen (bzw. Speicher voll)
			HexBcd5(MemTempMerk,&BcdBuf[6]);		
		}
		else if(SaveMax||SaveAVG)				// MAX / AVG anzeigen 
			HexBcd5(MANTemp,&BcdBuf[6]);
		else
			HexBcd5(DispTemp,&BcdBuf[6]);		//aktuellen Messwert anzeigen
		WriteDotMatrix(&BcdBuf[6]);				// Temp.-Wert in Dot-Matrix schreiben (4-stellig, gerundet, ganze Grad)
		
//************************************************************************
// 7-Segment Display 74HC4511
//************************************************************************
//1.19 Anzeige immer in ganzen Grad wenn SaveStatus,GanzeGrad = 1 
//			if(BcdBuf[0]==0){				// Messwert kleiner 1000
		BcdBuf[0]=BcdBuf[6];		// Umladen f�r Anzeige in 1/10 Grad		
		BcdBuf[1]=BcdBuf[7];				
		BcdBuf[2]=BcdBuf[8];				
		BcdBuf[3]=BcdBuf[9];				
		BcdBuf[4]=BcdBuf[10];				
		if(BcdBuf[0]==0&&GanzeGrad==0){	// Messwert kleiner 1000 UND Flag "GanzeGrad" = 0 -> Anzeige in 1/10el Grad	
			DezPunkt=0;					// DezPunkt ein
			BcdBuf[0]=BcdBuf[1];		// Umladen f�r Anzeige in 1/10 Grad		
			BcdBuf[1]=BcdBuf[2];				
			BcdBuf[2]=BcdBuf[3];				
			BcdBuf[3]=BcdBuf[4];				
		}
		else{							// Messwert >= 1000 ODER "GanzeGrad" = 1 -> Anzeige in ganzen Grad
			DezPunkt=1;					// DezPunkt aus
			if(BcdBuf[4]>4){			// Messwert runden
				BcdBuf[3]++;
				if(BcdBuf[3]>9){
					BcdBuf[3]=0;
					BcdBuf[2]++;
					if(BcdBuf[2]>9){
						BcdBuf[2]=0;
						BcdBuf[1]++;
						if(BcdBuf[1]>9){
							BcdBuf[1]=0;
							BcdBuf[0]++;
						}				
					}
				}
			}
		}
	}

// 7-Segment Display schreiben
	DigitCount=(DigitCount+1)&0x03;		// DigitCount: 0,1,2,3

	c=BcdBuf[DigitCount];				// c = BCD-Wert 0...9				
	switch(DigitCount)					// Digit einstellen 0...3
	{
		case 0:							// 1000er
			Sg0001=0;
			Sg0010=0;
			Sg0100=0;
			Sg1000=1;
			if(c==0)
				c=15;
			break;
		case 1:							// 100er
			Sg0001=0;
			Sg0010=0;
			Sg0100=1;
			Sg1000=0;
			break;
		case 2:							// 10er
			Sg0001=0;
			Sg0010=1;
			Sg0100=0;
			Sg1000=0;

			break;
		case 3:							// 1er
			Sg0001=1;
			Sg0010=0;
			Sg0100=0;
			Sg1000=0;
			break;
	}

//BatMode > 0:= Bat low = blinkendes Display 
	if(BatMode&&BlinkOn){				// schwache Batterie  -> Anzeige blinkt
		Zw2016=0;						// SLR2016 dunkel schalten					
		c=15;							// 10...15 -> Blank an 74HC4511
	}	
	else{
		Zw2016=1;						// SLR2016 Ein 
	}
//1.12
//	else if(BatMode>1){
//		Zw2016=0;						// Aus-Zustand SLR2016 merken					
//		c=15;							// 10...15 erzeugt Blank an 74HC4511
//	}

	if(c&1)								// Daten schreiben
		D0=1;
	else
		D0=0;				
	if(c&2)
		D1=1;
	else
		D1=0;
	if(c&4)
		D2=1;
	else
		D2=0;
	if(c&8)
		D3=1;
	else
		D3=0;
}
/******************************************************************************
* Funktion: SpiSendeLcd
* Beschreibung: LCD-Display Dogm128 schreiben per SPI 
* Achtung, laut Datenblatt betr�gt die maximale SPI-Frequenz 10 MHz !
*
*							FCY								 9,216 MHz
* FSCK  =	------------------------------------------ = 	---------- = 9,216 MHz
*			Primary Prescaler * Secondary Prescaler            1 * 1	===========
* 
* Zeitbedarf: Ein Bit @ 9.216 MHz = 108.5 ns -> 16 Bit = 1.74 �s
******************************************************************************/
void SpiSendeLcd(void)
{
	SPI1STATbits.SPIEN=0;				// SPI f�r LCD-Senden konfigurieren
	SPI1CON1=0x053F;					// 16 Bit,Mastermode,PPRE=1:1,SPRE=1:1,CKE=1,CKP=0
	SPI1STATbits.SPIEN=1;				

	if(SpiCnt==4)
		ANull=1;						//1=Sende Daten, 0=Sende Kommando

	SPI1BUF=256*TxRSpi1Buf[SpiCnt]+TxRSpi1Buf[SpiCnt+1];


//dma0 funktioniert nicht.
//	DMA0CONbits.CHEN=0;					//reset DMA0 
//	DMA0CNT=LcdData-1; 					//Anzahl Byte (-1) = 6 x 16 = 96
//	DMA0CONbits.CHEN=1;					//DMA0 ON 
//	DMA0REQbits.FORCE=1;				//Schreiben starten

}
/******************************************************************************
* Funktion: SpiWriteRtc
* Beschreibung: Schreiben von Uhrdaten ins RTC-Modul. Da die Daten im  
* Standard-Hex-Format vorliegen, m�ssem sie vor dem Schreiben 
* f�r das RTC-Modul ins BCD-Format gewandelt werden.
* Eingang: 				code			wert
*						------------------------
*					 	0x86=Jahr		00...99
*						0x85=Monat		01...12
*						0x84=Date		01...31		
*						0x82=Stunde		00...23 (l�uft im 24h-Modus)
*						0x81=Minute		00...59
*						0x80=Sekunde	00...59
* SPI-Clock je Nach Vcc (2.0V...5V) 0,6...2MHz muss vorher eingestellt werden *)
******************************************************************************/
void SpiWriteRtc(unsigned char code, unsigned char wert)
{

	CS_LCD=1;							//Schreibvorgang LCD beenden

	SPI1STATbits.SPIEN=0;				//8 Bit Modus  
	SPI1CON1=0x003A;					//PPRE = 4:1, SPRE=2:1
	SPI1STATbits.SPIEN=1;				//CKE=0,CKP=0												

	CE1305=1;							//Chip enable RTC
	WarteUsec(1);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x8F;						//Schreibe Controlregister
	while(!IFS0bits.SPI1IF);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Disable Write-Protect,Oscilator On,Alarm disable
	while(!IFS0bits.SPI1IF);

	CE1305=0;							//Sequenz n�tig f�r Write Enable 
	WarteUsec(1);
	CE1305=1;
	WarteUsec(1);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=code;						//Auswahl Jahr/Monat/Tag/Stunde/Minute/Sekunde					
	while(!IFS0bits.SPI1IF);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=wert/10*16+wert-(wert/10*10);	//Neuer Wert in BCD
	while(!IFS0bits.SPI1IF);

	CE1305=0;							//Chip disable RTC

//#### DMA0 Transmit geht nicht
//	TxRSpi1Buf[0]=0x8F;					//Schreibe Controlregister
//	TxRSpi1Buf[1]=0x00;					//Disable Write-Protect,Oscilator On,Alarm disable
//	TxRSpi1Buf[2]=code;					//Auswahl Jahr/Monat/Tag/Stunde/Minute/Sekunde
//	TxRSpi1Buf[3]=wert/10*16+wert-(wert/10*10);	//Neuer Wert (BCD)
//	TxRSpi1Buf[4]=0x8F;					//Schreibe Controlregister
//	TxRSpi1Buf[5]=0x40;					//Enable Write-Protect,Oscilator On,Alarm disable
//	DMA0CNT=5; 							//Anzahl Byte (-1) = 5
//	DMA0REQbits.FORCE=1;				//Schreiben starten
}
/******************************************************************************
* Funktion: SpiReadRtc
* Beschreibung: Lesen von Uhrdaten aus RTC-Modul
* Um einen komplett gepufferten Datensatz zu erhalten, muss im Burst-Mode 
* gelesen werden. Es werden 7 Byte Daten in einem Rutsch nach Chip-Enable 
* per DMA2 lesen. An dieser Stelle erfolgt nur die Sendung des Adressbytes,
* nach empfang der 7 Datenbyte erfolgt ein DMA2 Interrupt. Dort werden die 
* empfangenen BCD-Daten ins Hex-Format gewandelt. 			
*
* Adresse:	0x00=Sekunde			00...59
*			0x01=Minute				00...59
*			0x02=Stunde				00...23
*			0x03=Day of the week	01...07 (unbenutzt) 
*			0x04=Date				01...31		
*			0x05=Monat				01...12
*			0x06=Jahr				00...99
*
* SPI-Clock je Nach Vcc (2.0V...5V) 0,6...2MHz muss vorher eingestellt werden
* Zeitbedarf: 1210 IC =>  ca. 65 �s @ 1.152MHz 
******************************************************************************/
void SpiReadRtc(void)
{
	CS_LCD=1;							//Schreibvorgang LCD beenden

	SPI1STATbits.SPIEN=0;				//8 Bit Modus  
	SPI1CON1=0x003A;					//PPRE =4:1, SPRE=2:1
	SPI1STATbits.SPIEN=1;				//CKE=0,CKP=0												

	CE1305=1;							//CE RTC
	WarteUsec(1);						//Setup Time CE

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Command 0x00 = Read 10 Seconds
	while(!IFS0bits.SPI1IF);
	TxRSpi1Buf[0]=SPI1BUF;

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Sekunden
	while(!IFS0bits.SPI1IF);
	TxRSpi1Buf[1]=SPI1BUF;
	
	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Minuten
	while(!IFS0bits.SPI1IF);
	TxRSpi1Buf[2]=SPI1BUF;

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Stunden
	while(!IFS0bits.SPI1IF);
	TxRSpi1Buf[3]=SPI1BUF;

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Wochentag, n.b.
	while(!IFS0bits.SPI1IF);
	TxRSpi1Buf[4]=SPI1BUF;

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Datum, Tag
	while(!IFS0bits.SPI1IF);
	TxRSpi1Buf[5]=SPI1BUF;

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Datum, Monat
	while(!IFS0bits.SPI1IF);
	TxRSpi1Buf[6]=SPI1BUF;

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Datum, Jahr
	while(!IFS0bits.SPI1IF);
	TxRSpi1Buf[7]=SPI1BUF;

	CE1305=0;							//Chip disable RTC

	RtcSek=TxRSpi1Buf[1]/16*10+(TxRSpi1Buf[1]&0x0F);	//Da die Daten der Uhr
	RtcMin=TxRSpi1Buf[2]/16*10+(TxRSpi1Buf[2]&0x0F);	//im gepacktem BCD-Format
	RtcStd=TxRSpi1Buf[3]/16*10+(TxRSpi1Buf[3]&0x0F);	//empfangen werden,
	RtcTag=TxRSpi1Buf[5]/16*10+(TxRSpi1Buf[5]&0x0F);	//ist eine Umwandlung
	RtcMon=TxRSpi1Buf[6]/16*10+(TxRSpi1Buf[6]&0x0F);	//in das Standard Hex-
	RtcJahr=TxRSpi1Buf[7]/16*10+(TxRSpi1Buf[7]&0x0F);	//Format erforderlich.
}

/******************************************************************************
* Funktionen: SpiStoppRtc
* Beschreibung: Uhr anhalten (RTC DS1305) 
******************************************************************************/
void SpiStoppRtc(void)
{
	CS_LCD=1;							//Schreibvorgang LCD beenden

	SPI1STATbits.SPIEN=0;				//8 Bit Modus  
	SPI1CON1=0x003A;					//PPRE = 4:1, SPRE=2:1
	SPI1STATbits.SPIEN=1;				//CKE=0,CKP=0												

	CE1305=1;							//Chip enable RTC
	WarteUsec(1);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x8F;						//Schreibe Controlregister
	while(!IFS0bits.SPI1IF);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x80;						//Oscilator Off
	while(!IFS0bits.SPI1IF);

	CE1305=0;							//Chip disable RTC
}
/******************************************************************************
* Funktionen: SpiStartRtc
* Beschreibung: Uhr starten (RTC DS1305) 
******************************************************************************/
void SpiStartRtc(void)
{
	CS_LCD=1;							//Schreibvorgang LCD beenden

	SPI1STATbits.SPIEN=0;				//8 Bit Modus  
	SPI1CON1=0x003A;					//PPRE = 4:1, SPRE=2:1
	SPI1STATbits.SPIEN=1;				//CKE=0,CKP=0												

	CE1305=1;							//Chip enable RTC
	WarteUsec(1);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x8F;						//Schreibe Controlregister
	while(!IFS0bits.SPI1IF);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x00;						//Oscilator On, Alarm disabled
	while(!IFS0bits.SPI1IF);

	CE1305=0;							//Chip disable RTC

}
/******************************************************************************
* Funktionen: EEpromRead
* Beschreibung: ext. EEPROM FM25256B lesen / max. 20MHz 
* EEPROM hat 32768 Byte (0000h...7FFFh), Adressiert wird auf Byteadresse ! 
* Eingang: 	adr		Byteadresse im EEPROM ab der gelesen wird
* 			count	Wordanzahl die gelesen werden soll 
*			*ptr	RAM-Adresse ab der gespeichert wird
******************************************************************************/
void EEpromRead(unsigned int adr, unsigned int count,unsigned int *ptr)
{
	CLRWDT();

	CS_LCD=1;							//Schreibvorgang LCD beenden

	SPI1STATbits.SPIEN=0;				 
	SPI1CON1=0x013F;					//8 Bit Modus, PPRE = 1:1, SPRE=1:1
	SPI1STATbits.SPIEN=1;				//CKE=1,CKP=0												

	CS_EE=0;							

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x03;						//0x03 = Read
	while(!IFS0bits.SPI1IF);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=adr/256;					//Adresse High
	while(!IFS0bits.SPI1IF);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=adr;						//Adresse Low
	while(!IFS0bits.SPI1IF);
	
	adr=SPI1BUF;						//Dummy read -> reset SPI1BUF

	for(;count>0;count--){				//Daten
		IFS0bits.SPI1IF=0;
		SPI1STATbits.SPIROV=0;
		SPI1BUF=0;
		while(!IFS0bits.SPI1IF);
		*ptr=SPI1BUF&0x00FF;
		IFS0bits.SPI1IF=0;
		SPI1STATbits.SPIROV=0;
		SPI1BUF=0;
		while(!IFS0bits.SPI1IF);
		*ptr++|=SPI1BUF*256;
	}

	CS_EE=1;							//EE terminieren
}
/******************************************************************************
* Funktionen: EEpromWrite
* Beschreibung: schreiben in ext. EEPROM FM25256B per SPI1 
* Die EEPROM Schreib/Leseroutinen haben h�chste SPI1-Priorit�t und brechen
* alle anderen SPI1 Vorg�nge (LCD, RTC) ab.
* Daten werden in Real-Time geschrieben, 4.608 MHz, 1 Byte = 1,736 �s 
* keine Warteschleifen n�tig !
* Eingang: 	adr		Byteadresse im EEPROM ab der geschrieben wird
* 			count	Anzahl Word zu schreiben 
*			*ptr	RAM-Adresse ab der gelesen wird
******************************************************************************/
void EEpromWrite(unsigned int adr, unsigned int count,unsigned int *ptr)
{
	CLRWDT();

	CS_LCD=1;							//Schreibvorgang LCD beenden
										
	SPI1STATbits.SPIEN=0;				 
	SPI1CON1=0x013F;					//8 Bit Modus, PPRE = 1:1, SPRE=1:1
	SPI1STATbits.SPIEN=1;				//CKE=1,CKP=0												

	CS_EE=0;							//CS Enable

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x06;						//Opcode 0x06 = WREN
	while(!IFS0bits.SPI1IF);

	CS_EE=1;							//Enable WREN
	CS_EE=0;

	SPI1STATbits.SPIROV=0;
	IFS0bits.SPI1IF=0;
	SPI1BUF=0x02;						//0x02 = WRITE
	while(!IFS0bits.SPI1IF);

	SPI1STATbits.SPIROV=0;
	IFS0bits.SPI1IF=0;
	SPI1BUF=adr/256;					//Adresse High
	while(!IFS0bits.SPI1IF);

	SPI1STATbits.SPIROV=0;
	IFS0bits.SPI1IF=0;
	SPI1BUF=adr;						//Adresse Low
	while(!IFS0bits.SPI1IF);

	for(;count>0;count--){				//Daten
		SPI1STATbits.SPIROV=0;
		IFS0bits.SPI1IF=0;
		SPI1BUF=*ptr;
		while(!IFS0bits.SPI1IF);
		SPI1STATbits.SPIROV=0;
		IFS0bits.SPI1IF=0;
		SPI1BUF=*ptr++/256;
		while(!IFS0bits.SPI1IF);
	}
	CS_EE=1;							//EE schreiben terminieren
}

/******************************************************************************
* Funktionen: EEpromVerify
* Beschreibung: Datenabgleich ext. EEPROM FM25256B <-> RAM 
* Eingang: 	adr		Byteadresse im EEPROM ab der verglichen wird
* 			count	Wordanzahl die verglichen werden soll 
*			*ptr	RAM-Adresse ab der verglichen wird
* Bei Fehler wird Argument <1> zur�ckgegeben, sonst <0>
******************************************************************************/
unsigned char EEpromVerify(unsigned int adr, unsigned int count,unsigned int *ptr)
{
	unsigned int i;

	SPI1STATbits.SPIEN=0;				 
	SPI1CON1=0x013F;					//8 Bit Modus, PPRE = 1:1, SPRE=1:1, CKE=1,CKP=0
	SPI1STATbits.SPIEN=1;			

	CS_EE=0;

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=0x03;						//0x03 = Read
	while(!IFS0bits.SPI1IF);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=adr/256;					//Adresse High
	while(!IFS0bits.SPI1IF);

	IFS0bits.SPI1IF=0;
	SPI1STATbits.SPIROV=0;
	SPI1BUF=adr;						//Adresse Low
	while(!IFS0bits.SPI1IF);

	i=SPI1BUF;							//Dummy read -> reset SPI1BUF

	for(;count>0;count--){				//Daten
		IFS0bits.SPI1IF=0;
		SPI1STATbits.SPIROV=0;
		SPI1BUF=0;
		while(!IFS0bits.SPI1IF);
		i=SPI1BUF&0x00FF;
		IFS0bits.SPI1IF=0;
		SPI1STATbits.SPIROV=0;
		SPI1BUF=0;
		while(!IFS0bits.SPI1IF);
		i|=SPI1BUF*256;
		if(i!=*ptr++){
			CS_EE=1;					//EEprom lesen terminieren
			return(1);					//Fehler 
		
		}
	}
	CS_EE=1;							//EEprom lesen terminieren
	return(0);
}
/******************************************************************************
* Funktion: MesswertSenden
* Beschreibung: Xmtms	Messwert in 1/10� C/F senden
*				Xmtrw   ungemittelte Rohwerte senden
*				XmtRM	gemittelte Rohwerte senden 
*				Xmtmd	gespeicherte verpackte Messwerte senden				
*				XmtOut	aktuellen verpackten Messwert senden
******************************************************************************/
void MesswertSenden(void)
{
	if(Xmtms){							//Messwert (ms) senden ?	
		if(MSNeu){						//Neuer Messwert vorhanden ?
			MSNeu=0;
			HexAsc5(MessTemp,&XmtBuf[0]); 				
			if(--mscnt==0)				// Ein Wert weniger
				Xmtms=0;
			XmtCnt=6;	
			SendeAntwort();
		}
	}
	else if(Xmtrw){						// ungemittelte Rohwert (rw) senden ?
		if(RwNeu){
			RwNeu=0;
			cHexAscii(RwKurz_.Up,&XmtBuf[0]);
			cHexAscii(RwKurz_.Hi,&XmtBuf[2]);
			cHexAscii(RwKurz_.Lo,&XmtBuf[4]);
			cHexAscii(RwLang_.Up,&XmtBuf[6]);
			cHexAscii(RwLang_.Hi,&XmtBuf[8]);
			cHexAscii(RwLang_.Lo,&XmtBuf[10]);
			if(--mscnt==0){				// Ein Wert weniger
				Xmtrw=0;
			}
			XmtCnt=13;	
			SendeAntwort();
		}
	}

	else if(XmtRM){												// gemmittelte Rohwerte (RM) senden
		if(RMNeu){
			RMNeu=0;			
			cHexAscii(RwMKurz_.Up,&XmtBuf[0]);					// kurze Ti
			cHexAscii(RwMKurz_.Hi,&XmtBuf[2]);
			cHexAscii(RwMKurz_.Lo,&XmtBuf[4]);
			cHexAscii(RwMLang_.Up,&XmtBuf[6]);					// lange Ti
			cHexAscii(RwMLang_.Hi,&XmtBuf[8]);
			cHexAscii(RwMLang_.Lo,&XmtBuf[10]);
			if(OVLang)											// f�r Infrajust: 
				Rohwert=Mul3216Div10(RwMKurz,RwFkt[Range]);		// hochgerechneter RW kurze Ti (falls OV)
			else												// sonst RW lange Ti  
				Rohwert=RwMLang;
			cHexAscii(Rohwert_.Tp,&XmtBuf[12]);
			cHexAscii(Rohwert_.Up,&XmtBuf[14]);
			cHexAscii(Rohwert_.Hi,&XmtBuf[16]);
			cHexAscii(Rohwert_.Lo,&XmtBuf[18]);
			iHexAscii(RschAmp,&XmtBuf[20]);
			mscnt=0;
			XmtRM=0;
			XmtCnt=25;	
			SendeAntwort();
		}
	}

	else if(Xmtmd){						// gepackte Speicherdaten (md) senden:
		if(--mscnt==4001){				// 1. Sendung
			EEStart=MemStart;
			mscnt=MemBelegt+1;			// Anzahl gesp. Datens�tze + Startzeichen
			XmtBuf[0]=0x02;				// Startzeichen STX (02h) senden
			XmtCnt=1;					// OHNE CR !!!	
			XmtBufCnt=0;				// darum hier Sendung starten
			RCVBefehl=0;
			while(IEC1bits.U2TXIE);
			IFS1bits.U2TXIF=1;
			IEC1bits.U2TXIE=1;
			return;		
		}	
		else if(mscnt>0){
			EEpromRead(EEStart*8,4,&EE_Dat[0]);	//EEPROM-Datensatz 0...3999 lesen, 4 Worte
// Blockstartbit (MessTemp, Bit 15) f�r PORTAWIN nicht ausblenden !
//			EE_Dat[0]&=0x7FFF;
			if(++EEStart>3999)			// Speicheradr. korrigieren
				EEStart=0;
			iHexAscii(EE_Dat[0],&XmtBuf[0]);
			iHexAscii(EE_Dat[1],&XmtBuf[4]);
			iHexAscii(EE_Dat[2],&XmtBuf[8]);
			iHexAscii(EE_Dat[3],&XmtBuf[12]);
			XmtCnt=17;	
		}
		else{
			XmtBuf[0]=0x03;				//Abschlusszeichen ETX (03h) laden
			XmtCnt=2;	
			Xmtmd=0;
		}
		SendeAntwort();
	}

	else if(MSNeu){						// XmtOut: verpackte Messdaten senden
		MSNeu=0;
		XmtOut=0;
		mscnt=0;
		MessdatenVerpacken();
		iHexAscii(EE_Dat[0],&XmtBuf[0]);
		iHexAscii(EE_Dat[1],&XmtBuf[4]);
		iHexAscii(EE_Dat[2],&XmtBuf[8]);
		iHexAscii(EE_Dat[3],&XmtBuf[12]);
		XmtCnt=17;	
		SendeAntwort();
	}
}
/******************************************************************************
* Funktion: COM_Auswertung
* Beschreibung: Empfangenen Befehl decodieren und auswerten
******************************************************************************/
void COM_Auswertung(void) 
{
	unsigned int i;

	i=RcvBuf[0]*256+RcvBuf[1];		
	switch (i) 
	{ 

//		case 'aa': Befehlaa();			//#### Test Befehl
//			return;
//		case 'ab': Befehlab();			//#### Test Befehl
//			return;
//		case 'br': Befehlbr();			//#### Test Befehl
//			return;

		case 'bn': Befehlbn();			//Artikel-Nr.
			return;
		case 'gt': Befehlgt();			//Ger�tetemperatur
			return;
		case 've': Befehlve();			//Ger�teversion (Ger.-ID/SW-Monat,SW-Jahr)
			return;
		case 'em': Befehlem();			//Emisionsgrad
			return;
		case 'md': Befehlmd();			//alle belegten Datens�tze (Memory) lesen
			return;
		case 'ms': Befehlms();			//Messwert lesen
			return;
		case 're': Befehlre();			//Ger�te-Reset
			return;
		case 'rw': Befehlrw();			//Rohwerte lesen (kurze Ti / lange Ti)
			return;
		case 'ul': Befehlul();			//Datum + Uhrzeit lesen
			return;	
		case 'us': Befehlus();			//Datum + Uhrzeit schreiben
			return;
		case 'KE': BefehlKE();			//KE geht immer
			return;
		case 'KS': BefehlKS(); 			//KS geht immer
			return;
	}
	if(KaliEin){ 						//Kalibrierbefehle
		switch(i){ 		
			case 'WR': BefehlWR();		//Daten in EEPROM/Flash schreiben
				return;	
			case 'RD': BefehlRD();		//Daten aus EEPROM/Flash lesen
				return;
			case 'KV': BefehlKV();		//Range DDC114 stellen (Integrationskondensatoren)
				return;
			case 'RM': BefehlRM();		//Rohwerte (gemittelt) lesen (kurze Ti / lange Ti) 
				return;
			case 'RW': BefehlRW();		//Rohwerte int. ADW lesen (Batteriespannung/NTC Ger.-Temp)
				return;
		}
	}	

	KeineAntwort(); 						//Befehl wird nicht unterst�tzt
}
/******************************************************************************
* Funktion: Befehlaa, Befehlbr 
* Beschreibung: zum Testen
******************************************************************************/
//#### nur zum Test

void Befehlaa(void)
{
//#### Test
	if(Test3==0)
		Test3=1;
	else
		Test3=0;
	XmtBuf[0]=Test3+0x30;
	XmtCnt=2;	
	SendeAntwort();

}

void Befehlab(void)
{
	if(RcvBuf[2]==CR){					//abfragen	

		iHexAscii(GrenzeMin,&XmtBuf[0]);
		iHexAscii(GrenzeMax,&XmtBuf[4]);
		cHexAscii(MenuAktuell,&XmtBuf[8]);
		cHexAscii(StJahr,&XmtBuf[10]);

		
		XmtCnt=13;	
		SendeAntwort();
	}
	else{
		ZwCount=0x100*AsciiHex(RcvBuf[2],RcvBuf[3])+AsciiHex(RcvBuf[4],RcvBuf[5]);
		iHexAscii(ZwCount,&XmtBuf[0]);
		XmtCnt=5;	
		SendeAntwort();
	}
}


void Befehlbr(void)
{
//	cHexAscii(Testc_.Tp,&XmtBuf[0]);
//	cHexAscii(Testc_.Up,&XmtBuf[2]);
//	cHexAscii(Testc_.Hi,&XmtBuf[4]);
//	cHexAscii(Testc_.Lo,&XmtBuf[6]);
	XmtCnt=9;	
	SendeAntwort();

//	XmtBuf[0]='4';
//	XmtCnt=2;	
//	SendeAntwort();

}
/******************************************************************************
* Funktion: Befehlbn 
* Beschreibung: Artikelnr. senden
******************************************************************************/
void Befehlbn(void)
{
	unsigned char c;

	if (RcvBuf[2]==CR){	
		EEpromRead(EE_ArtnrL,1,&ZwInt);
		//ZwInt=0x1876;
		XmtBuf[0]=ZwInt/16;
		ZwInt-=XmtBuf[0]*16;
		XmtBuf[1]=ZwInt;		

		EEpromRead(EE_ArtnrH,1,&ZwInt);
		//ZwInt=0x003A;
		XmtBuf[2]=ZwInt/4096;
		ZwInt-=XmtBuf[2]*4096;
		XmtBuf[3]=ZwInt/256;
		ZwInt-=XmtBuf[3]*256;
		XmtBuf[4]=ZwInt/16;
		ZwInt-=XmtBuf[4]*16;
		XmtBuf[5]=ZwInt;

		for(c=0;c<6;c++){
			if(XmtBuf[c]<10)
				XmtBuf[c]+=0x30;
			else
				XmtBuf[c]+=0x37;
		}
		XmtCnt=7;	
		SendeAntwort();
	}
	else
		SendeNo();
}
/******************************************************************************
* Funktion: Befehlem 
* Beschreibung: Emissionsgrad abfragen, Grenzen abfragen, setzen
* Antwort 4-stellig Ascii-Dez, das Setzen ist auch 2-stellig m�glich (00=1000) 
* Bsp.: em1000 setzt Emi auf 1, em 95 setzt Emi auf 0,95
******************************************************************************/
void Befehlem(void)
{
	unsigned int i;
	if (RcvBuf[2]==CR){					//aktuelle Einstellung abfragen	
		HexAsc4(Emi,&XmtBuf[0]);
		XmtCnt=5;	
		SendeAntwort();
	}
	else if(RcvBuf[2]=='?'){			//Grenzen abfragen 				
		HexAsc4(EmiMin,&XmtBuf[0]);
		HexAsc4(EmiMax,&XmtBuf[4]);
		XmtCnt=9;	
		SendeAntwort();
	}
	else if(RcvBuf[2]>=0x30&&RcvBuf[2]<=0x39&&RcvBuf[3]>=0x30&&RcvBuf[3]<=0x39) //setzen
	{
		i=(int)(RcvBuf[2]-0x30)*100+(RcvBuf[3]-0x30)*10;	//Eingabe 2-stellig
		if(RcvBuf[4]==CR){
			if (i==0)						
				i=1000;
		}
		else if(RcvBuf[6]==CR)
			i=i*10+(RcvBuf[4]-0x30)*10+RcvBuf[5]-0x30; 		//Eingabe 4-stellig
		else{
			SendeNo();
			return;
		}
		if(i>=EmiMin&&i<=EmiMax){
			if(i!=Emi){
				Emi=i;
				EEpromWriteVerify(EE_Emi,1,&Emi);	// ins EEprom schreiben
				if(ErrEEprom==1){		
					SendeNo(); 			//EEprom Schreibfehler
					return;
				}
				SetupEmiNk(Emi);	//OK, Faktor berechnen 
			}
			SendeOk();
		}
		else
			SendeNo();
	}
}
/******************************************************************************
* Funktion: Befehlgt 
* Beschreibung: Ger�tettemperatur ausgeben (Grad C * 256, 0x0000...0xFFFF)
******************************************************************************/
void Befehlgt(void) 
{
	if(RcvBuf[2]==CR){	
		iHexAscii(GerTemp,&XmtBuf[0]);
		XmtCnt=5;	
		SendeAntwort();
	}
	else
		SendeNo();
}
/******************************************************************************
* Funktion: Befehlmd 
* Beschreibung: alle belegten Datens�tze (max. 4000) aus Datenspeicher (Memory)
* lesen und senden 
* jeder Datensatz besteht aus 8 Byte im gepacktem Format = 16 Zeichen Ascii-Hex
* Geraet sendet:STX (02h)
*				<Daten aller belegten Adressen> / je + CR
*				ETX (03h)
* Leere Speicherstellen werden nicht ausgegeben.
******************************************************************************/
void Befehlmd(void)
{
	Xmtmd=1;							//setzen
	mscnt=4002;							//Anzahl Werte + Startzeichen + Stoppzeichen
	RCVBefehl=0;						//Fertig. Das Senden erfolgt in -> main
}
/******************************************************************************
* Funktion: Befehlms 
* Beschreibung: Messwerte senden. An dieser Stelle wird der Messwertez�hler
* geladen, das Senden erfolgt sp�ter. 
******************************************************************************/
void Befehlms(void)
{
	unsigned int i;

	if (KaliEin)
		SendeNo();
	else{
		i=RechAnzahlWerte();
		if (i>0&&i<1000){
			mscnt=i;					//Anzahl Werte laden
			Xmtms=1;					//Anforderungsflag setzen
			RCVBefehl=0;				//Fertig. Das Senden erfolgt in -> main
		}
		else
			SendeNo();
	}
}
/******************************************************************************
* Funktion: Befehlre 
* Beschreibung: Softwarereset ausf�hren und anschl. 'OK' senden 
******************************************************************************/
void Befehlre(void)
{
	SwReset=1;
	SendOK=1;
	KeineAntwort();	
}
/******************************************************************************
* Funktion: Befehlrw 
* Beschreibung: ungemittellte Rohwerte senden  
******************************************************************************/
void Befehlrw(void)
{
	Xmtrw=1;							// Anforderungsflag setzen
	mscnt=1;							// Einen Wert senden
	RCVBefehl=0;						// Das Senden erfolgt in -> MesswertSenden
}
/******************************************************************************
* Funktion: Befehlul 
* Beschreibung: Datum und Uhrzeit senden 
* 				Format: ulTTMMJJHHMMSS (Tag,Monat,Jahr,Stunde,Minute,Sekunde)
*				(24-Stumden-Modus)
******************************************************************************/
void Befehlul(void)
{
	SpiReadRtc();					//Uhrdaten lesen
	XmtBuf[0]='u';
	XmtBuf[1]='l';
	HexAsc2(RtcTag,&XmtBuf[2]);
	HexAsc2(RtcMon,&XmtBuf[4]);
	HexAsc2(RtcJahr,&XmtBuf[6]);
	HexAsc2(RtcStd,&XmtBuf[8]);
	HexAsc2(RtcMin,&XmtBuf[10]);
	HexAsc2(RtcSek,&XmtBuf[12]);
	XmtCnt=15;	
	SendeAntwort();
}
/******************************************************************************
* Funktion: Befehlus 
* Beschreibung: Uhr stellen, immer 24-Stunden-Modus
* Handgeraet empfaengt:  usTTMMJJHHMMSS<CR (BCD, ungepackt)
* Handgeraet sendet:	ulTTMMJJHHMMSS<CR> (BCD, ungepackt)
*				Tag: 	1...31
*				Monat: 	1...12
*				Jahr: 	0...63
*				Stunde: 0...23 	
*				Minute: 0...59
*				Sekunde:0...59
* ACHTUNG: Hier sind gesendeter und empfangener Befehl nicht identisch !!!
* Zuerst werden die empfangenen Daten in den Uhr-Buffer geschrieben
* Danach wird der Uhr-Buffer in die Uhr geschrieben.
* Der Befehl ul (Uhrzeit lesen/senden) wird ausgefuehrt.
* Bei Fehleingabe wird 'no' gesendet.
******************************************************************************/
void Befehlus(void)
{
	unsigned char c;
	for(c=2;c<13;c++){					//Nur Dezimalzahlen (0..9) erlaubt
		if(RcvBuf[c]<0x30||RcvBuf[c]>0x39){
			SendeNo();
			return;						//Fehler ->
		}
	}									
	StTag=(RcvBuf[2]-0x30)*10+RcvBuf[3]-0x30;	//Uhrdaten ins Hexformat wandeln
	StMon=(RcvBuf[4]-0x30)*10+RcvBuf[5]-0x30;	//und zwischenspeichern
	StJahr=(RcvBuf[6]-0x30)*10+RcvBuf[7]-0x30;
	StStd=(RcvBuf[8]-0x30)*10+RcvBuf[9]-0x30;
	StMin=(RcvBuf[10]-0x30)*10+RcvBuf[11]-0x30;
	StSek=(RcvBuf[12]-0x30)*10+RcvBuf[13]-0x30;

	if(StTag==0||StTag>31||StMon==0||StMon>12||StJahr>63||StStd>23||StMin>59||StSek>59)
		SendeNo();						//Bereichsfehler ->
	else{				
		SpiWriteRtc(wrday,StTag);		//Daten ins rtc-Modul schreiben
		SpiWriteRtc(wrmonat,StMon);
		SpiWriteRtc(wrjahr,StJahr);
		SpiWriteRtc(wrstunde,StStd);
		SpiWriteRtc(wrminute,StMin);
		SpiWriteRtc(wrsekunde,StSek);
		SpiReadRtc();					//Uhrdaten zur�cklesen
		if(CE1305)						//warten bis fertig
			;
		Befehlul();						//Daten senden
	}
}
/******************************************************************************
* Funktion: Befehlve 
* Beschreibung: Geraete-Kennung ausgeben
* Geraet empfaengt:		ve<CR>
* Geraet sendet:		ve+<Geraete-ID>
******************************************************************************/
void Befehlve(void)
{
	if(RcvBuf[2]==CR){
		XmtBuf[0]='v';
		XmtBuf[1]='e';
		cHexAscii(GeraeteID,&XmtBuf[2]);
		XmtCnt=5;	
		SendeAntwort();
		CountHold=THold;				// Hold-Time neu starten	
	}
	else
		KeineAntwort();
}
/******************************************************************************
* Funktionen: BefehlKE
* Beschreibung: Kalibriermodus beenden (Reset ausf�hren und anschl. OK senden)
* Geraet empfaengt:		KE<CR>
* Geraet sendet:		ok<CR>
******************************************************************************/
void BefehlKE(void) 
{
	if(KaliEin){
		KaliEin=0;
		ReqAxM=1;		//'KAL' Anzeige
	}
	else{
		IntCal=0;
		ReqCtHdBt=1;	//'CAL' Anzeige
	}
	Befehlre();
}

/******************************************************************************
* Funktionen: BefehlKS
* Beschreibung: Ger�t in den Kalibriermodus setzen
* Geraet empfaengt:		KS<CR>
* Geraet sendet:		<Ger�te-ID><Softwareversion><CR>
******************************************************************************/
void BefehlKS(void) 
{
	KBDNoMenu();						// Men� verlassen falls aktiv
	EnabGrf=0;							// Grafikmode abschalten
	KaliEin=1;
	IntCal=1;
	ReqCls=1;							// CLS  
	ReqAxM=1;							// 'KAL' anzeigen
	ReqCtHdBt=1;
	cHexAscii(GeraeteID,&XmtBuf[0]);
	iHexAscii(SoftVers,&XmtBuf[2]);
	XmtCnt=7;	
	SendeAntwort();
}
/******************************************************************************
* Funktion: BefehlKV
* Beschreibung: Abfragen / Setzen verschiedener Schalter. Bisher: 
* Integrations-Kondensatoren (Range 0,1,2) DDC114 abfragen / schalten
* Geraet empfaengt: KV0[XXYY]<CR>
* Geraet sendet:	ok<CR> / XXYY<CR> 
*
*	Range	R2	R1	R0	pC		YY
*	0		0	0	0	3		xxxxxxx000
*	1		0	0	1	12,5	xxxxxxx001
*	2		0	1	0	25		xxxxxxx010
*	3		0	1	1	37,5	xxxxxxx011
*	4		1	0	0	50		xxxxxxx100
*	5		1	0	1	62,5	xxxxxxx101
*	6		0	1	1	75		xxxxxxx110
*	7		1	1	1	87,5	xxxxxxx111
******************************************************************************/
void BefehlKV(void)
{
	unsigned char c;
	if(RcvBuf[2]=='0'){					//KV0 ?
		if(RcvBuf[3]==CR){				//Abfragen
			c=0;
			cHexAscii(c,&XmtBuf[0]);
			c=0;		
			c|=Range0;
			c|=Range1*2;
			c|=Range2*4;
			cHexAscii(c,&XmtBuf[2]);
			XmtCnt=5;	
			SendeAntwort();
		}
		else if(RcvBuf[7]==CR){			//Setzen
			c=RcvBuf[6]-'0';			
			Range0=0x01&c;
			Range1=0x01&(c/2);
			Range2=0x01&(c/4);	
			Range=Range0+Range1*2+Range2*4;	//Range=0...7
			SendeOk();
		}
		else							//nein, Fehler
			SendeNo();
	}
	else
		SendeNo();						//nein, Fehler
}
/******************************************************************************
* Funktion: BefehlWR
* Beschreibung: 1 bis 4 Word Daten ins Flash bzw. EEPROM schreiben 
* Eingang: [RcvBuf] WRNNXAAAdddd[dddddddddddd] 
*					NN		Anzahl Word zu Schreiben (1...4)
*					X		Ziel, E=EEPROM  F=Flash
*					AAA		Adresse auf Wordbasis 
*					dddd	Daten 
******************************************************************************/
void BefehlWR(void)
{
	unsigned char count;
	unsigned char a,c;
	unsigned int adr;

	count=AsciiHex(RcvBuf[2],RcvBuf[3]);
	adr=256*AsciiHex(0,RcvBuf[5])+AsciiHex(RcvBuf[6],RcvBuf[7]);
	if(count>8||count==0||RcvBuf[8+4*count]!=CR){
		SendeNo();						// Fehler
		return;
	}	
	for(c=0;c<count*4;c++){	
		if(RcvBuf[8+c]>0x60)			// Kleinbuchstabe => Grossbuchstabe
			RcvBuf[8+c]-=0x20;
		a=RcvBuf[8+c];	
		if(a<'0'||a>'F'||(a>'9'&&a<'A')){
			SendeNo();					// Fehler
			return;
		}
		if(c==3||c==7||c==11||c==15||c==19)	// je 4 Asciizeichen zu einem Word nach EE_Dat laden
			EE_Dat[(c-3)/4]=256*AsciiHex(RcvBuf[c+5],RcvBuf[c+6])+AsciiHex(RcvBuf[c+7],RcvBuf[c+8]);
	}
	if(RcvBuf[4]=='E'){
		EEpromWriteVerify(adr*2+EE_Offset,count,&EE_Dat[0]);	//EEprom: adr=0x000...0x180 (pysik. 7D00...7FFF)
		if(ErrEEprom){
			SendeNo();
			return;
		}
	}
	else if(RcvBuf[4]=='F'){
		FlashWriteVerify(adr*2+FL_Offset,count,&EE_Dat[0]); 	//Flash: adr=0x000...0xC00
		if(ErrFlash){
			SendeNo();
			return;
		}
	}
	else{
		SendeNo();
		return;
	}
	a=count*4+4;
	for(c=0;c<a;c++)			
		XmtBuf[c]=RcvBuf[c+4];
	XmtCnt=a+1;
	SendeAntwort();
}
/******************************************************************************
* Funktionen: BefehlRD
* Beschreibung: Daten aus Flash / EEPROM lesen und senden
* Eingang: 		RDXXYZZZ + CR
*				XX 		Anzahl Word zu lesen (0...255, 0=256)
*				Y		EEPROM / Flash Auswahl (E=EEPROM, F=Flash)
*				ZZZ		Adresse ab der gelesen wird (000...FFF)		
******************************************************************************/
void BefehlRD(void)
{
	unsigned int count;
	unsigned int adr;
	unsigned char max;
	unsigned char c,d;

	count=AsciiHex(RcvBuf[2],RcvBuf[3]);
	if(count==0)
		count=256;							//0 = 256 
	adr=256*AsciiHex(0,RcvBuf[5])+AsciiHex(RcvBuf[6],RcvBuf[7]);
	max=(sizeof XmtBuf-1)/4;

	while(count>0){
		c=(count>max)? max:count;			//Zu grosse Sendungen teilen
		if(RcvBuf[4]=='E')					//EEPROM lesen
			EEpromRead(adr*2+0x7D00,c,&EE_Dat[0]);	
		else if(RcvBuf[4]=='F')				//Flash lesen
			FlashRead(adr*2+0xA000,c,&EE_Dat[0]);
		else{
			SendeNo();
			return;
		}
		count=count-c;
		for(d=0;d<c;d++)
			iHexAscii(EE_Dat[d],&XmtBuf[d*4]);
		XmtCnt=c*4;
		if(count>0){						//Sendung ohne CR
			adr+=c;
			XmtBufCnt=0;
			IFS1bits.U2TXIF=1;				//Senden starten
			IEC1bits.U2TXIE=1;
			while(IEC1bits.U2TXIE)			//warten			
				;
		}
		else{
			XmtCnt++;						//letze Sendung mit CR
			SendeAntwort();
		}
	}
}	
/******************************************************************************
* Funktionen: BefehlRM
* Beschreibung: sendet gemittelte Rohwerte (1024 Werte, ca 1 Sek.)
* Ausgabe AAAAAABBBBBB + CR
* Beide Rohwerte minus Offset, ca. 0x000000...0x0FEFFF
*	AAAAAA = Rohwert Ti kurz  
*	BBBBBB = Rohwert Ti lang
******************************************************************************/
void BefehlRM(void)
{
	XmtRM=1;							// gemittelte Rohwerte senden
	mscnt=1;							// Einen Wert senden
	RCVBefehl=0;						// Das Senden erfolgt in -> MesswertSenden
}
/******************************************************************************
* Funktionen: BefehlRW
* Beschreibung: Rohwerte der internen Wandlerkan�le A0 und A1  lesen.
* die Rohwerte werden im DMA1 IRQ gelesen
* Daher werden die Rohwerte durch 4 geteilt.
* Ausgabe: 8 Zeichen Ascii-Hex, je 4 pro Kanal. 
*	0AAA = Rohwert RW0 0000...03FF	(Batteriespannung) 
*	0BBB = Rohwert RW1 0000...03FF	(NTC,Ger�tetemperatur)
******************************************************************************/
void BefehlRW(void)
{
	iHexAscii(RWAn0/4,&XmtBuf[0]);
	iHexAscii(RWAn1/4,&XmtBuf[4]);
	XmtCnt=9;	
	SendeAntwort();
}
/******************************************************************************
* Funktionen: SendeNo, SendeOk, SendeAntwort, KeineAntwort
* Beschreibung: M�gliche Antworten auf einen UPP-Befehl 
******************************************************************************/
void SendeNo(void)
{ 
	XmtBuf[0]='n';
	XmtBuf[1]='o';
	XmtCnt=3;	
	SendeAntwort();
}

void SendeOk(void)
{ 
	XmtBuf[0]='o';
	XmtBuf[1]='k';
	XmtCnt=3;	
	SendeAntwort();
}

void SendeAntwort(void)
{
	XmtBuf[XmtCnt-1]=CR;
	XmtBufCnt=0;
	RCVBefehl=0;			//Ende Befehlsauswertung

	CLRWDT();
	while(IEC1bits.U2TXIE)			//Warten falls noch gesendet wird
		;
	IFS1bits.U2TXIF=1;				//dann senden
	IEC1bits.U2TXIE=1;
}

void KeineAntwort(void)
{
	RCVBefehl=0;					//Befehlsflag l�schen
	XmtCnt=0;						//Nichts senden 	
} 
/******************************************************************************
* Funktion: RechAnzahlWerte
* Beschreibung: Berechnung der Anzahl zu sendender Werte (1...999) aus den
* Daten im Empfangspuffer f�r ms, rw Befehl. 
******************************************************************************/
int RechAnzahlWerte(void)
{
	if (RcvBuf[2]==CR)
		return(1);
	else if (RcvBuf[5]==CR)
		return((unsigned int)100*(RcvBuf[2]-0x30)+(unsigned int)10*(RcvBuf[3]-0x30)+(unsigned int)RcvBuf[4]-0x30);
	else
		return(0);
}
/******************************************************************************
* Funktionen: EEpromWriteVerify
* Beschreibung: Schreiben und Verifizieren von Daten ins EEPROM 
* Eingang: 	adr = physikalische (Byte)Adresse im EEPROM 
* 			count = Anzahl Word zu schreiben
*			ptr = RAM-Adresse der zu schreibenden Daten
* Bei 1. Schreibfehler nochmal schreiben
* Bei 2. Schreibfehler Fehlerflag schreiben
******************************************************************************/
void EEpromWriteVerify(unsigned int adr,unsigned int count,unsigned int *ptr)
{
	ErrEEprom=0;
	EEpromWrite(adr,count,ptr);	
	if(EEpromVerify(adr,count,ptr))
	{
		EEpromWrite(adr,count,ptr);
		if(EEpromVerify(adr,count,ptr))
		{
			ErrEEprom=1;
			ErrEEpromCnt++;
			EEpromWrite(EE_ErrStatus,1,&ErrStatus);
		}
	}
}
/******************************************************************************
* Funktion: FlashWriteVerify
* Beschreibung: Schreiben und Verifizieren von Daten im Flash
* Eingang: 	adr = physikalische (gradzahlige) Adresse im Flash 
* 			count = Anzahl Word zu schreiben
*			ptr = RAM-Adresse der zu schreibenden Daten
* Bei Schreibfehler Fehlerflag in EEPROM schreiben
* Da nach einem Schreibfehler erst Aufwendig ein 512-Word Block gel�scht
* werden muss, erfolgt hier nur 1 Schreibversuch.
******************************************************************************/
void FlashWriteVerify(unsigned int adr, unsigned char count, unsigned int *ptr)
{
	ErrFlash=0;
	FlashWrite(adr,count,ptr);
	if(FlashVerify(adr,count,ptr))
	{
		ErrFlash=1;
		EEpromWrite(EE_ErrStatus,1,&ErrStatus);
	}
}

/****** START OF INTERRUPT SERVICE ROUTINES *********/

/******************************************************************************
* DMA0 SPI1 Transmit (LCD, RTC)
******************************************************************************/
//#### nur testweise SPI1 Interupt, wegen DMA0 
void __attribute__((__interrupt__,auto_psv)) _SPI1Interrupt(void)
{ 
		IFS0bits.SPI1IF=0; 
}
//#### hier DMA0 -> funktioniert nicht !
void __attribute__((__interrupt__,auto_psv)) _DMA0Interrupt(void) 
{
		IFS0bits.DMA0IF=0; 				//Clear the DMA0 Interrupt Flag
		CS_LCD=1;						//Dissable LCD
		CS_LCD=1;						//Dissable LCD
		CE1305=0;						//Disable RTC
}
/******************************************************************************
* DMA1 (ADC1, 2 Kan�le, (AN0, AN1) beide Kan�le werden gleitend gemittelt
* IRQ ca. alle 64 x 312.44 �s = 20ms (DMA1CNT = 63, ADC-Intervall 312.44 �s) 
* Format ungemittelt: 10 Bit
* RW AN0 (RB0): Batteriespannung
* RW AN1 (RB1): NTC (KTY82), Innentemperatur
******************************************************************************/
void __attribute__ ((__interrupt__,auto_psv)) _DMA1Interrupt(void)
{
	if(CntAnalog==0){
		RWAn0=4*BufferA.Adc1Ch0[0];			//Init nach dem Einschalten
		RWAn1=4*BufferA.Adc1Ch1[0];
		RechGerTemp();						//1. mal Ger�tetemperatur berechnen
		RechUBat();							//1. mal Batteriespannung berechnen
	}
	else{
		RWAn0-=RWAn0/4;						//4-fache Mittelung
		RWAn1-=RWAn1/4;						//4-fache Mittelung			
		RWAn0+=BufferA.Adc1Ch0[0];
		RWAn1+=BufferA.Adc1Ch1[0];
	}
	CntAnalog++;				//Intervall Ger�tetemperatur/Batteriespannung
	IFS0bits.DMA1IF=0;
}
/******************************************************************************
* DMA2 SPI Recieve (RTC-Daten)
* Es Werden 7 Byte Daten empfangen, Byte Nr. 3 (Wochentag) verbleibt ungenutzt.
******************************************************************************/
//void __attribute__((__interrupt__,auto_psv)) _DMA2Interrupt(void) 
//{
//	RtcSek=TxRSpi1Buf[0]/16*10+(TxRSpi1Buf[0]&0x0F);	//Da die Daten der Uhr
//	RtcMin=TxRSpi1Buf[1]/16*10+(TxRSpi1Buf[1]&0x0F);	//im gepacktem BCD-Format
//	RtcStd=TxRSpi1Buf[2]/16*10+(TxRSpi1Buf[2]&0x0F);	//empfangen werden,
//	RtcTag=TxRSpi1Buf[4]/16*10+(TxRSpi1Buf[4]&0x0F);	//ist eine Umwandlung
//	RtcMon=TxRSpi1Buf[5]/16*10+(TxRSpi1Buf[5]&0x0F);	//in das Standard Hex-
//	RtcJahr=TxRSpi1Buf[6]/16*10+(TxRSpi1Buf[6]&0x0F);	//Format erforderlich.
//	CE1305=0;											//Chip disable RTC
//	IFS1bits.DMA2IF=0; 
//}
/******************************************************************************
* UART Recieve
******************************************************************************/
void __attribute__ ((__interrupt__,auto_psv)) _U2RXInterrupt(void)
{
	unsigned char c;

	c=U2RXREG;							//Zeichen lesen
	IFS1bits.U2RXIF=0;					//IF zur�cksetzen

//	if (!RcvUSART2Err)					//Kein Fehler ?

//	if(U2STAbits.UTXEN==0){			//klappt so nicht ! Wird noch gesendet ? -> Fehler
		if(U2STAbits.PERR==0){			//Parity Error: Parit�tsfehler
			if(U2STAbits.OERR==0){		//Overrun Error: 4 Byter FIFO ist voll
				if(U2STAbits.FERR==0){	//Framing Error: Stop Bit = 0
					if(RCVBefehl==0){	//Wird noch ein Befehl verarbeitet ? -> Fehler
						if(RcvCnt<=sizeof(RcvBuf)){	//Ist Empfangspuffer voll ? -> Fehler	
							RcvBuf[RcvCnt++]=c;
							if(c==CR){	//Endekennungszeichen (CR) empfangen ?
								RCVBefehl=1;		//ja
								RcvCnt=0;					
							}
							return;
						}
					}
				}
			}
		}
//	}

//Empfangfehler
	U2MODEbits.UARTEN=0;			//UART aus -> Komplettreset
	RcvCnt=0;						//Reset Bufferzaehler
	U2MODEbits.UARTEN=1;			//UART wieder ein
	U2STAbits.UTXEN=1;	
}
/******************************************************************************
* UART Transmit
******************************************************************************/
void __attribute__ ((__interrupt__,auto_psv)) _U2TXInterrupt(void)
{
	IFS1bits.U2TXIF=0;

	if(XmtCnt==0){
		while(!U2STAbits.TRMT);			//Warten bis TSR leer
		IEC1bits.U2TXIE=0;
	}
	else{
		XmtCnt--;
		U2TXREG=XmtBuf[XmtBufCnt++];
	}
}
/******************************************************************************
* Output Compare 1
* Ansteuerung des DDC114 durch togglen des CONV-Pin
* Clocksource ist Timer2 
* Erreicht Timer2 den Wert von 0C1R, erfolgt im Hintergrund das Conv-Toggle (OC1, Pin RD0)
* und der IRQ l�st den Sprung hierhin aus. 
* Timer2 wird zur�ckgesetzt (auf 0 gesetzt) sobald er den Wert von PR2 erreicht.  
* Da PR2=OC1R erfolgen beide Aktionen zeitgleich. 
******************************************************************************/
void __attribute__((__interrupt__,auto_psv)) _OC1Interrupt(void)
{
	IFS0bits.OC1IF=0;

	switch(++IntCnt)
	{
		case 1: 
			PR2=TmKurz;
			TiKurz=1;
//Hier muss High Pegel anliegen sonst Neu-Ini DDC114
			asm ("btss PORTD,#0");		//if(OC1==0) 
			DDCInit=1;
			break;
		case 2: 
			PR2=TmLang;
			break;
		case 3: 
			PR2=TmWait1;
			break;
		case 4: 
			PR2=TmWait2;
			IntCnt=0;
			break;
	}
}
/******************************************************************************
* Timer1 to PR1 Overflow Interrupt 
* DotMatrix Intervall 
******************************************************************************/
void __attribute__((__interrupt__,auto_psv)) _T1Interrupt(void)
{
//Timer1 l�uft durch 
//	T1CONbits.TON=0; 					//Timer1 ausschalten

	IFS0bits.T1IF=0;					//Clear IF

	if(PR1==TSLROn){					//Ein-Zeit abgel.
		BL2016=0;						//DotMatrix ausschalten
		PR1=TSLROff;
	}
	else{								//Aus-Zeit abgelaufen:	
		PR1=TSLROn;
//		if(Test3!=0){
			asm ("btsc _AuxStatus_,#7");	// DotMatrix einschalten BL2016=Zw2016
			BL2016=1;		
			asm ("btss _AuxStatus_,#7");
			BL2016=0;		
//		}
	}

//	T1CONbits.TON=1; 				// Timer1 starten
}
/******************************************************************************
* Timer4 to PR4 Overflow Interrupt 
* Summer ausschalten 
******************************************************************************/
void __attribute__((__interrupt__,auto_psv)) _T4Interrupt(void)
{
	OC2CON=0;					//Summer ausschalten	
	IFS1bits.T4IF=0;			//Clear IF		
	T4CONbits.TON=0; 			//Timer4 ausschalten
	TMR4=0;						//Timer Reset (sollte eigentlich selbst resetten ?) 
}
/******************************************************************************
* OC2 IRQ
* Nicht aktiv, IRQ ist dissabled. 
******************************************************************************/
//void __attribute__((__interrupt__,auto_psv)) _OC2Interrupt(void)
//{
//	IFS0bits.OC2IF=0;
//}
/******************************************************************************
* Data Valid DDC114 (External Interrupt 1, RD8)
* Intervall: 1000�s
* Lesen des ADW-Signals per SPI2, Clock = 9,216 MHz (max.= 16MHz)
* Ben�tigt wird nur der erste Kanal (20 Bit) 
* Das Signal liegt im Bereich 0x00000h bis 0xFFFFFh
* Der Wandleroffset von ca. 4096 wird abgezogen, RW => -4096...+1044479
* Durch die �bertragung von MSB als erstes Bit landen die 20Bit linksb�ndig
* im 32 Bit Rohwertspeicher und m�ssen daher 12 mal rechts geschoben werden. 
* Die Rohwertspeicher (IntRwKurz,IntRwLang) sind als unsigned definiert !
******************************************************************************/
void __attribute__((__interrupt__,auto_psv)) _INT1Interrupt(void)
{					
	IFS1bits.INT1IF=0;					//Clear the External Interrupt 1 status flag

	if(TiKurz){	
		TiKurz=0;
		SPI2BUF=1;						// Bits 0...15
		while(!SPI2STATbits.SPIRBF);	// wait
		IntRwKurz_.WHi=SPI2BUF;			
		SPI2BUF=2;						// Bits 16...31
		while(!SPI2STATbits.SPIRBF);	// wait
		IntRwKurz_.WLo=SPI2BUF;
		IntRwKurz>>=12;					// Rohwert arithm. schieben
		IntRwKurz-=RwOffKurz[Range];	// Wandleroffset abziehen
	}
	else{
		SPI2BUF=1;						// Bits 0...15
		while(!SPI2STATbits.SPIRBF);	// wait
		IntRwLang_.WHi=SPI2BUF;			
		SPI2BUF=2;						// Bits 16...31
		while(!SPI2STATbits.SPIRBF);	// wait
		IntRwLang_.WLo=SPI2BUF;
		IntRwLang>>=12;					// Rohwert arithm. schieben
		if(OVLang){						// �berlaufflag lange Ti einstellen
			if(IntRwLang<=950000)		// Hysterese nach �berlauf  		
				OVLang=0;				// (angenommene Linearit�tsgrenze)
		}
		else{
			if(IntRwLang>=975000) 
				OVLang=1;
		}
		IntRwLang-=RwOffLang[Range];	// Wandleroffset abziehen

//1ms Intervall hier
		PR1=TSLRSt;
		TMR1=0x00;						// 
		BL2016=0;						// DotMatrix ausschalten


		AdwNeu=1;						// Flag, neuer Rohwert
		CntIntMs++;						// Intervall Messdaten EEPROM Speichern/Senden
		CntTastenAbfrage++;				// Intervall Tastenabfrage
		CntSegDisp--;
		CntSendeMesswert++;
		if(++Cnt1000MSec>999){			
			Cnt1000MSec=0;	
			if(++RtcSek>59){			//alle 60 Sek...
				RtcSek=0;				//
				ReqRtc=1;				//...RTC lesen
				if(++RtcMin>59){		// Minuten... 
					RtcMin=0;
					if(++RtcStd>23){	// und Stunden anpassen
						RtcStd=0;
					}
				}
			}
		}
	}
}
/******************************************************************************
* DefaultInterrupt
******************************************************************************/
void __attribute__((__interrupt__,auto_psv)) _DefaultInterrupt(void)
{		
	ZwInt=INTCON1;
	INTCON1=0;		//Reset Trap Error Flag
	EEpromWriteVerify(EE_TrapErr,1,&ZwInt);
}
/********* END OF INTERRUPT SERVICE ROUTINES ********/

//*********************************** ENDE ************************************

