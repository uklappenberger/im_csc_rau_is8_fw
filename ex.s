;-------------------------------------------------------------------------
;Programm: ex.s 
;Beschreibung: externe Assemblerdatei f�r IX8 / C30
;Datum: 19.06.09
;Autor: Johannes Koch
;Firma:	IMPAC
;-------------------------------------------------------------------------
;	include	"C:\Programme\Microchip\mcc18\src\traditional\math\CMATH18.INC"
;	include	"C:\Programme\Microchip\mcc18\src\traditional\math\AARG.INC"
;	include	"C:\Programme\Microchip\mcc18\src\traditional\math\BARG.INC"
;	include	"C:\Programme\Microchip\mcc18\src\traditional\math\TEMPARG.INC"

	.equ 		__24HJ64GP206, 1
	.list		.radix = DEC
	.include	"p24HJ64GP206.inc"
;-------------------------------------------------------------------------
;Definitionen von Konstanten
;-------------------------------------------------------------------------
	.equ	FontTab,	0x8000	;Start der Zeichensatztabellen
	.equ	TabTK,		0xA000	;TK-Tabelle 
	.equ	TabST,		0xA220	;St�tzstellentabelle
	.equ	TabLin,		0xA400	;Linearisierungstabelle 448 Word
;-------------------------------------------------------------------------
; Bootlader, Adressbereich 0x400 - 0x7FF
; 
; Befehle:
;
; RD_VER	0x00 Version lesen
; RD_MEM	0x01 Programmspeicher lesen
; WR_MEM	0x02 Write Program Memory
; ER_MEM	0x03 Erase Program Memory
; RD_EE		0x04 Read EEDATA Memory
; WR_EE		0x05 Write EEDATA Memory
;-------------------------------------------------------------------------
 	.section *,code,boot
  	;.global _SetupBoot
	
	.equ 	ST1,		0x7F
	.equ 	ST2,		0xB5

	.equ	RdCount,	0x850	
	.equ	CHKSUM,		0x852	
	.equ	COUNTER,	0x854		; Zaehler fuer Verschiedenes
	.equ	cipher,		0x856

	.equ 	DATA_BUFF,	0x860		; Startadresse Empfangspuffer
	.equ 	COMMAND,	DATA_BUFF	; Befehlsbyte
	.equ	DLEN,		DATA_BUFF+2	; Laenge des Datenbereichs
	.equ 	ADDRESS_L,	DATA_BUFF+4	; Adresse low-Byte
	.equ	ADDRESS_H,	DATA_BUFF+6	; Adresse high-Byte
	.equ	ADDRESS_U,	DATA_BUFF+8	; Adresse upper-Byte
	.equ	DATA_COUNT,	DATA_BUFF+10	; Beginn der Daten (nur schreiben)
	
_SetupBoot:
;#### Test 
;	goto	__resetPRI
;	goto	__reset			;nur zum Test: Bootlader verlassen -> Sprung nach Main	

;Dot Matrix als erstes abschalten (PORTG1=0)
	mov 	#0x80,w0		;TRISG
	mov 	w0,TRISG
	mov 	#0x0,w0			;LATG
	mov 	w0,LATG

	clr	INTCON1			;Clear Trap Error Flags
	bclr	INTCON1,#NSTDIS		;Nesting Interrupts enable,
	clr	INTCON2			;Standard vector table
	setm	AD1PCFGH		;ADW als digital I/O
	setm	AD1PCFGL

	mov 	#0x3fb,w0		;TRISB
	mov 	w0,TRISB
	mov 	#0x73f8,w0		;LATB
	mov 	w0,LATB
	mov 	#0x1002,w0		;TRISC
	mov 	w0,TRISC
	mov 	#0x0,w0			;LATC (Selbsthaltung erstmal aus)
	mov 	w0,LATC
	mov 	#0x300,w0		;TRISD
	mov 	w0,TRISD
	mov 	#0x200,w0		;LATD
	mov 	w0,LATD
	mov 	#0x14,w0		;TRISF
	mov 	w0,TRISF
	mov 	#0x0,w0			;LATF
	mov 	w0,LATF
 
	clr	SPI1STAT		;Setup Spi 1
	mov	#0x013F,w0		;8 Bit Modus, PPRE = 1:1, SPRE=1:1
	mov	w0,SPI1CON1
	clr	SPI1CON2
	mov	#0xC000,w0	
	mov	w0,SPI1STAT

;Bootword aus EEPROM lesen
	bclr	LATB,#12		;CS_EE=0;
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x03,w0		;0x03 = Read
	mov	w0,SPI1BUF
EE_Wait_1:
	btss	IFS0,#SPI1IF
	bra	EE_Wait_1
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x7D,w0		;Adresse Bootword High 7D
	mov	w0,SPI1BUF
EE_Wait_2:
	btss	IFS0,#SPI1IF
	bra	EE_Wait_2
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x68,w0		;Adresse Bootword Low 34h*2=68h
	mov	w0,SPI1BUF
EE_Wait_3:
	btss	IFS0,#SPI1IF
	bra	EE_Wait_3
	mov	SPI1BUF,w0		;Dummy read -> reset SPI1BUF
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x00,w0
	mov	w0,SPI1BUF
EE_Wait_4:
	btss	IFS0,#SPI1IF
	bra	EE_Wait_4
	mov	SPI1BUF,w1		;w1=Bootword Low
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x00,w0
	mov	w0,SPI1BUF
EE_Wait_5:
	btss	IFS0,#SPI1IF
	bra	EE_Wait_5
	mov	SPI1BUF,w2		;w2=Bootword High
	bset	LATB,#12		;CS_EE = 1
	sl	w2,#8,w2
	ior	w1,w2,w1		;Bootword nach w1

	mov	#0x0101,w2		;Ist es 0x0101 ?
	cpseq	w1,w2
	goto	__resetPRI		;Bootlader verlassen -> Sprung nach Main

; ab hier Ladeprogramm
	mov	#0x00E0,w0 		;Disable interrupts
	ior 	SR

	mov	#0x3B,w0		;9600 Baud einstellen
	mov	w0,U2BRG
	mov	#0xC006,w0		;9 Datenbits, No Parity, 1 Stoppbit, Brgh=0
	mov	w0,U2MODE

	mov	#0x5A,w0		;Startwert Schluesselbyte
	mov	WREG,cipher

RXreset:	
	mov	#0x0400,w0		;Reset und Uart Enable
	mov	w0,U2STA		;
	clr	RdCount			;
	clr	CHKSUM			;
	mov	#DATA_BUFF,w7		;Zeiger auf Daten-Puffer im RAM
	mov	#0xFF,w0
	mov	w0,COUNTER		;erstmal L�ngenz�hler auf Maximum
Autobaud:
	clrwdt
	bset	LATC,#2			;zur Sicherheit: Selbsthaltung ein	
	btss	IFS1,#U2RXIF		;Warten bis etwas auf UART empfangen wird
	bra	Autobaud

	mov	U2RXREG,w0		;Zeichen lesen
	bclr	IFS1,#U2RXIF

	xor.b	#ST1,w0			;erstes Startzeichen ? (0x7F)
	bra	nz,Autobaud	
	rcall	RdRs232
	xor.b	#ST2,w0			;zweites Startzeichen ? (0xB5)
	bra	nz,RXreset		;nein, Neustart

GetNextDat:	
	rcall	RdRs232			;n�chstes Zeichen nach w0 holen
	inc	RdCount
	add	CHKSUM			;Zeichen zu checksum addieren und ..
	mov	w0,[w7]			;zus�tzlich im Puffer ablegen
	inc2	w7,w7
	mov	RdCount,w1		
	cp	w1,#2			;2. Zeichen = Anzahl Daten
	bra	nz,NoCount
	mov	DLEN,w0	
	add	#5,w0
	mov	w0,COUNTER
NoCount:	
	dec	COUNTER
	bra	nz,GetNextDat

;alle im Laengenbyte angekuendigten Zeichen sind empfangen worden
	mov	CHKSUM,WREG		;checksum in Ordnung (0) ??
	and	#0xFF,w0
	bra	nz,RXreset		;nein, alles verwerfen

; Pruefsumme des Datensatzes in Ordnung, jetzt entschluesseln
	mov	COMMAND,WREG
	and	#0xF8,w0
	lsr	w0,#3,w0
	add	cipher
	mov	cipher,WREG
	mov	#ADDRESS_L,w7		;Zeiger auf Daten-Puffer im RAM
Entschl:
	mov	[w7],w1
	xor	w0,[w7],[w7]
	mov	#0xFF,w0		
	and	w0,[w7],[w7]
	inc2	w7,w7
	mov	w1,w0
	add	cipher,WREG
	dec	RdCount
	bra	nz,Entschl
	mov	#DATA_COUNT,w7

	cp0	DLEN			;Datenlaenge 0 ??
	btsc	SR,#Z
	reset				; ja, Neustart ausf�hren

; Befehlsbyte pruefen
	mov	COMMAND,WREG		;gueltiger Befehl (0..5) ??
	and	#7,w0
	bra	z,ReadVersion
	dec	w0,w0
	bra	z,ReadProgMem
	dec	w0,w0
	bra	z,WriteProgMem
	dec	w0,w0
	bra	z,EraseProgMem
	dec	w0,w0
	bra	z,ReadEE
	dec	w0,w0
	bra	z,WriteEE
	bra	RXreset			; nein, Empfang neu starten
;------------------------------------------------------------------------------
;Version des Bootladers lesen
;------------------------------------------------------------------------------
ReadVersion:
	bra	RXreset			; n.v.
;------------------------------------------------------------------------------
;Programmspeicher lesen
;------------------------------------------------------------------------------
ReadProgMem:
	bra	RXreset			; n.v.
;------------------------------------------------------------------------------
; Empf: <ST1><ST2>[<0x02><DLEN><ADDRL><ADDRH><ADDRU><DATA>...]<CHKSUM><return>
; Send: [<0x32>]<CHKSUMH><CHKSUML><return>
;------------------------------------------------------------------------------
WriteProgMem:
	mov	DLEN,WREG
	lsr	w0,#2,w0
	mov	w0,COUNTER
	mov	ADDRESS_U,w0
	mov	ADDRESS_H,w1
	mov	ADDRESS_L,w2
	swap	w1
	ior	w1,w2,w1
	bclr	SR,#C
	rrc	w0,w0
	rrc	w1,w1
	mov	w0,TBLPAG	
Lp4:	
	mov	[w7++],w2	
	mov	[w7++],w3	
	swap	w3
	ior	w3,w2,w4
	mov	[w7++],w2	
	mov	[w7++],w3	
	swap	w3
	ior	w3,w2,w5
; Perform the TBLWT instructions to write the latches
	TBLWTL 	w4,[w1]
	TBLWTH 	w5,[w1++]
; Setup NVMCON for programming one word to data Flash
	mov 	#0x4003,W0
	mov 	W0,NVMCON
; Write the key sequence
	mov 	#0x55,W0
	mov 	W0,NVMKEY
	mov 	#0xAA,W0
	mov 	W0,NVMKEY
; Start the write cycle
	bset 	NVMCON,#WR
	nop
	nop
	dec 	COUNTER
	bra	nz,Lp4
	mov	#7,w0			;Schluesselbyte anpassen
	and	COMMAND
	add	cipher			
	bra	SendAcknowledge	
;------------------------------------------------------------------------------
; Empf:<ST1><ST2>[<0x03><DLEN(1)><ADDRL><ADDRH><ADDRU><BLOCKS>]<CHKSUM><return>
; Send:[<0x33>]<0x43><0x44><return>
; Ein Block = 1024 Byte 
;------------------------------------------------------------------------------
EraseProgMem:
	mov	ADDRESS_H,w0
	mov	ADDRESS_U,w1
	swap	w0
	bclr	SR,#C
	rrc	w1,w1
	rrc	w0,w0
	mov	#0xFC00,w1
	and	w1,w0,w0		;Page Adress in 1024 Byte Bl�cken
EraseProgMemLoop:
	clrwdt

	mov	w0,w1			;Adresse sichern
	mov	#0x0400,w2		;Page 0x400 = Bootsektor
	cpsne	w0,w2	
	bra	EraseProgMemNext	;Bootsektor nicht l�schen
	clr 	TBLPAG 
	TBLWTL 	w0,[w0] 		;Dummy Write zum Adresse einstellen
	mov 	#0x4042,w0 		;Setup NVMCON to erase one row of Flash
	mov 	w0,NVMCON  
 	mov 	#0x55,w0 		;Write the KEY Sequence
	mov 	w0,NVMKEY 
	mov 	#0xAA,w0 
	mov 	w0,NVMKEY 
 	bset 	NVMCON,#WR 		;Start the erase operation
	nop				;wait
	nop				;wait
;Zur Sicherheit goto-Instuction schreiben
	cp0	w1			;Adressbereich 0...3FF ?
	bra	nz,EraseProgMemNext

	mov	#0x0400,w4		;Instruction auf Adr. 0
	mov	#0x0004,w5
	TBLWTL 	w4,[w1]
	TBLWTH 	w5,[w1]
	mov 	#0x4003,W0
	mov 	W0,NVMCON
	mov 	#0x55,W0
	mov 	W0,NVMKEY
	mov 	#0xAA,W0
	mov 	W0,NVMKEY
	bset 	NVMCON,#WR
	nop
	nop

EraseProgMemNext:
	mov	#0x400,w0
	add	w0,w1,w0
	dec	DATA_COUNT
	bra	nz,EraseProgMemLoop	
	mov	#7,w0			;Schluesselbyte anpassen
	and	COMMAND
	add	cipher			
	bra	SendAcknowledge	
;------------------------------------------------------------------------------
;
;------------------------------------------------------------------------------
ReadEE:
	mov	ADDRESS_H,w0
	and	#0x0F,w0
	swap	w0
	ior	ADDRESS_L,WREG	
	sl	w0,w0
	mov	#0x7D00,w1
	add	w0,w1,w1		;w1 Adr Low
	lsr	w1,#8,w2		;w2 Adr High	
	and	#0xFF,w1		
	mov	DATA_COUNT,w3		;w3 Anzahl Word
	rcall	ReadEE_Spi
	add	w3,w3,w0		;w0 Anzahl Byte senden
	bra	WritePacket

;externes EEPROM per SPI lesen 
;Eingang: 	w1 Adr Low
;		w2 Adr High
;		w3 Anzahl Word lesen
ReadEE_Spi:
	mov	#DATA_BUFF+2,w7		;Startadresse Sendedaten
	mov	w3,COUNTER		;Anzahl Word lesen

	bclr	LATB,#12		;CS_EE
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x03,w0		;0x03 = Read
	mov	w0,SPI1BUF
RdEE_Wait_1:
	btss	IFS0,#SPI1IF
	bra	RdEE_Wait_1
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	

	mov	w2,SPI1BUF		;Adr High
RdEE_Wait_2:
	btss	IFS0,#SPI1IF
	bra	RdEE_Wait_2
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	

	mov	w1,SPI1BUF		;AdrLow
RdEE_Wait_3:
	btss	IFS0,#SPI1IF
	bra	RdEE_Wait_3
	mov	SPI1BUF,w0		;Dummy read -> reset SPI1BUF
RdEE_Next:
	clrwdt
	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x00,w0
	mov	w0,SPI1BUF
RdEE_Wait_4:
	btss	IFS0,#SPI1IF
	bra	RdEE_Wait_4
	mov	SPI1BUF,w1		;Empfang Low Byte

	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x00,w0
	mov	w0,SPI1BUF
RdEE_Wait_5:
	btss	IFS0,#SPI1IF
	bra	RdEE_Wait_5
	mov	SPI1BUF,w2		;Empfang High Byte

	mov	w2,[w7]
	inc2	w7,w7
	mov	w1,[w7]
	inc2	w7,w7

	dec	COUNTER
	bra	nz,RdEE_Next

	bset	LATB,#12		;CS_EE De-Select

	return	
;------------------------------------------------------------------------------
;Ein Byte in ext. EEPROM schreiben
;Achtung: nach aussen hin wird das EEPROM Wordbasiert adressiert
;(evtl. sp�ter mal anpassen)
;------------------------------------------------------------------------------
WriteEE:
	mov	DLEN,w0
	mov	w0,COUNTER

	mov	ADDRESS_H,w0		;EE-Adresse 	
	and	#0x0F,w0		;zur Adressierung eines Byte
	swap	w0			;anpassen	
	ior	ADDRESS_L,WREG	
	sl	w0,w0
	mov	#0x7D00,w1
	add	w0,w1,w1		;w1 Adr Low
	lsr	w1,#8,w2		;w2 Adr High	
	and	#0xFF,w1		

	bclr	LATB,#12		;CS_EE=0;

	bclr	IFS0,#SPI1IF
	bclr	SPI1STAT,#SPIROV	
	mov	#0x06,w0		;0x06 = WREN
	mov	w0,SPI1BUF
WrEE_Wait_0:
	btss	IFS0,#SPI1IF
	bra	WrEE_Wait_0

	bset	LATB,#12		;Enable WREN
	bclr	LATB,#12

	bclr	SPI1STAT,#SPIROV	
	bclr	IFS0,#SPI1IF
	mov	#0x02,w0		;0x02 = Write
	mov	w0,SPI1BUF
WrEE_Wait_1:
	btss	IFS0,#SPI1IF
	bra	WrEE_Wait_1
	bclr	SPI1STAT,#SPIROV	
	bclr	IFS0,#SPI1IF

	mov	w2,SPI1BUF		;Adresse High
WrEE_Wait_2:
	btss	IFS0,#SPI1IF
	bra	WrEE_Wait_2
	bclr	SPI1STAT,#SPIROV	
	bclr	IFS0,#SPI1IF

	mov	w1,SPI1BUF		;Adresse Low
WrEE_Wait_3:
	btss	IFS0,#SPI1IF
	bra	WrEE_Wait_3
	bclr	SPI1STAT,#SPIROV	
	bclr	IFS0,#SPI1IF
WrEE_LP:
	mov	[w7++],w0
	mov	w0,SPI1BUF		;Datenbyte
WrEE_Wait_4:
	btss	IFS0,#SPI1IF
	bra	WrEE_Wait_4
	bclr	SPI1STAT,#SPIROV	
	bclr	IFS0,#SPI1IF
	dec	COUNTER
	bra	nz,WrEE_LP

	bset	LATB,#12		;CS_EE = 1

	bra	SendAcknowledge	
;------------------------------------------------------------------------------
;Ab hier RS232 Sende-und Datenaufbereitungsroutinen
;------------------------------------------------------------------------------
; Ein Byte in zwei Hex-ASCII Zeichen umformen und senden
WrData:
	mov	w0,w1
	lsr	w0,#4,w0		;1. High Nibble		
	and	#0x0F,w0
	cp	w0,#0x0A
	btsc	SR,#C
	add	w0,#0x07,w0
	add	#0x30,w0
	rcall	WrRS232
	and	w1,#0x0F,w0		;2. Low Nibble
	cp	w0,#0x0A
	btsc	SR,#C
	add	w0,#0x07,w0
	add	#0x30,w0
;N�chstes Zeichen senden (w0)
WrRS232:
	clrwdt
	add	CHKSUM
WrRS232Wait:
	btss	U2STA,#TRMT		;Warten bis TSR leer
	bra	WrRS232Wait
	mov	w0,U2TXREG		;Senden starten
	return
;N�chstes Zeichen holen (w0)
RdRs232:
	clrwdt
	btsc	U2STA,#OERR		;Reset bei Overrunfehler
	reset
	btss	IFS1,#U2RXIF		;Warten bis etwas auf UART empfangen wird
	bra	RdRs232
	mov	U2RXREG,w0		;Zeichen nach w0 lesen
	and	#0xFF,w0
	bclr	IFS1,#U2RXIF
	return
;Best�tigung senden
SendAcknowledge:
	clr	w0	
WritePacket:
	mov	WREG,COUNTER
	clr	CHKSUM	
	mov	#DATA_BUFF,w7
	mov	[w7],w0
	inc2	w7,w7
	add	#'0',w0
	rcall	WrRS232
	cp0	COUNTER
	bra	z,Sendchksum
SendNext:
	mov	[w7],w0
	inc2	w7,w7
	rcall	WrData
	dec	COUNTER
	bra	nz,SendNext
Sendchksum:
	neg	CHKSUM,WREG
	rcall	WrData
	mov	#0x0D,w0		;<return> als Endezeichen
	rcall	WrRS232
SendchksumWait:
	btss	U2STA,#TRMT		;Warten bis TSR leer
	bra	SendchksumWait
	bra	RXreset				
;-------------------------------------------------------------------------
;Global Definitionen
;-------------------------------------------------------------------------
	.text
	.extern _DatBuff	;Diverse Variablen definiert in .C
	.extern _TMinGes 	
	.extern _TMaxGes
	.extern _TAvgGes
	.extern _YMin
	.extern _YMax
	.extern _YAvg
	.extern _RMASum64
	.extern _RMAPtr
	.extern _XSpalte	


	.global	_cHexAscii	;1 Byte Hex in 2 Zeichen Ascii wandeln
	.global _iHexAscii	;2 Byte Hex in 4 Zeichen Ascii wandeln
	.global _wHexAscii	;4 Byte Hex in 8 Zeichen Ascii wandeln
	.global	_HexAsc5	;2 Byte Hex (0...65535) in 5 Zeichen Ascii
	.global	_HexAsc4	;2 Byte Hex (0...9999) in 4 Zeichen Ascii	
	.global	_HexAsc2	;1 Byte Hex (0...99) in 2 Zeichen Ascii
	.global	_HexBcd5	;2 Byte Hex (0...65535) in 5 Zeichen BCD
	.global _WandleC10F10
	.global _FlashRead
	.global _FlashWrite
	.global	_FlashErasePage
	.global _FlashVerify
	.global _AsciiHex
	.global _TK
	.global _Mul3216Div15
	.global _Mul3216Div10
	.global _Mul3232Div16

	.global _LinTemp
	.global _Lade16x32
	.global _Lade8x16
	.global _Lade8x8
	.global _Lade4x6Hi
	.global _Lade4x6Lo
	.global _Lade5x6
	.global _LadeVar4x6


	.global _MMAPositionen
	.global _MMASpalte
	
	.global _WarteUsec
	.global _DivUW1616
	.global _DivUD3216
	.global _DivLongUD3216
	.global _DivUD6432

	.global _Pixelspalte
	.global	_MinMaxAvg
	.global _InitGrfBuf	;alle Grafikbuffer auf 0 setzen
	.global _Uhrdaten
	.global _WriteDotMatrix
	.global _LadeSpiBufLow
	.global _LadeSpiBufHigh
	.global	_RMARauschen
	.global	_RMAInitialisieren
	.global _RMAMittelwert
	.global _RMARechStufe
;-------------------------------------------------------------------------
;notwendige Init-Sequenz f�r Stackpointer
;wird vom C-Compiler gemacht !
;-------------------------------------------------------------------------
;_setupStkPtr:
;	mov	#__SP_init,w15	
;	mov	#__SPLIM_init,w0
;	mov	w0,SPLIM
;	nop
;	return
;-------------------------------------------------------------------------
;Addaptive Rohwertmittelung
;(KAL-Mode)
;Betrag des Rauschens aus 64er Mittelung feststellen
;-------------------------------------------------------------------------
_RMARauschen:
	mov	[w0],w2		;w2 = Min	
	mov	w2,w3		;w3 = Max	
	mov	#64,w6
RMARauschLp:
	mov	[w0++],w1	;n�chster Rohwert	
	sub	w1,w2,w7	;w1 < w2(Min) ?
	btsc	SR,#N
	mov	w1,w2		;ja
	sub	w3,w1,w7	;w1 > w3(Max) ?
	btsc	SR,#N
	mov	w1,w3		;ja
	dec	w6,w6
	bra	nz,RMARauschLp
	sub	w3,w2,w0	;Differenz Max - Min = Rauschen
	return	
;-------------------------------------------------------------------------
;Addaptive Rohwertmittelung, Ringspeicher mit aktuellen Rohwert initialisieren
;Eing: 	w0	RNeu
;	w1	&RMABuf80[0]
;Ausg:	RMABuf[80]
;	RMASum64
;-------------------------------------------------------------------------
_RMAInitialisieren:
	repeat	#79
	mov	w0,[w1++]	;80er Ringspeicher laden
	mov	#0x40,w1
	mul.su	w0,w1,w2
	mov	w2,_RMASum64	;64er Summe f�r Rauschmittelung
	mov	w3,_RMASum64+2
	return
;-------------------------------------------------------------------------
;Addaptive Rohwertmittelung
;RMAMittelwert: Rohwert abh�ngig von Mittelungsstufe (1...80 fach) berechnen 
;Eing	RwSumme		[w0,w1]		Summe der Rohwerte
;	RwSumCount	[w2]		Anzahl der Werte in RwSumme
;Ausg	[w0]		gemittelter RW	
;-------------------------------------------------------------------------
_RMAMittelwert:
	repeat	#17
	div.sd	w0,w2			;Teile W5:W4 durch W6
	sl	w1,w1			;Ergebnis in w1:w0	
	cpsgt	w2,w1			;falls Rest > 0.5
	inc	w0,w0			;runden
	return

;-------------------------------------------------------------------------
;RMARechStufe: Mittelungsstufe ausrechnen
;Formel: Stufe=RSTMax-(((RSTMax-1)*RW)/RMAMax)	
;
;Eing	RwNeu 	[w0]	aktueller Rohwert der langsamen Integr. (8000...7FFF)	
;	RMAMax	[w1]	Schwellwert Beginn RW-Mittelung (0...7FFF)	 
;			Wert 0 
;	RSTMax	[w2]	Maximale Mittelungsstufe bei Rohwert 0 (ca. 8...80)
;Ausg	[w0]	RMAStufe (1...80) 		
;-------------------------------------------------------------------------
_RMARechStufe:
	cpslt	w0,w1			;RW < Schwelle ?
	bra	RMAStufeMin		;nein, min. Stufe (keine Mittelung)
	cp0	w1			;Schwelle = 0 ? (Division durch 0)	
	bra	Z,RMAStufeMin		;ja ->
	btsc	w0,#15			;RW negativ ?	
	bra	RMAStufeMax		;ja -> 
	cp	w2,#1			;Max-Sufe > 0 ?
	bra	nc,RMAStufeMin		;nein ->
	mov	#80,w3			;Max-Stufe pr�fen
	cpslt	w2,w3			;ggf. begrenzen
	mov	w3,w2			;nein, begrenzen 
	dec	w2,w2
	mul.uu	w0,w2,w4
	mov	w1,w6
	repeat	#17
	div.ud	w4,w6			;geteilt durch RMAMax	
	cpslt	w0,w2			;(Ergebnis muss 0 bis 79 sein)  				
	mov	w2,w0			;(ggf. begrenzen)
	inc	w2,w2			;Ergebnis von 80 abziehen
	sub	w2,w0,w0		;w0=w2-w0 
	return
RMAStufeMin:
	mov	#1,w0
	return
RMAStufeMax:
	mov	w2,w0			;
	return



;######### Alt ##################
;	cp0	w1			;wird durch 0 geteilt ?	
;	bra	Z,RMARechStufeMin	;ja min. Stufe (keine Mittelung)
;	btsc	w0,#15			;RW negativ ?	
;	bra	RMARechStufeMax		;ja max. Stufe
;	mov	#79,w2			;Stufe = 79 mal RW
;	mul.uu	w0,w2,w4
;	mov	w1,w6
;	repeat	#17
;	div.ud	w4,w6			;geteilt durch RMAMax	
;	cpslt	w0,w2			;(Ergebnis muss 0 bis 79 sein)  				
;	mov	#79,w0			;(ggf. begrenzen)
;	mov	#80,w2			;Ergebnis von 80 abziehen
;	sub	w2,w0,w0		;w0=w2-w0 
;	return
;RMARechStufeMin:
;	mov	#1,w0
;	return
;RMARechStufeMax:
;	mov	#80,w0
;	return
;-------------------------------------------------------------------------
;Umladen der Grafikbuffer in SPI-Sendebuffer
;Aus Zeitgr�nden in Assembler
;Je nach Einsprung werden die oberen / unteren 8 Bit geladen 
;
;Eingang W0	Adresse TxRSpi1Buf	LCD-Sendebuffer (8 Bit)
;	 W1	Adresse Pixelbuffer 	Max/Med/Min (16 Bit)
;
;Ausgang 128 Byte Daten in TxRSpi1Buf	
;-------------------------------------------------------------------------
_LadeSpiBufHigh:
	inc	w1,w1
_LadeSpiBufLow:
	mov.b	[w1++],[w0++]	;1...16
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1

	mov.b	[w1++],[w0++]	;17...32
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1

	mov.b	[w1++],[w0++]	;33...48
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1

	mov.b	[w1++],[w0++]	;49...64
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1

	mov.b	[w1++],[w0++]	;65...80
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1

	mov.b	[w1++],[w0++]	;81...96
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1

	mov.b	[w1++],[w0++]	;97...112
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1

	mov.b	[w1++],[w0++]	;113...128
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]
	inc	w1,w1
	mov.b	[w1++],[w0++]

	return
;-------------------------------------------------------------------------
;DotMatrix Display SLR2016 schreiben
;Eingang: w0	Adr. BcdBuf (5 Byte, enth�lt Messwert in 1/10el Grad im Bcd-Format)
;Aus Zeitgr�nden in Assembler
;-------------------------------------------------------------------------
_WriteDotMatrix:
	mov	[w0++],w1			;1000er und 100er 
	mov	[w0++],w2			;10er und 1er 
	mov	[w0++],w3			;1/10el 
	cp.b	w3,#5
	bra	nc,WrDtMatOk
	swap	w2				;Messwert in ganzen Grad
	inc	w2,w2				;aufrunden, falls 1/10 Grad > 4
	cp.b	w2,#0x0A
	swap	w2
	bra	nz,WrDtMatOk
	and	#0xFF,w2
	inc	w2,w2
	cp.b	w2,#0x0A
	bra	nz,WrDtMatOk
	clr	w2
	swap	w1
	inc	w1,w1
	cp.b	w1,#0x0A
	swap	w1
	bra	nz,WrDtMatOk
	and	#0xFF,w1
	inc	w1,w1

WrDtMatOk:
	bclr.w	LATG,#2				;7-Seg Sg0001 aus
	bclr.w	LATG,#3				;7-Seg Sg0010 aus
	bclr.w	LATG,#8				;7-Seg Sg0100 aus
	bclr.w	LATG,#9				;7-Seg Sg1000 aus

	bset	LATB,#14			;WR2016=1 -> SLR2016 Write disable
	bset	LATF,#0				;A0=1	-> Digit 3 (1000er Stelle) w�hlen
	bset	LATF,#1				;A1=1
	bclr	LATG,#0
	btsc	w1,#0	
	bset	LATG,#0	
	bclr	LATG,#14
	btsc	w1,#1	
	bset	LATG,#14	
	bclr	LATG,#12
	btsc	w1,#2	
	bset	LATG,#12	
	bclr	LATG,#13
	btsc	w1,#3	
	bset	LATG,#13	
	bclr	LATB,#14			;WR2016=1 -> SLR2016 Write enable

	bset	LATB,#14			;WR2016=1 -> SLR2016 Write disable
	bclr	LATF,#0				;A0=0	-> Digit 2 (100er Stelle) w�hlen
	bset	LATF,#1				;A1=1
	bclr	LATG,#0
	btsc	w1,#8	
	bset	LATG,#0	
	bclr	LATG,#14
	btsc	w1,#9	
	bset	LATG,#14	
	bclr	LATG,#12
	btsc	w1,#10	
	bset	LATG,#12	
	bclr	LATG,#13
	btsc	w1,#11	
	bset	LATG,#13	
	bclr	LATB,#14			;WR2016=1 -> SLR2016 Write enable

	bset	LATB,#14			;WR2016=1 -> SLR2016 Write disable
	bset	LATF,#0				;A0=1	-> Digit 1 (10er Stelle) w�hlen
	bclr	LATF,#1				;A1=0
	bclr	LATG,#0
	btsc	w2,#0	
	bset	LATG,#0	
	bclr	LATG,#14
	btsc	w2,#1	
	bset	LATG,#14	
	bclr	LATG,#12
	btsc	w2,#2	
	bset	LATG,#12	
	bclr	LATG,#13
	btsc	w2,#3	
	bset	LATG,#13	
	bclr	LATB,#14			;WR2016=1 -> SLR2016 Write enable

	bset	LATB,#14			;WR2016=1 -> SLR2016 Write disable
	bclr	LATF,#0				;A0=0	-> Digit 0 (1er Stelle) w�hlen
	bclr	LATF,#1				;A1=0
	bclr	LATG,#0
	btsc	w2,#8	
	bset	LATG,#0	
	bclr	LATG,#14
	btsc	w2,#9	
	bset	LATG,#14	
	bclr	LATG,#12
	btsc	w2,#10	
	bset	LATG,#12	
	bclr	LATG,#13
	btsc	w2,#11	
	bset	LATG,#13	
	bclr	LATB,#14			;WR2016=1 -> SLR2016 Write enable

	bset	LATB,#14			;WR2016=1 -> SLR2016 Write disable

	return
;-------------------------------------------------------------------------
;Umrechnung des Eingangswertes [msec] in Stunden, Minuten, Sekunden, MSek.
;Eingang: w0,w1	24 Bit in msec 
;Ausgang: w0	High Byte = Stunden 
;		Low Byte = Minuten
;	  w1	obere 6 Bit = Sekunden	
;		untere 10 Bit = Millisekunden
;-------------------------------------------------------------------------
_Uhrdaten:
	mov	w0,w2			
	mov	w1,w3
	mov	#0x6DDD,w4
	repeat	#17
	div.ud	w2,w4
	lsr	w0,#7,w5	
	mul.uu	w4,w5,w0
	swap	w0
	swap	w1
	ior.b	w0,w1,w1
	mov.b	#0,w0
	bclr	SR,#C
	rrc	w1,w1	
	rrc	w0,w0	
	sub	w2,w0,w2
	subb	w3,w1,w3
	mov	#0xEA60,w4
	repeat	#17
	div.ud	w2,w4
	swap	w5		;w5 High Byte = Stunden
	ior.b	w0,w5,w5	;   Low Byte = Minuten	
	mov	#0x03E8,w4
	repeat	#17
	div.uw	w1,w4			
	sl	w0,#10,w0
	ior	w1,w0,w0
	mov	w5,w1
	return
;-------------------------------------------------------------------------
;128 facher Temperaturspeicher (Grafikmodus) l�schen
;Aus Zeitgr�nden in Assembler
;Eingang: w0 	Adr. Buffer[0] 
;-------------------------------------------------------------------------
_InitGrfBuf:
	repeat	#127
	clr	[w0++]
	return
;-------------------------------------------------------------------------
;Minimal-,Maximal- und Mittelwert des 128-Werte Grafikbuffers feststellen
;Aus Zeitgr�nden in Assembler
;Eingang: w0	Startadresse Temperaturbuffer TAvgBuf[0] 
;		(TMaxBuf[0]=w0+256 TMinBuf[0]=w0+512)
;Ausgang: TMinGes,TMaxGes,TAvgGes	
;Zeitbedarf: 128 Eintr�ge ca. 1100 IC	
;-------------------------------------------------------------------------
_MinMaxAvg:
;schnelle Version, kann bei Platzproblemen ge�ndert werden
;	bra	Altversion

	cp0	[w0]			;mind. 1 Wert vorh. ?
	bra	NZ,MMAOKF
	clr	_TMaxGes		;nein, alle Werte = 0
	clr	_TMinGes
	clr	_TAvgGes
	return				;Ende

MMAOKF:
	clr	w5			;Clear High Adr. AVG Summensp.
	mov	w0,w2			;W2=Zwischensp. Bufferadr.[0] 
	mov	#128,w1			;w1=128

	add	w0,w1,w0		;Eintrag +64 adressieren
	cp0	[w0]			;Wert > 0 ?
	btsc	SR,#Z
	sub	w0,w1,w0
	lsr	w1,w1			;w1=64
	
	add	w0,w1,w0		;Eintrag +32 adressieren
	cp0	[w0]			;Wert > 0 ?
	btsc	SR,#Z
	sub	w0,w1,w0
	lsr	w1,w1			;w1=32

	add	w0,w1,w0		;Eintrag +16 adressieren
	cp0	[w0]			;Wert > 0 ?
	btsc	SR,#Z
	sub	w0,w1,w0
	lsr	w1,w1			;w1=16

	add	w0,w1,w0		;Eintrag +8 adressieren
	cp0	[w0]			;Wert > 0 ?
	btsc	SR,#Z
	sub	w0,w1,w0
	lsr	w1,w1			;w1=8

	add	w0,w1,w0		;Eintrag +4 adressieren
	cp0	[w0]			;Wert > 0 ?
	btsc	SR,#Z
	sub	w0,w1,w0
	lsr	w1,w1			;w1=4

	add	w0,w1,w0		;Eintrag +2 adressieren
	cp0	[w0]			;Wert > 0 ?
	btsc	SR,#Z
	sub	w0,w1,w0
	lsr	w1,w1			;w1=2

	add	w0,w1,w0		;Eintrag +1 adressieren
	cp0	[w0]			;Wert > 0 ?
	btsc	SR,#Z
	sub	w0,w1,w0		;w0: Min 0DA2 (0) Max 0EA0 (127)

	mov	[w0],w4			;Ersten AVG-Wert in Summenspeicher laden
	mov	[w0+256],w6		;Ersten MAX-Wert nach w6	
	mov	[w0+512],w7		;Ersten MIN-Wert nach w7	
;1
	dec2	w2,w2
	sub	w0,w2,w2
	lsr	w2,w3			;w3 = Anzahl Werte im Temp.Buffer
	mov	#128,w1
	sub	w1,w3,w1		;w1 = Anzahl Werte gleich 0
	sl	w1,#3,w1		;mal 8 nach w1
	bra	w1			;rel. Sprung -> w1

;-------> Sprung w1
;2
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;3
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;4
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;5
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;6
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;7
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;8
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;9
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;10
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;11
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;12
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;13
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;14
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;15
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;16
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;17
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;18
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;19
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;20
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;21
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;22
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;23
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;24
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;25
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;26
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;27
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;28
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;29
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;30
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;31
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;32
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;33
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;34
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;35
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;36
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;37
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;38
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;39
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;40
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;41
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;42
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;43
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;44
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;45
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;46
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;47
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;48
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;49
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;50
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;51
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;52
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;53
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;54
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;55
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;56
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;57
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;58
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;59
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;60
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;61
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;62
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;63
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;64
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;65
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;66
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;67
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;68
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;69
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;70
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;71
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;72
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;73
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;74
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;75
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;76
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;77
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;78
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;79
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;80
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;81
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;82
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;83
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;84
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;85
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;86
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;87
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;88
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;89
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;90
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;91
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;92
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;93
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;94
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;95
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;96
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;97
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;98
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;99
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;100
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;101
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;102
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;103
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;104
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;105
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;106
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;107
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;108
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;109
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;110
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;111
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;112
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;113
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;114
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;115
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;116
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;117
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;118
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;119
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;120
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;121
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;122
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;123
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;124
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;125
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;126
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;127
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7
;128
	add	w4,[--w0],w4
	addc	w5,#0,w5
	mov	[w0+256],w1	
	cpsgt	w6,w1
	mov	w1,w6
	mov	[w0+512],w1	
	cpslt	w7,w1
	mov	w1,w7

	repeat	#17
	div.ud	w4,w3			;Teile W5:W4 durch W3 
	sl	w1,w1			;Ergebnis in w1:w0
	cpslt	w1,w3			;falls Rest > 0.5
	inc	w0,w0			;runden

	mov	w6,_TMaxGes		;Maximalwert �bernehmen
	mov	w7,_TMinGes		;Minmalwert �bernehmen
	mov	w0,_TAvgGes		;AVG-Wert �bernehmen

	return

;##############################################################################
;Altversion B (etwas schneller als A)
;	cp0	[w0]			;gibt es mind. 1 Wert ?
;	bra	NZ,MinMaxOK1		;ja, -> weiter
;
;	clr	w0			;nein, keine Eintr�ge:
;	clr	w6			;alle werte = 0
;	clr	w7
;	bra	MMAReturnC		;Ende ->
;MinMaxOK1:
;	clr	w4			;w4,w5 = Avg Summenspeicher
;	clr	w5
;	mov	#129,w2			;Z�hler vorladen
;	add	#256,w0			;letzten Eintrag + 1 adressieren
;	mov	w0,w1			;Adresse merken in w1
;MinMaxLPB:
;	add	w4,[--w0],w4		;ersten AVG Wert > 0 suchen
;	bra	Z,MinMaxLPB		;wiederholen
;
;	sub	w1,w0,w1		;Anzahl verbleibende Werte
;	lsr	w1,w1			;ermitteln
;	sub	w2,w1,w2		;w2 = Z�hler
;	mov	w2,w3			;w3 = Merkregister
;
;	mov	[w0+256],w6		;Ersten MAX-Wert nach w6	
;	mov	[w0+512],w7		;Ersten MIN-Wert nach w7	
;	dec	w2,w2			;ist nur 1 Wert im Buffer ?
;	bra	Z,MMAEndC		;ja
;MinMaxLPC:
;	add	w4,[--w0],w4		;n�chsten Wert zum AVG-Wert addieren
;	addc	w5,#0,w5
;	mov	[w0+256],w1		;MAX-Wert nach w1	
;	cpsgt	w6,w1			;w6 > w1 ? -> kein neues Maximum
;	mov	w1,w6			;Neues Maximum
;	mov	[w0+512],w1		;MIN-Wert nach w1	
;	cpslt	w7,w1			;w7 < w1 ? -> kein neues Minimum
;	mov	w1,w7			;Neues Minimum
;
;	dec	w2,w2
;	bra	nz,MinMaxLPC
;MMAEndC:
;	repeat	#17
;	div.ud	w4,w3			;Teile W5:W4 durch W3 
;	sl	w1,w1			;Ergebnis in w1:w0
;	cpslt	w1,w3			;falls Rest > 0.5
;	inc	w0,w0			;runden
;MMAReturnC:
;	mov	w6,_TMaxGes		;Maximalwert �bernehmen
;	mov	w7,_TMinGes		;Minmalwert �bernehmen
;	mov	w0,_TAvgGes		;AVG-Wert �bernehmen
;	return
;------------------------------------------------------------------------------
;Altversion A
;Altversion:
;	mov	#127,w7			;128 - 1 (Erster Wert wird hier vorgeladen)	
;	clr	w1			;w1 = Hilfsregister f�r add w1,[...
;	mov	[w0],w4			;w4,w5 = Avg
;	clr	w5
;	mov	[w0+256],w3		;w3 = Max
;	mov	[w0+512],w2		;w2 = Min
;	cp0	w4			;gibt es Eintr�ge ?
;	bra	nz,MinMaxAvgLoop	;ja ->
;	clr	w0			;keine Eintr�ge:
;	mov	w0,_TMinGes		;alle Werte = 0
;	mov	w0,_TMaxGes
;	bra	MinMaxRet		;zum Prog.-Ende ->
;MinMaxAvgLoop:
;	add	w1,[++w0],w6		;AVG-Wert nach w6
;	bra	z,MinMaxAvgEnd		;Messwert = 0 -> kein Eintrag im Buffer
;	add	w6,w4,w4		;AVG-Summe + Wert
;	addc	w5,#0,w5
;	mov	[w0+256],w6		;MAX-Wert nach w6	
;	cpsgt	w3,w6			;w3 > w6 ? -> kein neues Maximum
;	mov	w6,w3			;Neues Maximum
;	mov	[w0+512],w6		;MIN-Wert nach w6	
;	cpslt	w2,w6			;w2 < w6 ? -> kein neues Minimum
;	mov	w6,w2			;Neues Minimum
;	dec	w7,w7
;	bra	nz,MinMaxAvgLoop
;MinMaxAvgEnd:
;	mov	w2,_TMinGes		;Minmalwert �bernehmen
;	mov	w3,_TMaxGes		;Maximalwert �bernehmen
;	mov	#128,w3
;	sub	w3,w7,w3		;w3 = Anzahl Werte
;	repeat	#17
;	div.ud	w4,w3			;Teile W5:W4 durch W3 
;	sl	w1,w1			;Ergebnis in w1:w0
;	cpslt	w1,w3			;falls Rest > 0.5
;	inc	w0,w0			;runden
;MinMaxRet:
;	mov	w0,_TAvgGes		;AVG-Wert �bernehmen
;	return
;-------------------------------------------------------------------------
;Pixeldaten f�r eine Spalte in Pixelbuffer eintragen
;Der Bereich zwischen 1. und 2. Pixel als vertikale Line laden.
;Eingang: w0:	1. Pixeladresse 0...47 (8 Bit)
;	  w1:	2. Pixeladresse 0...47 (8 Bit)
;	  w2:	Spaltenadresse PixMinBuf 0...127, (Pages 1...2), 16 Bit	
;	  w3:	Spaltenadresse PixMedBuf 0...127, (Pages 3...4), 16 Bit	
;	  w4:	Spaltenadresse PixMaxBuf 0...127, (Pages 5...6), 16 Bit
;Ausgang: Eine Pixelspalte (48 Bit) in PixMinBuf,PixMedBuf,PixMaxBuf
;
;
;Pixel-Position		Pix Buffer

;	50
;	49
;	48					  	
;
;	47		01 00	---- ---1 ---- ----
;	46		02 00	---- --1- ---- ----
;	45		04 00	---- -1-- ---- ----
;	44		08 00	---- 1--- ---- ----
;	43		10 00	---1 ---- ---- ----
;	42		20 00	--1- ---- ---- ----
;	41		40 00	-1-- ---- ---- ----
;	40		80 00	1--- ---- ---- ----
;
;	39		00 01	---- ---- ---- ---1
;	38		00 02	---- ---- ---- --1-
;	37		00 04	---- ---- ---- -1--
;	36		00 08	---- ---- ---- 1---
;	35		00 10	---- ---- ---1 ----
;	34		00 20	---- ---- --1- ----
;	33		00 40	---- ---- -1-- ----	
;	32		00 80	---- ---- 1--- ----	
;
;	31		01 00	---- ---1 ---- ----
;	30		02 00	---- --1- ---- ----
;	29		04 00	---- -1-- ---- ----
;	28		08 00	---- 1--- ---- ----
;	27		10 00	---1 ---- ---- ----
;	26		20 00	--1- ---- ---- ----
;	25		40 00	-1-- ---- ---- ----
;	24		80 00	1--- ---- ---- ----
;
;	23		00 01	---- ---- ---- ---1
;	22		00 02	---- ---- ---- --1-
;	21		00 04	---- ---- ---- -1--
;	20		00 08	---- ---- ---- 1---
;	19		00 10	---- ---- ---1 ----
;	18		00 20	---- ---- --1- ----
;	17		00 40	---- ---- -1-- ----	
;	16		00 80	---- ---- 1--- ----	
;
;	15		01 00	---- ---1 ---- ----
;	14		02 00	---- --1- ---- ----
;	13		04 00	---- -1-- ---- ----
;	12		08 00	---- 1--- ---- ----
;	11		10 00	---1 ---- ---- ----
;	10		20 00	--1- ---- ---- ----
;	9		40 00	-1-- ---- ---- ----
;	8		80 00	1--- ---- ---- ----
;
;	7		00 01	---- ---- ---- ---1
;	6		00 02	---- ---- ---- --1-
;	5		00 04	---- ---- ---- -1--
;	4		00 08	---- ---- ---- 1---
;	3		00 10	---- ---- ---1 ----
;	2		00 20	---- ---- --1- ----
;	1		00 40	---- ---- -1-- ----	
;	0		00 80	---- ---- 1--- ----	
;-------------------------------------------------------------------------
_Pixelspalte:
	clr	w6
	mov	w6,[w2]			;Pages 1...4 alle Pixel l�schen
	mov	w6,[w3]

	cpslt	w0,w1			;w0 = Min
	exch	w0,w1			;w1 = Max	
	sub	w1,w0,w0		;w0 = Anzahl Pixel (Max - Min)

	mov	#32,w5							
	cp	w1,w5			;Max > 32 ?
	bra	c,PixMaxSub		;ja -> 	
	mov	w6,[w4]
	mov	w3,w4
	mov	w2,w3
	mov	#16,w5
	cp	w1,w5			;Max > 16 ?
	bra	c,PixMaxSub		;ja ->
	mov	w6,[w4]
	mov	w3,w4
	bra	PixMaxLoop
PixMaxSub:
	sub	w1,w5,w1
PixMaxLoop:
	mov	#7,w5
	btsc	w1,#3
	mov	#23,w5
	sub	w5,w1,w7
	bclr	SR,#Z
	bsw.z	w6,w7			;Set bit W7 in W6 to the complement of the Z Bit  
	dec	w1,w1
	bra	nn,PixMaxNext
	and	#15,w1
	mov	w6,[w4]
	mov	w3,w4
	mov	w2,w3
	clr	w6
	dec	w0,w0
	bra	nn,PixMaxLoop
	return
PixMaxNext:
	dec	w0,w0			;alle Pixel abgearbeitet ?
	bra	nn,PixMaxLoop		;nein ->
	mov	w6,[w4]
	return
;-------------------------------------------------------------------------
;Spezialdivision 64 Bit / 32 Bit (unsigned / unsigned)
;Eingang: w0,w1	 Divisor 32 Bit	
;	  w2,w3	 Divident 32 Bit	
;
;Rechnung: w0,w1 * 2^32 / w2,w3 
;
;Ausgang: w0,w1  Ergebnis, 32 Bit  
;-------------------------------------------------------------------------
_DivUD6432:
	mov	w3,w5
	mov	w2,w4
	clr	w3	
	mov	w1,w2
	mov	w0,w1
	clr	w0	
	mov	#32,w7
	bclr	SR,#C
	rlc	w0,w0
	rlc	w1,w1
	rlc	w2,w2
	rlc	w3,w3
DivUD6432Loop:
	sub	w2,w4,w6
	subb	w3,w5,w6
	bra	nc,DivUD6432Next
	sub	w2,w4,w2
	subb	w3,w5,w3
DivUD6432Next:
	rlc	w0,w0
	rlc	w1,w1
	rlc	w2,w2
	rlc	w3,w3
	dec	w7,w7
	bra	nz,DivUD6432Loop	
	return
;-------------------------------------------------------------------------
;Division 32/16 Unsigned
;DivUD3216: Achtung: Der ganzzahlige Quotient darf nicht gr�sser 16 Bit werden, -> sonst Fehler 
;	  (siehe Beschreibung DIV.UD im PIC30F Reference Manuel)	
;Eingang: w0,w1: Divident	
;	  w2:	 Divisor		 
;Ausgang: w0:    Quotient, gerundet
;
;DivLongUD3216: ohne Einschr�nkung (s.o.), daf�r etwas mehr Zeitbedarf
;Eingang: w0,w1: Divident	
;	  w2:	 Divisor		 
;Ausgang: w0:    Quotient ganzzahliger Anteil
;	  w1:	 Quotient ganzzahliger Rest
;-------------------------------------------------------------------------
_DivUD3216:
	cp0	w2			;Division durch 0 ?
	bra	z,Div3216Err		;ja, fehler
	mov	w0,w4
	mov	w1,w5
	repeat	#17
	div.ud	w4,w2
	sl	w1,w1
	cpsgt	w2,w1
	inc	w0,w0	
	return

_DivLongUD3216:
	cp0	w2			;Division durch 0 ?
	bra	z,Div3216Err		;ja, fehler
	mov	w0,w4
	mov	w1,w5
	repeat	#17
	div.uw	w1,w2
	mov	w0,w3
	mul.uu	w0,w2,w6
	sub	w5,w6,w5
	repeat	#17
	div.ud	w4,w2
	mov	w3,w1
	return
Div3216Err:
	setm	w0
	setm	w1
	return
;-------------------------------------------------------------------------
;Unsigned Divison 16/16 f�r Skalierung grafische Messdatenanzeige 
;Eingang: w0:	16 Bit Divisor 
;	  w1: 	16 Bit Divident	
;Ausgang: w0:	Quotient ganzzahliger Anteil
;	  w1:	Quotient ganzzahliger Rest
;Rechenweg: Ergebnis = 128 * 256 = 32768 / Anzahl Werte	
;-------------------------------------------------------------------------
_DivUW1616:
	mov	w0,w2
	mov	w1,w4
	repeat	#17
	div.uw	w2,w4			;16/16 unsigned Divison
	return
;-------------------------------------------------------------------------
;Warteschleife in us
;Eingang: w0,w1:	Wartezeit in us
;Ausgang: ---			     		1
;Beschreibung: Timer 8 und 9 werden als 32 Bit Timer mit der Anzahl Instructen
;Cycle (IC) geladen, die der geforderten Zeit entspricht. Bei 18.432MHz ist ein
;IC= 108.506944 ns. F�r eine �s sind somit 1�s / 0.108506944�s = 9.216 TS n�tig.
;
;        			   1
; Anzahl Timerschritte = �s *  ----------- => �s * 9.216
;			       .108506944444   	=================
;
;Wegen der Genaugkeit wird mit 2e16 gerechnet. (9.216*2e16=0x0009374C)
;-------------------------------------------------------------------------
_WarteUsec:
	clr	TMR8			;Setup Timer 8 + Timer 9
	clr	TMR9			;for 32 Bit Timer Mode
	bset	T8CON,#T32
	bset	T8CON,#TON		;Timer starten
	cp0	w1			;falls nur eine �s gewartet 
	bra	nz,WarteLang		;werden soll, 
	cp	w0,#1			;die Warteschleife hier
	bra	z,WartenFertig		;beenden
WarteLang:
	mov	#0x374C,w2		;sonst Timerschritte
	mov	#0x0009,w3		;ausrechnen	
	mul.uu	w0,w2,w4
	mul.uu	w1,w3,w6
	push	w0
	mul.uu	w1,w2,w0
	add	w0,w5,w5
	addc	w1,w6,w6
	pop	w0
	mul.uu	w0,w3,w0
	add	w0,w5,w5
	addc	w1,w6,w6
	sub	#23,w5			;Korrekturfaktor abziehen
	subb	w6,#0,w6
Warten:	
	mov	TMR8,w0			;und warten...
	mov	TMR9,w1
	sub	w0,w5,w0
	subb	w1,w6,w1
	bra	n,Warten
WartenFertig:
	bclr	T8CON,#TON		;fertig.
	return

;-------------------------------------------------------------------------
; Position der Linien und der numerischen Anzeige im Grafikmodus pr�fen 
; Es d�rfen nur die Positionen 0...47 erzeugt werden, sonst gibt es wegen
; relativen Sprung Abst�rze !
; Eingangswerte: YMin,YAvg,YMax (16-Bit Werte, Mittelpositionen der num. Anzeige) 
; Ausgangswerte: korrigierte YMin,YAvg,YMax 
; !!! ----> Die Position der oberen Kante der numerischen Anzeige befinden sich
;  anschl. im Low Byte, die Position der Hilslinie im High Byte <--- !!!
;-------------------------------------------------------------------------
_MMAPositionen:
;Positionen pr�fen
	mov	_YMin,w0
	mov	_YAvg,w1
	mov	_YMax,w2
;obere Kante num. Anzeige einstellen, um 2 erh�hen und sichern 
	inc2	w0,w5			;Min 
	inc2	w1,w6			;Avg
	inc2	w2,w7			;Max
	mov	#4,w4			;Mindestabstand Hilfslinien
;Hilfslinien	
	sub	w2,w1,w3		;Mindestabstand Max->Avg
	cp	w3,w4			;
	bra	nc,MMALinKorr1		; zu klein ->
	sub	w1,w0,w3		;Mindestabstand Avg->Min
	cp	w3,w4			;
	bra	c,MMALinEnd		; alles OK ->
	sub	w1,w4,w0		;Min nach unten verschieben
	bra	nn,MMALinEnd		; alles OK ->
	mov	#0,w0			;Min/Avg nach oben verschieben
	mov	w4,w1
	sub	w2,w1,w3		;Mindestabstand Max->Avg
	cp	w3,w4			
	btss	SR,#C
	lsr	w4,w2			;Max nach oben verschieben
	bra	MMALinEnd
MMALinKorr1:
	add	w1,w4,w2		;Max nach oben verschieben
	sub	w1,w0,w3		;Mindestabstand Avg->Min
	cp	w3,w4			
	bra	nc,MMALinKorr2		;zu klein ->
	mov	#48,w3			;Max jetzt zu weit oben ?
	sub	w2,w3,w3		
	bra	nc,MMALinEnd		; alles OK ->	
	mov	#47,w2			;Max/Avg nach unten verschieben
	sub	w2,w4,w1
	sub	w1,w0,w3		;Mindestabstand Avg->Min
	cp	w3,w4			; immer noch OK ?
	btss	SR,#C
	sub	w1,w4,w0		;nein, Min nach unten verschieben
	bra	MMALinEnd
MMALinKorr2:
	sub	w1,w4,w0		;Min nach unten verschieben
	bra	nn,MMALinTestHigh	;Min zu weit unten ?
	mov	#0,w0			;ja, alle 3 nach oben verschieben
	add	w0,w4,w1
	add	w1,w4,w2
	bra	MMALinEnd
MMALinTestHigh:
	mov	#48,w3	
	sub	w2,w3,w3		;Max zu weit oben ?
	bra	nc,MMALinEnd		; alles OK ->
	mov	#47,w2			;ja, alle 3 nach unten verschieben
	sub	w2,w4,w1
	sub	w1,w4,w0
MMALinEnd:
	swap	w0			;Position der Hilfslinien
	swap	w1			;ins High-Byte
	swap	w2
;Positionen num. Anzeige pr�fen
	mov	#48,w3	
	cpslt	w5,w3	
	mov	#47,w5
	cpslt	w6,w3	
	mov	#47,w6
	cpslt	w7,w3	
	mov	#47,w7

	mov	#3,w3	
	cpsgt	w5,w3	
	mov	#4,w5
	cpsgt	w6,w3	
	mov	#4,w6
	cpsgt	w7,w3	
	mov	#4,w7

	sub	w7,w6,w3		;Mindestabstand Max->Avg
	cp	w3,#6			;
	bra	nc,MMAPosKorr1		; zu klein ->
	sub	w6,w5,w3		;Mindestabstand Avg->Min
	cp	w3,#6			;
	bra	c,MMAPosEnd		; alles OK ->
	sub	w6,#6,w5		;Min nach unten verschieben
	mov	#0x4,w3			;zu weit unten ?
	sub	w5,w3,w3	
	bra	nn,MMAPosEnd		; alles OK ->
	mov	#4,w5			;Min/Avg nach oben verschieben
	mov	#10,w6
	sub	w7,w6,w3		;Mindestabstand Max->Avg
	cp	w3,#6			
	btss	SR,#C
	mov	#16,w7			;Max nach oben verschieben
	bra	MMAPosEnd
MMAPosKorr1:
	add	w6,#6,w7		;Max nach oben verschieben
	sub	w6,w5,w3		;Mindestabstand Avg->Min
	cp	w3,#6			
	bra	nc,MMAPosKorr2		;zu klein ->
	mov	#48,w3			;Max jetzt zu weit oben ?
	sub	w7,w3,w3		
	bra	nc,MMAPosEnd		
	mov	#41,w6			;Max/Avg nach unten verschieben
	mov	#47,w7
	sub	w6,w5,w3		;Mindestabstand Avg->Min
	cp	w3,#6			; immer noch OK ?
	btss	SR,#C
	mov	#35,w5			;nein, Min nach unten verschieben
	bra	MMAPosEnd
MMAPosKorr2:
	sub	w6,#6,w5		;Min nach unten verschieben
	mov	#48,w3	
	sub	w7,w3,w3		;Max zu weit oben ?
	bra	nc,MMAPosTestLow
	mov	#35,w5			;ja, alle 3 nach unten verschieben
	mov	#41,w6
	mov	#47,w7
	bra	MMAPosEnd
MMAPosTestLow:
	mov	#0x4,w3			;Min zu weit unten ?
	sub	w5,w3,w3	
	bra	nn,MMAPosEnd
	mov	#4,w5			;ja, alle 3 nach oben verschieben
	mov	#10,w6
	mov	#16,w7
MMAPosEnd:
	ior	w0,w5,w5
	ior	w1,w6,w6
	ior	w2,w7,w7
	mov	w5,_YMin
	mov	w6,_YAvg
	mov	w7,_YMax
	return
;-------------------------------------------------------------------------
;Hilfslinien + num. Temperaturwerte zeichnen
;Eingang: 	w0	Linenposition MinTemp 0...47
;	 	w1	Linenposition AvgTemp 0...47
;	 	w2	Linenposition MaxTemp 0...47
;	 	w3	Spalte, 0...127
;	 	w4	Adr. PixMinBuf[Spalte]
;	 	w5	Adr. LineBuf[0], Min-/Avg-/Max- Temp je 5-stellig Ascii
;
;|<-- Spaltenpositionen 0...21, Bereich numerische Anzeige ------->|<-Grafikbereich->|
;|-----------------------------------------------------------------|-----------------|
;|00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22........... 127|	
;|1.Ziffer   |2.Ziffer   |3.Ziffer   |4.Ziffer   |Punkt|5.Ziffer   |		
;|<------>|
; Spalte 0...2 noch Grafikbereich, falls 1. Ziffer = 0
;-------------------------------------------------------------------------
_MMASpalte:
	mov	[w4],w6			;kompl. Spalte (48 Bit)
	mov	w6,_DatBuff+6		;PixMinBuf 
	mov	[w4+0x100],w6		;PixMedBuf
	mov	w6,_DatBuff+8
	mov	[w4+0x200],w6		;PixMaxBuf
	mov	w6,_DatBuff+10
	mov	w4,_DatBuff+12		;Adresse sichern

;1.15 Spalten 0..2 noch Grafikbereich    
	clr	_DatBuff+14		;Bits 0...2 sind Flags Spalten 0...2 

	push	w0			
;vertikale Hilfslinie zeichnen, je f�r MAX, AVG, MIN 
	mov	#0x1F,w7				
	and	w7,w3,w6		;Spaltenpositionen 31,63,95,
	cpseq	w7,w6
	bra	MMANoVertical_1
	mov	#0x7F,w6		;nicht Spalte 127 !
	cpslt	w3,w6
	bra	MMANoVertical_1
	mov	#0x9224,w0
	ior	_DatBuff+6
	mov	#0x2449,w0
	ior	_DatBuff+8
	mov	#0x4992,w0
	ior	_DatBuff+10
;vertikale Linie zur Markierung des gew�hlten Messwert (Nur Nachbetrachtung) 
;XSpalte enth�lt die Spalte (0...127) 
MMANoVertical_1:
	mov	_XSpalte,WREG		;falls XSpalte = 0 ... 			
	bra	z,MMANoVertical_2	; -> KEINE Linie

	and	#0x7F,w0		;sonst Spaltenposition (0...127) maskieren
	cpseq	w0,w3			;Spaltenpositionen = XSpalte ?
	bra	MMANoVertical_2		;nein
;	mov	#0xEEEE,w0		;Hilfslinie markiert ausgew. Einzelwert
	mov	#0xFFFF,w0		;Hilfslinie markiert ausgew. Einzelwert
	ior	_DatBuff+6		;innerhalb eines Blocks.
	ior	_DatBuff+8			
	ior	_DatBuff+10
MMANoVertical_2:

	pop	w0

;ohne 10el Grad und Dezpunkt
;	cp.b	w3,#16			; Spaltenbereich mumerische Anzeige ?
;	bra	c,MMADotLine		; nein -> Bereich der Hilfslinien

;10el Grad + Dezpunkt
	cp.b	w3,#22			; Spaltenbereich mumerische Anzeige ?
	bra	c,MMADotLine		; nein -> Bereich der Hilfslinien
	cp.b	w3,#16			; Spaltenbereich 0...15 ? (Ganze Grad)
	bra	nc,MMAGanz		; ja ->
	cp.b	w3,#18			; Spaltenbereich 18...21 ? (Zehntelgrad)
	bra	c,MMAZehntel		; ja ->
	mov	#15,w4
	add	w5,w4,w5
	mov.b	[w5],w4			; 16...17 Dezpunkt
	mov	w4,w6	
	mov	w4,w7	
	bra	MMADezLaden

MMAZehntel:
	dec2	w3,w3			
MMAGanz:
	lsr	w3,#2,w4		; Spaltenadresse / 4
	add	w5,w4,w5		; MIN adressieren
	mov.b	[w5],w4			; Ascii-Zahl (MIN) nach w4	
	and	#0xFF,w4		; obere Bits ausblenden
	sl	w4,#2,w4		; mal 4 f�r Adressierung Font Tab
	add	#5,w5			; AVG adressieren
	mov.b	[w5],w6			; Ascii-Zahl (AVG) nach w6	
	and	#0xFF,w6		; obere Bits ausblenden
	sl	w6,#2,w6		; mal 4 f�r Adressierung Font Tab
	add	#5,w5			; MAX adressieren
	mov.b	[w5],w7			; Ascii-Zahl (MAX) nach w7	
	and	#0xFF,w7		; obere Bits ausblenden
	sl	w7,#2,w7		; mal 4 f�r Adressierung Font Tab

;1.15 Spalten 0..2 noch Grafikbereich    
	cp.b	w3,#3			; Spaltenbereich 0...2 ?
	bra	c,MMANoSp02		; nein ->
	mov	#0x80,w5		; und Tausender Stelle Leerzeichen ?
	cpsne	w5,w4			; MIN
	bset	_DatBuff+14,#0
	cpsne	w5,w6			; AVG
	bset	_DatBuff+14,#1
	cpsne	w5,w7			; MAX
	bset	_DatBuff+14,#2
MMANoSp02:

	mov	#Fnt4x6-0x80,w5		; Startadresse Tabelle nach w5 (abz�glich 20h * 4)
	btsc	w3,#1			; Spalten 0/1 ?
	inc2	w5,w5			; nein, Spalten 2/3
	add	w5,w4,w4		; Adresse Ascii-Zeichen MIN	
	add	w5,w6,w6		; Adresse Ascii-Zeichen	AVG	
	add	w5,w7,w7		; Adresse Ascii-Zeichen	MAX
	TBLRDL 	[w4],w4 		; Pixelmuster ins RAM lesen
	TBLRDL 	[w6],w6 		; 
	TBLRDL 	[w7],w7
MMADezLaden:
	btss	w3,#0
	bra	MMANoSwap
	swap	w4
	swap	w6
	swap	w7
MMANoSwap:
	mov	w4,_DatBuff+0		; Min
	mov	w6,_DatBuff+2		; Avg
	mov	w7,_DatBuff+4		; Max

	and	#0xFF,w0		; High Byte ausblenden
	and	#0xFF,w1		; (sind die Linienpositionen)
	and	#0xFF,w2		; Low Byte sind die num. Oberkanten 	

;1.15 Spalten 0..2 noch Grafikbereich    
	btsc	_DatBuff+14,#2
	bra	MMAExit

; je 5 Pixel pro Spalte setzen, es ist jeweils die oberste Position gespeichert 
;MaxTemp		
	sl	w2,#2,w3		; w3 = Y-Position des Pixel (0...47) x 4 (wg. Sprungabstand) 
;ein Pixel �ber Oberkante wird immer gel�scht		
	add	w3,#4,w3		; w3 + 4 => ein Pixel �ber Oberkante
	bclr	SR,#C			; C-Flag l�schen
	rcall	MMAPixel		; Pixel entspr. C-Flag einstellen
;ab hier die 5 Zahlenpixel
	sub	w3,#4,w3
	rrc	_DatBuff+4		; Pixel in Carry schieben
	rcall	MMAPixel		; Pixel im Datenpuffer bearbeiten
	sub	w3,#4,w3
	rrc	_DatBuff+4
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+4
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+4
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+4
	rcall	MMAPixel
; ein Pixel unter Unterkante
	sub	w3,#4,w3		
	bclr	SR,#C			; Pixel l�schen
	rcall	MMAPixel

;1.15 Spalten 0..2 noch Grafikbereich    
	btsc	_DatBuff+14,#1
	bra	MMAExit

;AvgTemp
	sl	w1,#2,w3
	add	w3,#4,w3		; + 4 = ein Pixel �ber Oberkante
	bclr	SR,#C			; Pixel l�schen
	rcall	MMAPixel
;ab hier die 5 Zahlenpixel
	sub	w3,#4,w3
	rrc	_DatBuff+2
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+2
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+2
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+2
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+2
	rcall	MMAPixel
; ein Pixel unter Unterkante
	sub	w3,#4,w3		
	bclr	SR,#C			; Pixel l�schen
	rcall	MMAPixel

;1.15 Spalten 0..2 noch Grafikbereich    
	btsc	_DatBuff+14,#0
	bra	MMAExit

;MinTemp
	sl	w0,#2,w3
	add	w3,#4,w3		; + 4 = ein Pixel �ber Oberkante
	bclr	SR,#C			; Pixel l�schen
	rcall	MMAPixel
;ab hier die 5 Zahlenpixel
	sub	w3,#4,w3
	rrc	_DatBuff+0
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+0
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+0
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+0
	rcall	MMAPixel
	sub	w3,#4,w3
	rrc	_DatBuff+0
	rcall	MMAPixel
; ein Pixel unter Unterkante
	sub	w3,#4,w3		
	bclr	SR,#C			; Pixel l�schen
	rcall	MMAPixel
	bra	MMAExit

;horizontale Hilfslinien zeichen
MMADotLine:
	cp.b	w3,#24 			; Hilsflinien erst ab Spalte 24
	bra	nc,MMAExit
	and	w3,#1,w4		; nur jede 2. Spalte
	bra	z,MMAExit
	mov	#0x20,w4		; nur wenn was auszugeben ist
	mov.b	[w5+4],w6		; (w5+4 = 1/10 Grad Stell MinTemp)  	
	cpsne	w4,w6			 
	bra	MMAExit

	swap	w0			; Position der Linien 
	swap	w1			; ins Low Byte
	swap	w2
	and	#0xFF,w0		; High Byte ausblenden
	and	#0xFF,w1		
	and	#0xFF,w2
	sl	w0,#2,w3
	bset	SR,#C
	rcall	MMAPixel
	sl	w1,#2,w3
	rcall	MMAPixel
	sl	w2,#2,w3
	rcall	MMAPixel
MMAExit:
	mov	_DatBuff+12,w4		; ge�nderte Spalte (48 Bit)
	mov	_DatBuff+6,w6		; zur�ckladen
	mov	w6,[w4]
	mov	_DatBuff+8,w6
	mov	w6,[w4+0x100]
	mov	_DatBuff+10,w6
	mov	w6,[w4+0x200]
	return
;-------------------------------------------------------------------------
; relativer Sprung zum Einstellen eines Pixels in der Grafik
; Bereich 0...47, andere Werte erzeugen Programmabsturz !
; Eingang:	w3		Position des Pixel (0...47) mal 4
;  		Carry Flag 	1=Pixel gesetzt, 0=Pixel nicht gesetzt
;-------------------------------------------------------------------------
	return			;die returns m�ssen hier stehen wegen
	return			;mgl. negativer Sprungrichtung
	return
MMAPixel:
	bra	w3		
;Zeile 0
	bclr	_DatBuff+6,#7
	btsc	SR,#C
	bset	_DatBuff+6,#7
	return
;Zeile 1
	bclr	_DatBuff+6,#6
	btsc	SR,#C
	bset	_DatBuff+6,#6
	return
;Zeile 2
	bclr	_DatBuff+6,#5
	btsc	SR,#C
	bset	_DatBuff+6,#5
	return
;Zeile 3
	bclr	_DatBuff+6,#4
	btsc	SR,#C
	bset	_DatBuff+6,#4
	return
;Zeile 4
	bclr	_DatBuff+6,#3
	btsc	SR,#C
	bset	_DatBuff+6,#3
	return
;Zeile 5
	bclr	_DatBuff+6,#2
	btsc	SR,#C
	bset	_DatBuff+6,#2
	return
;Zeile 6
	bclr	_DatBuff+6,#1
	btsc	SR,#C
	bset	_DatBuff+6,#1
	return
;Zeile 7
	bclr	_DatBuff+6,#0
	btsc	SR,#C
	bset	_DatBuff+6,#0
	return
;Zeile 8
	bclr	_DatBuff+6,#15
	btsc	SR,#C
	bset	_DatBuff+6,#15
	return
;Zeile 9
	bclr	_DatBuff+6,#14
	btsc	SR,#C
	bset	_DatBuff+6,#14
	return
;Zeile 10
	bclr	_DatBuff+6,#13
	btsc	SR,#C
	bset	_DatBuff+6,#13
	return
;Zeile 11
	bclr	_DatBuff+6,#12
	btsc	SR,#C
	bset	_DatBuff+6,#12
	return
;Zeile 12
	bclr	_DatBuff+6,#11
	btsc	SR,#C
	bset	_DatBuff+6,#11
	return
;Zeile 13
	bclr	_DatBuff+6,#10
	btsc	SR,#C
	bset	_DatBuff+6,#10
	return
;Zeile 14
	bclr	_DatBuff+6,#9
	btsc	SR,#C
	bset	_DatBuff+6,#9
	return
;Zeile 15
	bclr	_DatBuff+6,#8
	btsc	SR,#C
	bset	_DatBuff+6,#8
	return

;Zeile 16
	bclr	_DatBuff+8,#7
	btsc	SR,#C
	bset	_DatBuff+8,#7
	return
;Zeile 17
	bclr	_DatBuff+8,#6
	btsc	SR,#C
	bset	_DatBuff+8,#6
	return
;Zeile 18
	bclr	_DatBuff+8,#5
	btsc	SR,#C
	bset	_DatBuff+8,#5
	return
;Zeile 19
	bclr	_DatBuff+8,#4
	btsc	SR,#C
	bset	_DatBuff+8,#4
	return
;Zeile 20
	bclr	_DatBuff+8,#3
	btsc	SR,#C
	bset	_DatBuff+8,#3
	return
;Zeile 21
	bclr	_DatBuff+8,#2
	btsc	SR,#C
	bset	_DatBuff+8,#2
	return
;Zeile 22
	bclr	_DatBuff+8,#1
	btsc	SR,#C
	bset	_DatBuff+8,#1
	return
;Zeile 23
	bclr	_DatBuff+8,#0
	btsc	SR,#C
	bset	_DatBuff+8,#0
	return
;Zeile 24
	bclr	_DatBuff+8,#15
	btsc	SR,#C
	bset	_DatBuff+8,#15
	return
;Zeile 25
	bclr	_DatBuff+8,#14
	btsc	SR,#C
	bset	_DatBuff+8,#14
	return
;Zeile 26
	bclr	_DatBuff+8,#13
	btsc	SR,#C
	bset	_DatBuff+8,#13
	return
;Zeile 27
	bclr	_DatBuff+8,#12
	btsc	SR,#C
	bset	_DatBuff+8,#12
	return
;Zeile 28
	bclr	_DatBuff+8,#11
	btsc	SR,#C
	bset	_DatBuff+8,#11
	return
;Zeile 29
	bclr	_DatBuff+8,#10
	btsc	SR,#C
	bset	_DatBuff+8,#10
	return
;Zeile 30
	bclr	_DatBuff+8,#9
	btsc	SR,#C
	bset	_DatBuff+8,#9
	return
;Zeile 31
	bclr	_DatBuff+8,#8
	btsc	SR,#C
	bset	_DatBuff+8,#8
	return

;Zeile 32
	bclr	_DatBuff+10,#7
	btsc	SR,#C
	bset	_DatBuff+10,#7
	return
;Zeile 33
	bclr	_DatBuff+10,#6
	btsc	SR,#C
	bset	_DatBuff+10,#6
	return
;Zeile 34
	bclr	_DatBuff+10,#5
	btsc	SR,#C
	bset	_DatBuff+10,#5
	return
;Zeile 35
	bclr	_DatBuff+10,#4
	btsc	SR,#C
	bset	_DatBuff+10,#4
	return
;Zeile 36
	bclr	_DatBuff+10,#3
	btsc	SR,#C
	bset	_DatBuff+10,#3
	return
;Zeile 37
	bclr	_DatBuff+10,#2
	btsc	SR,#C
	bset	_DatBuff+10,#2
	return
;Zeile 38
	bclr	_DatBuff+10,#1
	btsc	SR,#C
	bset	_DatBuff+10,#1
	return
;Zeile 39
	bclr	_DatBuff+10,#0
	btsc	SR,#C
	bset	_DatBuff+10,#0
	return
;Zeile 40
	bclr	_DatBuff+10,#15
	btsc	SR,#C
	bset	_DatBuff+10,#15
	return
;Zeile 41
	bclr	_DatBuff+10,#14
	btsc	SR,#C
	bset	_DatBuff+10,#14
	return
;Zeile 42
	bclr	_DatBuff+10,#13
	btsc	SR,#C
	bset	_DatBuff+10,#13
	return
;Zeile 43
	bclr	_DatBuff+10,#12
	btsc	SR,#C
	bset	_DatBuff+10,#12
	return
;Zeile 44
	bclr	_DatBuff+10,#11
	btsc	SR,#C
	bset	_DatBuff+10,#11
	return
;Zeile 45
	bclr	_DatBuff+10,#10
	btsc	SR,#C
	bset	_DatBuff+10,#10
	return
;Zeile 46
	bclr	_DatBuff+10,#9
	btsc	SR,#C
	bset	_DatBuff+10,#9
	return
;Zeile 47
	bclr	_DatBuff+10,#8
	btsc	SR,#C
	bset	_DatBuff+10,#8
	return
;Eine Zeile unterhalb des g�ltigen Bereichs um unn�tige Abfrage zu vermeiden
	return
;-------------------------------------------------------------------------
;Pixeldaten aus Fonttabelle FntVar4x6 laden
;Abruchbedingung 128+4 
;-------------------------------------------------------------------------
_LadeVar4x6:
	clr	TBLPAG
	mov	#FntVar4x6,w2

LadeVar4x6Next:

	mov.b	[w0++],w3		;Lese n�chstes Zeichen nach w3				
	and	#0xFF,w3
	bra	z,LadeVar4x6End		;Ist es 0 ? ja ->

	sub	#0x20,w3		;20h abziehen
	sl	w3,#1,w4		;mal 6		
	sl	w3,#2,w3
	add	w3,w4,w3
	add	w2,w3,w3		;Startadresse Pixel-Tabelle addieren	

	TBLRDL 	[w3++],w6		;Spalte 1
	mov.b	w6,[w1++]
	swap	w6			;Spalte 2	
	mov.b	w6,[w1++]
	TBLRDL 	[w3++],w6		;Spalte 3	
	btss	w6,#7
	mov.b	w6,[w1++]
	swap	w6			;Spalte 4
	btss	w6,#7
	mov.b	w6,[w1++]
	TBLRDL 	[w3++],w6		;Spalte 5
	btss	w6,#7
	mov.b	w6,[w1++]
	swap	w6			;Spalte 6
	btss	w6,#7
	mov.b	w6,[w1++]
	bra	LadeVar4x6Next	
LadeVar4x6End:
	return;
;-------------------------------------------------------------------------
;Pixeldaten aus Fonttabelle 5x6 laden
;
;-------------------------------------------------------------------------
_Lade5x6:
	clr	TBLPAG
	mov	#Fnt5x6,w2		;Startadresse Pixel-Tabelle nach w2

Ld5x6HiNext:
	mov.b	[w0++],w3		;Lese n�chstes Zeichen nach w3				
	and	#0xFF,w3
	cp0.b	w3			;Ist es 0 ?	
	bra	z,Ld5x6HiEnd		;ja ->

	sub	#0x20,w3		;20h abziehen
	sl	w3,#1,w4		;mal 6		
	sl	w3,#2,w3
	add	w3,w4,w3
	add	w2,w3,w3		;Startadresse Pixel-Tabelle addieren	

	
;	cp	w4,#4			;Ist es '"' ?
;	btss	SR,#Z
;	cp	w4,#10			;Ist es '%' ?
;	btsc	SR,#Z
;	clr.b	[w1++]			;ja, 2 Leerspalten einf�gen	
;	btsc	SR,#Z
;	clr.b	[w1++]

	TBLRDL 	[w3++],w6		;Spalte 1
	sl	w6,#1,w6
	mov.b	w6,[w1++]
	swap	w6			;Spalte 2	
	mov.b	w6,[w1++]

	TBLRDL 	[w3++],w6		;Spalte 3	
	sl	w6,#1,w6
	mov.b	w6,[w1++]
	swap	w6			;Spalte 4
	mov.b	w6,[w1++]

	sub	#28,w4			;Ist es '.' ?
	bra	z,Ld5x6HiNext

	TBLRDL 	[w3++],w6		;Spalte 5
	sl	w6,#1,w6
	mov.b	w6,[w1++]
	swap	w6			;Spalte 6
	mov.b	w6,[w1++]

	bra	Ld5x6HiNext	

Ld5x6HiEnd:
	return
;-------------------------------------------------------------------------
;Pixeldaten aus Tabelle Fnt4x6 laden und Trennlinie f�r obere Statuszeile einf�gen
;Jedes Zeichen besteht aus 4 Byte, in einer Page. Die Tabelle 
;beginnt mit Leerzeichen 20h, daher muss von jedem Ascii-Zeichen 20h vorher
;abgezogen werden.
;Zur Endekennung muss eine 0 in den Ascii-Buffer geschrieben werden.
;Eingang:	W0	Startadresse der zu wandelnden Ascii-Daten im RAM
;		W1	Startadresse des Pixelbuffers im RAM
;Ausgang:	Pixeldaten im Pixelbuffer 	
;-------------------------------------------------------------------------
_Lade4x6Hi:
	clr	TBLPAG
	mov	#Fnt4x6,w2		;Startadresse Pixel-Tabelle nach w2

;	mov	#0x8080,w7		;Begrenzungslinie oben	
	mov	#0x4040,w7		

Ld4x6HiNext:
	mov.b	[w0++],w6		;Lese n�chstes Zeichen				
	and	#0xFF,w6
	cp0.b	w6			;Ist es Endekennung 0 ?	
	bra	z,Ld4x6HiEnd		;ja ->

	sub.b	#0x20,w6		;20h abziehen
	sl	w6,#2,w3		;mal 4		
	add	w2,w3,w3		;zur Startadresse addieren	
	TBLRDL 	[w3++],w4 		;Lese 2 Byte ins RAM
	ior	w4,w7,[w1++]

;	cp	w6,#2			;Ist es Chr(34) -> (Leerzeichen) 2 Spalten ?
;	bra	z,Ld4x6HiNext
;	cp	w6,#14			;Ist es '.' ?	-> 2 Spalten ?
;	bra	z,Ld4x6HiNext
;	cp	w6,#26			;Ist es ':' ?	-> 2 Spalten ?
;	bra	z,Ld4x6HiNext

	TBLRDL 	[w3++],w4		;Lese 2 Byte ins RAM
	ior	w4,w7,[w1++]
	bra	Ld4x6HiNext
Ld4x6HiEnd:
	return


_Lade4x6Lo:
	clr	TBLPAG
	mov	#Fnt4x6,w2		;Startadresse Pixel-Tabelle nach w2

;	mov	#0x0101,w7		;Begrenzungslinie unten	
	mov	#0x0202,w7

Ld4x6LoNext:
	mov.b	[w0++],w6		;Lese n�chstes Zeichen nach w3				
	and	#0xFF,w6
	cp0.b	w6			;Ist es 0 ?	
	bra	z,Ld4x6LoEnd		;ja ->
	sub.b	#0x20,w6		;20h abziehen
	sl	w6,#2,w3		;mal 4		
	add	w2,w3,w3		;zur Startadresse addieren	
	TBLRDL 	[w3++],w4 		;Lese 2 Byte ins RAM
	sl	w4,#2,w4		;2 Pixel nach unten verschieben <--, 
	btss	w4,#15			; Ist es Chr(128) -> '|' ?	   |	
	sl	w4,#1,w4		;1 Pixel nach unten verschieben <--'
	ior	w4,w7,[w1++]

;	cp	w6,#2			;Ist es Chr(34) -> Leerzeichen 2 Spalten ?
;	bra	z,Ld4x6LoNext
;	cp	w6,#14			;Ist es '.' ?	
;	bra	z,Ld4x6LoNext
;	cp	w6,#26			;Ist es ':' ?	
;	bra	z,Ld4x6LoNext

	TBLRDL 	[w3++],w4		;Lese 2 Byte ins RAM
	sl	w4,#3,w4		;3 Pixel nach unten verschieben	
	ior	w4,w7,[w1++]
	bra	Ld4x6LoNext

Ld4x6LoEnd:
	return
;-------------------------------------------------------------------------
;Pixeldaten aus Fonttabelle 8x8 laden
;Jedes Zeichen besteht aus 8 Byte, in einer Page. Die Tabelle 
;beginnt mit Leerzeichen 20h, daher muss von jedem Ascii-Zeichen 20h vorher
;abgezogen werden.
;Zur Endekennung muss eine 0 in den Ascii-Buffer geschrieben werden.
;Eingang:	W0	Startadresse der zu wandelnden Ascii-Daten im RAM
;		W1	Startadresse des Pixelbuffers im RAM
;Ausgang:	Pixeldaten im Pixelbuffer 	
;-------------------------------------------------------------------------
_Lade8x8:
	clr	TBLPAG
	mov	#Fnt8x8,w2			;Startadresse Pixel-Tabelle nach w2
Ld8x8Next:
	mov.b	[w0++],w3			;Lese n�chstes Zeichen nach w3				
	and	#0xFF,w3
	cp0.b	w3				;Ist es 0 ?	
	bra	z,Ld8x8End			;ja ->
	sub.b	#0x20,w3			;20h abziehen
	sl	w3,#3,w4			;mal 8		
	add	w2,w4,w4			;zur Startadresse addieren	
	TBLRDL 	[w4++],[w1++] 			;Lese 8 Byte ins RAM
	TBLRDL 	[w4++],[w1++]
	TBLRDL 	[w4++],[w1++]
	TBLRDL 	[w4++],[w1++]
	bra	Ld8x8Next
Ld8x8End:
	return
;-------------------------------------------------------------------------
;Pixeldaten aus Fonttabelle 8x16 laden
;Jedes Zeichen besteht aus 16 Byte, je 8 Byte pro Page. Die Tabelle 
;beginnt mit Leerzeichen 20h, daher muss von jedem Ascii-Zeichen 20h vorher
;abgezogen werden. Der Offset muss je nach Page 0 oder 8 sein.
;Zur Endekennung muss eine 0 in den Ascii-Buffer geschrieben werden.
;Eingang: 	W0	Offset in Byte 
;		W1	Startadresse der zu wandelnden Ascii-Daten im RAM
;		W2	Startadresse des Pixelbuffers im RAM
;Ausgang:	Pixeldaten im Pixelbuffer 	
;-------------------------------------------------------------------------
_Lade8x16:
	clr	TBLPAG
	mov	#Fnt8x16,w3			;Startadresse Pixel-Tabelle
	and	#0xFF,w0
	add	w0,w3,w0			;plus Offset nach w0
Ld8x16Next:
	mov.b	[w1++],w3			;Lese n�chstes Zeichen nach w3				
	and	#0xFF,w3
	cp0.b	w3				;Ist es 0 ?	
	bra	z,Ld8x16End			;ja ->
	sub.b	#0x20,w3			;20h abziehen
	sl	w3,#4,w3			;mal 16		
	add	w0,w3,w3			;zur Startadresse addieren	
	TBLRDL 	[w3++],[w2++] 			;Lese 8 Byte ins RAM
	TBLRDL 	[w3++],[w2++]
	TBLRDL 	[w3++],[w2++]
	TBLRDL 	[w3++],[w2++]
	bra	Ld8x16Next
Ld8x16End:
	return
;-------------------------------------------------------------------------
;Pixeldaten aus Fonttabelle 16x32 laden
;Jedes Zeichen besteht aus 64 Byte, je 16 Byte pro Page. Die Tabelle 
;beginnt mit Leerzeichen 20h, daher muss von jedem Ascii-Zeichen 20h vorher
;abgezogen werden. Der Offset muss je nach Page 0,16,32 oder 48 sein.
;Zur Endekennung muss eine 0 in den Ascii-Buffer geschrieben werden.
;Eingang: 	W0	Offset in Byte 
;		W1	Startadresse der zu wandelnden Ascii-Daten im RAM
;		W2	Startadresse des Pixelbuffers im RAM
;Ausgang:	Pixeldaten im Pixelbuffer 	
;-------------------------------------------------------------------------
_Lade16x32:
	clr	TBLPAG
	mov	#Fnt16x32,w3			;Startadresse Pixel-Tabelle
	and	#0xFF,w0
	add	w0,w3,w0			;plus Offset nach w0
Ld16x32Next:
	mov.b	[w1++],w3			;Lese n�chstes Zeichen nach w3				
	and	#0xFF,w3
	cp0.b	w3				;Ist es 0 ?	
	bra	z,Ld16x32End			;ja ->
	sub.b	#0x20,w3			;20h abziehen
	sl	w3,#6,w3			;mal 64		
	add	w0,w3,w3			;zur Startadresse addieren	
	TBLRDL 	[w3++],[w2++] 			;Lese 16 Byte ins RAM
	TBLRDL 	[w3++],[w2++]
	TBLRDL 	[w3++],[w2++]
	TBLRDL 	[w3++],[w2++]
	TBLRDL 	[w3++],[w2++]
	TBLRDL 	[w3++],[w2++]
	TBLRDL 	[w3++],[w2++]
	TBLRDL 	[w3++],[w2++]
	bra	Ld16x32Next
Ld16x32End:
	return
;-------------------------------------------------------------------------
;A) Multiplikation Rohwert mal EmiNK ( -> MulRWE )
;B) Messwerte aus Linearisierungstabelle lesen und interpolieren
;
;##############################################################################
;			N E U   32 Bit VK / 16 Bit NK
;##############################################################################
;
;	|Vorkomma-------------------------->|	|Nachkomma------->|
;	|w2-------------->|w1-------------->|	|w0-------------->|
;	
;Max:	|BFFFFIII IIIIIIII|IIIIIxxx|xxxxxxxx|	|xxxxxxxx|xxxxxxxx|	 
;	 |\  |\	  	       /			
;	 | \ | \Interpolieren /			
;	 |  \|  				
;	 | Feld 0...15 		 		
;	 |			  Feld 0...15	
; 	 Block 27	        	|\	
;			         	| \	
;			         	|  \	
;Min:	|00000000 00000000|00000000 000BFFFF|	|IIIIIIII|IIIIIIII|	 
;		          	       |	 \		 /
;		      		    Block 0	  \Interpolieren/
;
;-------------------------------------------------------------------------
_LinTemp:
	call	_MulRWE			;Rohwert mal EmiNK

;w0=Interpolieren, w1,w2 = Rohwert  
	mov	#28,w5			;Block 12...27
	ff1l	w2,w3			;finde erste 1 von links Rohwert
	bra	nc,BitAFnd		;Bit gefunden -> w3=Position 1...16
	mov	#12,w5			;Block 0...11
	mov	w1,w2
	mov	w0,w1
	ff1l	w2,w3
	bra	c,BitSetMin		;Kein Bit gesetzt = Minimum ->
	cpsgt	w3,w5			;Pos 1...12 ok, 13...16 n.ok 
	bra	BitAFnd		
BitSetMin:
	mov	w5,w3			;Minimum = Pos 12
	mov	#16,w2			;minimale Rohwert Block 0 = 16
	mov	#0,w1
BitAFnd:
	sub	w5,w3,w5		;w5=Blockadresse 0...27
	subr	w3,#16,w6
	sl	w2,w3,w2		;1. gesetzte 1 links rausschieben	
	lsr	w1,w6,w7		;und mit low Word
	ior	w7,w2,w2		;unten auff�llen
	lsr	w2,#12,w4		;w4=Feld-Nr. 0...15
	sl	w1,w3,w1		;weitere Bits
	lsr	w0,w6,w0		;verschieben...
	ior	w1,w0,w1
	lsr	w1,#12,w1
	sl	w2,#4,w2
	ior	w1,w2,w1		;w1=Word zum interpolieren 0000..FFFF
	
	sl	w5,#4,w5		;Blockadresse mal 16
	add	w4,w5,w5		;plus Feld

	mov	#TabLin,w2		;w2=Startadresse Tabelle
	add	w2,w5,w2		;w2=w2+Adressenoffset
	add	w2,w5,w2		; mal 2
	TBLRDL 	[w2++],w0		;Ersten Tabellenwert lesen 
	mov	#0x01BF,w6		;Ist es letzter Eintrag (447) ?
	cpslt	w5,w6
	return				;ja, w0 = Messwert
	TBLRDL 	[w2],w2			;Zweiten Tabellenwert lesen 
	sub	w2,w0,w2		;mit Differenz
	mul.uu	w1,w2,w4		;interpolieren	
	btsc	w4,#15
	inc	w5,w5
	add	w0,w5,w0	
	return

;-------------------------------------------------------------------------
;Spezialmultiplikation 32*16/2^10 zur Berechnung RW(kurz) x TiFaktor(Range)
;Eingang: Rohwert, 32 Bit, signed in w0, w1
;	  Faktor 16 Bit, unsigned, mal 2^10 in w2	
;Ausgang: Ergebnis (geteilt durch 2^10) in w0, w1	
;
;Max. Ergebnis: (0xFFFFF * 0xFFFF) / 0x400 = 0x3FFFBC0
;-------------------------------------------------------------------------
_Mul3216Div10:
	lsr	w0,#(16-5),w3		;Wert w0,w1 mal 32 (2^5)
	sl	w0,#5,w0
	sl	w1,#5,w1
	ior	w1,w3,w1
;-------------------------------------------------------------------------
;Spezialmultiplikation 32*16/2^15 zur Berechnung Rohwert x TK
;Eingang: Wert, 32 Bit, signed in w0, w1 
;	  Faktor 16 Bit, unsigned, mal 2^15 in w2 (0x8000=1)	
;Ausgang: Ergebnis (geteilt durch 2^15) in w0, w1	
;-------------------------------------------------------------------------
_Mul3216Div15:
	bclr	SR,#C			;Wert w0,w1 mal 2
	rlc	w0,w0	
	rlc	w1,w1	
	mul.uu	w0,w2,w4		;mal Faktor
	mul.su	w1,w2,w2		;Vorzeichen beachten
	add	w5,w2,w0		;Ergebnis hat 32 Bit
	addc	w3,#0,w1
	return
;-------------------------------------------------------------------------
;Spezialmultiplikation 32*32 zur Berechnung Rohwert x EmiNK
;Eingang: Rohwert, 32 Bit, signed w0,w1 (max: 0x00FFFFFF)
;	  EmiNK, 32 Bit, unsigned w2,w3	(max: 0x00180000)
;
;Ausgang: 
;	signed long Rohwert	Ergebnis/Neg: Signed Rohwert = 0	
;				Ergebnis/Overflow: Signed Rohwert =0x7FFFFFFF	
;	unsigned int RohwNK	16 Bit Nachkomma zum interpolieren
;-------------------------------------------------------------------------
_MulRWE:
	btsc	w1,#15			;Negativer Rohwert ?
	bra	SetMin			;ja, Ergebnis = 0

	mul.uu	w1,w3,w4
	mul.uu	w1,w2,w6
	push	w7
	push	w6
	mul.uu	w0,w3,w6
	mul.uu	w0,w2,w0
	mov	w4,w2
	mov	w5,w3
	add	w1,w6,w1
	addc	w2,w7,w2
	addc	w3,#0,w3
	pop	w6
	pop	w7
	add	w1,w6,w1
	addc	w2,w7,w2
	addc	w3,#0,w3
	cp0	w3			;�berlauf ?
	bra	nz,SetMax		;ja
	return
SetMin:
	clr	w0
	clr	w1
	clr	w2
	return
SetMax:
	setm	w0
	setm	w1
	setm	w2
	return
;-------------------------------------------------------------------------
;Spezialmultiplikation 32*32/2^16 (unsigned, unsigned) 
;zur Berechnung EmiNK mit Rangeanpassung
;Eingang: 	w0,w1	EmiNK, 32 Bit, unsigned w2,w3	(max: 0x00180000)
;		w2,w3	RwKalLang[7] Kal.-Rohwert Range 7			
;-------------------------------------------------------------------------
_Mul3232Div16:
	mul.uu	w1,w3,w4
	mul.uu	w1,w2,w6
	push	w7
	push	w6
	mul.uu	w0,w3,w6
	mul.uu	w0,w2,w0
	mov	w4,w2
	mov	w5,w3
	add	w1,w6,w1
	addc	w2,w7,w2
	addc	w3,#0,w3
	pop	w6
	pop	w7
	add	w1,w6,w1
	addc	w2,w7,w2
	addc	w3,#0,w3
	cp0	w3			;�berlauf ?
	bra	nz,Mul3232Div16SetMax	;ja
	mov	w1,w0
	mov	w2,w1
	return
Mul3232Div16SetMax:
	setm	w0
	setm	w1
	return
;-------------------------------------------------------------------------
;Tk-Faktor aus Matrix ermiteln
;Eingang: unsigned int MessTemp [w0], in 1/10�C
;	unsigned int GerTemp [w1], High Byte = VK, Low Byte = NK
;Ausgang: TKFaktor * 2e15 [w0]
;1. Erste St�tzstelle finden > Messtemp
;2. Zwischen den Spalten (GerTemp) der St�tzstelle -1 interpolieren 
;3. Zwischen den Spalten (GerTemp) der St�tzstelle 0 interpolieren 
;4. Zwischen diesen zwei TK-Werten mit MessTemp interpolieren. 
;   Bei fehlender Matrix wird TK = 1 = 0x8000 gesetzt
;Zeitbedarf: ca. 85..95 IC
;-------------------------------------------------------------------------
_TK:
	clr	TBLPAG
	mov	#TabST,w2
	TBLRDL 	[w2++],w4			;Erste St�tzstellentemperatur lesen 
	com	w4,w3				;Ist es FFFF ?
	bra	z,StNotFound			;ja, Matrix nicht eingetragen -> TK auf 1 setzen (0x8000)	
	cp	w0,w4				;Ist St�tzstellentemperatur > Messtemp ?
	btss	SR,#C				
	mov	w4,w0				;ja, Messtemp = erste St�tzstellentemperatur
St1Lp:
	TBLRDL 	[w2++],w4			;Erste St�tzstelle > Messtemp suchen 
	cp	w0,w4
	bra	c,St1Lp	
	com	w4,w3				;St�tzstelle = FFFF ?
	bra	z,StNotFound			;ja, -> TK auf 1 setzen (0x8000)	
	dec2	w2,w2	
	TBLRDL 	[--w2],w3
	mov	#TabST,w5
	sub	w2,w5,w2
	mul.uu	w2,#17,w6
	mov	#TabTK,w2
	add	w2,w6,w2
	mov	#0x3333,w5			;Ger.-Temp / 5  * 256 ( -> * 51,2 -> 0x33,33)
	mul.uu	w1,w5,w6			
	sl	w7,#8,w1
	lsr	w6,#8,w6
	mov.b	w6,w1				;w1 = 5� Spaltenoffset in 256el
	lsr	w7,#8,w6
	add	w6,w6,w6
	add	w2,w6,w2
	sub	w0,w3,w0			;w0 = Temp.-Diff. Messwert - St�tzA
	sub	w4,w3,w4			;w4 = Temp.-Diff. St�tzB - St�tzA 
	TBLRDL 	[w2++],w5			;w5 = Basis TK
	TBLRDL 	[w2],w6
	sub	w5,w6,w6
	mul.uu	w6,w1,w6
	sub	w5,w7,w5			;w5 = Basis-TK + Spaltenoffset (St�tz A)
	add	#0x20,w2
	TBLRDL 	[w2++],w6
	TBLRDL 	[w2],w2
	sub	w6,w2,w2
	mul.uu	w1,w2,w2
	sub	w6,w3,w6			;w6 = TK + Spaltenoffset (St�tz B)
 	cpsgt	w5,w6				;Interpolieren zwischen den St�tzstellen
	bra	StBGrStA
	sub	w5,w6,w6			;St�tz A > B
	mul.uu	w6,w0,w2
	repeat	#17
	div.ud	w2,w4
	sub	w5,w0,w0
	return
StBGrStA:
	sub	w6,w5,w6			;St�tz B >= A
	mul.uu	w6,w0,w2
	repeat	#17
	div.ud	w2,w4
	add	w5,w0,w0
	return
StNotFound:
	mov	#0x8000,w0			;TK = 1
	return
;-------------------------------------------------------------------------
;Umwandlung Ascii nach Hex
;Eingang: 2 Byte Ascii (w0,w1)
;Ausgang: 1 Byte Hex in (w0)
;Beispiel: Ascii 0x41(A) + 0x46(F) = 0x00AF (in w0)
;-------------------------------------------------------------------------
_AsciiHex:
	btsc	w0,#6			;Falls Kleinbuchstaben
	bclr.b	w0,#5			;Grossbuchstaben erzeugen
	btsc	w1,#6
	bclr.b	w1,#5
	mov	#0x41,w2
	cpslt	w0,w2
	add.b	#9,w0
	and	#0x000F,w0
	sl	w0,#4,w0
	cpslt	w1,w2
	add.b	#9,w1
	and	#0x000F,w1
	ior	w1,w0,w0
	return
;-------------------------------------------------------------------------
;Eingang: w0	Flash-Addresse ab der gelesen wird (NUR gradzahlig)
;	  w1	Anzahl Worte (1...4)
;	  w2	RAM-Addresse mit der verglichen wird 
;Ausgang: 1 bis 4 Word Data ab RAM-Adresse 
;-------------------------------------------------------------------------
_FlashVerify:
	clr	TBLPAG
	clr.b	w3				;Fehlerz�hler = 0
FlashVfLp:
	TBLRDL 	[w0++],w4 			;Lese low Wort nach W4
	cp	w4,[w2++]
	bra	z,NoFlashVfErr
	inc.b	w3,w3
NoFlashVfErr:
	dec.b	w1,w1
	bra	nz,FlashVfLp	
	mov.b	w3,w0
	return
;-------------------------------------------------------------------------
;Eingang: w0	Flash-Addresse ab der gelesen wird (NUR gradzahlig)
;	  w1	Anzahl Worte (1...4)
;	  w2	RAM-Addresse in die kopiert wird 
;Ausgang: 1 bis 4 Word Data ab RAM-Adresse 
;-------------------------------------------------------------------------
_FlashRead:
	clr	TBLPAG
FlashRdLp:
	TBLRDL 	[w0++],w4 			;Lese low Wort nach W4
	mov	w4,[w2++]
	dec.b	w1,w1
	bra	nz,FlashRdLp	
	return
;-------------------------------------------------------------------------
;Flash schreiben -> wortweise
;ist die Flash-Adresse eine 512-Word-Grenze
;wird die betreffende Page vorher gel�scht.
;Eingang: w0	Flash schreiben ab Addresse (NUR gradzahlig !)
;	  w1	Anzahl Word schreiben n = 1...4
;	  w2	RAM-Addresse
;Ausgang: --- 
;-------------------------------------------------------------------------
_FlashWrite:
	clr	TBLPAG
	mov	w0,w4			;Flash-Adresse sichern in w4
FlashWrtLp:
	mov	#1023,w0		;Ist es eine 512-Wort-Grenze ?	
	and	w4,w0,w0
	bra	nz,FlashWrtWort		;nein ->
	mov	w4,w0
	call	_FlashErasePage		;ja, zuerst Page l�schen
FlashWrtWort:
	TBLWTL 	[w2++],[w4++]		;N�chstes Wort in Latch laden 
	mov 	#0x4003,W0 		;NVMCON konfigurieren, 1 Wort ins data Flash
	mov 	W0,NVMCON
	push 	SR 			;Disable interrupts, if enabled 
	mov	#0x00E0,w0 
	ior 	SR
	mov 	#0x55,w0
	mov 	W0, NVMKEY 
	mov	#0xAA,w0 
	mov 	W0, NVMKEY 		; NOP not required 
	bset 	NVMCON,#WR 		; Start the program/erase cycle 
	nop 
	nop 
	pop 	SR 			; Re-enable interrupts
	dec.b	w1,w1
	bra	nz,FlashWrtLp	
	return
;-------------------------------------------------------------------------
;Flash Page l�schen. Flash-Bereich = A000...ABFF = 3 Pages, je 512 Word
;Eingang: w0	Adresse einer 512 Wort-Grenze A000,A400,A800 
;-------------------------------------------------------------------------
_FlashErasePage:
	clr 	TBLPAG 
	TBLWTL 	w0,[w0] 		;Dummy Read zum Adresse einstellen
	mov 	#0x4042,w0 		;Setup NVMCON to erase one row of Flash
	mov 	w0,NVMCON  
	push 	SR 			;Disable interrupts
	mov 	#0x00E0,w0 
	ior 	SR 
 	mov 	#0x55,w0 		;Write the KEY Sequence
	mov 	w0,NVMKEY 
	mov 	#0xAA,w0 
	mov 	w0,NVMKEY 
 	bset 	NVMCON,#WR 		;Start the erase operation
 	nop 				;2 NOPs erforderlich
	nop  
	pop 	SR			;Re-enable interrupts
	return
;-------------------------------------------------------------------------
;HexBcd5: Umwandlung 2 Byte Hex -> 5 Zeichen BCD (0...65535)
;	Eingang: 1 Word Daten, 2 Byte Zeiger RAM-Adresse
;	Ausgang: 5 Zeichen BCD ab Zeiger RAM-Adresse
;	Beispiel: 0xABCD = 43981 = 0x04,0x03,0x09,0x08,0x01
;Dauer: 22 IC (f�r alle Werte)
;-------------------------------------------------------------------------
_HexBcd5:
	mov		w1,w4
	mul.uu	w0,#6,w2
	mov		#0x8DB9,w1
	mul.uu	w1,w0,w0
	add		w1,w2,w0
	addc.b	w3,#0,[w4++]
	inc		w0,w0
	mul.uu	w0,#10,w0
	mov.b	w1,[w4++]
	mul.uu	w0,#10,w0
	mov.b	w1,[w4++]
	mul.uu	w0,#10,w0
	mov.b	w1,[w4++]
	mul.uu	w0,#10,w0
	mov.b	w1,[w4++]
	return

;eine andere M�glichkeit....dauert aber viel l�nger !
;	setm.b	[w1]
;	mov	#10000,w2
;HexBcd_10000:
;	inc.b	[w1],[w1]
;	sub	w0,w2,w0
;	bra	c,HexBcd_10000
;	mov	#10,w3
;	mov.b	w3,[++w1]
;HexBcd_1000:
;	dec.b	[w1],[w1]
;	add	#1000,w0
;	bra	n,HexBcd_1000
;	setm.b	[++w1]
;HexBcd_100:
;	inc.b	[w1],[w1]
;	sub	#100,w0
;	bra	c,HexBcd_100
;	mov	#10,w3
;	mov.b	w3,[++w1]
;HexBcd_10:
;	dec.b	[w1],[w1]
;	add	#10,w0
;	bra	n,HexBcd_10
;	mov.b	w0,[++w1]
;	return
;-------------------------------------------------------------------------
;HexAsc5: Umwandlung 2 Byte Hex -> 5 Zeichen BCD-Ascii (0...65535)
;	Eingang: 	w0:	1 Word Daten 2 Byte
;				w1:	Zeiger RAM-Adresse
;	Ausgang: 5 Zeichen BCD-Ascii ab Zeiger RAM-Adresse
;	Beispiel: 0xABCD = 43981 = 0x34,0x33,0x39,0x38,0x31
;
;HexAsc4: Umwandlung 2 Byte Hex -> 4 Zeichen BCD-Ascii (0...9999)
;	Eingang: 	w0:	1 Word Daten 2 Byte
;				w1:	Zeiger RAM-Adresse
;	Ausgang: 4 Zeichen BCD-Ascii ab Zeiger RAM-Adresse
;	Beispiel: 0x12AB = 4779 = 0x34,0x37,0x37,0x39
;
;HexAsc2: Umwandlung 1 Byte Hex -> 2 Zeichen BCD-Ascii (0...99)
;	Eingang: 	w0:	1 Byte Daten
;				w1:	Zeiger RAM-Adresse
;	Ausgang: 2 Zeichen BCD-Ascii ab Zeiger RAM-Adresse
;	Beispiel: 0x1F = 31 = 0x33,0x31
;-------------------------------------------------------------------------
_HexAsc5:
	mov		w1,w4
	mul.uu	w0,#6,w2
	mov		#0x8DB9,w1
	mul.uu	w1,w0,w0
	add		w1,w2,w0
	mov		#'0',w2
	addc.b	w3,w2,[w4++]
	inc		w0,w0
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	return
_HexAsc4:
	mov		w1,w4
	mul.uu	w0,#6,w2
	mov		#0x8DB9,w1
	mul.uu	w1,w0,w0
	add		w1,w2,w0
	mov		#'0',w2
	inc		w0,w0
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	return
_HexAsc2:
	and		#0xFF,w0
	mov		w1,w4
	mul.uu	w0,#6,w2
	mov		#0x8DB9,w1
	mul.uu	w1,w0,w0
	add		w1,w2,w0
	mov		#'0',w2
	inc		w0,w0
	mov		#1000,w3
	mul.uu	w0,w3,w0
	ior.b	w1,w2,[w4++]
	mul.uu	w0,#10,w0
	ior.b	w1,w2,[w4++]
	return

;#### Altvariante, dauert l�nger
;_HexAsc5:
;	mov	#0x2F,w3
;	mov	#10000,w2
;HexAsc_10000:
;	inc	w3,w3
;	sub	w0,w2,w0
;	bra	c,HexAsc_10000
;	mov.b	w3,[w1++]
;	add	w0,w2,w0
;_HexAsc4:
;	mov	#0x2F,w3
;HexAsc_1000:
;	inc	w3,w3
;	sub	#1000,w0
;	bra	c,HexAsc_1000
;	mov.b	w3,[w1++]
;	add	#1000,w0
;	mov	#0x2F,w3
;HexAsc_100:
;	inc	w3,w3
;	sub	#100,w0
;	bra	c,HexAsc_100
;	mov.b	w3,[w1++]
;	add	#100,w0
;_HexAsc2:
;	mov	#0x2F,w3
;HexAsc_10:
;	inc	w3,w3
;	sub.b	#10,w0
;	bra	c,HexAsc_10
;	mov.b	w3,[w1++]
;	add.b	#10+0x30,w0
;	mov.b	w0,[w1++]
;	return
;-------------------------------------------------------------------------
;Umwandlung 4 Byte Hex -> 8 Byte Ascii
;Eingang: 4 Byte Hex in w0,w1, 2 Byte Zeiger auf RAM-Adresse in w2
;Ausgang: 8 Byte Ascii ab Zeiger RAM-Adresse
;Beispiel: Hex 0x0123AFFE =  
;0x30(0) + 0x31(1) + 0x32(2) + 0x33(3) + 0x41(A) + 0x46(F) + 0x46(F) + 0x45(E)
;-------------------------------------------------------------------------
_wHexAscii:
	mov	w0,w4
	mov	w1,w0
	mov	w2,w1
	call	_iHexAscii
	mov	w4,w0
;-------------------------------------------------------------------------
;Umwandlung 2 Byte Hex -> 4 Byte Ascii
;Eingang: 2 Byte Hex in w0, 2 Byte Zeiger auf RAM-Adresse in w1
;Ausgang: 4 Byte Ascii ab Zeiger RAM-Adresse
;Beispiel: Hex 0xAFFE = Ascii 0x41(A) + 0x46(F) + 0x46(F) + 0x45(E)
;-------------------------------------------------------------------------
_iHexAscii:
	mov	w0,w3
	lsr	w0,#8,w0
	call	_cHexAscii
	mov.b	w3,w0
;-------------------------------------------------------------------------
;Umwandlung 1 Byte Hex -> 2 Byte Ascii
;Eingang: 1 Byte Hex in w0, 2 Byte Zeiger auf RAM-Adresse in w1
;Ausgang: 2 Byte Ascii ab Zeiger RAM-Adresse
;Beispiel: Hex 0xAF = Ascii 0x41(A) + 0x46(F)
;-------------------------------------------------------------------------
_cHexAscii:
	lsr	w0,#4,w2	;1. High Nibble		
	and	#0x0F,w2
	cp	w2,#0x0A
	btsc	SR,#C
	add	w2,#0x07,w2
	add	#0x30,w2
	mov.b	w2,[w1++]
	and	#0x0F,w0	;2. Low Nibble
	cp	w0,#0x0A
	btsc	SR,#C
	add	w0,#0x07,w0
	add	#0x30,w0
	mov.b	w0,[w1++]
	return
;-------------------------------------------------------------------------
;Wandelt 1/10�C -> 1/10�F
;Eingang:	W0	Messwert in 1/10�C 
;Ausgang:	W0	Messwert in 1/10�F
;Rechenweg:	1/10�F = 1/10�C * 1,8 + 320 
;-------------------------------------------------------------------------
_WandleC10F10:
	mov	#0xE667,w2	;Messwert erst mal 0,9
	mul.uu	w2,w0,w0	
	sl	w0,w0		;dann mal 2
	sl	w1,w0
	add	#320,w0		;plus 320
	return
;-------------------------------------------------------------------------
;Tabellenbereich 0xA000...0xABFF =  3072 Byte (1536 adressierbare Word) 
;-------------------------------------------------------------------------
.data
.space   2200
.section *,psv,address(TabTK)
;TK 300�C 17 Spaltenwerte
	.word 47545,44134,40968,38029,35300,32768,30417,28235,26210,24329,22584,20964,19460,18064,16768,15565,14448
;TK 500�C 17 Spaltenwerte
	.word 40090,38505,36983,35521,34117,32768,31473,30228,29033,27885,26783,25724,24707,23730,22792,21891,21026
;TK 600�C 17 Spaltenwerte
	.word 37584,36567,35578,34615,33679,32768,31882,31019,30180,29364,28569,27796,27044,26313,25601,24908,24235

.section *,psv,address(TabST)
	.word 3000,5000,6000,0xFFFF

.section *,psv,address(TabLin);Test-Linearisierungstabelle 90-887,5�C, 28 x 16 Eintr�ge a 1 Word
	.word 900,925,950,975,1000,1025,1050,1075,1100,1125,1150,1175,1200,1225,1250,1275	
	.word 1300,1325,1350,1375,1400,1425,1450,1475,1500,1525,1550,1575,1600,1625,1650,1675
	.word 1700,1725,1750,1775,1800,1825,1850,1875,1900,1925,1950,1975,2000,2025,2050,2075
	.word 2100,2125,2150,2175,2200,2225,2250,2275,2300,2325,2350,2375,2400,2425,2450,2475
	.word 2500,2525,2550,2575,2600,2625,2650,2675,2700,2725,2750,2775,2800,2825,2850,2875
	.word 2900,2925,2950,2975,3000,3025,3050,3075,3100,3125,3150,3175,3200,3225,3250,3275
	.word 3300,3325,3350,3375,3400,3425,3450,3475,3500,3525,3550,3575,3600,3625,3650,3675
	.word 3700,3725,3750,3775,3800,3825,3850,3875,3900,3925,3950,3975,4000,4025,4050,4075
	.word 4100,4125,4150,4175,4200,4225,4250,4275,4300,4325,4350,4375,4400,4425,4450,4475
	.word 4500,4525,4550,4575,4600,4625,4650,4675,4700,4725,4750,4775,4800,4825,4850,4875
	.word 4900,4925,4950,4975,5000,5025,5050,5075,5100,5125,5150,5175,5200,5225,5250,5275
	.word 5300,5325,5350,5375,5400,5425,5450,5475,5500,5525,5550,5575,5600,5625,5650,5675
	.word 5700,5725,5750,5775,5800,5825,5850,5875,5900,5925,5950,5975,6000,6025,6050,6075
	.word 6100,6125,6150,6175,6200,6225,6250,6275,6300,6325,6350,6375,6400,6425,6450,6475
	.word 6500,6525,6550,6575,6600,6625,6650,6675,6700,6725,6750,6775,6800,6825,6850,6875
	.word 6900,6925,6950,6975,7000,7025,7050,7075,7100,7125,7150,7175,7200,7225,7250,7275
	.word 7300,7325,7350,7375,7400,7425,7450,7475,7500,7525,7550,7575,7600,7625,7650,7675
	.word 7700,7725,7750,7775,7800,7825,7850,7875,7900,7925,7950,7975,8000,8025,8050,8075
	.word 8100,8125,8150,8175,8200,8225,8250,8275,8300,8325,8350,8375,8400,8425,8450,8475
	.word 8500,8525,8550,8575,8600,8625,8650,8675,8700,8725,8750,8775,8800,8825,8850,8875
	.word 8900,8925,8950,8975,9000,9025,9050,9075,9100,9125,9150,9175,9200,9225,9250,9275
	.word 9300,9325,9350,9375,9400,9425,9450,9475,9500,9525,9550,9575,9600,9625,9650,9675
	.word 9700,9725,9750,9775,9800,9825,9850,9875,9900,9925,9950,9975,10000,10025,10050,10075
	.word 10100,10125,10150,10175,10200,10225,10250,10275,10300,10325,10350,10375,10400,10425,10450,10475
	.word 10500,10525,10550,10575,10600,10625,10650,10675,10700,10725,10750,10775,10800,10825,10850,10875
	.word 10900,10925,10950,10975,11000,11025,11050,11075,11100,11125,11150,11175,11200,11225,11250,11275
	.word 11300,11325,11350,11375,11400,11425,11450,11475,11500,11525,11550,11575,11600,11625,11650,11675
	.word 11700,11725,11750,11775,11800,11825,11850,11875,11900,11925,11950,11975,12000,12025,12050,12075
;-------------------------------------------------------------------------
;Zeichensatztabelle 8x16 Bit, Ascii 32...126 
;96 Zeichen a 16 Byte = 1520 Byte = 760 Word
;Flashbereich 0x9000...0x95EF
;Beispiel: Ascii 53 = '5' ist wie folgt codiert:
;  	09 -------- 01 --------
;	10 ---11--- 02 111111--
;	11 --111--- 03 111111--
; 	12 --11---- 04 11--11--
; 	13 --11---- 05 11--11--
; 	14 --111111 06 11--11--
;	15 ---11111 07 1---11--
;	16 -------- 08 --------
;
;Pfeil runter auf Ascii 40		Pfeil rauf auf Ascii 41
;  	09 -------- 01 --------			09 -------- 01 --------
;	10 ------11 02 --------			10 -------- 02 11------
;	11 ----11-- 03 --------			11 -------- 03 --11----
; 	12 --111111 04 111111--			12 --111111 04 111111--	
; 	13 --111111 05 111111--			13 --111111 05 111111--
; 	14 ----11-- 06 --------			14 -------- 06 --11----
;	15 ------11 07 --------			15 -------- 07 11------	
;	16 -------- 08 --------			16 -------- 08 --------

;Batteriesymbol liegt auf Ascii 123...126
;  	09 -------- 01 --------		123	
;	10 -------- 02 --------
;	11 -------- 03 --------
; 	12 -------- 04 --------
; 	13 -------- 05 --------
; 	14 -------- 06 --------
;	15 -------- 07 --------
;	16 --111111 08 11111---
;
;  	09 --1----- 01 ----1---		124
;	10 --1----1 02 ----11--
;	11 --1----1 03 ----11--
; 	12 --1----1 04 ----11--
; 	13 --1----1 05 ----11--
; 	14 --1----1 06 ----11--
;	15 --1----1 07 ----11--
;	16 --1----1 08 ----1---
;
;  	09 --1----- 01 ----1---		125
;	10 --111111 02 11111---
;	11 --1----- 03 ----1---
; 	12 --1----1 04 ----1---
; 	13 --1----1 05 ----11--
; 	14 --1----1 06 ----11--
;	15 --1-1111 07 111-11--
;	16 --1----1 08 ----11--
;
;  	09 --1----1 01 ----11--		126	
;	10 --1----1 02 ----1---
;	11 --1----- 03 ----1---
; 	12 --111111 04 11111---
; 	13 -------- 05 --------
; 	14 -------- 06 --------
;	15 -------- 07 --------
;	16 -------- 08 --------
;-------------------------------------------------------------------------
.section *,psv,address(FontTab)	;Start der Zeichensatztabellen
Fnt8x16:
 	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;32 ' '

;das �Gradzeichen ist auf Ascii Nr. 33 (!) gelegt.
; 	.byte   0,  0,  0,252,252,  0,  0,  0,  0,  0,  0, 51, 51,  0,  0,  0	;33 '!'
	.byte  0,  0,  0,  12, 18,  18,  12,  0, 0,  0,  0,  0, 0,  0,  0,  0	;   '�'

  	.byte   0,252,252,  0,  0,252,252,  0,  0,  0,  0,  0,  0,  0,  0,  0	;34 '"'
  	.byte  48,252,252, 48, 48,252,252, 48,  3, 15, 15,  3,  3, 15, 15,  3	;35 '#'
  	.byte   0,120,252,207,207,204,140,  0,  0, 12, 12, 60, 60, 15,  7,  0	;36 '$'
  	.byte   0, 28, 28,192,240, 60, 12,  0,  0, 12, 15,  3,  0, 14, 14,  0	;37 '%'
  	.byte   0,206,255,243, 63, 14,  0,  0, 31, 63, 48, 51, 31, 63, 51,  0 	;38 '&'
  	.byte   0,  0,  0,252,252,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;39 '''

;Pfeil runter ist auf Ascii Nr. 40 '(' gelegt.
  	.byte   0,  0,  0, 252,252, 0,  0,  0,  0,  3, 12, 63, 63, 12,  3,  0	;40 'Pfeil runter'
;  	.byte   0,  0,  0,240,248, 28,  4,  0,  0,  0,  0, 15, 31, 56, 32,  0	;40 '('

;Pfeil rauf ist auf Ascii Nr. 41 ')' gelegt.
  	.byte   0,192, 48,252,252, 48,192,  0,  0,  0,  0, 63, 63,  0,  0,  0	;41 'Pfeil hoch'
;  	.byte   0,  4, 28,248,240,  0,  0,  0,  0, 32, 56, 31, 15,  0,  0,  0	;41 ')'

  	.byte 192,204,252,240,240,252,204,192,  0, 12, 15,  3,  3, 15, 12,  0	;42 '*'
  	.byte   0,192,192,248,248,192,192,  0,  0,  0,  0,  7,  7,  0,  0,  0	;43 '+'
  	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,192,124, 60,  0,  0,  0	;44 ','
  	.byte   0,192,192,192,192,192,192,  0,  0,  0,  0,  0,  0,  0,  0,  0	;45 '-'
  	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 60, 60,  0,  0,  0	;46 '.'
  	.byte   0,  0,  0,128,224,124, 28,  0,  0, 56, 62,  7,  1,  0,  0,  0	;47 '/'
  	.byte   0,248,252, 12,140,252,248,  0,  0, 31, 63, 49, 48, 63, 31,  0	;48 '0'
  	.byte   0,  0, 48,252,252,  0,  0,  0,  0, 48, 48, 63, 63, 48, 48,  0	;49 '1'
  	.byte   0, 56, 60, 12,204,252, 56,  0,  0, 48, 60, 63, 51, 48, 48,  0	;50 '2'
  	.byte   0, 12, 12,204,252, 60, 12,  0,  0, 28, 60, 48, 51, 63, 28,  0	;51 '3'
  	.byte   0,  0,192,240,252,252,  0,  0,  0, 15, 15, 12, 63, 63, 12,  0	;52 '4'
  	.byte   0,252,252,204,204,204,140,  0,  0, 24, 56, 48, 48, 63, 31,  0	;53 '5'
  	.byte   0,240,248,156,140,140,  0,  0,  0, 31, 63, 49, 49, 63, 31,  0	;54 '6'
  	.byte   0, 12, 12, 12,204,252, 60,  0,  0,  0, 60, 63,  3,  0,  0,  0	;55 '7'
  	.byte   0, 56,252,204,204,252, 56,  0,  0, 31, 63, 48, 48, 63, 31,  0	;56 '8'
  	.byte   0,120,252,204,204,252,248,  0,  0,  0, 48, 48, 56, 31, 15,  0	;57 '9'
  	.byte   0,  0,  0,240,240,  0,  0,  0,  0,  0,  0, 60, 60,  0,  0,  0	;58 ':'
  	.byte   0,  0,  0,240,240,  0,  0,  0,  0,  0,192,124, 60,  0,  0,  0	;59 ';'
  	.byte 128,192,224,112, 56, 24,  8,  0,  0,  1,  3,  7, 14, 12,  8,  0	;60 '<'
  	.byte   0, 48, 48, 48, 48, 48, 48,  0,  0,  3,  3,  3,  3,  3,  3,  0	;61 '='
  	.byte   8, 24, 56,112,224,192,128,  0,  8, 12, 14,  7,  3,  1,  0,  0	;62 '>'
  	.byte   0, 56, 60, 12,204,252, 56,  0,  0,  0,  0, 55, 55,  0,  0,  0	;63 '?'
	.byte 240,248, 28,204, 76,152,240,  0, 15, 31, 56, 51, 50, 51, 25,  0	;64 '@'
	.byte   0,240,248, 28, 28,248,240,  0,  0, 63, 63,  3,  3, 63, 63,  0	;65 'A'
	.byte   0,252,252,204,204,252,120,  0,  0, 63, 63, 48, 48, 63, 31,  0	;66 'B'
	.byte   0,248,252, 12, 12, 60, 56,  0,  0, 31, 63, 48, 48, 60, 28,  0	;67 'C'
	.byte   0,252,252, 12, 28,248,240,  0,  0, 63, 63, 48, 56, 31, 15,  0	;68 'D'
  	.byte   0,252,252,204,204,204, 12,  0,  0, 63, 63, 48, 48, 48, 48,  0	;69 'E'
  	.byte   0,252,252,204,204,204, 12,  0,  0, 63, 63,  0,  0,  0,  0,  0	;70 'F'
  	.byte   0,248,252, 12,204,204,204,  0,  0, 31, 63, 48, 48, 63, 31,  0	;71 'G'
  	.byte   0,252,252,192,192,252,252,  0,  0, 63, 63,  0,  0, 63, 63,  0	;72 'H'
  	.byte   0, 12, 12,252,252, 12, 12,  0,  0, 48, 48, 63, 63, 48, 48,  0	;73 'I'
	.byte   0,  0,  0,  0,  0,252,252,  0,  0, 28, 60, 48, 48, 63, 31,  0	;74 'J'
	.byte 252,252,192,240, 60, 12,  0,  0, 63, 63,  0,  3, 15, 60, 48,  0	;75 'K'
	.byte   0,252,252,  0,  0,  0,  0,  0,  0, 63, 63, 48, 48, 48, 48,  0	;76 'L'
	.byte 252,252,112,192,112,252,252,  0, 63, 63,  0,  1,  0, 63, 63,  0	;77 'M'
	.byte   0,252,252,224,128,252,252,  0,  0, 63, 63,  1,  7, 63, 63,  0	;78 'N'
  	.byte   0,248,252, 12, 12,252,248,  0,  0, 31, 63, 48, 48, 63, 31,  0	;79 'O'
  	.byte   0,252,252, 12, 12,252,248,  0,  0, 63, 63,  3,  3,  3,  1,  0	;80 'P'
  	.byte   0,248,252, 12, 12,252,248,  0,  0, 31, 63, 48, 24, 55, 47,  0	;81 'Q'
  	.byte 252,252,140,140,252,248,  0,  0, 63, 63,  1,  3, 15, 60, 48,  0	;82 'R'
  	.byte   0,120,252,204,140, 12, 12,  0,  0, 48, 48, 49, 51, 63, 30,  0	;83 'S'
	.byte   0, 12, 12,252,252, 12, 12,  0,  0,  0,  0, 63, 63,  0,  0,  0	;84 'T'
	.byte   0,252,252,  0,  0,252,252,  0,  0, 31, 63, 48, 48, 63, 31,  0	;85 'U'
	.byte   0,252,252,  0,  0,252,252,  0,  0,  3, 15, 60, 60, 15,  3,  0	;86 'V'
	.byte 252,252,  0,128,  0,252,252,  0, 63, 31, 14,  7, 14, 31, 63,  0	;87 'W'
	.byte   0, 28,124,224,224,124, 28,  0,  0, 56, 62,  7,  7, 62, 56,  0	;88 'X'
  	.byte   0, 60,252,192,192,252, 60,  0,  0,  0,  0, 63, 63,  0,  0,  0	;89 'Y'
  	.byte   0, 12, 12,204,252, 60, 12,  0,  0, 60, 63, 51, 48, 48, 48,  0	;90 'Z'
  	.byte   0,  0,  0,252,252, 12, 12,  0,  0,  0,  0, 63, 63, 48, 48,  0	;91 '['
  	.byte   0, 28,124,224,128,  0,  0,  0,  0,  0,  0,  1,  7, 62, 56,  0	;92 '\'
  	.byte   0, 12, 12,252,252,  0,  0,  0,  0, 48, 48, 63, 63,  0,  0,  0	;93 ']'
	.byte 128,224,120, 30,120,224,128,  0,  1,  1,  0,  0,  0,  1,  1,  0	;94 '^'
	.byte   0,  0,  0,  0,  0,  0,  0,  0, 48, 48, 48, 48, 48, 48, 48,  0	;95 '_'
	.byte   0,  7, 14, 28, 56,112,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;96 '`'
	.byte   0,  0, 96, 96, 96,224,192,  0,  0, 30, 63, 51, 51, 63, 63,  0	;97 'a'
	.byte   0,252,252, 96, 96,224,192,  0,  0, 63, 63, 48, 48, 63, 31,  0	;98 'b'
  	.byte   0,192,224, 96, 96, 96,  0,  0,  0, 31, 63, 48, 48, 48, 48,  0	;99 'c'
  	.byte   0,192,224, 96, 96,252,252,  0,  0, 31, 63, 48, 48, 63, 63,  0	;100'd'
  	.byte   0,192,224, 96, 96,224,192,  0,  0, 31, 63, 50, 50, 51, 51,  0	;101'e'
  	.byte   0,192,192,248,252,204,204,  0,  0,  0,  0, 63, 63,  0,  0,  0	;102'f'
  	.byte   0,192,224, 96, 96,224,224,  0,  0,207,223,216,216,255,127,  0	;103'g'
  	.byte   0,252,252, 96, 96,224,192,  0,  0, 63, 63,  0,  0, 63, 63,  0	;104'h'
  	.byte   0,  0, 96,236,236,  0,  0,  0,  0,  0, 48, 63, 63, 48,  0,  0	;105'i'
  	.byte   0,  0,  0,  0,236,236,  0,  0,  0,192,192,192,255,127,  0,  0	;106'j'
  	.byte 252,252,128,192,224, 96,  0,  0, 63, 63,  3,  7, 30, 56, 48,  0	;107'k'
  	.byte   0,  0, 12,252,252,  0,  0,  0,  0,  0, 48, 63, 63, 48,  0,  0	;108'l'
	.byte 192,224,224,192,224,224,192,  0, 63, 63,  0,  7,  0, 63, 63,  0	;109'm'
	.byte   0,192,224, 96, 96,224,192,  0,  0, 63, 63,  0,  0, 63, 63,  0	;110'n'
	.byte   0,192,224, 96, 96,224,192,  0,  0, 31, 63, 48, 48, 63, 31,  0	;111'o'
	.byte   0,224,224, 96, 96,224,192,  0,  0,255,255, 48, 48, 63, 31,  0	;112'p'
	.byte   0,192,224, 96, 96,224,224,  0,  0, 31, 63, 48, 48,255,255,  0	;113'q'
  	.byte   0,224,224, 96, 96,224,192,  0,  0, 63, 63,  0,  0,  0,  0,  0	;114'r'
  	.byte   0,192,224, 96, 96, 96, 96,  0,  0, 49, 51, 51, 54, 62, 28,  0	;115's'
  	.byte   0, 96, 96,248,248, 96, 96,  0,  0,  0,  0, 31, 63, 48, 48,  0	;116't'
  	.byte   0,224,224,  0,  0,224,224,  0,  0, 31, 63, 48, 48, 63, 63,  0	;117'u'
  	.byte   0,224,224,  0,  0,224,224,  0,  0,  3, 15, 60, 60, 15,  3,  0	;118'v'
	.byte 224,224,  0,128,  0,224,224,  0, 63, 31, 14,  7, 14, 31, 63,  0	;119'w'
	.byte   0, 96,224,128,128,224, 96,  0,  0, 48, 61, 15, 15, 61, 48,  0	;120'x'
	.byte   0,224,224,  0,  0,224,224,  0,  0,207,223,216,216,255,127,  0	;121'y'
	.byte   0, 96, 96, 96,224,224, 96,  0,  0, 48, 60, 63, 51, 48, 48,  0	;122'z'
;Batteriesymbol Ascii 123...126	 
	.byte 	0,  0,  0,  0,  0,  0,  0,248,  0,  0,  0,  0,  0,  0,  0, 63	;Bat->123
	.byte 	8, 8, 12, 12, 12, 12, 12, 8, 32, 33, 33, 33, 33, 33, 33, 33	;Bat->124
	.byte 	8,248,  8,  8, 12, 12,204, 12, 32, 63, 32, 33, 33, 33, 39, 33	;Bat->125
	.byte 	12, 8,  8,248,  0,  0,  0,  0, 33, 33, 32, 63,  0,  0,  0,  0	;Bat->126
;	.byte 128,128,192,252,126,  2,  2,  0,  1,  1,  3, 63,126, 64, 64,  0	;123'{'
;	.byte   0,  0,  0,254,254,  0,  0,  0,  0,  0,  0,127,127,  0,  0,  0	;124'|'
;	.byte   2,  2,126,252,192,128,128,  0, 64, 64,126, 63,  3,  1,  1,  0	;125'}'
;	.byte 192, 96,224,192,128,128,224,  0,  1,  0,  0,  1,  1,  1,  0,  0	;126'~'

; 	.byte   0,  0,128,224,224,128,  0,  0, 24, 30, 19, 16, 16, 19, 30, 24	;127
; 	.byte   0,248,252, 12, 12, 60, 56,  0,  0, 31,191,176,240,124, 28,  0	;
;	.byte   0,236,236,  0,  0,236,236,  0,  0, 31, 63, 48, 48, 63, 63,  0	;
;	.byte   0,192,224,104,108,230,194,  0,  0, 31, 63, 50, 50, 51, 51,  0	;130
;	.byte   0,  8,108,102,102,236,200,  0,  0, 30, 63, 51, 51, 63, 63,  0	;
;	.byte   0, 12,108, 96, 96,236,204,  0,  0, 30, 63, 51, 51, 63, 63,  0	;
;	.byte   0,  2,102,108,104,224,192,  0,  0, 30, 63, 51, 51, 63, 63,  0	;
;	.byte   0,  0,100,110,106,238,196,  0,  0, 30, 63, 51, 51, 63, 63,  0	;
;	.byte   0,192,224, 96, 96, 96,  0,  0,  0, 31,191,176,240,112, 48,  0	;135
;	.byte   0,200,236,102,102,236,200,  0,  0, 31, 63, 50, 50, 51, 51,  0	;
;	.byte   0,204,236, 96, 96,236,204,  0,  0, 31, 63, 50, 50, 51, 51,  0	;
;	.byte   0,194,230,108,104,224,192,  0,  0, 31, 63, 50, 50, 51, 51,  0	;
;	.byte   0, 12,108,224,224, 12, 12,  0,  0,  0, 48, 63, 63, 48,  0,  0	;
;	.byte   0,  8,108,230,230, 12,  8,  0,  0,  0, 48, 63, 63, 48,  0,  0	;140
;	.byte   0,  2,102,236,232,  0,  0,  0,  0,  0, 48, 63, 63, 48,  0,  0	;
;	.byte   0,230,246, 56, 56,246,230,  0,  0, 63, 63,  3,  3, 63, 63,  0	;
;	.byte   0,226,247, 61, 61,247,226,  0,  0, 63, 63,  3,  3, 63, 63,  0	;
;	.byte   0,248,248,156,158,155, 25,  0,  0, 63, 63, 49, 49, 49, 48,  0	;
;	.byte   0, 96, 96,224,192, 96,224,192, 30, 63, 51, 63, 63, 50, 51, 51	;145
;	.byte 248,252, 14,254,254,198,198,  0, 63, 63,  3, 63, 63, 48, 48,  0	;
;	.byte   0,200,236,102,102,236,200,  0,  0, 31, 63, 48, 48, 63, 31,  0	;
;	.byte   0,204,236, 96, 96,236,204,  0,  0, 31, 63, 48, 48, 63, 31,  0	;
;	.byte   0,194,230,108,104,224,192,  0,  0, 31, 63, 48, 48, 63, 31,  0	;
;	.byte   0,232,236,  6,  6,236,232,  0,  0, 31, 63, 48, 48, 63, 63,  0	;150
;	.byte   0,226,230, 12,  8,224,224,  0,  0, 31, 63, 48, 48, 63, 63,  0	;
;	.byte   0,236,236,  0,  0,236,236,  0,  0,207,223,216,216,255,127,  0	;
;	.byte   0,230,246, 48, 48,246,230,  0,  0, 31, 63, 48, 48, 63, 31,  0	;
;	.byte   0,246,246,  0,  0,246,246,  0,  0, 31, 63, 48, 48, 63, 31,  0	;
;	.byte   0,224,240, 60, 60,112, 96,  0,  0,  7, 15, 60, 60, 14,  6,  0	;155
;	.byte   0,  0,248,252, 14,  6,  6,  0, 33, 49, 63, 63, 49, 49, 49, 48	;
;	.byte   0,124,252,192,192,252,124,  0,  0,  2,  2, 63, 63,  2,  2,  0	;
;	.byte   0,240,248,140,140,248,112,  0,  0,127, 63,  8, 24, 31,  7,  0	;
;	.byte   0,128,128,248,252,140,140,  0,  0, 96, 96, 63, 31,  0,  0,  0	;
;	.byte   0,  0, 96,104,108,230,194,  0,  0, 30, 63, 51, 51, 63, 63,  0	;160
;	.byte   0,  0, 96,232,236,  6,  2,  0,  0,  0, 48, 63, 63, 48,  0,  0	;
;	.byte   0,192,224,104,108,230,194,  0,  0, 31, 63, 48, 48, 63, 31,  0	;
;	.byte   0,224,224,  8, 12,230,226,  0,  0, 31, 63, 48, 48, 63, 63,  0	;
;  	.byte   0,204,230,102,108,232,198,  0,  0, 63, 63,  0,  0, 63, 63,  0	;
;  	.byte   0,236,230,134, 12,232,230,  0,  0, 63, 63,  3,  7, 63, 63,  0	;165
;  	.byte   0,  0,176,176,176,240,224,  0,  0, 79, 95, 89, 89, 95, 95,  0	;
;  	.byte   0,224,240, 48, 48,240,224,  0,  0, 79, 95, 88, 88, 95, 79,  0	;
;  	.byte   0,  0,  0,236,236,  0,  0,  0,  0, 28, 63, 51, 48, 60, 28,  0	;
;	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0, 62, 62,  6,  6,  6,  6,  0	;
;	.byte   0,  0,  0,  0,	0,  0,  0,  0,  0,  6,  6,  6,  6, 62, 62,  0	;170
;	.byte   0,  2,126,128,192, 96, 48, 16,  4,  6,  3,101,114, 90, 78, 68	;
;	.byte   0,  2,126,128,192, 96, 48, 16,  4,  6, 51, 57, 44, 38,127, 32	;
;	.byte   0,  0,  0,216,216,  0,  0,  0,  0,  0,  0,127,127,  0,  0,  0	;
;	.byte   0,  0,128,192, 64,128,192,  0,  2,  7, 13, 26, 23, 13, 24,  0	;
;	.byte 192,128, 64,192,128,  0,  0,  0, 24, 13, 23, 26, 13,  7,  2,  0	;175
;	.byte  17,  0,  0,  0, 17,  0,  0,  0, 17,  0,  0,  0, 17,  0,  0,  0	;
;	.byte  17,  0, 68,  0, 17,  0, 68,  0, 17,  0, 68,  0, 17,  0, 68,  0	;
;	.byte 170, 85,170, 85,170, 85,170, 85,170, 85,170, 85,170, 85,170, 85	;
;	.byte   0,  0,  0,255,255,  0,  0,  0,  0,  0,  0,255,255,  0,  0,  0	;
;	.byte 128,128,128,255,255,  0,  0,  0,  1,  1,  1,255,255,  0,  0,  0	;180
;	.byte  96, 96, 96,255,255,  0,  0,  0,  3,  3,  3,255,255,  0,  0,  0	;
;	.byte 128,128,255,255,  0,255,255,  0,  1,  1,255,255,  0,255,255,  0	;
;	.byte 128,128,128,128,128,128,128,  0,  1,  1,255,255,  1,255,255,  0	;
;	.byte  96, 96, 96,224,224,  0,  0,  0,  3,  3,  3,255,255,  0,  0,  0	;
;	.byte  96, 96,127,127,  0,255,255,  0,  3,  3,255,255,  0,255,255,  0	;185
;	.byte   0,  0,255,255,  0,255,255,  0,  0,  0,255,255,  0,255,255,  0	;
;	.byte  96, 96, 96, 96, 96,224,224,  0,  3,  3,255,255,  0,255,255,  0	;
;	.byte  96, 96,127,127,  0,255,255,  0,  3,  3,  3,  3,  3,  3,  3,  0	;
;	.byte 128,128,255,255,128,255,255,  0,  1,  1,  1,  1,  1,  1,  1,  0	;
;	.byte  96, 96, 96,255,255,  0,  0,  0,  3,  3,  3,  3,  3,  0,  0,  0	;190
;	.byte 128,128,128,128,128,  0,  0,  0,  1,  1,  1,255,255,  0,  0,  0	;
;	.byte   0,  0,  0,255,255,128,128,128,  0,  0,  0,  1,  1,  1,  1,  1	;
;	.byte 128,128,128,255,255,128,128,128,  1,  1,  1,  1,  1,  1,  1,  1	;
;	.byte 128,128,128,128,128,128,128,128,  1,  1,  1,255,255,  1,  1,  1	;
;	.byte   0,  0,  0,255,255,128,128,128,  0,  0,  0,255,255,  1,  1,  1	;195
;	.byte 128,128,128,128,128,128,128,128,  1,  1,  1,  1,  1,  1,  1,  1	;
;	.byte 128,128,128,255,255,128,128,128,  1,  1,  1,255,255,  1,  1,  1	;
;	.byte   0,  0,  0,255,255, 96, 96, 96,  0,  0,  0,255,255,  3,  3,  3	;
;	.byte   0,  0,255,255,  0,255,255,128,  0,  0,255,255,  0,255,255,  1	;
;	.byte   0,  0,255,255,  0,127,127, 96,  0,  0,  3,  3,  3,  3,  3,  3	;200
;	.byte   0,  0,224,224, 96, 96, 96, 96,  0,  0,255,255,  0,255,255,  3	;
;	.byte  96, 96,127,127,  0,127,127, 96,  3,  3,  3,  3,  3,  3,  3,  3	;
;	.byte  96, 96, 96, 96, 96, 96, 96, 96,  3,  3,255,255,  0,255,255,  3	;
;	.byte   0,  0,255,255,  0,127,127, 96,  0,  0,255,255,  0,255,255,  3	;
;	.byte  96, 96, 96, 96, 96, 96, 96, 96,  3,  3,  3,  3,  3,  3,  3,  3	;205
;	.byte  96, 96,127,127,  0,127,127, 96,  3,  3,255,255,  0,255,255,  3	;
;	.byte  96, 96, 96,127,127, 96, 96, 96,  3,  3,  3,  3,  3,  3,  3,  3	;
;	.byte 128,128,255,255,128,255,255,128,  1,  1,  1,  1,  1,  1,  1,  1	;
;	.byte  96, 96, 96, 96, 96, 96, 96, 96,  3,  3,  3,255,255,  3,  3,  3	;
;	.byte 128,128,128,128,128,128,128,128,  1,  1,255,255,  1,255,255,  1	;210
;	.byte   0,  0,255,255,128,255,255,128,  0,  0,  1,  1,  1,  1,  1,  1	;
;	.byte   0,  0,  0,255,255, 96, 96, 96,  0,  0,  0,  3,  3,  3,  3,  3	;
;	.byte   0,  0,  0,224,224, 96, 96, 96,  0,  0,  0,255,255,  3,  3,  3	;
;	.byte   0,  0,128,128,128,128,128,128,  0,  0,255,255,  1,255,255,  1	;
;	.byte 128,128,255,255,128,255,255,128,  1,  1,255,255,  1,255,255,  1	;215
;	.byte  96, 96, 96,255,255, 96, 96, 96,  3,  3,  3,255,255,  3,  3,  3	;
;	.byte 128,128,128,255,255,  0,  0,  0,  1,  1,  1,  1,  1,  0,  0,  0	;
;	.byte   0,  0,  0,128,128,128,128,128,  0,  0,  0,255,255,  1,  1,  1	;
;	.byte 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255	;
;	.byte   0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,255,255,255,255	;220
;	.byte 255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0	;
;	.byte   0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,255,255	;
;	.byte 255,255,255,255,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0	;
;	.byte 224,112, 48, 96,192, 96, 48,  0,  7, 14, 12,  6,  3,  6, 12,  0	;
;	.byte   0,240,248,140,140,248,112,  0,  0,127, 63,  8, 24, 31,  7,  0	;225
;	.byte   4,252,252, 12, 12, 12, 28,  0, 32, 63, 63, 32,  0,  0,  0,  0	;
;	.byte 192, 96,224, 96,224, 96, 48,  0,  0,  0, 15,  0, 15,  0,  0,  0	;
;	.byte  12, 28, 60,108,204,140, 12,  0, 48, 56, 60, 54, 51, 49, 48,  0	;
;	.byte 128,192, 96, 48,112,208,144,  0,  7, 15, 24, 16, 24, 15,  7,  0	;
;	.byte   0,224,224,  0,  0,224,224,  0, 96, 63, 15, 24, 24, 31, 15, 24	;230
;	.byte  96, 48,176,240, 48, 48, 24,  0,  0,  0, 15,  7,  0,  0,  0,  0	;
;	.byte 224, 50, 18, 30, 18, 50,224,  0,  7, 76, 72,120, 72, 76,  7,  0	;
;	.byte   0,248,156,140,140,156,248,  0,  0, 15, 28, 24, 24, 28, 15,  0	;
;	.byte 240,248, 12,  4, 12,248,240,  0, 12, 25, 31,  0, 31, 25, 12,  0	;
;	.byte   0,128,220,118,102,198,134,  0,  0, 15, 28, 24, 24, 28, 15,  0	;235
;	.byte 224, 48, 16,252, 22, 54,230,  0,103,108,104, 63,  8, 12,  7,  0	;
;	.byte 224, 48, 16,252, 16, 48,224,  0,  7, 12,  8, 63,  8, 12,  7,  0	;
;	.byte 240,248,156,140,140,140, 12,  0, 15, 31, 57, 49, 49, 49, 48,  0	;
;	.byte 248,252, 12, 12, 12,252,248,  0, 63, 63,  0,  0,  0, 63, 63,  0	;
;	.byte   0,176,176,176,176,176,176,  0,  0, 13, 13, 13, 13, 13, 13,  0	;240
;	.byte   0,192,192,248,248,192,192,  0,  0, 48, 48, 55, 55, 48, 48,  0	;
;	.byte   0, 12, 28,184,240,224, 64,  0,  0, 54, 55, 51, 49, 48, 48,  0	;
;	.byte   0, 64,224,240,184, 28, 12,  0,  0, 48, 48, 49, 51, 55, 54,  0	;
;	.byte   0,  0,  0,248,252,  4, 52, 56,  0,  0,  0,255,255,  0,  0,  0	;
;	.byte   0,  0,  0,255,255,  0,  0,  0, 28, 44, 32, 63, 31,  0,  0,  0	;245
;	.byte   0,192,192,216,216,192,192,  0,  0,  0,  0,  6,  6,  0,  0,  0	;
;	.byte   0, 96, 48, 48, 48, 48,152,  0,  0,  6,  3,  3,  3,  3,  1,  0	;
;	.byte   0, 28, 54, 34, 54, 28,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;
;	.byte   0, 28, 62, 62, 62, 28,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;
;	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 12, 30, 30, 12,  0,  0	;250
;	.byte 128,128,  0,224, 32, 32, 32, 32,  1,  7, 12,  7,  0,  0,  0,  0	;
;	.byte   0,248, 12, 12,248,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;
;	.byte   0,200,228,180,152,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;
;	.byte   0, 68,148,188,108,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;
;	.byte   6,  6,  6,  6,  6,  6,  6,  0,  0,  0,  0,  0,  0,  0,  0,  0	;255
;-------------------------------------------------------------------------
;Zeichensatztabelle 16x32 Bit, Ascii 32...57 (bei Bedarf bis 127) 
;Beispiel: Ascii 48 = '0' ist wie folgt codiert:
;
;	49 -------- 33 -------- 17 -------- 01 --------
;	50 -------- 34 -------- 18 -------- 02 --------
; 	51 -------- 35 ---11111 19 11111111 03 1111----
; 	52 -------- 36 -1111111 20 11111111 04 11111---
; 	53 -------- 37 11111111 21 11111111 05 111111--
; 	54 -------- 38 11111111 22 11111111 06 111111--
;	55 -------- 39 111----1 23 1------- 07 ----11--
;	56 -------- 40 111----- 24 -11----- 08 ----11--
;	57 -------- 41 111----- 25 ---11--- 09 ----11--
;	58 -------- 42 111----- 26 -----11- 10 ----11--
;	59 -------- 43 11111111 27 11111111 11 111111--
;	60 -------- 44 11111111 28 11111111 12 111111--
;	61 -------- 45 -1111111 29 11111111 13 11111---
;	62 -------- 46 ---11111 30 11111111 14 1111----
;	63 -------- 47 -------- 31 -------- 15 --------
;	64 -------- 48 -------- 32 -------- 16 -------- 
;
;Beispiel: Ascii 53 = '5' ist wie folgt codiert:
;
;	49 -------- 33 -------- 17 -------- 01 --------
;	50 -------- 34 -------- 18 -------- 02 --------
; 	51 -------- 35 ---11--- 19 ----1111 03 111111--
; 	52 -------- 36 -1111--- 20 ----1111 04 111111--
; 	53 -------- 37 1111---- 21 ----1111 05 111111--
; 	54 -------- 38 111----- 22 ----1111 06 111111--
;	55 -------- 39 111----- 23 ----111- 07 ----11--
;	56 -------- 40 111----- 24 ----111- 08 ----11--
;	57 -------- 41 111----- 25 ----111- 09 ----11--
;	58 -------- 42 1111---- 26 ---1111- 10 ----11--
;	59 -------- 43 11111111 27 1111111- 11 ----11--
;	60 -------- 44 11111111 28 111111-- 12 ----11--
;	61 -------- 45 -1111111 29 11111--- 13 ----11--
;	62 -------- 46 ---11111 30 1111---- 14 ----11--
;	63 -------- 47 -------- 31 -------- 15 --------
;	64 -------- 48 -------- 32 -------- 16 -------- 
;-------------------------------------------------------------------------
Fnt16x32:
 	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;32 ' '
 	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

	.byte   0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,  0,  0	;33 '!'
	.byte   0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,248,248,248,248,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

	.byte   0,  0,252,252,252,252,  0,  0,252,252,252,252,  0,  0,  0,  0	;34 '"'
	.byte   0,  0, 63, 63, 31, 15,  0,  0, 63, 63, 31, 15,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0


;# = �C
;	.byte 128,128,128,128,252,252,128,128,128,128,252,252,128,128,128,  0	;35 '#'
;	.byte 193,193,193,193,255,255,193,193,193,193,255,255,193,193,193,  0
;	.byte   0,  0,  0,  0, 31, 31,  0,  0,  0,  0, 31, 31,  0,  0,  0,  0
;	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
	.byte  	0,  0,  0,  12, 18,  18,  12,  0	;'�'
	.byte   0,248,252, 12, 12, 60, 56,  0
	.byte	0,0,0,0,0,0,0,0
	.byte  	0, 31, 63, 48, 48, 60, 28,  0
	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

;$ = �F
;	.byte   0,240,248, 28, 12,255,255, 12, 12,255,255, 12, 24,112,  0,  0	;36 '$'
;  	.byte   0,  3, 15, 28, 56,255,255,112,224,255,255,192,128,  0,  0,  0
;  	.byte   0, 24,112,224,192,255,255,192,192,255,255,225,127, 31,  0,  0
;  	.byte   0,  0,  0,  0,  0, 31, 31,  0,  0, 31, 31,  0,  0,  0,  0,  0
	.byte  	0,  0,  0,  12, 18,  18,  12,  0	;'�'
	.byte   0,252,252,204,204,204, 12,  0
	.byte	0,0,0,0,0,0,0,0
	.byte  	0, 63, 63,  0,  0,  0,  0,  0
	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0


  	.byte   0,192, 96, 32, 32, 96,192,  0,  0,  0,  0,128,224,240,  0,  0	;37 '%'
  	.byte   0,  3,  6,  4,132,198,227,112, 60, 30, 15,  3,  1,  0,  0,  0
	.byte 240,120, 28, 15,  7,  3,  0,  0, 30, 51, 33, 33, 51, 30,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

	.byte  40,248,252, 28, 12, 12, 12, 12, 12,252,248,240,  0,  0,  0,  0	;38 '&'
	.byte 225,243,255,255, 14, 12,  0,  0,242,243,243,241, 48, 48, 28,  0
	.byte  31,127,255,255,224,224,224,224,255,255,127, 31,  0,  0,  0,  0
  	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

  	.byte   0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,  0,  0	;39 '''
  	.byte   0,  0,  0,  0,  0,  0, 63, 63, 31, 15,  0,  0,  0,  0,  0,  0
  	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
  	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

	.byte   0,  0,  0,  0,  0,  0,240,248,252,255,  3,  0,  0,  0,  0,  0	;40 '('
	.byte   0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0, 31,127,255,255,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  3,  2,  0,  0,  0,  0

	.byte   0,  0,  0,  0,  0,  3,255,252,248,240,  0,  0,  0,  0,  0,  0	;41 ')'
  	.byte	0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0
  	.byte   0,  0,  0,  0,  0,  0,255,255,127, 31,  0,  0,  0,  0,  0,  0
  	.byte   0,  0,  0,  0,  2,  3,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0

  	.byte   0,  0,128,128,  0,  0,240,240,  0,  0,128,128,  0,  0,  0,  0	;42 '*'
  	.byte  48, 48, 49,179,255,254,255,255,254,255,179, 49, 48, 48,  0,  0
  	.byte   0,  0,  7,  7,  3,  0, 31, 31,  0,  3,  7,  7,  0,  0,  0,  0
  	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

  	.byte   0,  0,  0,  0,  0,  0,240,240,  0,  0,  0,  0,  0,  0,  0,  0	;43 '+'
  	.byte  48, 48, 48, 48, 48, 48,255,255, 48, 48, 48, 48, 48, 48,  0,  0
  	.byte   0,  0,  0,  0,  0,  0, 31, 31,  0,  0,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;44 ','
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,248,248,248,248,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0, 31, 31, 15,  3,  0,  0,  0,  0,  0,  0

	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;45 '-'
	.byte   0,  0, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;46 '.'
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0, 62, 62, 62, 62,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

	.byte   0,  0,  0,  0,  0,  0,  0,  0,128,240,252,252,124, 12,  0,  0	;47 '/'
	.byte   0,  0,  0,  0,  0,192,240,254,255, 63, 15,  1,  0,  0,  0,  0
	.byte   0,  0,224,248,255,255, 31,  7,  0,  0,  0,  0,  0,  0,  0,  0
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;48 '0'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11110000
	.byte	0b11111000
	.byte	0b11111100
	.byte	0b11111100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111000	
	.byte	0b11110000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b11111111	
	.byte	0b11000000	
	.byte	0b00110000	
	.byte	0b00001100	
	.byte	0b00000011	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000111
	.byte	0b00011111
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00011111
	.byte	0b00000111
	.byte	0b00000000
	.byte	0b00000000

	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

;49 '1'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11000000
	.byte	0b11100000
	.byte	0b11110000
	.byte	0b01111000	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000001
	.byte	0b00000001
	.byte	0b00000000
	.byte	0b00000000	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000	
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0


;50 '2'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b01110000
	.byte	0b01111000
	.byte	0b00011100
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00011100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111000	
	.byte	0b11110000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b11000000	
	.byte	0b11100000	
	.byte	0b11110000	
	.byte	0b11111000	
	.byte	0b11111100	
	.byte	0b01111111	
	.byte	0b00111111	
	.byte	0b00011111	
	.byte	0b00001111	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00111110
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00111011	
	.byte	0b00111001	
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;51 '3'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100	
	.byte	0b10001100	
	.byte	0b11001100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b00111100	
	.byte	0b00011100	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00001110
	.byte	0b00001111	
	.byte	0b00001111	
	.byte	0b00001111	
	.byte	0b00001111	
	.byte	0b00011111	
	.byte	0b11111110	
	.byte	0b11111100	
	.byte	0b11111000	
	.byte	0b11110000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000110
	.byte	0b00011110
	.byte	0b00111100
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111100	
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00011111	
	.byte	0b00000111	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;52 '4'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000	
	.byte	0b11000000	
	.byte	0b11100000	
	.byte	0b11110000	
	.byte	0b11111000	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b11110000
	.byte	0b11111100
	.byte	0b11111110
	.byte	0b10011111
	.byte	0b10001111	
	.byte	0b10000111	
	.byte	0b10000011	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b10000000	
	.byte	0b10000000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000011
	.byte	0b00000011
	.byte	0b00000011
	.byte	0b00000011
	.byte	0b00000011
	.byte	0b00000011
	.byte	0b00000011
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00111111
	.byte	0b00000011
	.byte	0b00000011
	.byte	0b00000000	
	.byte	0b00000000	
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;53 '5'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11111100
	.byte	0b11111100
	.byte	0b11111100
	.byte	0b11111100
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000111
	.byte	0b00000111
	.byte	0b00000111
	.byte	0b00000111
	.byte	0b00000111
	.byte	0b00000111
	.byte	0b00000111
	.byte	0b00001111	
	.byte	0b11111111	
	.byte	0b11111110	
	.byte	0b11111100	
	.byte	0b11111000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000110
	.byte	0b00011110
	.byte	0b00111100
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111100
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00011111
	.byte	0b00000111
	.byte	0b00000000	
	.byte	0b00000000	
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;54 '6'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b11100000
	.byte	0b11110000
	.byte	0b11111000
	.byte	0b00011100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00001100	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b00001110	
	.byte	0b00001110	
	.byte	0b00001110	
	.byte	0b00001110	
	.byte	0b11111110	
	.byte	0b11111110	
	.byte	0b11111100	
	.byte	0b11110000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000111
	.byte	0b00011111
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111000	
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00011111	
	.byte	0b00000111	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;55 '7'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11000000
	.byte	0b11100000
	.byte	0b11110000
	.byte	0b11111000	
	.byte	0b01111111	
	.byte	0b00111111	
	.byte	0b00011111	
	.byte	0b00001111	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;56 '8'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11110000
	.byte	0b11111000
	.byte	0b11111100
	.byte	0b11111100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111000	
	.byte	0b11110000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11100111
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b00011000
	.byte	0b00011000
	.byte	0b00011000
	.byte	0b00011000
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b11111111	
	.byte	0b11100111	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000111
	.byte	0b00011111
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111111	
	.byte	0b00111111	
	.byte	0b00011111	
	.byte	0b00000111	
	.byte	0b00000000	
	.byte	0b00000000	
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;57 '9'
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b11110000
	.byte	0b11111000
	.byte	0b11111100
	.byte	0b11111100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100
	.byte	0b00001100	
	.byte	0b11111100	
	.byte	0b11111100	
	.byte	0b11111000	
	.byte	0b11110000	
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00001111
	.byte	0b00011111
	.byte	0b00111111
	.byte	0b00111111
	.byte	0b00110000
	.byte	0b00110000
	.byte	0b00110000
	.byte	0b00110000
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b11111111
	.byte	0b00000000	
	.byte	0b00000000	

	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111000
	.byte	0b00111100
	.byte	0b00011110
	.byte	0b00001111
	.byte	0b00000111
	.byte	0b00000011
	.byte	0b00000001
	.byte	0b00000000	
	.byte	0b00000000	
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0

;Rest kann bei Bedarf eingebaut werden
;	.byte   0,  0,  0,  0,  0,  0,128,128,128,128,  0,  0,  0,  0,  0,  0	;58 ':'
;	.byte   0,  0,  0,  0,  0,  0, 15, 15, 15, 15,  0,  0,  0,  0,  0,  0
;	.byte   0,  0,  0,  0,  0,  0,248,248,248,248,  0,  0,  0,  0,  0,  0
;	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
;
;	.byte   0,  0,  0,  0,  0,  0,128,128,128,128,  0,  0,  0,  0,  0,  0	;59 '<'
;	.byte   0,  0,  0,  0,  0,  0, 15, 15, 15, 15,  0,  0,  0,  0,  0,  0
;	.byte   0,  0,  0,  0,  0,  0,248,248,248,248,  0,  0,  0,  0,  0,  0
;	.byte   0,  0,  0,  0,  0,  0, 31, 31, 15,  3,  0,  0,  0,  0,  0,  0
;
;	.byte   0,  0,  0,  0,  0,  0,128,192,224,240,112, 48,  0,  0,  0,  0	;60 '='
;	.byte   0,  0, 56,124,254,255,199,131,  1,  0,  0,  0,  0,  0,  0,  0
;	.byte   0,  0,  0,  0,  0,  1,  3,  7, 15, 30, 28, 24,  0,  0,  0,  0
;	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
;
;	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;61 '>'
;	.byte   0,  0,206,206,206,206,206,206,206,206,206,206,206,206,  0,  0
;	.byte   0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  0
;	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
;
;  0,  0,  0,  0, 48,112,240,224,192,128,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,131,199,255,254,124, 56,  0,  0,
;  0,  0,  0,  0, 24, 28, 30, 15,  7,  3,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,112,120, 28, 12, 12, 12,
; 12, 28,252,252,248,240,  0,  0,  0,  0,  0,  0,  0,  0,224,240,248,252,127, 63,
; 31, 15,  0,  0,  0,  0,  0,  0,  0,  0,248,248,248,248,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,240,248,252,252,
; 12, 12, 12, 12, 12, 12,252,252,248,240,  0,  0,255,255,255,255,  0,  0,124,254,
;206,206,255,255,127, 63,  0,  0, 31,127,255,255,224,224,224,224,224,224,224,224,
;112, 24,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,240,248,252,252, 12, 12, 12, 12,252,252,248,240,  0,  0,  0,  0,255,255,
;255,255, 48, 48, 48, 48,255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,252,252,252,252, 12, 12, 12, 12,252,252,248,240,  0,  0,
;  0,  0,255,255,255,255, 28, 28, 28, 28,255,255,239,195,  0,  0,  0,  0,255,255,
;255,255,224,224,224,224,255,255,127, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,240,248,252,252, 28, 12, 12, 12, 12, 28,
;120,112,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0, 31,127,255,255,240,224,224,224,224,240,120, 24,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,252,252, 12, 12,
; 12, 28,252,252,248,240,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,
;255,255,  0,  0,  0,  0,255,255,255,255,224,224,224,240,255,255,127, 31,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,
;252,252, 12, 12, 12, 12, 12, 12, 12, 12,  0,  0,  0,  0,255,255,255,255, 48, 48,
; 48, 48, 48, 48,  0,  0,  0,  0,  0,  0,255,255,255,255,224,224,224,224,224,224,
;224,224,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,252,252,252,252, 12, 12, 12, 12, 12, 12, 12, 12,  0,  0,  0,  0,255,255,
;255,255, 48, 48, 48, 48, 48, 48,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,240,248,252,252, 28, 12, 12, 12, 12, 28,120,112,  0,  0,
;  0,  0,255,255,255,255,  0,  0, 16, 16,240,240,240,240,  0,  0,  0,  0, 31,127,
;255,255,224,224,224,224,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,252,252,
;252,252,  0,  0,  0,  0,255,255,255,255, 48, 48, 48, 48,255,255,255,255,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,
;252,252,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0, 24,120,240,224,224,224,224,240,255,255,
;127, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;252,252,252,252,  0,  0,128,192,224,240,120, 60, 28, 12,  0,  0,255,255,255,255,
;254,255,199,131,  1,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  1,  3,  7,
; 15, 30, 60,120,240,224,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,
;255,255,224,224,224,224,224,224,224,224,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,252,252,252,252,240,192,128,128,192,240,252,252,
;252,252,  0,  0,255,255,255,255,  1,  3, 15, 15,  3,  1,255,255,255,255,  0,  0,
;255,255,255,255,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,252,252,240,192,  0,  0,
;  0,  0,252,252,252,252,  0,  0,255,255,255,255, 15, 63,255,252,240,192,255,255,
;255,255,  0,  0,255,255,255,255,  0,  0,  0,  3, 15, 63,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,240,248,
;252,252, 28, 12, 12, 28,252,252,248,240,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0, 31,127,255,255,240,224,224,240,255,255,
;127, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,252,252,252,252, 28, 12, 12, 28,252,252,248,240,  0,  0,  0,  0,255,255,
;255,255,224,192,192,224,255,255,127, 63,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,240,248,252,252, 28, 12, 12, 28,252,252,248,240,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0, 31,127,
;255,255,240,224,224,240,255,255,127, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  1,  3,  3,  3,  0,  0,  0,  0,252,252,252,252, 12, 12, 12, 28,252,252,
;248,240,  0,  0,  0,  0,255,255,255,255, 48, 48, 48,120,255,255,223,143,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,240,248,252,252, 28, 12,
; 12, 12, 12, 28,120,112,  0,  0,  0,  0,  3,  7, 15, 31, 63,126,252,248,240,224,
;192,128,  0,  0,  0,  0, 24,120,240,224,224,224,224,241,255,255,127, 63,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 12, 12,
; 12, 12,252,252,252,252, 12, 12, 12, 12,  0,  0,  0,  0,  0,  0,  0,  0,255,255,
;255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,252,252,252,252,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,255,255,
;255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0, 31,127,255,255,240,224,
;224,240,255,255,127, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,252,252,252,252,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  7, 15,
; 31, 63,120,240,240,120, 63, 31, 15,  7,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,  0,  0,252,252,
;252,252,  0,  0,255,255,255,255,  0,  0,240,240,  0,  0,255,255,255,255,  0,  0,
; 31,127,255,255,224,224,255,255,224,224,255,255,127, 31,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,
;  0,  0,252,252,252,252,  0,  0,  0,  0,131,207,255,255,120, 48, 48,120,255,255,
;207,131,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,
;252,252,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0, 15, 31, 63,127,240,240,
;240,240,127, 63, 31, 15,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0, 12, 12, 12, 12, 12, 12, 12, 12,252,252,252,252,  0,  0,  0,  0,  0,128,
;192,224,240,248,124, 62, 31, 15,  7,  3,  0,  0,  0,  0,255,255,255,255,225,224,
;224,224,224,224,224,224,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  3,  3,  3,  3,  0,  0,
;  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 31, 31,
; 31, 31, 28, 28, 28, 28,  0,  0,  0,  0, 12,124,252,252,240,128,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  1, 15, 63,255,254,240,192,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  7, 31,255,255,248,224,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  3,  3,  3,  3,255,255,
;255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,
;  0,  0, 28, 28, 28, 28, 31, 31, 31, 31,  0,  0,  0,  0,  0,  0,  0,  0,240,120,
; 60, 30, 15, 15, 30, 60,120,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,224,224,224,224,224,224,224,224,
;224,224,224,224,224,224,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,124,252,252,252,128,128,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,128,128,128,128,
;  0,  0,  0,  0,  0,  0,134,199,227,225, 97, 97, 97, 99,255,255,255,254,  0,  0,
;  0,  0, 63,127,255,255,224,224,224,224,255,255,255,255,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,252,252,128,128,
;128,128,128,128,  0,  0,  0,  0,  0,  0,255,255,255,255,  1,  1,  1,  1,255,255,
;255,254,  0,  0,  0,  0,255,255,255,255,224,224,224,224,255,255,127, 31,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;128,128,128,128,128,128,128,128,  0,  0,  0,  0,  0,  0,254,255,255,255,  3,  1,
;  1,  1,  1,  3, 15, 14,  0,  0,  0,  0, 63,127,255,255,240,224,224,224,224,240,
;120, 24,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,128,128,128,128,128,128,252,252,252,252,  0,  0,  0,  0,254,255,
;255,255,  1,  1,  1,  1,255,255,255,255,  0,  0,  0,  0, 31,127,255,255,224,224,
;224,224,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,128,128,128,128,  0,  0,  0,  0,
;  0,  0,254,255,255,255,193,193,193,193,255,255,255,126,  0,  0,  0,  0, 31,127,
;255,255,240,224,224,224,224,240,120, 24,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,240,248,252,252,140,140,140,
; 12,  4,  0,  0,  0,  0,  0,  1,  1,255,255,255,255,  1,  1,  1,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,
;128,128,128,128,128,128,  0,  0,  0,  0,254,255,255,255,  1,  1,  1,  1,255,255,
;255,255,  0,  0,  0,  0, 31,127,255,255,224,224,224,224,255,255,255,255,  0,  0,
;  0,  0, 24, 56,112, 96, 96, 96, 96,112,127,127, 63, 31,  0,  0,  0,  0,252,252,
;252,252,128,128,128,128,128,128,  0,  0,  0,  0,  0,  0,255,255,255,255,  1,  1,
;  1,  1,255,255,255,254,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,
;255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,140,140,140,140,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,
;255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,140,140,140,140,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0, 24, 56,112, 96, 96,112,
;127,127, 63, 31,  0,  0,  0,  0,  0,  0,252,252,252,252,  0,  0,  0,  0,128,128,
;128,128,  0,  0,  0,  0,255,255,255,255,248,188, 30, 15,  7,  3,  1,  0,  0,  0,
;  0,  0,255,255,255,255,  3,  7, 15, 30, 60,120,240,224,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,252,
;252,252,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 31,127,255,255,224,224,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,
;128,128,128,128,128,128,128,128,  0,  0,  0,  0,255,255,255,255,  1,  1,255,255,
;  1,  1,255,255,255,254,  0,  0,255,255,255,255,  0,  0,  7,  7,  0,  0,255,255,
;255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,128,128,128,128,128,128,128,128,128,128,  0,  0,  0,  0,  0,  0,255,255,
;255,255,  1,  1,  1,  1,255,255,255,254,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,128,128,128,128,  0,  0,  0,  0,
;  0,  0,254,255,255,255,  1,  1,  1,  1,255,255,255,254,  0,  0,  0,  0, 31,127,
;255,255,224,224,224,224,255,255,127, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,128,128,128,128,128,128,
;  0,  0,  0,  0,  0,  0,255,255,255,255,  1,  1,  1,  1,255,255,255,254,  0,  0,
;  0,  0,255,255,255,255,224,224,224,224,255,255,127, 31,  0,  0,  0,  0,127,127,
;127,127,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,
;128,128,128,128,128,128,  0,  0,  0,  0,254,255,255,255,  1,  1,  1,  1,255,255,
;255,255,  0,  0,  0,  0, 31,127,255,255,224,224,224,224,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,127,127,127,127,  0,  0,  0,  0,128,128,
;128,128,  0,  0,  0,128,128,128,128,  0,  0,  0,  0,  0,255,255,255,255, 60, 14,
;  7,  3,  1,  1,  1,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,128,128,128,128,128,128,  0,  0,  0,  0,  0,  0,  0,  0, 30,
; 63,127,255,241,225,193,131,  7,  6,  0,  0,  0,  0,  0,  0, 48,112,224,224,225,
;243,255,255,127, 62,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,128,128,252,252,252,252,128,128,  0,  0,  0,  0,
;  0,  0,  0,  0,  1,  1,255,255,255,255,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0, 31,127,255,255,224,224,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,  0,  0,  0,  0,128,128,
;128,128,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0, 31,127,255,255,240,224,224,240,255,255,127, 31,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,  0,  0,
;  0,  0,128,128,128,128,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,
;255,255,  0,  0,  0,  0,  7, 15, 31, 63,120,240,240,120, 63, 31, 15,  7,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,128,128,
;  0,  0,  0,  0,  0,  0,128,128,128,128,  0,  0,255,255,255,255,  0,  0,240,240,
;  0,  0,255,255,255,255,  0,  0, 31,127,255,255,224,224,255,255,224,224,255,255,
;127, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,128,128,128,128,  0,  0,  0,  0,128,128,128,128,  0,  0,  0,  0, 31, 63,
;255,255,224,192,192,224,255,255, 63, 31,  0,  0,  0,  0,252,255,255,255,  1,  0,
;  0,  1,255,255,255,252,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,128,128,128,128,  0,  0,  0,  0,128,128,128,128,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,255,255,255,255,  0,  0,  0,  0, 31,127,
;255,255,224,224,224,224,255,255,255,255,  0,  0,  0,  0, 24, 56,112, 96, 96, 96,
; 96,112,127,127, 63, 31,  0,  0,  0,  0,128,128,128,128,128,128,128,128,128,128,
;128,128,  0,  0,  0,  0,  1,  1,  1,  1,129,193,225,241,127, 63, 31, 15,  0,  0,
;  0,  0,248,252,254,255,231,227,225,224,224,224,224,224,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,252,254,
;255,255,  3,  1,  0,  0,  0,  0,  0,  0, 96, 96, 96, 96,159,159, 15,  7,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,254,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0, 15, 31, 63, 63, 48, 32,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,255,255,255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,
;255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,255,255,255,255,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,127,127,127,127,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  1,  3,255,255,254,252,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  7, 15,159,159, 96, 96, 96, 96,  0,  0,  0,  0,  0,  0,  0,  0,254,255,
;255,255,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 32, 48, 63, 63, 31, 15,  0,  0,
;  0,  0,  0,  0,  0,  0,128,128,128,128,  0,  0,  0,  0,  0,  0,128,128,  0,  0,
;254,255, 31, 15, 15, 31, 63,126,252,240,240,252,127, 63,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,  0,  0,  0,  0,
;  0,  0,  0,  0,  0,  0,  0,192,240, 62, 15,  3, 15, 62,240,192,  0,  0,  0,  0,
;224,248, 31,  7,  0,  0,  0,  0,  0,  0,  0,  7, 31,248,224,  0,  3,  3,  3,  3,
;  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  0
;-------------------------------------------------------------------------
;Zeichensatztabelle 8x8 Bit, Ascii 32...126 
;Beispiel: Ascii 53 = '5'	Ascii 122 = 'z'		ist wie folgt codiert:
;  	01 --------		01 --------
;	02 --1--111		02 -1---1--
;	03 -11--111		03 -11--1--
; 	04 -1---1-1		04 -111-1--
; 	05 -1---1-1		05 -1-111--
; 	06 -11111-1		06 -1--11--
;	07 --111--1		07 -1---1--
;	08 --------		08 --------
;
;Batteriesymbol auf Ascii 33'!', Asc 34'"',Asc 35'#'
;  	01 --------
;	02 -1111111
;	03 -1-111-1
; 	04 -1-111-1
; 	05 -1-111-1
; 	06 -1-111-1
;	07 -1-111-1
;	08 -1-111-1

;  	01 -1-111-1
;	02 -1-111-1
;	03 -1-111-1
; 	04 -1-111-1
; 	05 -1-----1
; 	06 -1-----1
;	07 -1-----1
;	08 -1-----1

;  	01 -1-----1
;	02 -1-----1
;	03 -1-----1
; 	04 -1-----1
; 	05 -1-----1
; 	06 -111-111
;	07 ---111--
;	08 --------



;-------------------------------------------------------------------------
Fnt8x8:
 	.byte	0,  0,  0,  0,  0,  0,  0,  0	;32 ' '
 	.byte   0,  0,  0, 95, 95,  0,  0,  0	;33 '!'
 	.byte   0,  7,  7,  0,  0,  7,  7,  0	;34 '"'
 	.byte  36,126,126, 36,126,126, 36,  0	;35 '#'
	.byte   0, 36, 46,107,107, 58, 18,  0	;36 '$'
	.byte   0,102, 54, 24, 12,102, 98,  0	;37 '%'

;Batteriesymbol
;	.byte  48,122, 79, 93, 55,114, 80,  0	;38 '&'
;	.byte   0,  0,  0,  7,  7,  0,  0,  0	;39 '''
;	.byte   0,  0,  0, 62,127, 99, 65,  0	;40 '('
 	.byte   0,127, 93, 93, 93, 93, 93, 65	;38 Bat
 	.byte  65, 65, 65, 65, 65, 65, 65, 65	;39 Bat
 	.byte  65, 65, 65, 65, 65,119, 28, 0	;40 Bat


	.byte   0, 65, 99,127, 62,  0,  0,  0	;41 ')'
	.byte   8, 42, 62, 28, 28, 62, 42,  8	;42 '*'
	.byte   0,  8,  8, 62, 62,  8,  8,  0	;43 '+'
	.byte   0,128,224, 96,  0,  0,  0,  0	;44 ','
	.byte   0,  8,  8,  8,  8,  8,  8,  0	;45 '-'
	.byte   0,  0,  0, 96, 96,  0,  0,  0	;46 '.'
	.byte   0, 96, 48, 24, 12,  6,  3,  0	;47 '/'
	.byte   0, 62,127, 73, 69,127, 62,  0	;48 '0'
	.byte   0, 64, 66,127,127, 64, 64,  0	;49 '1'
	.byte   0, 66, 99,113, 89, 79, 70,  0	;50 '2'
	.byte   0, 33, 97, 69, 79,123, 49,  0	;51 '3'
	.byte   0, 24, 28, 22,127,127, 16,  0	;52 '4'
	.byte   0, 39,103, 69, 69,125, 57,  0	;53 '5'
	.byte   0, 62,127, 73, 73,121, 48,  0	;54 '6'
	.byte   0,  1,113,121, 13,  7,  3,  0	;55 '7'
	.byte   0, 54,127, 73, 73,127, 54,  0	;56 '8'
	.byte   0,  6, 79, 73,105, 63, 30,  0	;57 '9'
	.byte   0,  0,  0, 54, 54,  0,  0,  0	;58 ':'
	.byte   0,  0, 64,118, 54,  0,  0,  0	;59 '<'
	.byte   0,  0,  8, 28, 54, 99, 65,  0	;60 '='
	.byte   0, 36, 36, 36, 36, 36, 36,  0	;61 '='
	.byte   0, 65, 99, 54, 28,  8,  0,  0	;62 '>'
	.byte   0,  2,  3, 81, 89, 15,  6,  0	;63 '?'
	.byte   0, 62,127, 65, 93, 87, 94,  0	;64 '@'
	.byte   0,124,126, 19, 19,126,124,  0	;65 'A'
	.byte   0,127,127, 73, 73,127, 54,  0	;66 'B'
	.byte   0, 62,127, 65, 65, 99, 34,  0	;67 'C'
	.byte   0,127,127, 65, 99, 62, 28,  0	;68 'D'
	.byte   0,127,127, 73, 73, 73, 65,  0	;69 'E'
	.byte   0,127,127,  9,  9,  9,  1,  0	;70 'F'
	.byte   0, 62,127, 65, 73,121,121,  0	;71 'G'
	.byte   0,127,127,  8,  8,127,127,  0	;72 'H'
	.byte   0,  0, 65,127,127, 65,  0,  0	;73 'I'
	.byte   0, 32, 96, 64, 64,127, 63,  0	;74 'J'
	.byte   0,127,127, 28, 54, 99, 65,  0	;75 'K'
	.byte   0,127,127, 64, 64, 64, 64,  0	;76 'L'
	.byte 127,127,  6, 12,  6,127,127,  0	;77 'M'
	.byte   0,127,127, 14, 28,127,127,  0	;78 'N'
	.byte   0, 62,127, 65, 65,127, 62,  0	;79 'O'
	.byte   0,127,127,  9,  9, 15,  6,  0	;80 'P'
	.byte   0, 62,127, 81, 33,127, 94,  0	;81 'Q'
	.byte   0,127,127,  9, 25,127,102,  0	;82 'R'
	.byte   0, 38,111, 73, 73,123, 50,  0	;83 'S'
	.byte   0,  1,  1,127,127,  1,  1,  0	;84 'T'
	.byte   0, 63,127, 64, 64,127,127,  0	;85 'U'
	.byte  	0, 31, 63, 96, 96, 63, 31,  0	;86 'V'
	.byte 127,127, 48, 24, 48,127,127,  0	;87 'W'
	.byte   0, 99,119, 28, 28,119, 99,  0	;88 'X'
	.byte   0,  7, 15,120,120, 15,  7,  0	;89 'Y'
	.byte   0, 97,113, 89, 77, 71, 67,  0	;90 'Z'
	.byte  	0,  0,  0,127,127, 65, 65,  0	;91 '['
	.byte   0,  3,  6, 12, 24, 48, 96,  0	;92 '\'
	.byte   0, 65, 65,127,127,  0,  0,  0	;93 ']'
	.byte   8, 12,  6,  3,  6, 12,  8,  0	;94 '^'
	.byte  64, 64, 64, 64, 64, 64, 64,  0	;95 '_'
	.byte   2,  6, 12,  8,  0,  0,  0,  0	;96 '`'
	.byte   0, 32,116, 84, 84,124,120,  0	;97 'a'
	.byte   0,127,127, 68, 68,124, 56,  0	;98 'b'
	.byte   0, 56,124, 68, 68, 68,  0,  0	;99 'c'
	.byte   0, 56,124, 68, 68,127,127,  0	;100'd'
	.byte   0, 56,124, 84, 84, 92, 24,  0	;101'e'
	.byte   0,  4,126,127,  5,  5,  0,  0	;102'f
	.byte   0,152,188,164,164,252,124,  0	;103'g'
	.byte   0,127,127,  4,  4,124,120,  0	;104'h'
	.byte   0,  0, 68,125,125, 64,  0,  0	;105'i'
	.byte   0,128,128,253,125,  0,  0,  0	;106'j'
	.byte   0,127,127, 16, 56,108, 68,  0	;107'k'
	.byte   0,  0, 65,127,127, 64,  0,  0	;108'l'
	.byte 124,124, 12, 24, 12,124,120,  0	;109'm'
	.byte   0,124,124,  4,  4,124,120,  0	;110'n'
	.byte   0, 56,124, 68, 68,124, 56,  0	;111'o'
	.byte   0,252,252, 68, 68,124, 56,  0	;112'p'
	.byte   0, 56,124, 68, 68,252,252,  0	;113'q'
	.byte   0,124,124,  4,  4, 12,  8,  0	;114'r'
	.byte   0, 72, 92, 84, 84,116, 36,  0	;115's'
	.byte   0,  4,  4, 62,126, 68, 68,  0	;116't'
	.byte   0, 60,124, 64, 64,124,124,  0	;117'u'
	.byte   0, 28, 60, 96, 96, 60, 28,  0	;118'v'
	.byte  28,124, 96, 48, 96,124, 28,  0	;119'w'
	.byte   0, 68,108, 56, 56,108, 68,  0	;120'x'
	.byte   0,156,188,160,160,252,124,  0	;121'y'
	.byte   0, 68,100,116, 92, 76, 68,  0	;122'z'
	.byte   0,  0,  8, 62,119, 65, 65,  0	;123'{'
	.byte   0,  0,  0,255,255,  0,  0,  0	;124'|'
	.byte   0, 65, 65,119, 62,  8,  0,  0	;125'}'
	.byte  12,  6,  6, 12, 24, 24, 12,  0	;126'~'
;-------------------------------------------------------------------------
;Zeichensatztabelle 4x6 Bit 
;127 Zeichen a 4 Byte = 508 Byte = 254 Word
;Flashbereich
;Beispiel: 	Ascii 53 = '5':			Ascii 76 = 'L'
;									
;		01 ---1-111			01 ---11111						
;		02 ---1-1-1			02 ---1----
;		03 ---111-1			03 ---1----
;		04 --------			04 --------
;
;	Batteriesymbol auf Ascii 40,41,42
;		01 ---11111	'('	
;		02 ---1-1-1
;		03 ---1-1-1
;		04 ---1---1
;		01 ---1---1	')'
;		02 ---1---1
;		03 ---1---1
;		04 ---1---1
;		01 ---1---1	'*'
;		02 ---1---1
;		03 ---11-11	
;		04 ----111-	
;
;-------------------------------------------------------------------------
Fnt4x6:
;das �Gradzeichen ist auf Ascii Nr. 33 (!) gelegt.
	.byte   0,  0,  0,  0,  0,  3, 3,  0,  0,  0,  0,  3, 10, 31, 31, 10 	;32 ' ',33 '�',34 ' ',35 '#'
;	.byte   0,  0,  0,  0,  0,  0, 23,  0,  0,  3,  0,  3, 10, 31, 31, 10 	;32 ' ',33 '!',34 '"',35 '#'

	.byte   0, 23, 53, 29,  9,  4, 18,  0, 10, 21, 26,  0,  0,  3,  0,  0	;36 '$',37 '%',38 '&',39 '''

;Batteriesymbol
;  	.byte   0, 14, 17,  0,  0, 17, 14,  0, 21, 14, 21,  0,  4, 14,  4,  0	;40 '(',41 ')',42 '*',43 '+'
  	.byte   31, 21, 21, 17, 17, 17, 17, 17, 17, 17, 27, 14,  4, 14,  4,  0	;40 '(',41 ')',42 '*',43 '+'

;
  	.byte   0, 48,  0,  0,  4,  4,  4,  0,  0, 16,  0,  0, 24,  6,  1,  0	;44 ',',45 '-',46 '.',47 '/'
;  	.byte   0, 48,  0,  0,  4,  4,  4,  0, 16,  0,  0,  0, 24,  6,  1,  0	;44 ',',45 '-',46 '.',47 '/'

  	.byte  31, 17, 31,  0, 18, 31, 16,  0, 29, 21, 23,  0, 21, 21, 31,  0	;48 '0',49 '1',50 '2',51 '3'
  	.byte   7,  4, 31,  0, 23, 21, 29,  0, 31, 21, 29,  0,  1, 25,  7,  0	;52 '4',53 '5',54 '6',55 '7'
 
 	.byte  31, 21, 31,  0, 23, 21, 31,  0,  0, 10,  0,  0,  0, 52,  0,  0	;56 '8',57 '9',58 ':',59 ';'
 ; 	.byte  31, 21, 31,  0, 23, 21, 31,  0, 10,  0,  0,  0,  0, 52,  0,  0	;56 '8',57 '9',58 ':',59 ';'
 
  	.byte   4, 10, 17,  0, 10, 10, 10,  0, 17, 10,  4,  0,  3, 41,  7,  0	;60 '<',61 '=',62 '>',63 '?'
  	.byte  30, 33, 45, 14, 30,  5, 30,  0, 31, 21, 10,  0, 14, 17, 17,  0	;64 '@',65 'A',66 'B',67 'C'
 	.byte  31, 17, 14,  0, 31, 21, 21,  0, 31,  5,  5,  0, 14, 17, 29,  0	;68 'D',69 'E',70 'F',71 'G'
  	.byte  31,  4, 31,  0, 17, 31, 17,  0, 24, 16, 31,  0, 31,  4, 27,  0	;72 'H',73 'I',74 'J',75 'K'
 	.byte  31, 16, 16,  0, 31,  2, 31,  0, 31,  1, 30,  0, 14, 17, 14,  0	;76 'L',77 'M',78 'N',79 'O'
	.byte  31,  5,  2,  0, 14, 17, 30,  0, 31,  5, 26,  0, 22, 21, 13,  0	;80 'P',81 'Q',82 'R',83 'S'
	.byte   1, 31,  1,  0, 31, 16, 31,  0, 15, 16, 15,  0, 31,  8, 31,  0	;84 'T',85 'U',86 'V',87 'W'
	.byte  27,  4, 27,  0,  3, 28,  3,  0, 25, 21, 19,  0,  0, 31, 17,  0	;88 'X',89 'Y',90 'Z',91 '['
	.byte   1, 14, 16,  0,  0, 17, 31,  0,  2,  1,  2,  0, 16, 16, 16, 16	;92 '\',93 ']',94 '^',95 '_'
	.byte   0,  1,  2,  0, 24, 20, 28,  0, 31, 20, 28,  0, 28, 20, 20,  0	;96 '`',97 'a',98 'b',99 'c'
	.byte  28, 20, 31,  0, 12, 26, 20,  0,  4, 31,  5,  0, 46, 42, 62,  0	;100 'd',101 'e',102 'f',103 'g'
	.byte  31,  4, 28,  0,  0, 29,  0,  0, 32, 29,  0,  0, 31,  8, 20,  0	;104 'h',105 'i',106 'j',107 'k'
	.byte   0, 31, 16,  0, 28,  8, 28,  0, 28,  4, 24,  0, 28, 20, 28,  0	;108 'l',109 'm',110 'n',111 'o'
	.byte  60, 20, 28,  0, 28, 20, 60,  0,  0, 28,  4,  0, 16, 28,  4,  0	;112 'p',113 'q',114 'r',115 's'
	.byte   2, 31, 18,  0, 28, 16, 28,  0, 12, 16, 12,  0, 28, 24, 28,  0	;116 't',117 'u',118 'v',119 'w'
	.byte  20,  8, 20,  0, 44, 40, 60,  0, 20, 28, 20,  0,  4, 14, 17,  0	;120 'x',121 'y',122 'z',123 '{'
	.byte   0, 63,  0,  0, 17, 14,  4,  0,  4,  6,  4,  2, 24, 20, 24,  0	;124 '|',125 '}',126 '~',127 ''

;-------------------------------------------------------------------------
;Zeichensatztabelle 5x6 Bit 
;127 Zeichen a 4 Byte = 508 Byte = 254 Word
;
;Ascii 32 = ' '
;								
;1 -------- 0
;2 -------- 0
;3 -------- 0
;4 -------- 0
;5 -------- 0
;6 -------- 0
;
;Ascii 33 = '�'	Ascii 34 = ' '	Ascii 35 = ' '	Ascii 36 = ' '	Ascii 37 = '%'
;								
;1 ------11 3	--------	--------	--------	--1---11 35 
;2 ------11 3	--------	--------	--------	---1--11 19
;3 -------- 	--------	--------	--------	----1---  8
;4 --------	--------	--------	--------	-----1--  4
;5 --------	--------	--------	--------	--11--1- 50 
;6 --------   	--------   	--------	--------	--11---1 49
;
;Ascii 38 = ' '	Ascii 39 = ' '	Ascii 40 = ' '	Ascii 41 = ' '	Ascii 42 = ' '
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	--------	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 43 = ' '	Ascii 44 = ' '	Ascii 45 = ' '	Ascii 46 = '.'	Ascii 47 = ' '
;								
;1 --------	--------	--------	1------- 128	--------
;2 --------	--------	--------	1------- 128	--------
;3 --------	--------	--------	--1----- 64	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 48 = '0'	Ascii 49 = '1'	Ascii 49 = '2'	Ascii 50 = '3'	Ascii 51 = '4'
;								
;1 ---1111-  30	--------   0	--11--1-  50	---1--1-  18	---11---  24
;2 --1----1  33	--1---1-  34	--1-1--1  41	--1----1  33	---1-1--  20
;3 --1----1  33	--111111  63	--1-1--1  41	--1--1-1  37	---1--1-  18
;4 --1----1  33	--1-----  32	--1-1--1  41	--1--1-1  37	--111111  63
;5 ---1111-  30	--------   0	--1--11-  38	---11-1-  26	---1----  16
;6 --------   0 --------   0  	--------   0	--------   0	--------   0
;
;Ascii 52 = '5'	Ascii 53 = '6'	Ascii 54 = '7'	Ascii 55 = '8'	Ascii 56 = '9'
;								
;1 ---1-111  23	---1111-  30	-------1   1	---11-1-  26	--1--11-  38
;2 --1--1-1  37	--1--1-1  37	-------1   1	--1--1-1  37	--1-1--1  41
;3 --1--1-1  37	--1--1-1  37	--111--1  57	--1--1-1  37	--1-1--1  41
;4 --1--1-1  37	--1--1-1  37	-----1-1   5	--1--1-1  37	--1-1--1  41
;5 ---11--1  25	---11--1  25	-----111   7	---11-1-  26	---1111-  30
;6 --------   0	--------   0	--------   0	--------   0	--------   0
;
;Ascii 57 = ' '	Ascii 58 = ' '	Ascii 59 = ' '	Ascii 60 = ' '	Ascii 61 = ' '
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	---1-1-- 20	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 65 = 'A'	Ascii 66 = 'B'	Ascii 67 = 'C'	Ascii 68 = 'D'	Ascii 69 = 'E'
;								
;1 --11111-  62	--111111  63	---1111-  30 	--111111  63	--111111  63
;2 -----1-1   5	--1--1-1  37	--1----1  33 	--1----1  33	--1--1-1  37
;3 -----1-1   5	--1--1-1  37	--1----1  33	--1----1  33	--1--1-1  37
;4 -----1-1   5	--1--1-1- 37	--1----1  33	--1----1  33	--1--1-1  37
;5 --11111-  62	---11-1-  26	---1--1-  18	---1111-  30	--1----1  33
;6 --------   	--------   0	--------	--------   0	--------   0
;
;Ascii 70 = 'F'	Ascii 71 = 'G'	Ascii 72 = 'H'	Ascii 73 = 'I'	Ascii 74 = 'J'
;								
;1 --111111  63	---1111-  30	--111111  63	--------   0	--------
;2 -----1-1   5 --1----1  33	-----1--   4	--1----1  33
;3 -----1-1   5 --1-1--1  41	-----1--   4	--111111  63	--------
;4 -----1-1   5 --1-1--1  41	-----1--   4	--1----1  33	--------
;5 -------1   1 --111--1  57	--111111  63	--------   0	--------
;6 --------   0	--------   0	--------   0	--------   0	--------
;
;Ascii 75 = 'K'	Ascii 76 = 'L'	Ascii 77 = 'M'	Ascii 78 = 'N'	Ascii 79 = 'O'
;								
;1 --111111 63	--111111  63	--111111  63	--111111  63	---1111-  30
;2 -----1--  4	--1-----  32 	------1-   2	------1-   2	--1----1  33
;3 ----1-1- 10	--1-----  32	-----1--   4	----11--  12	--1----1  33
;4 ---1---1 17	--1-----  32	------1-   2	---1----  16 	--1----1  33
;5 --1----- 32	--1-----  32	--111111  63	--111111  63	---1111-  30
;6 --------  0 	--------   0 	--------   0	--------   0	--------   0
;
;Ascii 80 = 'P'	Ascii 81 = 'Q'	Ascii 82 = 'R'	Ascii 83 = 'S'	Ascii 84 = 'T'
;								
;1 --------	--------	--111111  63	--------	-------1   1
;2 --------	--------	-----1-1   5	--------	-------1   1
;3 --------	--------	----11-1  13	--------	--111111  63
;4 --------	--------	---1-1-1  21	--------	-------1   1
;5 --------	--------	--1---1-  34	--------	-------1   1
;6 --------   	--------   	--------   0	--------	--------   0
;
;Ascii 85 = 'U'	Ascii 86 = 'V'	Ascii 87 = 'W'	Ascii 88 = 'X'	Ascii 89 = 'Y'
;								
;1 ---11111  31	-----111  7	----1111  7	--------	--------
;2 --1-----  32	---11--- 24	--11---- 48 	--------	--------
;3 --1-----  32	--1----- 32	----11-- 12	--------	--------
;4 --1-----  32	---11--- 24	--11---- 48	--------	--------
;5 ---11111  31	-----111  7	----1111  7	--------	--------
;6 --------   	--------  0 	--------  0	--------	--------
;
;Ascii 90 = 'Z'	Ascii 91 = ' '	Ascii 92 = ' '	Ascii 93 = ' '	Ascii 94 = ' '
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	--------	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 95 = ' '	Ascii 96 = ' '	Ascii 97 = 'a'	Ascii 98 = 'b'	Ascii 99 = 'c'
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	--------	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 100='d'	Ascii 101='e'	Ascii 102='f'	Ascii 103='g'	Ascii 104='h'
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	--------	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 105='i'	Ascii 106='j'	Ascii 107='k'	Ascii 108='l'	Ascii 109='m'
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	--------	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 110='n'	Ascii 111='o'	Ascii 112='p'	Ascii 113='q'	Ascii 114='r'
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	--------	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 115='s'	Ascii 116='t'	Ascii 117='u'	Ascii 118='v'	Ascii 119='w'
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	--------	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 --------   	--------   	--------	--------	--------
;
;Ascii 120='x'	Ascii 121='y'	Ascii 122='z'	Ascii 123=' '	Ascii 124=' '
;								
;1 --------	--------	--------	--------	--------
;2 --------	--------	--------	--------	--------
;3 --------	--------	--------	--------	--------
;4 --------	--------	--------	--------	--------
;5 --------	--------	--------	--------	--------
;6 -------- 	-------- 	--------   	--------	--------
;-------------------------------------------------------------------------
Fnt5x6:
	.byte   0,  0,  0,  0,  0,  0,  3,  3,  0,  0,  0,  0	;32 ' ' 33 '�'
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;34 ' ' 35 ' '
	.byte   0,  0,  0,  0,  0,  0, 35, 19,  8,  4, 50, 49 	;36 ' ' 37 '%'
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;38 ' ' 39 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;40 ' ' 41 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;42 ' ' 43 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;44 ' ' 45 ' '
	.byte   0, 32,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0	;46 '.' 47 ' '
	.byte  30, 33, 33, 33, 30,  0,  0, 34, 63, 32,  0,  0	;48 '0' 49 '1'
	.byte  50, 41, 41, 41, 38,  0, 18, 33, 37, 37, 26,  0	;50 '2' 51 '3'
	.byte  24, 20, 18, 63, 16,  0, 23, 37, 37, 37, 25,  0	;52 '4' 53 '5'
	.byte  30, 37, 37, 37, 25,  0,  1,  1, 57,  5,  7,  0	;54 '6' 55 '7'
	.byte  26, 37, 37, 37, 26,  0, 38, 41, 41, 41, 30,  0	;56 '8' 57 '9'

	.byte   0,  0, 20,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;58 ':' 59 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;60 ' ' 61 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;62 ' ' 63 ' '
	.byte   0,  0,  0,  0,  0,  0, 62,  5,  5,  5, 62,  0 	;64 ' ' 65 'A'
	.byte  63, 37, 37, 37, 26,  0, 30, 33, 33, 33, 18,  0 	;66 'B' 67 'C'
	.byte  63, 33, 33, 33, 30,  0, 63, 37, 37, 37, 33,  0 	;68 'D' 69 'E'

	.byte  63,  5,  5,  5,  1,  0, 30, 33, 41, 41, 57,  0 	;70 'F' 71 'G'
	.byte  63,  4,  4,  4, 63,  0,  0, 33, 63, 33,  0,  0 	;72 'H' 73 'I'
	.byte   0,  0,  0,  0,  0,  0, 63,  4, 10, 17, 32,  0 	;74 ' ' 75 'K'
	.byte  63, 32, 32, 32, 32,  0, 63,  2,  4,  2, 63,  0 	;76 ' ' 77 'M'
	.byte  63,  2, 12, 16, 63,  0, 30, 33, 33, 33, 30,  0 	;78 'N' 79 'O'
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;80 ' ' 81 ' '
	.byte  63,  5, 13, 21, 34,  0,  0,  0,  0,  0,  0,  0 	;82 'R' 83 ' '
	
	.byte   1,  1, 63,  1,  1,  0, 31, 32, 32, 32, 31,  0 	;84 'T' 85 'U'
	.byte   7, 24, 32, 24,  7,  0, 15, 48, 12, 48, 15,  0 	;86 'V' 87 'W'
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;88 ' ' 89 ' '

	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;90 ' ' 91 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;92 ' ' 93 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;94 ' ' 95 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;96 ' ' 97 ' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;98 ' ' 99 ' '

	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;100' ' 101' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;102' ' 103' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;104' ' 105' 
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;106' ' 107' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;108' ' 109' '

	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;110' ' 111' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;112' ' 113' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;114' ' 115' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;116' ' 117' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;118' ' 119' '

	.byte 162,148,136,148,162,128,  0,  0,  0,  0,  0,  0 	;120' ' 121' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;122' ' 123' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;124' ' 125' '
	.byte   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 	;126' ' 127' '

;-------------------------------------------------------------------------
;Zeichensatztabelle 4x6/5x6 
;-------------------------------------------------------------------------
FntVar4x6:

;	.byte	0b00000000	;32 ' '	Leerzeichen 5 Bit Breit
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000110	;33 '�'
;	.byte	0b00000110
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;34 '"' = ' ' Leerzeichen 4 Bit Breit
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b00100100	;35 '#'
;	.byte	0b01111110
;	.byte	0b00100100
;	.byte	0b01111110
;	.byte	0b00100100
;	.byte	0b10000000
;
;	.byte	0b00000000	;36 '$' = Leerzeichen 6 Bit Breit
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;37 '%'
;	.byte	0b01001100	
;	.byte	0b00101100
;	.byte	0b00010000
;	.byte	0b01101000
;	.byte	0b01100100
;
;	.byte	0b00000000	;38 '&' = Leerzeichen 2 Bit Breit
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b01000000	;39 '''	= Punkt 5 Bit
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;40 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;41 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;42 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;43 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b01000000	;44 ',' Punkt 2 Bit breit
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;45 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;46 '.'
;	.byte	0b01000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b01100000	;47 '/'
;	.byte	0b00011000
;	.byte	0b00000110
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b--1111--	;48 '0'
;	.byte	0b-1-1--1-
;	.byte	0b-1--1-1-
;	.byte	0b--1111--
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b--------	;49 '1'
;	.byte	0b-1---1--	
;	.byte	0b-111111-
;	.byte	0b-1------
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b-1---1--	;50 '2'
;	.byte	0b-11---1-
;	.byte	0b-1-1--1-
;	.byte	0b-1--11--
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b--1--1--	;51 '3'
;	.byte	0b-1----1-
;	.byte	0b-1--1-1-
;	.byte	0b--11-1--
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b---1111-	;52 '4'
;	.byte	0b---1----
;	.byte	0b-111111-
;	.byte	0b---1----
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b--1-111-	;53 '5'
;	.byte	0b-1--1-1-
;	.byte	0b-1--1-1-
;	.byte	0b--11--1-
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b--1111--	;54 '6'
;	.byte	0b-1--1-1-
;	.byte	0b-1--1-1-
;	.byte	0b--11--1-
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b--------	;55 '7'
;	.byte	0b-11---1-
;	.byte	0b---1--1-
;	.byte	0b----111-
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b--11-1--	;56 '8'
;	.byte	0b-1--1-1-
;	.byte	0b-1--1-1-
;	.byte	0b--11-1--
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b-1--11--	;57 '9'
;	.byte	0b-1-1--1-
;	.byte	0b-1-1--1-
;	.byte	0b--1111--
;	.byte	0b--------
;	.byte	0b1-------
;
;	.byte	0b--------	;58 ':'	DP 4 Bit
;	.byte	0b-1--1---
;	.byte	0b--------
;	.byte	0b--------
;	.byte	0b1-------
;	.byte	0b1-------
;
;	.byte	0b01001000	;59 ';' DP 2 Bit
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;60 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;61 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;62 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;63 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;64 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b01111100	;65 'A'
;	.byte	0b00010010
;	.byte	0b00010010
;	.byte	0b01111100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;66 'B'
;	.byte	0b01001010
;	.byte	0b01001010
;	.byte	0b00110100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00111100	;67 'C'
;	.byte	0b01000010
;	.byte	0b01000010
;	.byte	0b00100100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;68 'D'
;	.byte	0b01000010
;	.byte	0b01000010
;	.byte	0b00111100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;69 'E'
;	.byte	0b01001010
;	.byte	0b01001010
;	.byte	0b01000010
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;70 'F'
;	.byte	0b00001010
;	.byte	0b00001010
;	.byte	0b00000010
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00111100	;71 'G'
;	.byte	0b01000010
;	.byte	0b01010010
;	.byte	0b00110100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;72 'H'
;	.byte	0b00001000
;	.byte	0b00001000
;	.byte	0b01111110
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01000010	;73 'I'
;	.byte	0b01111110
;	.byte	0b01000010
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b00100000	;74 'J'
;	.byte	0b01000000
;	.byte	0b01000000
;	.byte	0b00111110
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;75 'K'
;	.byte	0b00011000
;	.byte	0b00100100
;	.byte	0b01000010
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;76 'L'
;	.byte	0b01000000
;	.byte	0b01000000
;	.byte	0b01000000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;77 'M'
;	.byte	0b00000100
;	.byte	0b00001000
;	.byte	0b00000100
;	.byte	0b01111110
;	.byte	0b00000000
;
;	.byte	0b01111110	;78 'N'
;	.byte	0b00001100
;	.byte	0b00110000
;	.byte	0b01111110
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00111100	;79 'O'
;	.byte	0b01000010
;	.byte	0b01000010
;	.byte	0b00111100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;80 'P'
;	.byte	0b00010010
;	.byte	0b00010010
;	.byte	0b00001100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;81 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b01111110	;82 'R'
;	.byte	0b00010010
;	.byte	0b00110010
;	.byte	0b01001100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00100100	;83 'S'
;	.byte	0b01001010
;	.byte	0b01001010
;	.byte	0b00110000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000010	;84 'T'
;	.byte	0b00000010
;	.byte	0b01111110
;	.byte	0b00000010
;	.byte	0b00000010
;	.byte	0b00000000
;
;	.byte	0b00111110	;85 'U'
;	.byte	0b01000000
;	.byte	0b01000000
;	.byte	0b00111110
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00001110	;86 'V'
;	.byte	0b00110000
;	.byte	0b01000000
;	.byte	0b00110000
;	.byte	0b00001110
;	.byte	0b00000000
;
;	.byte	0b00111110	;87 'W'
;	.byte	0b01000000
;	.byte	0b00100000
;	.byte	0b01000000
;	.byte	0b00111110
;	.byte	0b00000000
;
;	.byte	0b01000010	;88 'X'
;	.byte	0b00100100
;	.byte	0b00011000
;	.byte	0b00100100
;	.byte	0b01000010
;	.byte	0b00000000
;
;	.byte	0b00000000	;89 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b01100010	;90 'Z'
;	.byte	0b01010010
;	.byte	0b01001010
;	.byte	0b01000110
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;91 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;92 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;93 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;94 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;95 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;96 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00100000	;97 'a'
;	.byte	0b01010100
;	.byte	0b01010100
;	.byte	0b01111000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;98 'b'
;	.byte	0b01001000
;	.byte	0b01001000
;	.byte	0b00110000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00110000	;99 'c'
;	.byte	0b01001000
;	.byte	0b01001000
;	.byte	0b01001000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00110000	;100 'd'
;	.byte	0b01001000
;	.byte	0b01001000
;	.byte	0b01111110
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00111000	;101 'e'
;	.byte	0b01010100
;	.byte	0b01010100
;	.byte	0b01001000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111100	;102 'f'
;	.byte	0b00001010
;	.byte	0b00000010
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000	
;
;	.byte	0b00110000	;103 'g'
;	.byte	0b10101000
;	.byte	0b10101000
;	.byte	0b01111000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111110	;104 'h'
;	.byte	0b00001000
;	.byte	0b00001000
;	.byte	0b01110000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01001000	;105 'i'
;	.byte	0b01111010
;	.byte	0b01000000
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;106 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b01111110	;107 'k'
;	.byte	0b00010000
;	.byte	0b00101000
;	.byte	0b01000100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00111110	;108 'l'
;	.byte	0b01000000
;	.byte	0b01000000
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b01111100	;109 'm'
;	.byte	0b00000100
;	.byte	0b01111000
;	.byte	0b00000100
;	.byte	0b01111000
;	.byte	0b00000000
;
;	.byte	0b01111100	;110 'n'
;	.byte	0b00000100
;	.byte	0b00000100
;	.byte	0b01111000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00111000	;111 'o'
;	.byte	0b01000100
;	.byte	0b01000100
;	.byte	0b00111000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b11111100	;112 'p'
;	.byte	0b00100100
;	.byte	0b00100100
;	.byte	0b00011000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;113 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b01111000	;114 'r'
;	.byte	0b00000100
;	.byte	0b00000100
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b01001000	;115 's'
;	.byte	0b01010100
;	.byte	0b01010100
;	.byte	0b00100100
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000100	;116 't'
;	.byte	0b00111110
;	.byte	0b01000100
;	.byte	0b00000000
;	.byte	0b10000000
;	.byte	0b10000000
;
;	.byte	0b00000000	;117 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00000000	;118 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b00111100	;119 'w'
;	.byte	0b01000000
;	.byte	0b00111000
;	.byte	0b01000000
;	.byte	0b00111100
;	.byte	0b00000000
;
;	.byte	0b01000100	;120 'x'
;	.byte	0b00101000
;	.byte	0b00010000
;	.byte	0b00101000
;	.byte	0b01000100
;	.byte	0b00000000
;
;	.byte	0b00000000	;121 ' '
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;	.byte	0b00000000
;
;	.byte	0b01000100	;122 'z'
;	.byte	0b01100100
;	.byte	0b01010100
;	.byte	0b01001100
;	.byte	0b00000000
;	.byte	0b10000000

;#### Test Zeichen 7 Bit hoch
	.byte	0b00000000	;32 ' '	Leerzeichen 5 Bit
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000110	;33 '�'
	.byte	0b00000110
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00000000	;34 '"' = Leerzeichen 4 Bit
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00100100	;35 '#'
	.byte	0b01111110
	.byte	0b00100100
	.byte	0b01111110
	.byte	0b00100100
	.byte	0b10000000

	.byte	0b00000000	;36 '$' = Leerzeichen 6 Bit
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;37 '%'
	.byte	0b01000011	
	.byte	0b00110011	
	.byte	0b00001000
	.byte	0b01100110
	.byte	0b01100001

	.byte	0b00000000	;38 '&' = Leerzeichen 2 Bit
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b01000000	;39 '''	= Punkt 5 Bit
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000000	;40 '('	= Leerzeichen 3 Bit
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00000000	;41
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b01000000	;42 '*' Punkt 3 Bit
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00000000	;43
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b01000000	;44 ',' Punkt 2 Bit
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00001000	;45 '-'
	.byte	0b00001000
	.byte	0b00001000
	.byte	0b00001000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000000	;46 '.'
	.byte	0b01000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b01100000	;47 '/'
	.byte	0b00011100
	.byte	0b00000011
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000


;########################
;Originalzeichentabelle
	.byte	0b00111110	;48 '0'
	.byte	0b01000001
	.byte	0b01000001
	.byte	0b00111110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000000	;49 '1'
	.byte	0b01000010	
	.byte	0b01111111
	.byte	0b01000000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01100010	;50 '2'
	.byte	0b01010001
	.byte	0b01001001
	.byte	0b01000110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00100010	;51 '3'
	.byte	0b01000001
	.byte	0b01001001
	.byte	0b00110110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00001111	;52 '4'
	.byte	0b00001000
	.byte	0b01111110
	.byte	0b00001000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00100111	;53 '5'
	.byte	0b01000101
	.byte	0b01000101
	.byte	0b00111001
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111110	;54 '6'
	.byte	0b01001001
	.byte	0b01001001
	.byte	0b00110010
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000001	;55 '7'
	.byte	0b01110001
	.byte	0b00001001
	.byte	0b00000111
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00110110	;56 '8'
	.byte	0b01001001
	.byte	0b01001001
	.byte	0b00110110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00100110	;57 '9'
	.byte	0b01001001
	.byte	0b01001001
	.byte	0b00111110
	.byte	0b00000000
	.byte	0b10000000

;###################################################
;zum Testen 
;	.byte	0b01111111	;48 '0'
;	.byte	0b01000001
;	.byte	0b01000001
;	.byte	0b01111111
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000001	;49 '1'
;	.byte	0b01000001	
;	.byte	0b01111111
;	.byte	0b01000000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111001	;50 '2'
;	.byte	0b01001001
;	.byte	0b01001001
;	.byte	0b01001111
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01001001	;51 '3'
;	.byte	0b01001001
;	.byte	0b01001001
;	.byte	0b01111111
;	.byte	0b00000000
;	.byte	0b10000000
;
;
;	.byte	0b00001111	;52 '4'
;	.byte	0b00001000
;	.byte	0b01111111
;	.byte	0b00001000
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01001111	;53 '5'
;	.byte	0b01001001
;	.byte	0b01001001
;	.byte	0b01111001
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111111	;54 '6'
;	.byte	0b01001001
;	.byte	0b01001001
;	.byte	0b01111001
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b00000001	;55 '7'
;	.byte	0b01110001
;	.byte	0b00001101
;	.byte	0b00000011
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01111111	;56 '8'
;	.byte	0b01001001
;	.byte	0b01001001
;	.byte	0b01111111
;	.byte	0b00000000
;	.byte	0b10000000
;
;	.byte	0b01001111	;57 '9'
;	.byte	0b01001001
;	.byte	0b01001001
;	.byte	0b01111111
;	.byte	0b00000000
;	.byte	0b10000000
;####################################################

	.byte	0b00000000	;58 ':'	DP 4 Bit
	.byte	0b00100100
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00100100	;59 ';' DP 2 Bit
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00000000	;60 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;61 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;62 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;63 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;64 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b01111110	;65 'A'
	.byte	0b00010001
	.byte	0b00010001
	.byte	0b01111110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;66 'B'
	.byte	0b01001001
	.byte	0b01001001
	.byte	0b00110110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111110	;67 'C'
	.byte	0b01000001
	.byte	0b01000001
	.byte	0b00100010
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;68 'D'
	.byte	0b01000001
	.byte	0b01000001
	.byte	0b00111110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;69 'E'
	.byte	0b01001001
	.byte	0b01001001
	.byte	0b01000001
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;70 'F'
	.byte	0b00001001
	.byte	0b00001001
	.byte	0b00000001
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111110	;71 'G'
	.byte	0b01000001
	.byte	0b01010001
	.byte	0b00110010
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;72 'H'
	.byte	0b00001000
	.byte	0b00001000
	.byte	0b01111111
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01000001	;73 'I'
	.byte	0b01111111
	.byte	0b01000001
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00100000	;74 'J'
	.byte	0b01000000
	.byte	0b01000000
	.byte	0b00111111
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;75 'K'
	.byte	0b00010100
	.byte	0b00100010
	.byte	0b01000001
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;76 'L'
	.byte	0b01000000
	.byte	0b01000000
	.byte	0b01000000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;77 'M'
	.byte	0b00000010
	.byte	0b00000100
	.byte	0b00000010
	.byte	0b01111111
	.byte	0b00000000

	.byte	0b01111111	;78 'N'
	.byte	0b00000010
	.byte	0b00000100
	.byte	0b01111111
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111110	;79 'O'
	.byte	0b01000001
	.byte	0b01000001
	.byte	0b00111110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111111	;80 'P'
	.byte	0b00001001
	.byte	0b00001001
	.byte	0b00000110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000000	;81 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b01111111	;82 'R'
	.byte	0b00001001
	.byte	0b00001001
	.byte	0b01110110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00100110	;83 'S'
	.byte	0b01001001
	.byte	0b01001001
	.byte	0b00110010
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000001	;84 'T'
	.byte	0b00000001
	.byte	0b01111111
	.byte	0b00000001
	.byte	0b00000001
	.byte	0b00000000

	.byte	0b00111111	;85 'U'
	.byte	0b01000000
	.byte	0b01000000
	.byte	0b00111111
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111111	;86 'V'
	.byte	0b01000000
	.byte	0b00100000
	.byte	0b00011111
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111111	;87 'W'
	.byte	0b01000000
	.byte	0b00100000
	.byte	0b01000000
	.byte	0b00111111
	.byte	0b00000000

	.byte	0b01110111	;88 'X'
	.byte	0b00001000
	.byte	0b00001000
	.byte	0b01110111
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000000	;89 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b01110001	;90 'Z'
	.byte	0b01001001
	.byte	0b01001001
	.byte	0b01000111
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000000	;91 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;92 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;93 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;94 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;95 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;96 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00100000	;97 'a'
	.byte	0b01010100
	.byte	0b01010100
	.byte	0b01111000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111110	;98 'b'
	.byte	0b01001000
	.byte	0b01001000
	.byte	0b00110000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00110000	;99 'c'
	.byte	0b01001000
	.byte	0b01001000
	.byte	0b01001000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00110000	;100 'd'
	.byte	0b01001000
	.byte	0b01001000
	.byte	0b01111110
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111000	;101 'e'
	.byte	0b01010100
	.byte	0b01010100
	.byte	0b01001000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111100	;102 'f'
	.byte	0b00001010
	.byte	0b00000010
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000	

	.byte	0b00110000	;103 'g'
	.byte	0b10101000
	.byte	0b10101000
	.byte	0b01111000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01111110	;104 'h'
	.byte	0b00001000
	.byte	0b00001000
	.byte	0b01110000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b01001000	;105 'i'
	.byte	0b01111010
	.byte	0b01000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00000000	;106 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b01111110	;107 'k'
	.byte	0b00010000
	.byte	0b00101000
	.byte	0b01000100
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111110	;108 'l'
	.byte	0b01000000
	.byte	0b01000000
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b01111100	;109 'm'
	.byte	0b00000100
	.byte	0b01111000
	.byte	0b00000100
	.byte	0b01111000
	.byte	0b00000000

	.byte	0b01111100	;110 'n'
	.byte	0b00000100
	.byte	0b00000100
	.byte	0b01111000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00111000	;111 'o'
	.byte	0b01000100
	.byte	0b01000100
	.byte	0b00111000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b11111100	;112 'p'
	.byte	0b00100100
	.byte	0b00100100
	.byte	0b00011000
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000000	;113 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b01111000	;114 'r'
	.byte	0b00000100
	.byte	0b00000100
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b01001000	;115 's'
	.byte	0b01010100
	.byte	0b01010100
	.byte	0b00100100
	.byte	0b00000000
	.byte	0b10000000

	.byte	0b00000100	;116 't'
	.byte	0b00111110
	.byte	0b01000100
	.byte	0b00000000
	.byte	0b10000000
	.byte	0b10000000

	.byte	0b00000000	;117 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00000000	;118 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b00111100	;119 'w'
	.byte	0b01000000
	.byte	0b00111000
	.byte	0b01000000
	.byte	0b00111100
	.byte	0b00000000

	.byte	0b01000100	;120 'x'
	.byte	0b00101000
	.byte	0b00010000
	.byte	0b00101000
	.byte	0b01000100
	.byte	0b00000000

	.byte	0b00000000	;121 ' '
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000
	.byte	0b00000000

	.byte	0b01000100	;122 'z'
	.byte	0b01100100
	.byte	0b01010100
	.byte	0b01001100
	.byte	0b00000000
	.byte	0b10000000

;##################

;-------------------------------------------------------------------------
;Programm Start
;-------------------------------------------------------------------------
;.text		;Startadresse 0x200 wird in .gld Datei festgelegt
;__reset:
;-------------------------------------------------------------------------
;Setup Stackpointer 
;-------------------------------------------------------------------------
;	mov	#__SP_init,w15		;initialize w15
;	mov	#__SPLIM_init,w0	; 
;	mov	w0,_SPLIM		;initialize SPLIM
;	nop				;wait 1 cycle
;-------------------------------------------------------------------------
;Reset-Flags auswerten, <RCON 15:0>, alle Reset-Flags High-aktiv
;	 ______ ______ ______ ______ ______ ______ ______ ______
;	|__15__|__14__|__13__|__12__|__11__|__10__|___9__|___8__|
;	| TRAPR|IOPUWR| ---- | ---- | ---- | ---- |  CM  | VREGS|
;	 ______ ______ ______ ______ ______ ______ ______ ______
;	|___7__|___6__|___5__|___4__|___3__|___2__|___1__|___0__|
;	| EXTR |  SWR |SWDTEN| WDTO | SLEEP| IDLE |  BOR |  POR |
;-------------------------------------------------------------------------
;	btsc	RCON,#POR		;Power-On Reset ?				
;	bra	ResPor			;ja
;	btsc	RCON,#BOR		;Brown-Out Reset ?
;	call	WriteEE_BOR		;ja
;	btsc	RCON,#WDTO		;Watch-Dog Time Out ?	
;	call	WriteEE_WDTO		;ja
;	btsc	RCON,#TRAPR		;Trap Conflict Reset ?
;	call	WriteEE_TRAPR		;ja	
;
;
;ResPor:
;	mov	#0,w0			;alle Reset-Flags l�schen
;	mov	w0,RCON
;-------------------------------------------------------------------------
.end
